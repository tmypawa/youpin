//
//  UIImageView+SDWebImage_Effects.h
//  youpinwei
//
//  Created by tmy on 14/10/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"

typedef enum : NSUInteger {
    SDWebImageEffectNone,
    SDWebImageEffectFade,
    SDWebImageEffectProgress,
} SDWebImageEffect;

@interface UIImageView (SDWebImage_Effects)

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect options:(SDWebImageOptions)options;

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect;

- (void)setImageWithURL:(NSString *)url effect:(SDWebImageEffect)effect;

- (void)setImageWithURL:(NSString *)url;

- (void)cancelImageLoad;

@end

@interface UIButton (SDWebImage_Effects)

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect options:(SDWebImageOptions)options;

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect;

- (void)setImageWithURL:(NSString *)url effect:(SDWebImageEffect)effect;

- (void)setImageWithURL:(NSString *)url;

- (void)cancelImageLoad;

@end
