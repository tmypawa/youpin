//
//  UIImageView+SDWebImage_Effects.m
//  youpinwei
//
//  Created by tmy on 14/10/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "UIImageView+SDWebImage_Effects.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"

#define DEFAULT_OPTIONS SDWebImageRetryFailed|SDWebImageLowPriority
#define FADE_DURATION 0.3

@implementation UIImageView (SDWebImage_Effects)

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect options:(SDWebImageOptions)options {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:placeholderImage effect:effect options:options];
    }
}

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:placeholderImage effect:effect options:DEFAULT_OPTIONS];
    }
}

- (void)setImageWithURL:(NSString *)url effect:(SDWebImageEffect)effect {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:nil effect:effect options:DEFAULT_OPTIONS];
    }
}

- (void)setImageWithURL:(NSString *)url {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:nil effect:SDWebImageEffectNone options:DEFAULT_OPTIONS];
    }
}

- (void)cancelImageLoad {
    [self sd_cancelCurrentImageLoad];
}


- (void)internalSetImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder effect:(SDWebImageEffect)effect options:(SDWebImageOptions)options {
    [self.layer removeAllAnimations];
    self.alpha = 1;
    
    [self sd_setImageWithURL:url placeholderImage:placeholder options:options progress:effect == SDWebImageEffectProgress? ^(NSInteger receivedSize, NSInteger expectedSize) {
        
        
    } : nil completed:effect == SDWebImageEffectFade || effect == SDWebImageEffectNone ? ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error) {
            return;
        }
        
        if(cacheType == SDImageCacheTypeNone && effect == SDWebImageEffectFade) {
            self.alpha = 0;
            [UIView animateWithDuration:FADE_DURATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.alpha = 1;
            } completion:nil];
        }else {
            self.alpha = 1;
        }
    } : nil];
}

@end

@implementation UIButton (SDWebImage_Effects)

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect options:(SDWebImageOptions)options {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:placeholderImage effect:effect options:options];
    }
}

- (void)setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage effect:(SDWebImageEffect)effect {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:placeholderImage effect:effect options:DEFAULT_OPTIONS];
    }
}

- (void)setImageWithURL:(NSString *)url effect:(SDWebImageEffect)effect {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:nil effect:effect options:DEFAULT_OPTIONS];
    }
}

- (void)setImageWithURL:(NSString *)url {
    NSURL *imageUrl = [NSURL URLWithString:url];
    if (imageUrl) {
        [self internalSetImageWithURL:imageUrl placeholderImage:nil effect:SDWebImageEffectNone options:DEFAULT_OPTIONS];
    }
}

- (void)cancelImageLoad {
    [self sd_cancelImageLoadForState:UIControlStateNormal];
}


- (void)internalSetImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder effect:(SDWebImageEffect)effect options:(SDWebImageOptions)options {
    [self.layer removeAllAnimations];
    self.alpha = 1;
    
    [self sd_setImageWithURL:url forState:UIControlStateNormal placeholderImage:placeholder options:options completed: effect == SDWebImageEffectFade || effect == SDWebImageEffectNone? ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error) {
            return;
        }
        
        if(cacheType == SDImageCacheTypeNone && effect == SDWebImageEffectFade) {
            self.alpha = 0;
            [UIView animateWithDuration:FADE_DURATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.alpha = 1;
            } completion:nil];
        }else {
            self.alpha = 1;
        }
    } : nil];
}

@end
