//
//  NSDate+Utility.h
//  JapanDrama
//
//  Created by tmy on 14-8-7.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utility)

- (NSString *)prettyShortString;
- (NSString *)prettyFullString;

@end
