//
//  NSDate+Utility.m
//  JapanDrama
//
//  Created by tmy on 14-8-7.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "NSDate+Utility.h"

@implementation NSDate (Utility)

- (NSString *)prettyShortString {
    NSDate *nowDate = [NSDate date];
    NSDateComponents *nowComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:nowDate];
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:self];
    NSString *prettyTimeStr = @"";
    
    if([dateComponents year] == [nowComponents year]) {
        if ([dateComponents month] == [nowComponents month]) {
            if ([dateComponents day] == [nowComponents day]) {
                if (dateComponents.hour == nowComponents.hour) {
                    if ([dateComponents minute] == [nowComponents minute]) {
                        prettyTimeStr = @"刚刚";
                    }else {
                        prettyTimeStr = [NSString stringWithFormat:@"%d分钟前", (int)([nowComponents minute] - [dateComponents minute])];
                    }
                }else {
                    prettyTimeStr = [NSString stringWithFormat:@"%d小时前", (int)([nowComponents hour] - [dateComponents hour])];
                }
            }else {
                int diffDay = (int)([nowComponents day] - [dateComponents day]);
                if(diffDay == 1) {
                    prettyTimeStr = @"昨天";
                }else if(diffDay == 2) {
                    prettyTimeStr = @"前天";
                }else if(diffDay <= 10){
                    prettyTimeStr = [NSString stringWithFormat:@"%d天前", diffDay];
                }else {
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    dateFormatter.dateFormat = @"MM-dd HH:mm";
                    prettyTimeStr = [dateFormatter stringFromDate:self];
                }
            }
        }else {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"MM-dd HH:mm";
            prettyTimeStr = [dateFormatter stringFromDate:self];
        }
    }else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
        prettyTimeStr = [dateFormatter stringFromDate:self];
    }

    return prettyTimeStr;
}

- (NSString *)prettyFullString {
    NSDate *nowDate = [NSDate date];
    NSDateComponents *nowComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:nowDate];
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:self];
    NSString *prettyTimeStr = @"";
    
    if([dateComponents year] == [nowComponents year]) {
        if ([dateComponents month] == [nowComponents month]) {
            if ([dateComponents day] == [nowComponents day]) {
                if (dateComponents.hour == nowComponents.hour) {
                    if ([dateComponents minute] == [nowComponents minute]) {
                        prettyTimeStr = @"刚刚";
                    }else {
                        prettyTimeStr = [NSString stringWithFormat:@"%d分钟前", (int)([nowComponents minute] - [dateComponents minute])];
                    }
                }else {
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    dateFormatter.dateFormat = @"HH:mm";
                    prettyTimeStr = [NSString stringWithFormat:@"今天 %@", [dateFormatter stringFromDate:self]];
                }
            }else {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"HH:mm";
                int diffDay = (int)([nowComponents day] - [dateComponents day]);
                if(diffDay == 1) {
                    prettyTimeStr = [NSString stringWithFormat:@"昨天 %@", [dateFormatter stringFromDate:self]];
                }else if(diffDay == 2) {
                    prettyTimeStr = [NSString stringWithFormat:@"前天 %@", [dateFormatter stringFromDate:self]];
                }else {
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    dateFormatter.dateFormat = @"MM-dd HH:mm";
                    prettyTimeStr = [dateFormatter stringFromDate:self];
                }
            }
        }else {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"MM-dd HH:mm";
            prettyTimeStr = [dateFormatter stringFromDate:self];
        }
    }else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
        prettyTimeStr = [dateFormatter stringFromDate:self];
    }
    
    return prettyTimeStr;
}

@end
