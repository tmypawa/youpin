//
//  JTAnimationButton.m
//  youpinwei
//
//  Created by tmy on 14/12/31.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "JTAnimationButton.h"

@implementation JTAnimationButton
{
    BOOL        _isHightlighted;
    UIImageView *_animatedView;
}

- (id)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self internalInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self internalInit];
    }
    
    return self;
}

- (void)internalInit {
    _animatedView = [[UIImageView alloc] init];
    _animatedView.userInteractionEnabled = NO;
    self.highlightScale = 1;
}

- (void)setNormalImage:(UIImage *)normalImage {
    if (_normalImage != normalImage) {
        _normalImage = normalImage;
        [self setImage:_normalImage forState:UIControlStateNormal];
    }
}

- (void)setHighlightImage:(UIImage *)highlightImage {
    if (_highlightImage != highlightImage) {
        _highlightImage = highlightImage;
        _animatedView.image = self.highlightImage;
    }
}

+ (instancetype)buttonWithNormalImage:(UIImage *)normalImage highlightImage:(UIImage *)hightlightImage {
    JTAnimationButton *button = [JTAnimationButton buttonWithType:UIButtonTypeCustom];
    button.adjustsImageWhenHighlighted = NO;
    button.normalImage = normalImage;
    button.highlightImage = hightlightImage;
    [button setImage:normalImage forState:UIControlStateNormal];
    return button;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _animatedView.center = CGPointMake(self.imageView.center.x, self.imageView.center.y);
}

- (void)showHightlighted:(BOOL)highlighted animate:(BOOL)isAnimated {
    _isHightlighted = highlighted;
    [_animatedView.layer removeAllAnimations];
    _animatedView.transform = CGAffineTransformIdentity;
    _animatedView.alpha = 1;
    _animatedView.frame = CGRectMake(0, 0, (self.imageView.frame.size.width + 2) * self.highlightScale, (self.imageView.frame.size.height + 2) * self.highlightScale);
    
    if (!isAnimated) {
        if (_isHightlighted) {
            [self addSubview:_animatedView];
        }else {
            [_animatedView removeFromSuperview];
        }
    }else {
        if (_isHightlighted) {
            [self addSubview:_animatedView];
            _animatedView.transform = CGAffineTransformMakeScale(0.2f, 0.2f);
            _animatedView.alpha = 0;
            [UIView animateWithDuration:0.75f delay:0 usingSpringWithDamping:0.4f initialSpringVelocity:0 options:0 animations:^{
                _animatedView.transform = CGAffineTransformMakeScale(1, 1);
                _animatedView.alpha = 1;
            } completion:^(BOOL finished) {
               
            }];
        }else {
            [self setImage:self.normalImage forState:UIControlStateNormal];
            [UIView animateWithDuration:0.8f delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
                _animatedView.transform = CGAffineTransformMakeScale(0.45f, 0.45f);
                _animatedView.alpha = 0;
            } completion:^(BOOL finished) {
                [_animatedView removeFromSuperview];
            }];
        }
    }
}

@end
