//
//  JTAnimationButton.h
//  youpinwei
//
//  Created by tmy on 14/12/31.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JTAnimationButton : UIButton

@property (nonatomic, strong) UIImage *normalImage;
@property (nonatomic, strong) UIImage *highlightImage;
@property (nonatomic) CGFloat highlightScale;

+ (instancetype)buttonWithNormalImage:(UIImage *)normalImage highlightImage:(UIImage *)hightlightImage;

- (void)showHightlighted:(BOOL)highlighted animate:(BOOL)isAnimated;

@end
