//
//  UIImage+Color.h
//  Wundercast
//
//  Created by Admin on 1/6/14.
//  Copyright (c) 2014 tmy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+ (UIImage *)imageFromColor:(UIColor *)color;
+ (UIImage *)imageFromColor:(UIColor *)color size:(CGSize)size;

@end
