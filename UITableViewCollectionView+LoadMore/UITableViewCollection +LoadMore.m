//
//  UITableView+LoadMore.m
//  HaiTao
//
//  Created by tmy on 14-6-29.
//
//

#import "UITableViewCollection+LoadMore.h"
#import <objc/runtime.h>

NSString * const footerViewKey = @"footerViewKey";
NSString * const footerHeightKey = @"footerHeightKey";
NSString * const targetKey = @"targetKey";
NSString * const actionKey = @"actionKey";
NSString * const isLoadingMoreKey = @"isLoadingMoreKey";

@implementation UITableView (LoadMore)

- (BOOL)isLoadingMore {
    id value = objc_getAssociatedObject(self, isLoadingMoreKey);
    return value ? [value boolValue] : NO;
}

- (void)setIsLoadingMore:(BOOL)isLoadingMore {
    objc_setAssociatedObject(self, isLoadingMoreKey, @(isLoadingMore), OBJC_ASSOCIATION_RETAIN);
}

- (void)setLoadMoreView:(UIView *)loadMoreView
{
    objc_setAssociatedObject(self, footerViewKey, loadMoreView, OBJC_ASSOCIATION_RETAIN);
}

- (UIView *)loadMoreView
{
    return objc_getAssociatedObject(self, footerViewKey);
}

- (void)setLoadMoreHeight:(CGFloat)loadMoreHeight
{
    objc_setAssociatedObject(self, footerHeightKey, @(loadMoreHeight), OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)loadMoreHeight
{
    id value = objc_getAssociatedObject(self, footerHeightKey);
    return value? [value floatValue] : 0;
}

- (void)setLoadTarget:(id)target
{
    objc_setAssociatedObject(self, targetKey, target, OBJC_ASSOCIATION_ASSIGN);
}

- (id)loadTarget
{
    return objc_getAssociatedObject(self, targetKey);
}

- (void)setLoadAction:(SEL)action
{
    objc_setAssociatedObject(self, actionKey, NSStringFromSelector(action), OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (SEL)loadAction
{
    id value = objc_getAssociatedObject(self, actionKey);
    return value? NSSelectorFromString(value) : nil;
}

- (UILabel *)loadMoreTitleLabel
{
    return (UILabel *)[self.loadMoreView viewWithTag:2000];
}

- (UIActivityIndicatorView *)loadMoreIndicatorView
{
    return (UIActivityIndicatorView *)[self.loadMoreView viewWithTag:3000];
}

- (void)setupLoadMoreWithTitle:(NSString *)title
{
    self.loadMoreHeight = 60;
    UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.loadMoreHeight)] autorelease];
    footerView.backgroundColor = [UIColor clearColor];
    self.loadMoreView = footerView;

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 200)/2, (self.loadMoreHeight - 20)/2, 200, 20)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor darkTextColor];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.text = title;
    titleLabel.tag = 2000;
    [footerView addSubview:titleLabel];
    [titleLabel release];
    
    UIActivityIndicatorView *activityView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    activityView.center = CGPointMake(self.frame.size.width/2, self.loadMoreHeight/2);
    activityView.tag = 3000;
    [footerView addSubview:activityView];
    
    [self addMaskView];
}

- (void)setupLoadMoreWithCustomView:(UIView *)customView
{
    CGFloat height = 0;
    if(customView){
        customView.tag = 1000;
        height = customView.frame.size.height;
    }
    
    self.loadMoreHeight = height;
    UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.loadMoreHeight)] autorelease];
    footerView.backgroundColor = [UIColor clearColor];
    [footerView addSubview:customView];
    self.loadMoreView = footerView;
    
    [self addMaskView];
}

- (void)startLoadMore
{
    [self loadMore:nil];
}

- (void)endLoadMore
{
    self.isLoadingMore = NO;
    UILabel *titleLabel = [self loadMoreTitleLabel];
    UIActivityIndicatorView *indicatorView = [self loadMoreIndicatorView];
    if(titleLabel && indicatorView){
        titleLabel.hidden = NO;
        indicatorView.hidden = YES;
        [indicatorView stopAnimating];
    }
    
    [self endLoadMoreWithHandler:nil];
}

- (void)endLoadMoreWithHandler:(void(^)(UIView *customView))endingHandler
{
    self.isLoadingMore = NO;
    if(endingHandler){
        endingHandler([self.loadMoreView viewWithTag:1000]);
    }
}

- (void)addTarget:(id)target forLoadMoreAction:(SEL)action
{
    [self setLoadTarget:target];
    [self setLoadAction:action];
}

- (void)showLoadMoreView
{
    [self setTableFooterView: self.loadMoreView];
}

- (void)dismissLoadMore
{
    [self setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)addMaskView
{
    UIButton *maskBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    maskBtn.frame = CGRectMake(0, 0, self.frame.size.width, self.loadMoreHeight);
    [maskBtn addTarget:self action:@selector(loadMore:) forControlEvents:UIControlEventTouchUpInside];
    maskBtn.tag = 4000;
    [self.loadMoreView addSubview:maskBtn];
}

- (void)loadMore:(id)sender
{
    self.isLoadingMore = YES;
    UILabel *titleLabel = [self loadMoreTitleLabel];
    UIActivityIndicatorView *indicatorView = [self loadMoreIndicatorView];
    if(titleLabel && indicatorView){
        titleLabel.hidden = YES;
        indicatorView.hidden = NO;
        [indicatorView startAnimating];
    }
    
    id target = [self loadTarget];
    SEL action = [self loadAction];
    if(target && action){
        [target performSelector:action withObject:[self.loadMoreView viewWithTag:1000]];
    }
}

@end


@implementation UICollectionView (LoadMore)

- (BOOL)isLoadingMore {
    id value = objc_getAssociatedObject(self, isLoadingMoreKey);
    return value ? [value boolValue] : NO;
}

- (void)setIsLoadingMore:(BOOL)isLoadingMore {
    objc_setAssociatedObject(self, isLoadingMoreKey, @(isLoadingMore), OBJC_ASSOCIATION_RETAIN);
}

- (void)setLoadMoreView:(UIView *)loadMoreView
{
    objc_setAssociatedObject(self, footerViewKey, loadMoreView, OBJC_ASSOCIATION_RETAIN);
}

- (UIView *)loadMoreView
{
    return objc_getAssociatedObject(self, footerViewKey);
}

- (void)setLoadMoreHeight:(CGFloat)loadMoreHeight
{
    objc_setAssociatedObject(self, footerHeightKey, @(loadMoreHeight), OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)loadMoreHeight
{
    id value = objc_getAssociatedObject(self, footerHeightKey);
    return value? [value floatValue] : 0;
}

- (void)setLoadTarget:(id)target
{
    objc_setAssociatedObject(self, targetKey, target, OBJC_ASSOCIATION_ASSIGN);
}

- (id)loadTarget
{
   return objc_getAssociatedObject(self, targetKey);
}

- (void)setLoadAction:(SEL)action
{
    objc_setAssociatedObject(self, actionKey, NSStringFromSelector(action), OBJC_ASSOCIATION_RETAIN);
}

- (SEL)loadAction
{
    id value = objc_getAssociatedObject(self, actionKey);
    return value? NSSelectorFromString(value) : nil;
}

- (UILabel *)loadMoreTitleLabel
{
    return (UILabel *)[self.loadMoreView viewWithTag:2000];
}

- (UIActivityIndicatorView *)loadMoreIndicatorView
{
    return (UIActivityIndicatorView *)[self.loadMoreView viewWithTag:3000];
}

- (void)setupLoadMoreWithTitle:(NSString *)title
{
    self.loadMoreHeight = 50;
    UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.loadMoreHeight)] autorelease];
    footerView.backgroundColor = [UIColor clearColor];
    self.loadMoreView = footerView;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 200)/2, (self.loadMoreHeight - 20)/2, 200, 20)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor darkTextColor];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.text = title;
    titleLabel.tag = 2000;
    [footerView addSubview:titleLabel];
    [titleLabel release];
    
    UIActivityIndicatorView *activityView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    activityView.center = CGPointMake(self.frame.size.width/2, self.loadMoreHeight/2);
    activityView.tag = 3000;
    [footerView addSubview:activityView];
 
    [self addMaskView];
}

- (void)setupLoadMoreWithCustomView:(UIView *)customView
{
    CGFloat height = 0;
    if(customView){
        customView.tag = 1000;
        height = customView.frame.size.height;
    }
    
    self.loadMoreHeight = height;
    UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.loadMoreHeight)] autorelease];
    footerView.backgroundColor = [UIColor clearColor];
    [footerView addSubview:customView];
    self.loadMoreView = footerView;
    
    [self addMaskView];
}

- (void)startLoadMore
{
    [self loadMore:nil];
}

- (void)endLoadMore
{
    self.isLoadingMore = NO;
    UILabel *titleLabel = [self loadMoreTitleLabel];
    UIActivityIndicatorView *indicatorView = [self loadMoreIndicatorView];
    if(titleLabel && indicatorView){
        titleLabel.hidden = NO;
        indicatorView.hidden = YES;
        [indicatorView stopAnimating];
    }
    
    [self endLoadMoreWithHandler:nil];
}

- (void)endLoadMoreWithHandler:(void(^)(UIView *customView))endingHandler
{
    self.isLoadingMore = NO;
    if(endingHandler){
        endingHandler([self.loadMoreView viewWithTag:1000]);
    }
}

- (void)addTarget:(id)target forLoadMoreAction:(SEL)action
{
    [self setLoadTarget:target];
    [self setLoadAction:action];
}

- (void)addMaskView
{
    UIButton *maskBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    maskBtn.frame = CGRectMake(0, 0, self.frame.size.width, self.loadMoreHeight);
    [maskBtn addTarget:self action:@selector(loadMore:) forControlEvents:UIControlEventTouchUpInside];
    maskBtn.tag = 4000;
    [self.loadMoreView addSubview:maskBtn];
}

- (void)loadMore:(id)sender
{
    self.isLoadingMore = YES;
    UILabel *titleLabel = [self loadMoreTitleLabel];
    UIActivityIndicatorView *indicatorView = [self loadMoreIndicatorView];
    if(titleLabel && indicatorView){
        titleLabel.hidden = YES;
        indicatorView.hidden = NO;
        [indicatorView startAnimating];
    }
    
    id target = [self loadTarget];
    SEL action = [self loadAction];
    if(target && action){
        [target performSelector:action withObject:[self.loadMoreView viewWithTag:1000]];
    }
}

@end
