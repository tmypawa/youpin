//
//  UITableView+LoadMore.h
//  HaiTao
//
//  Created by tmy on 14-6-29.
//
//

#import <UIKit/UIKit.h>

@interface UITableView (LoadMore)
@property (nonatomic) BOOL isLoadingMore;
@property (nonatomic, strong) UIView* loadMoreView;
@property (nonatomic) CGFloat loadMoreHeight;

- (void)setupLoadMoreWithTitle:(NSString *)title;
- (void)setupLoadMoreWithCustomView:(UIView *)customView;
- (void)addTarget:(id)target forLoadMoreAction:(SEL)action;
- (void)showLoadMoreView;
- (void)dismissLoadMore;
- (void)startLoadMore;
- (void)endLoadMore;
- (void)endLoadMoreWithHandler:(void(^)(UIView *customView))endingHandler;
- (UILabel *)loadMoreTitleLabel;
- (UIActivityIndicatorView *)loadMoreIndicatorView;
@end

@interface UICollectionView (LoadMore)
@property (nonatomic) BOOL isLoadingMore;
@property (nonatomic, strong) UIView* loadMoreView;
@property (nonatomic) CGFloat loadMoreHeight;

- (void)setupLoadMoreWithTitle:(NSString *)title;
- (void)setupLoadMoreWithCustomView:(UIView *)customView;
- (void)addTarget:(id)target forLoadMoreAction:(SEL)action;
- (void)startLoadMore;
- (void)endLoadMore;
- (void)endLoadMoreWithHandler:(void(^)(UIView *customView))endingHandler;

- (UILabel *)loadMoreTitleLabel;
- (UIActivityIndicatorView *)loadMoreIndicatorView;

@end
