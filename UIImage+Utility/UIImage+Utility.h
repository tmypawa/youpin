//
//  UIImage+Utility.h
//  UIImageUtility
//
//  Created by Jason on 12-7-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

//像素格式
typedef enum 
{
    PixelFormatRGBA=kCGImageAlphaLast,
    PixelFormatPremultipliedRGBA=kCGImageAlphaPremultipliedLast,
    PixelFormatARGB=kCGImageAlphaFirst,
    PixelFormatPremultipliedARGB=kCGImageAlphaPremultipliedFirst,
    PixelFormatBGRA,
    PixelFormatRGB=kCGImageAlphaNone,
}PixelFormat;


@interface PuzzleInfo : NSObject
@property (nonatomic,retain) UIImage *imageSource;
@property (nonatomic) CGRect   frame;
@property (nonatomic) CGAffineTransform transform;

+ (PuzzleInfo *)puzzle;

@end

@interface UIImage (Utility)

+ (UIImage *)generatePuzzleImageWithSources:(NSArray *)puzzleInfos size:(CGSize)size scale:(float)scale;

//获取图片的像素数据
- (Byte *)copyPixelData;

//转换图片的像素格式
- (UIImage *)convertToPixelFormat:(PixelFormat)format;

//根据指定区域裁剪图片
- (UIImage *)clipImageWithRect:(CGRect)rect;

//根据一定的比例裁剪图片,比如 3:2
- (UIImage *)clipImageWithProportionWidth:(int)width height:(int)height;

//等比例缩放图片
- (UIImage *)zoomImageWithScale:(float)scale;

//根据指定大小自适应缩放图片
- (UIImage *)zoomImageWithSize:(CGSize)size;

//旋转或翻转图片
- (UIImage *)rotateImageWithOrientaiton:(UIImageOrientation)orientation;


@end
