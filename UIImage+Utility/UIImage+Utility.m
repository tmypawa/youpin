//
//  UIImage+Utility.m
//  UIImageUtility
//
//  Created by Jason on 12-7-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "UIImage+Utility.h"


@implementation PuzzleInfo

@synthesize imageSource,frame,transform;

+ (PuzzleInfo *)puzzle
{
    return [[[PuzzleInfo alloc] init] autorelease];
}

@end

@implementation UIImage (Utility)

+ (UIImage *)generatePuzzleImageWithSources:(NSArray *)puzzleInfos size:(CGSize)size scale:(float)scale
{
    CGSize scaleSize=CGSizeMake(size.width*scale, size.height*scale);

    UIGraphicsBeginImageContextWithOptions(scaleSize, YES, 0);     
    CGContextRef ctx=UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(ctx, YES);
    CGContextSetShouldAntialias(ctx, YES);
    
    for(int i=0;i<[puzzleInfos count];i++)
    {
        PuzzleInfo *puzzleInfo=[puzzleInfos objectAtIndex:i];
        CGContextSaveGState(ctx);
        
        CGContextTranslateCTM(ctx, puzzleInfo.transform.tx*scale, puzzleInfo.transform.ty*scale);
        
        CGContextTranslateCTM(ctx, size.width/2, size.height/2);
        float rotationAngle=atan2f(puzzleInfo.transform.b, puzzleInfo.transform.a);
        CGContextRotateCTM(ctx, rotationAngle);
        CGContextTranslateCTM(ctx, -size.width/2, -size.height/2);
        
        float scaleX=sqrtf(powf(puzzleInfo.transform.a, 2)+powf(puzzleInfo.transform.c, 2));
        float scaleY=sqrtf(powf(puzzleInfo.transform.b, 2)+powf(puzzleInfo.transform.d, 2));

        CGRect scaleRect=CGRectMake((puzzleInfo.frame.size.width*(1-scaleX)/2)*scale, (puzzleInfo.frame.size.height*(1-scaleY)/2)*scale, puzzleInfo.frame.size.width*scaleX*scale, puzzleInfo.frame.size.height*scaleY*scale);
        
        [puzzleInfo.imageSource drawInRect:scaleRect];
        CGContextRestoreGState(ctx);
    }
    
    UIImage *newImage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (Byte *)copyPixelData
{
    int width=self.size.width;
    int height=self.size.height;
    int pixelCount=width*height;
    CGImageRef imageRef=self.CGImage;
    CGColorSpaceRef spaceRef=CGImageGetColorSpace(imageRef);
    CGImageAlphaInfo alphaInfo=CGImageGetAlphaInfo(imageRef);
    CGBitmapInfo bitmapInfo=CGImageGetBitmapInfo(imageRef);
    Byte *data=(Byte *)malloc(pixelCount*4);
    
    if(!data)
    {
        NSLog(@"Memory allocation fail!");
        return nil;
    }
    
    memset(data, 0, pixelCount*4);
    
    CGContextRef context=CGBitmapContextCreate(data, width, height, 8, width*4, spaceRef, alphaInfo|bitmapInfo);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGColorSpaceRelease(spaceRef);
    CGContextRelease(context);
    return data;
}

- (UIImage *)convertToPixelFormat:(PixelFormat)format
{
    int width=self.size.width;
    int height=self.size.height;
    int pixelCount=width*height;
    CGImageRef imageRef=self.CGImage;
    CGColorSpaceRef spaceRef=CGImageGetColorSpace(imageRef);
    CGImageAlphaInfo alphaInfo=CGImageGetAlphaInfo(imageRef);
    CGBitmapInfo bitmapInfo=CGImageGetBitmapInfo(imageRef);
    Byte *data=(Byte *)malloc(pixelCount*4);
    
    if(!data)
    {
        NSLog(@"Memory allocation fail!");
        return nil;
    }
    
    CGContextRef context;
    
    if(format!=PixelFormatBGRA)
    {
        context=CGBitmapContextCreate(data, width, height, 8, width*4, spaceRef, format|bitmapInfo);
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    }
    else 
    {
        context=CGBitmapContextCreate(data, width, height, 8, width*4, spaceRef, alphaInfo|bitmapInfo);
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
        for(int i=0;i<width*height*4;i+=4)
        {
            Byte temp=data[i];
            data[i]=data[i+3];
            data[i+3]=temp;
        }
    }
        
    CGImageRef newImageData=CGBitmapContextCreateImage(context);
    UIImage *newImage=[UIImage imageWithCGImage:newImageData];
    CGColorSpaceRelease(spaceRef);
    CGContextRelease(context);
    CGImageRelease(newImageData);
    free(data);

    return newImage;

}

- (UIImage *)clipImageWithRect:(CGRect)rect
{
    CGImageRef imageRef=CGImageCreateWithImageInRect(self.CGImage, rect);
    UIImage *clipedImage=[UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return clipedImage;
}

- (UIImage *)clipImageWithProportionWidth:(int)width height:(int)height
{
    int imageWidth=self.size.width,imageHeight=self.size.height;
    int newWidth,newHeight;
    newWidth=imageWidth;
    newHeight=(float)newWidth*height/width;
    
    if(newHeight>imageHeight)
    {
        newWidth=(float)imageHeight/newHeight*newWidth;
        newHeight=imageHeight;
    }
    
    float x=(imageWidth-newWidth)/2.0f;
    float y=(imageHeight-newHeight)/2.0f;
    return [self clipImageWithRect:CGRectMake(x, y, newWidth, newHeight)];
}

- (UIImage *)zoomImageWithScale:(float)scale
{
    int newWidth=self.size.width*scale;
    int newHeight=self.size.height*scale;
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    CGContextSetInterpolationQuality(UIGraphicsGetCurrentContext(), kCGInterpolationHigh);
    [self drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (UIImage *)zoomImageWithSize:(CGSize)size
{
    int imageWidth=self.size.width,imageHeight=self.size.height;
    int newWidth,newHeight;
    newWidth=size.width;
    newHeight=(float)newWidth*imageHeight/imageWidth;
    
    if(newHeight>size.height)
    {
        newWidth=(float)size.height/newHeight*newWidth;
        newHeight=size.height;
    }
    
    if([[UIDevice currentDevice].systemVersion floatValue]>=5.0f || [[UIDevice currentDevice].systemVersion floatValue]<4.0f)
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    else 
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newHeight), YES, 0);
    
    [self drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    
    UIImage *newImage=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)rotateImageWithOrientaiton:(UIImageOrientation)orientation
{
    return [UIImage imageWithCGImage:self.CGImage scale:1.0f orientation:orientation];
}

@end