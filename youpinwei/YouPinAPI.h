//
//  YouPinAPI.h
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


typedef enum {
    LoginTypePhone = 1,
    LoginTypePC = 2,
    LoginTypeWeixin = 3
}LoginType;

@class TasteItem;
@class Comment;
@class User;
@class Tag;
@class RecommendTag;

@interface YouPinAPI : NSObject

+ (instancetype)sharedAPI;

- (void)cancelRequestForSender:(UIViewController *)sender;

- (void)fecthInitConfigInfo:(void(^)(NSDictionary *configDict, NSError *error))callback;

- (void)applyInvitationCode:(NSString *)code sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback;

- (void)registerWithPhone:(NSString *)phone loginType:(LoginType)loginType sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback;

- (void)logout:(void(^)(NSError *error))callback;

- (void)updateDeviceInfoWithCallback:(void(^)(NSError *error))callback;

- (void)verifyCode:(NSString *)code phone:(NSString *)phone sender:(UIViewController *)sender callback:(void(^)(User *existUser,  NSArray *knownFriendList,NSError *error))callback;

- (void)setProfileWithNickname:(NSString *)nickname
                        gender:(int)gender
                        avatar:(UIImage *)avatar
                         phone:(NSString *)phone
                       headUrl:(NSString *)headUrl
                      location:(NSString *)location
                    userOpenId:(NSString *)userOpenId
                        sender:(UIViewController *)sender
                      callback:(void(^)(User *userInfo,NSError *error))callback;

- (void)uploadAddressBook:(NSArray *)addressBookContacts callback:(void(^)(NSError *error))callback;

- (void)uploadPurchaseHistory:(NSString * )purchaseHistory account:(NSString *)account sender:(UIViewController *)sender callback:(void(^)(NSArray *purchaseItems ,NSError *error))callback;

- (void)fetchPurchaseHistory:(UIViewController *)sender callback:(void(^)(NSArray *purchaseItems ,NSError *error))callback;

- (void)fetchTasteTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext,NSError *error))callback;

- (void)fetchPublicTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, RecommendTag *recommendTag, BOOL hasNext,NSError *error))callback;

- (void)fetchPersonalTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error))callback;

- (void)fetchWishListWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp userId:(NSString *)userId Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error))callback;

- (void)fetchFriendTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp userId:(NSString *)userId Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error))callback;

- (void)fetchDefaultTags:(void(^)(NSArray *tagList, NSError *error))callback;

- (void)fetchFavoriteTagTimelineWithTagName:(NSString *)tagName startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, Tag *tag, BOOL hasNext,NSError *error))callback;

- (void)fetchLatestTagTimelineWithTagName:(NSString *)tagName startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, Tag *tag, BOOL hasNext,NSError *error))callback;

- (void)fetchTagTimelineWithTagName:(NSString *)tagName tagType:(NSInteger)type startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, Tag *tag, BOOL hasNext,NSError *error))callback;

- (void)queryGoodsInfoForTaste:(NSString *)Id callback:(void(^)(NSError *error))callback;

- (void)subscribeTag:(NSString *)tagName value:(BOOL)subscribe callback:(void(^)(NSError *error))callback;

- (void)fetchTasteDetailWithTasteId:(NSString *)Id sender:(UIViewController *)sender callback:(void(^)(TasteItem *item, BOOL hasQuery, NSArray *queryUsers,NSError *error))callback;

- (void)fetchPraiseListWithTatesId:(NSString *)Id sender:(UIViewController *)sender callback:(void(^)(NSArray *users, BOOL hasNext, NSError *error))callback;

- (void)fetchMultipleGoodsPicWithItemId:(NSString *)itemId sender:(UIViewController *)sender callback:(void(^)(NSArray *picURLs, NSError *error))callback;

- (void)publishCommentWithTasteId:(NSString *)Id comment:(Comment *)comment replyTargetPhone:(NSString *)receiverPhone sender:(UIViewController *)sender callback:(void(^)(Comment *newComment,NSError *error))callback;

- (void)deleteComment:(Comment *)comment sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback;

- (void)publishTaste:(TasteItem *)taste tags:(NSArray *)tags sender:(UIViewController *)sender callback:(void(^)(TasteItem *newTaste,NSError *error))callback;

//v2 api
- (void)publishPost:(TasteItem *)post sender:(UIViewController *)sender callback:(void(^)(TasteItem *newTaste,NSError *error))callback;

- (void)deleteTasteWithProductId:(NSString *)productId sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback;

- (void)praiseTaste:(NSString *)Id value:(int)praiseValue callback:(void(^)(int pinCount, NSArray *currentPinUsers,NSError *error))callback;

- (void)addWishListWithTaste:(NSString *)Id add:(BOOL)add callback:(void(^)(int wishCount, NSArray *currentWishListUsers,NSError *error))callback;

- (void)uploadPushInfoWithUserId:(NSString *)userId channelId:(NSString *)channelId callback:(void(^)(NSError *error))callback;

- (void)fetchHotTags:(void(^)(NSArray *tags, NSError *error))callback;

- (void)fetchRelatedTagsWithTag:(NSString *)tag sender:(UIViewController *)sender callback:(void(^)(NSArray *tags, NSError *error))callback;

- (void)reportTasteItem:(TasteItem *)item content:(NSString *)reportContent sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback;

- (void)reportComment:(Comment *)comment content:(NSString *)reportContent sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback;

- (void)fetchNotificationListWithUpdateTime:(NSTimeInterval)updateTime sender:(UIViewController *)sender callback:(void(^)(NSArray *notificationList, NSInteger notificationCount, NSError *error))callback;

- (void)fetchFriendNotificationListWithUpdateTime:(NSTimeInterval)updateTime sender:(UIViewController *)sender callback:(void(^)(NSArray *notificationList, NSInteger notificationCount, NSError *error))callback;

- (void)fetchPublicBannerInfoWithSender:(UIViewController *)sender callback:(void(^)(NSArray *banners, NSError *error))callback;

- (void)fetchFriendTimelineBadgeWithUpdateTime:(NSTimeInterval)updateTime sender:(UIViewController *)sender callback:(void(^)( NSInteger newTimelineCount, NSError *error))callback;

- (void)searchUserByNickname:(NSString *)nickname sender:(UIViewController *)sender callback:(void(^)(NSArray *userList, NSError *error))callback;

- (void)sendFriendInvitationToUser:(NSString *)userId introduce:(NSString *)introduce callback:(void(^)(NSError *error))callback;

- (void)fetchFriendList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback;

- (void)fetchRecommendFriendList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback;

- (void)fetchContactFriendList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback;

- (void)fetchContactInvitationList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback;

- (void)acceptFriend:(NSString *)userId notifyId:(NSString *)notifyId callback:(void(^)(NSError *error))callback;

- (void)deleteFriend:(NSString *)userId callback:(void(^)(NSError *error))callback;

- (void)acceptFriendInvitationWithCode:(NSString *)invitationCode callback:(void(^)(BOOL success, User *inviter, NSArray *knownUserList, NSError *error))callback;

- (void)fetchCategoryChooseListWithStartTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime callback:(void(^)(NSArray *tags, NSError *error))callback;

- (void)fetchMoreRelatedTagListWithTag:(NSString *)tagName startTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime callback:(void(^)(NSArray *tags, NSError *error))callback;

- (void)selectCategories:(NSArray *)categories callback:(void(^)(NSError *error))callback;

- (void)queryTagsWithKeyword:(NSString *)keyword sender:(UIViewController *)sender callback:(void(^)(NSArray *tags, NSError *error))callback;

- (void)queryPostWithKeyword:(NSString *)keyword start:(NSInteger)start sender:(UIViewController *)sender callback:(void(^)(NSArray *items, NSInteger start,NSError *error))callback;

- (void)fetchDiscoveryListWithEndTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *banners, NSArray *recommendTag, BOOL hasNext,NSError *error))callback;

@end
