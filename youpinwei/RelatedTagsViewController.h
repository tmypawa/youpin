//
//  RelatedTagsViewController.h
//  youpinwei
//
//  Created by tmy on 15/3/23.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaCollectionViewController.h"

@interface RelatedTagsViewController : ShaibaCollectionViewController

@property (nonatomic, strong) NSString *tagName;

@end
