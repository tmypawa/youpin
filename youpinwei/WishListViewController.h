//
//  WishListViewController.h
//  youpinwei
//
//  Created by tmy on 14/12/23.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WishListViewController : ShaibaBaseViewController<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) NSString *userId;

@end
