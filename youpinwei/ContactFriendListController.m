//
//  ContactFriendListController.m
//  youpinwei
//
//  Created by tmy on 15/1/17.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ContactFriendListController.h"
#import "FriendInfoCell.h"
#import "FriendInfoViewController.h"
#import "JTAddressBookManager.h"

@implementation ContactFriendListController
{
    NSArray    *_friendList;
    NSInteger   _currentActionIndex;
    NSMutableSet *_invitationSet;
    IBOutlet UIView *_defaultView;
    IBOutlet UIButton *_addButton;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _currentActionIndex = -1;
        _invitationSet = [[NSMutableSet alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setTableHeaderView:nil];
    
    [_addButton setBackgroundImage:[[UIImage imageNamed:@"btn_red_normal"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    [_addButton setBackgroundImage:[[UIImage imageNamed:@"btn_red_highlight"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateHighlighted];
    
    if ([self.navigationController.viewControllers firstObject] != self) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    [self refreshData];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)allowContactAccess:(id)sender {
    Alert(@"允许访问通讯录", @"请到iPhone的设置 -> 隐私 -> 通讯录中允许晒吧访问通讯录", @"确定");
}

- (void)refreshData {
    [[JTAddressBookManager defaultManager] requestAuthorization:^(bool granted) {
        if (granted) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[JTAddressBookManager defaultManager] fetchAllContacts:^(NSArray *allContacts, NSArray *updatedContacts) {
                [[YouPinAPI sharedAPI] uploadAddressBook:updatedContacts callback:^(NSError *error) {
                    if (!error) {
                        NSLog(@"uploaded address book %ld", JTGlobalCache.lastAddressBookSyncTime == 0? allContacts.count : updatedContacts.count);
                        JTGlobalCache.lastAddressBookSyncTime = [[NSDate date] timeIntervalSince1970];
                        [self loadFriendList];
                    }
                }];
            }];
        }else {
            [self.tableView addSubview:_defaultView];
            self.tableView.scrollEnabled = NO;
        }
    }];
}

- (void)loadFriendList {
    [[YouPinAPI sharedAPI] fetchContactFriendList:self callback:^(NSArray *friendList, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        if (!error) {
            _friendList = friendList;
            [self.tableView reloadData];
        }
    }];
}

- (void)addFriend:(UIButton *)sender {
    User *friend = nil;
    NSInteger index = sender.tag - 1;
    _currentActionIndex = index;
    friend = _friendList[index];
    
    FriendInvitationController *invitationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInvitationController"];
    invitationController.userId = friend.userId;
    invitationController.delegate = self;
    [self.navigationController pushViewController:invitationController animated:YES];
}

- (void)viewFriendInfo:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    User *friend =  _friendList[index];
    FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
    friendInfoController.userId = friend.userId;
    [self.navigationController pushViewController:friendInfoController animated:YES];
}

- (void)friendInvitationControllerDidSendInvitation:(FriendInvitationController *)controller {
    if (_currentActionIndex != -1) {
        User *friend = nil;
        friend = _friendList[_currentActionIndex];
        
        [_invitationSet addObject:friend.userId];
 
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_currentActionIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        _currentActionIndex = -1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _friendList ? _friendList.count : 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FriendInfoCell" forIndexPath:indexPath];
    User *friend = _friendList[indexPath.row];
    
    if (cell.addBtn.tag == 0) {
        [cell.addBtn addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
        [cell.avatarBtn addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.addBtn.tag = indexPath.row + 1;
    cell.avatarBtn.tag = indexPath.row + 1;
    
    [cell.avatarBtn setImageWithURL:friend.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    cell.nameTitleLabel.text = friend.nickname;
    
    if(friend.relation == UserRelationTypeWaitAccept || [_invitationSet containsObject:friend.userId]){
        cell.statusLabel.text = @"等待验证";
        cell.statusLabel.hidden = NO;
        cell.addBtn.hidden = YES;
    }else if(friend.relation == UserRelationTypeFriend) {
        cell.addBtn.hidden = YES;
        cell.statusLabel.hidden = NO;
        cell.statusLabel.text = @"已添加";
    }else if(friend.relation == UserRelationTypeNotFriend) {
        [cell.addBtn setTitle:@"添加" forState:UIControlStateNormal];
        [cell.addBtn setTitleColor:NavigationBarTintColor forState:UIControlStateNormal];
        cell.addBtn.hidden = NO;
        cell.statusLabel.hidden = YES;
    }
    
    [cell updateLayout];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *friend =  _friendList[indexPath.row];
    FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
    friendInfoController.userId = friend.userId;
    [self.navigationController pushViewController:friendInfoController animated:YES];
}

@end
