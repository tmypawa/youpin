//
//  ImageContentCell.h
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostContentCell.h"

@interface ImageContentCell : PostContentCell

@property (strong, nonatomic) IBOutlet UIImageView *thumnailView;
@end
