//
//  PublishStatusCell.m
//  youpinwei
//
//  Created by tmy on 15/2/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishStatusCell.h"

@implementation PublishStatusCell
{
    
    IBOutlet UIActivityIndicatorView *_indicatorView;
    IBOutlet UIImageView *_failStatusView;
    IBOutlet UILabel *_statusLabel;
}

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.thumbnailView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:255/255.0];
}

- (void)updateViewForStatus {
    switch (self.status) {
        case PublishStatusCellTypeSending:
            _failStatusView.hidden = YES;
            _statusLabel.hidden = YES;
            self.refteshButton.hidden = YES;
            self.deleteButton.hidden = YES;
            _indicatorView.hidden = NO;
            [_indicatorView startAnimating];
            self.titleLabel.hidden = NO;
            break;
        case PublishStatusCellTypeFailed:
            _statusLabel.text = @"发布失败";
            _failStatusView.hidden = NO;
            _statusLabel.hidden = NO;
            self.refteshButton.hidden = NO;
            self.deleteButton.hidden = NO;
            _indicatorView.hidden = YES;
            [_indicatorView stopAnimating];
            self.titleLabel.hidden = YES;

            break;
        default:
            break;
    }
    
}

@end
