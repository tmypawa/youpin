//
//  DetailViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "DetailViewController.h"
#import "LoginViewController.h"
#import "PraiseListViewController.h"
#import "JTImageBrowerController.h"
#import "JTImageZoomAnimationController.h"
#import "PBWebViewController.h"
#import "TagViewController.h"
#import "FriendInfoViewController.h"
#import "TasteItem.h"
#import "Comment.h"
#import "CommentCell.h"
#import "ProvidePurchaseInfoCell.h"
#import "AskPurchaseInfoCell.h"
#import "DWTagList.h"
#import "JTAddressBookManager.h"
#import "UIKit+RoundImage.h"
#import "UIScrollView+UzysAnimatedGifPullToRefresh.h"
#import "JTTextView.h"
#import "PostEditViewController.h"
#import "PostContentItem.h"
#import "DetailCells.h"
#import "UMSocial.h"
#import "BannerViewController.h"



#define MORE_ITEM_BUY_PAGE @"查看宝贝详情"
#define MORE_ITEM_REPORT @"举报不良内容"
#define MORE_ITEM_DELETE @"删除"
#define MORE_ITEM_EDIT @"编辑"

#define DEFAULT_INPUT_PLACEHOLDER @"发表一下高见~~"

@interface DetailViewController ()

@end

@implementation DetailViewController
{
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UIView *_headerView;
    IBOutlet UIButton *_avatarView;
    __weak IBOutlet UILabel *_friendInfoLabel;
    IBOutlet UIImageView *_levelIconView;
    IBOutlet UILabel *_levelTitleLabel;
    IBOutlet UILabel *_whoseFriendInfoLabel;
    IBOutlet UIImageView *_footerImageView;
    IBOutlet UIView *_footerView;
    IBOutlet UIView *_titleBgView;
    IBOutlet UILabel *_titleLabel;
    IBOutlet UIButton *_shareBtn;
    UIImageView     *_thumbnailShotView;
    
    //input
    JTInputView             *_inputView;
    UIImageView             *_inputBgImageView;
    UIButton                *_backBtn;
    IBOutlet JTAnimationButton       *_favBtn;

    NSMutableArray      *_contentCellClassArray;
    TasteItem           *_tasteItem;
    Comment             *_toActionComment;
    BOOL                _isReplyComment;
    NSMutableDictionary  *_localCommentsDict;
    JTImageBrowerController *_imageBrowerController;
    BOOL                _shouldUpdateLayout;
    BOOL                _hasQueryGoodsInfo;
    NSArray             *_queryUserList;
}
 

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _localCommentsDict = [[NSMutableDictionary alloc] init];
        _contentCellClassArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [_tableView removePullToRefreshActionHandler];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tasteDidFinishEditing:) name:YouPinNotificationTasteItemDidEditedSuccess object:nil];
    
    self.title = @"宝贝详情";
    self.autoCancelRequestWhenDisappear = YES;
 
    _avatarView.backgroundColor = [UIColor whiteColor];
    [_avatarView makeRound];
    
    _tableView.hidden = YES;
    
    _shareBtn.hidden = !JTGlobalCache.isLogin;
    [_shareBtn setBackgroundImage:[[UIImage imageNamed:@"btn_blue_normal"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    [_shareBtn setBackgroundImage:[[UIImage imageNamed:@"btn_blue_highlight"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateHighlighted];
    
    _favBtn.highlightScale = 1;
    _favBtn.normalImage = [UIImage imageNamed:@"icon_dreamlist_normal"];
    _favBtn.highlightImage = [UIImage imageNamed:@"icon_dreamlist_done"];
    
//    SetFrameHeight(_tableView, contentHeight - _inputView.frame.size.height);
    
    [self loadPullRereshAnimation];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self refreshData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_imageBrowerController) {
        [_thumbnailShotView removeFromSuperview];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)initInputView {
//    _inputView = [[JTInputView alloc] initWithFrame:CGRectMake(0, screenFrame.size.height - 64 - 44, screenFrame.size.width, 44)];

//    _inputView.attachedScrollView = _tableView;
//    _inputView.delegate = self;
//    _inputView.textView.delegate = self;
//    _inputView.textView.placeholderColor = [UIColor colorWithRed:0.64 green:0.64 blue:0.64 alpha:1];
//    _inputView.textView.textColor = [UIColor colorWithRed:0.03 green:0.03 blue:0.03 alpha:1];
//    _inputView.textView.backgroundColor = [UIColor whiteColor];
//    
//    _inputView.textView.font = [UIFont systemFontOfSize:15];
//    _inputView.textView.placeholderText = DEFAULT_INPUT_PLACEHOLDER;
//    _inputView.textView.tintColor = NavigationBarTintColor;
//    _inputView.hidden = YES;
//    
//    CGRect textFrame = _inputView.textView.frame;
//    textFrame.size.width = 180;
//    textFrame.size.height = 32;
//    textFrame.origin.x = 40;
//    _inputView.textView.frame = textFrame;
//    
//    CGRect sendFrame = _inputView.sendButton.frame;
//    sendFrame.size.width = 55;
//    sendFrame.origin.x = screenWidth - sendFrame.size.width - 5;
//    sendFrame.origin.y = 8;
//    sendFrame.size.height -= 1;
//    _inputView.sendButton.frame = sendFrame;
//    [_inputView.sendButton setTitle:@"发表" forState:UIControlStateNormal];
//    [_inputView.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [_inputView.sendButton setBackgroundImage:[[UIImage imageNamed:@"btn_send_comment"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
//    _inputView.sendButton.titleLabel.font = [UIFont systemFontOfSize:15];
//    _inputView.sendButton.adjustsImageWhenDisabled = NO;
//    _inputView.sendButton.alpha = 0;
//    
//    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _inputView.frame.size.width, _inputView.frame.size.height)];
//    backgroundView.backgroundColor = [UIColor colorWithRed:0.96 green:0.94 blue:0.94 alpha:1];
//    
//    UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 0.5f)];
//    topLineView.backgroundColor = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1];
//    [backgroundView addSubview:topLineView];
//    
//    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _backBtn.frame = CGRectMake(3, 4, 36, 36);
//    [_backBtn setImage:[UIImage imageNamed:@"btn_comment_arrow_back"] forState:UIControlStateNormal];
//    [_backBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
//    _backBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [backgroundView addSubview:_backBtn];
//    
//    _favBtn = [JTAnimationButton buttonWithNormalImage:[UIImage imageNamed:@"icon_dreamlist_normal"] highlightImage:[UIImage imageNamed:@"icon_dreamlist_done"]];
//    _favBtn.highlightScale = 1.05f;
//    _favBtn.frame = CGRectMake(CGRectGetMaxX(textFrame) + 10, 4, 36, 36);
//    [_favBtn addTarget:self action:@selector(fav:) forControlEvents:UIControlEventTouchUpInside];
//    _favBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [backgroundView addSubview:_favBtn];
//    
//    _praiseBtn = [JTAnimationButton buttonWithNormalImage:[UIImage imageNamed:@"icon_like_normal"] highlightImage:[UIImage imageNamed:@"icon_like_done"]];
//    _praiseBtn.highlightScale = 1.05f;
//    _praiseBtn.frame = CGRectMake(CGRectGetMaxX(_favBtn.frame) + 5, 4, 36, 36);
//    [_praiseBtn addTarget:self action:@selector(praise:) forControlEvents:UIControlEventTouchUpInside];
//    _praiseBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
//    [backgroundView addSubview:_praiseBtn];
//    
//    _inputView.backgroundView = backgroundView;
//    [self.view addSubview:_inputView];
}

- (void)loadPullRereshAnimation {
    NSMutableArray *pullAnimations = [[NSMutableArray alloc] init];
    NSMutableArray *loadingAnimations = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 23; ++i) {
        [pullAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
    for (int i = 0; i < 45; ++i) {
        [loadingAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
     __weak DetailViewController *_self = self;
    [_tableView addPullToRefreshActionHandler:^{
        [_self refreshData];
    } ProgressImages:pullAnimations LoadingImages:loadingAnimations ProgressScrollThreshold:20 LoadingImagesFrameRate:12];
    _tableView.showAlphaTransition = NO;
    _tableView.showVariableSize = NO;
    _tableView.showPullToRefresh = YES;
    _tableView.showTitle = NO;
 
    [_tableView addTopInsetInPortrait:0 TopInsetInLandscape:0];
}

- (void)refreshData {
    [[YouPinAPI sharedAPI] fetchTasteDetailWithTasteId:self.tasteId sender:self callback:^(TasteItem *item, BOOL hasQuery, NSArray *queryUsers, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            _tasteItem = item;
            _hasQueryGoodsInfo = hasQuery;
            _queryUserList = queryUsers;
            [MobClick event:@"show_detail_page" attributes:@{@"product": _tasteItem.productId }];
            [self showDetailView];
            
            if (_tableView.pullToRefreshView.state == UZYSGIFPullToRefreshStateLoading) {
                [_tableView stopRefreshAnimation];
            }
        }else {
            if (error.code == ERROR_NOT_EXIST) {
                MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hudView.mode = MBProgressHUDModeText;
                hudView.labelText = @"该贴已经不存在";
                [hudView hide:YES afterDelay:3];
            }
        }
    }];
}

- (void)showDetailView {
    [self generateCellClasses];
    [self updateLayout];
    
    _inputView.hidden = NO;
    [_tableView reloadData];
    
    [self updateFooterView];
    [self updateRelatedItem];
    
    if (self.shouldCommentImmediately) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (_tasteItem.comments && _tasteItem.comments.count > 0) {
                [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_contentCellClassArray.count + _tasteItem.comments.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
        });
    }
}

- (void)updateFooterView {
    _footerImageView.image = [UIImage imageNamed:_tasteItem.comments.count > 0 ?@"icon_no more comment_tips" : @"icon_no comment_tips"];
//    [[_footerView viewWithTag:1000] setHidden:(_tasteItem.comments.count > 0)];
}

- (void)generateCellClasses {
    [_contentCellClassArray removeAllObjects];
    
    for (PostContentItem *contentItem  in _tasteItem.contentItems) {
        if (contentItem.type == PostContentTypeText) {
            [_contentCellClassArray addObject:[TextContentCell class]];
        }else if(contentItem.type == PostContentTypeImage) {
            [_contentCellClassArray addObject:[ImageContentCell class]];
        }
    }
    
    if (_tasteItem.tags && _tasteItem.tags.count > 0) {
        [_contentCellClassArray addObject:[TagListCell class]];
    }
    
    if (_tasteItem.goods) {
        [_contentCellClassArray addObject:[GoodsInfoCell class]];
    }else {
        if (JTGlobalCache.isLogin && [_tasteItem.userId isEqualToString:JTGlobalCache.currentUser.userId]) {
            [_contentCellClassArray addObject:[ProvidePurchaseInfoCell class]];
        }else {
            [_contentCellClassArray addObject:[AskPurchaseInfoCell class]];
        }
    }
    
    [_contentCellClassArray addObject:[PraiseListCell class]];
    
    [_contentCellClassArray addObject:[CommentCountCell class]];
    
    if (_tasteItem.editorComment) {
        [_contentCellClassArray addObject:[EditorCommentCell class]];
    }
}


- (void)updateLayout {
    CGFloat offsetY = _tableView.contentOffset.y;
   
    if (_tasteItem.relation != 2) {
        _whoseFriendInfoLabel.hidden = YES;
        _friendInfoLabel.text = [_tasteItem displayName];
    }else {
        _friendInfoLabel.text = _tasteItem.nick;
        _whoseFriendInfoLabel.text = [_tasteItem displayName];
        _whoseFriendInfoLabel.hidden = NO;
    }
    
    [_friendInfoLabel sizeToFit];
    
    if (_whoseFriendInfoLabel.hidden) {
        CGPoint nameCenter = _friendInfoLabel.center;
        nameCenter.y = _avatarView.center.y;
        _friendInfoLabel.center = nameCenter;
    }else {
        SetFrameY(_friendInfoLabel, _avatarView.frame.origin.y);
    }
    
    SetFrameX(_levelIconView, CGRectGetMaxX(_friendInfoLabel.frame) + 5);
    _levelIconView.center = CGPointMake(_levelIconView.center.x, _friendInfoLabel.center.y);
    
    if (_tasteItem.degree == 0) {
        _levelIconView.hidden = YES;
        _levelTitleLabel.hidden = YES;
    }else {
        _levelIconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_lv_%ld", _tasteItem.degree]];
        _levelIconView.hidden = NO;
        _levelTitleLabel.hidden = NO;
        NSMutableAttributedString *levelTitleStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"体验家%ld级", _tasteItem.degree]];
        [levelTitleStr addAttributes:@{NSForegroundColorAttributeName: LevelColor(_tasteItem.degree)} range:NSMakeRange(3, levelTitleStr.length - 4)];
        _levelTitleLabel.attributedText = levelTitleStr;
        _levelTitleLabel.center = CGPointMake(_levelTitleLabel.center.x, _avatarView.center.y);
    }
    
    [_avatarView setImageWithURL:_tasteItem.headImage placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
  
    [_favBtn showHightlighted:_tasteItem.addWishList animate:NO];
    
    _tableView.hidden = NO;
    
    if (NotEmptyValue(_tasteItem.title)) {
        _titleBgView.hidden = NO;
        _titleLabel.text = _tasteItem.title;
        SetFrameHeight(_headerView, 86);
    }else {
        _titleBgView.hidden = YES;
        SetFrameHeight(_headerView, 54);
    }
    
    [_tableView setTableHeaderView:nil];
    [_tableView setTableHeaderView:_headerView];
    _tableView.contentOffset = CGPointMake(0, offsetY);
}

- (void)tasteDidFinishEditing:(NSNotification *)notification {
    NSMutableArray *comments = _tasteItem.comments;
    NSInteger commentCount = _tasteItem.commentCount;
    TasteItem *newTaste = notification.userInfo[YouPinNotificationPublishTasteItemKey];
    _tasteItem = newTaste;
    _tasteItem.comments = comments;
    _tasteItem.commentCount = commentCount;
    [self showDetailView];
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateRelatedItem {
    if (self.relatedTasteItem && _tasteItem) {
        [self.relatedTasteItem updateStatusFromItem:_tasteItem];
    }
}

- (void)viewPhotos:(NSInteger)showImageIndex {
    if (_inputView.textView.isFirstResponder) {
        return;
    }
    
    NSMutableArray *urls = [[NSMutableArray alloc] init];
    for (NSString *url in _tasteItem.realShotPics) {
        [urls addObject:[NSURL URLWithString:url]];
    }
    
    if (urls.count > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _imageBrowerController = [[JTImageBrowerController alloc] initWithImageURLs: urls];
            _imageBrowerController.transitioningDelegate = self;
            _imageBrowerController.showPageIndex = showImageIndex;
            [self presentViewController:_imageBrowerController animated:YES completion:nil];

        });
    }
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    JTImageZoomAnimationController *animationController = [[JTImageZoomAnimationController alloc] initWithReferenceView:_thumbnailShotView];
    return animationController;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    JTImageZoomAnimationController *animationController = [[JTImageZoomAnimationController alloc] initWithReferenceView:_thumbnailShotView];
    return animationController;
}

- (IBAction)showShareMenu:(id)sender {
    [self showShareMenuWithTasteItem:_tasteItem];
}

- (void)showRegisterAlert {
    AlertWithActionsAndTag(1000, @"登录晒吧", @"继续操作之前先登录一下吧~~", @"看看再说", @"马上GO~~");
}

- (IBAction)showMore:(id)sender {
    UIActionSheet *moreActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
//    if (_tasteItem.buyLink && ![_tasteItem.buyLink isEqualToString:@""]) {
//        [moreActionSheet addButtonWithTitle:MORE_ITEM_BUY_PAGE];
//    }
    
    if (JTGlobalCache.isLogin && ![JTGlobalCache.currentUser.userId isEqualToString:_tasteItem.userId]) {
        [moreActionSheet addButtonWithTitle:MORE_ITEM_REPORT];
    }
    
    if (JTGlobalCache.currentUser && [_tasteItem.userId isEqualToString:JTGlobalCache.currentUser.userId]) {
        [moreActionSheet addButtonWithTitle:MORE_ITEM_EDIT];
        [moreActionSheet addButtonWithTitle:MORE_ITEM_DELETE];
    }
    
    moreActionSheet.cancelButtonIndex = [moreActionSheet addButtonWithTitle:@"取消"];
    
    moreActionSheet.tag = 100;
    [moreActionSheet showInView:self.navigationController.view];
}

- (void)showCommentReportMenu {
    UIActionSheet *reportSheet = [[UIActionSheet alloc] initWithTitle:@"举报" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"内容与宝贝不符", @"内容不健康", @"有广告嫌疑", @"垃圾信息", nil];
    reportSheet.tag = 500;
    [reportSheet showInView:self.navigationController.view];
}
- (IBAction)showPraiseList:(id)sender {
    PraiseListViewController *praiseListController = [self.storyboard instantiateViewControllerWithIdentifier:@"PraiseListViewController"];
    praiseListController.tasteItem = _tasteItem;
    [self.navigationController pushViewController:praiseListController animated:YES];
}

- (IBAction)viewFriendInfo:(UIButton *)sender {
    TasteItem *tastItem = _tasteItem;
    
    if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:tastItem.userId]) {
        PersonalInfoViewController *personInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
        [self.navigationController pushViewController:personInfoController animated:YES];
    }else {
        FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
        friendInfoController.userId = tastItem.userId;
        [self.navigationController pushViewController:friendInfoController animated:YES];
    }
}

- (void)viewPinUserInfo:(UIButton *)sender {
    NSInteger pinUserIndex = sender.tag;
    if (_tasteItem.pinUsers && _tasteItem.pinUsers.count > 0) {
        User *pinUser = _tasteItem.pinUsers[pinUserIndex];
        if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:pinUser.userId]) {
            PersonalInfoViewController *personInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
            [self.navigationController pushViewController:personInfoController animated:YES];
        }else {
            FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
            friendInfoController.userId = pinUser.userId;
            [self.navigationController pushViewController:friendInfoController animated:YES];
        }
    }
}

- (void)viewCommentPersonInfo:(UIButton *)sender {
    NSInteger commentIndex = sender.tag - 1;
    Comment *comment = _tasteItem.comments[commentIndex];
    FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
    friendInfoController.userId = comment.userId;
    [self.navigationController pushViewController:friendInfoController animated:YES];
}

- (IBAction)showCommentPublishView:(id)sender {
    PublishCommentViewController *publishCommentController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishCommentViewController"];
    publishCommentController.tasteItem = _tasteItem;
    publishCommentController.delegate = self;
    if (_toActionComment) {
        publishCommentController.replyComment = _toActionComment;
        _toActionComment = nil;
    }
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:publishCommentController];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)publishCommentViewController:(PublishCommentViewController *)controller didPublishComment:(Comment *)newComment {
    [_tasteItem.comments addObject:newComment];
    _tasteItem.commentCount += 1;
    [self updateRelatedItem];
    
    NSInteger commentCountRowIndex = [_contentCellClassArray indexOfObject:[CommentCountCell class]];
    [_tableView beginUpdates];
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:commentCountRowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    [_tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_contentCellClassArray.count + _tasteItem.comments.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [_tableView endUpdates];
    
    [self updateFooterView];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSInteger lastRowIndex = _contentCellClassArray.count + _tasteItem.comments.count - 1;
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRowIndex inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });

}

- (IBAction)fav:(id)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else {
        TasteItem *tastItem = _tasteItem;
        _tasteItem.addWishList = !_tasteItem.addWishList;
        [_favBtn showHightlighted:tastItem.addWishList animate:YES];
        
        NSString *toastTitle = _tasteItem.addWishList ? @"已加入心愿单" : @"已移除心愿单";
        [self showToastWithTitle:toastTitle];
        
        [[YouPinAPI sharedAPI] addWishListWithTaste:tastItem.productId add:tastItem.addWishList callback:^(int wishCount, NSArray *currentWishListUsers, NSError *error) {
            if (!error) {
                [self updateRelatedItem];
            }
        }];
    }
}

- (IBAction)praise:(id)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else if(!_tasteItem.pin){
        _tasteItem.pin = !_tasteItem.pin;
        _tasteItem.pincount++;
        
        NSMutableArray *pinUsers = [[NSMutableArray alloc] init];
        if (_tasteItem.pinUsers) {
            [pinUsers addObjectsFromArray:_tasteItem.pinUsers];
        }
        [pinUsers addObject:JTGlobalCache.currentUser];
        _tasteItem.pinUsers = pinUsers;
        
        [sender showHightlighted:_tasteItem.pin animate:YES];
        [((UIButton *)sender) setTitle:_tasteItem.pincount == 0 ? @"赞" : [NSString stringWithFormat:@"%d", _tasteItem.pincount] forState:UIControlStateNormal];
        
        NSString *toastTitle = _tasteItem.pincount > 0? @"谢谢你送我1米阳光" : @"第一个点赞，奖励你1米阳光";
        [self showToastWithTitle:toastTitle];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[YouPinAPI sharedAPI] praiseTaste:_tasteItem.productId value:_tasteItem.pin? 1 : 2 callback:^(int pinCount, NSArray *pinUsers, NSError *error) {
                if (!error) {
                    _tasteItem.pincount = pinCount;
                    _tasteItem.pinUsers = pinUsers;
                    NSInteger praiseCellIndex = [_contentCellClassArray indexOfObject:[PraiseListCell class]];
                    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:praiseCellIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [self updateRelatedItem];
                    [self updateLayout];
                }
            }];
        });
    }
}

- (IBAction)comment:(id)sender {
    
}

- (void)deleteTasteItem {
    MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hudView.labelText = @"正在删除...";
    
    [[YouPinAPI sharedAPI] deleteTasteWithProductId:_tasteItem.productId sender:self callback:^(NSError *error) {
        [hudView hide:YES];
        if (!error) {
            [hudView hide:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationDidRemoveTasteItem object:self];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            Alert(error.localizedDescription, @"", @"确定");
        }
    }];

}

- (void)DWTagList:(DWTagList *)tagList willDisplayTagView:(DWTagView *)tagView atTagIndex:(NSInteger)tagIndex {
    TasteItem *item = _tasteItem;
    if (item) {
        NSString *tag = item.tags[tagIndex];
        BOOL isSystemTag = [tag hasSuffix:@"-sys"];
        
        if (isSystemTag) {
            tagView.button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        }
    }
}


- (void)DWTagList:(DWTagList *)tagList selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex {
    TagViewController *tagViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
    tagViewController.tagName = _tasteItem.tags[tagIndex];
    [self.navigationController pushViewController:tagViewController animated:YES];
}


- (void)publishComment:(NSString *)comment {
    NSString *toSendComment = @"";
    
    Comment *newComment = [[Comment alloc] init];
    newComment.headImage = JTGlobalCache.currentUser.avatar;
    newComment.nick = JTGlobalCache.currentUser.nickname;
    newComment.extraId = JTGlobalCache.currentUser.phone;
    newComment.productId = _tasteItem.productId;
    newComment.createTime = [[NSDate date] timeIntervalSince1970]*1000;
    
    if (!_isReplyComment) {
        toSendComment = comment;
        newComment.receiverNick = @"";
        newComment.receiverPhone = @"";
    }else {
        toSendComment = comment;
        newComment.receiverNick = _toActionComment.nick;
        newComment.receiverPhone = _toActionComment.extraId;
        newComment.receiverId = _toActionComment.userId;
    }
    
    newComment.comment = toSendComment;
    newComment.requestId = [[NSUUID UUID] UUIDString];
    _localCommentsDict[newComment.requestId] = newComment;
    
    [_tasteItem.comments addObject:newComment];
    _tasteItem.commentCount += 1;
    [self updateRelatedItem];
    
    NSInteger commentCountRowIndex = [_contentCellClassArray indexOfObject:[CommentCountCell class]];
    [_tableView beginUpdates];
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:commentCountRowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    [_tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_contentCellClassArray.count + _tasteItem.comments.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [_tableView endUpdates];
    
    [self updateFooterView];
   
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSInteger lastRowIndex = _contentCellClassArray.count + _tasteItem.comments.count - 1;
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRowIndex inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
 
    [[YouPinAPI sharedAPI] publishCommentWithTasteId:self.tasteId comment:newComment replyTargetPhone:_isReplyComment ? _toActionComment.extraId : nil sender:self callback:^(Comment *newComment, NSError *error) {
        if (!error) {
            if(newComment) {
                [self syncLocalCommentWithOnlineComment:newComment];
            }
        }
    }];
}

- (void)syncLocalCommentWithOnlineComment:(Comment *)comment {
    Comment *localComment = _localCommentsDict[comment.requestId];
    if (localComment) {
       NSInteger existIndex = [_tasteItem.comments indexOfObject:localComment];
        if (existIndex != NSNotFound) {
            [_tasteItem.comments replaceObjectAtIndex:existIndex withObject:comment];
        }
        
        [_localCommentsDict removeObjectForKey:comment.requestId];
    }
}

- (void)deleteComment {
    if (_toActionComment) {
        NSInteger rowIndex = [_tasteItem.comments indexOfObject:_toActionComment] + _contentCellClassArray.count;
        [[YouPinAPI sharedAPI] deleteComment:_toActionComment sender:self callback:^(NSError *error) {
            if (!error) {
                
            }
        }];
        
        [_tasteItem.comments removeObject:_toActionComment];
        _tasteItem.commentCount -= 1;

        NSInteger commentCountRowIndex = [_contentCellClassArray indexOfObject:[CommentCountCell class]];
        
        [_tableView beginUpdates];
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:commentCountRowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
        [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [_tableView endUpdates];
        
        [self updateFooterView];
        _toActionComment = nil;
    }
}

- (void)showOrHideInputActionButtons:(BOOL)isShow {
    if (isShow) {
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.9 initialSpringVelocity:0 options:0 animations:^{
            _backBtn.alpha = 1;
            _favBtn.alpha = 1;
            _inputView.sendButton.alpha = 0;
            SetFrameWidth(_inputBgImageView, 180);
            SetFrameX(_inputBgImageView, 40);
            SetFrameWidth(_inputView.textView, 176);
            SetFrameX(_inputView.textView, 40);

        } completion:nil];
      
    }else {
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
            _backBtn.alpha = 0;
            _favBtn.alpha = 0;
            _inputView.sendButton.alpha = 1;
            SetFrameWidth(_inputBgImageView, screenWidth - 20 - _inputView.sendButton.frame.size.width);
            SetFrameX(_inputBgImageView, 10);
            SetFrameWidth(_inputView.textView, screenWidth - 20 - _inputView.sendButton.frame.size.width);
            SetFrameX(_inputView.textView, 10);
        } completion:nil];
    }
}

- (void)JTInputViewDidPressSendButton:(JTInputView *)inputView {
        if (JTGlobalCache.isLogin) {
            NSString *comment = [inputView.inputText stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
            [self publishComment:comment];
            
            [inputView clearInput];
        }else {
            [inputView.textView resignFirstResponder];
            [self showRegisterAlert];
        }
}

- (void)JTInputViewDidChangeHeight:(JTInputView *)inputView {

}

- (void)JTInputViewDidEndEdit:(JTInputView *)inputView {
}

- (void)profileSettingViewControllerDidSetProfile:(ProfileSettingViewController *)controller {

}

- (void)profileSettingViewControllerDidCancel:(ProfileSettingViewController *)controller {

}

- (IBAction)viewGoods:(id)sender {
    NSString *buyPageUrl = _tasteItem.buyLink;
    PBWebViewController *webViewController = [[PBWebViewController alloc] init];
    webViewController.URL = [NSURL URLWithString:buyPageUrl];
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)editPost {
    [PublishParamSet sharedInstance].goods = _tasteItem.goods;
    
    PostEditViewController *editController = [self.storyboard instantiateViewControllerWithIdentifier:@"PostEditViewController"];
    editController.editedTasteItem = _tasteItem;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:editController];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

- (void)showReportMenu {
    UIActionSheet *reportSheet = [[UIActionSheet alloc] initWithTitle:@"举报" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"内容与宝贝不符", @"内容不健康", @"有广告嫌疑", @"垃圾信息", nil];
    reportSheet.tag = 200;
    [reportSheet showInView:self.navigationController.view];
}

- (void)queryGoodsInfo {
    [self showHUDViewWithTitle:@""];
    [[YouPinAPI sharedAPI] queryGoodsInfoForTaste:_tasteItem.productId callback:^(NSError *error) {
        [self hideHUDView];
        if (!error) {
            _hasQueryGoodsInfo = YES;
            NSInteger cellIndex = [_contentCellClassArray indexOfObject:[AskPurchaseInfoCell class]];
            [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:cellIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            Alert(@"已经把话带给楼主啦~~", @"楼主更新后，晒小吧会通知你的哦~~", @"我知道啦~~");
        }
    }];
}

- (void)purchaseInfoController:(PurchaseInfoController *)controller didFillGoods:(Goods *)goods {
    _tasteItem.name = goods.title;
    _tasteItem.price = goods.price;
    _tasteItem.buyLink = goods.buylink;
    _tasteItem.source = [NSString stringWithFormat:@"%ld", goods.source];
    [self showHUDViewWithTitle:@""];
    [[PostManager defaultManager] updatePost:_tasteItem callback:^(TasteItem *newTaste, NSError *error) {
        [self hideHUDView];
        if (!error) {
            NSMutableArray *comments = _tasteItem.comments;
            NSInteger commentCount = _tasteItem.commentCount;
            _tasteItem = newTaste;
            _tasteItem.comments = comments;
            _tasteItem.commentCount = commentCount;
            [self showDetailView];
        }
    }];
    
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 100) {
        NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
        if ([title isEqualToString:MORE_ITEM_BUY_PAGE]) {
            [self viewGoods:nil];
        }else if([title isEqualToString:MORE_ITEM_REPORT]) {
            [self showReportMenu];
        }else if([title isEqualToString:MORE_ITEM_DELETE]){
            AlertWithActionsAndTag(500, @"删除宝贝", @"您确定要删除此宝贝吗?", @"取消", @"确定");
        }else if ([title isEqualToString:MORE_ITEM_EDIT]){
            [self editPost];
        }
    }else if(actionSheet.tag == 200) {
        if (buttonIndex != 4) {
            NSString *reportContent = [actionSheet buttonTitleAtIndex:buttonIndex];
            [[YouPinAPI sharedAPI] reportTasteItem:_tasteItem content:reportContent sender:self callback:^(NSError *error) {
                if (!error) {
                    
                }
            }];
        }
    }else if(actionSheet.tag == 500) {
        if (buttonIndex != 4) {
            if(_toActionComment) {
                NSString *reportContent = [actionSheet buttonTitleAtIndex:buttonIndex];
                [[YouPinAPI sharedAPI] reportComment:_toActionComment content:reportContent sender:self callback:^(NSError *error) {
                    
                }];
                
                _toActionComment = nil;
                
            }
            
            [_tableView setEditing:NO animated:YES];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 500 && buttonIndex == 1) {
        [self deleteTasteItem];
    }else if (alertView.tag == 1000) {
        if (buttonIndex == 1) {
            [self presentLoginController];
        }
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self showOrHideInputActionButtons:NO];
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [self showOrHideInputActionButtons:YES];
    if (textView.text.length == 0) {
        _inputView.textView.placeholderText = DEFAULT_INPUT_PLACEHOLDER;
        _isReplyComment = NO;
        _toActionComment = nil;
    }
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (_isReplyComment) {
        return !([textView.text isEqualToString:[NSString stringWithFormat:@"回复%@:", _toActionComment.nick]] && text.length == 0);
    }else {
        return YES;
    }
}

//#pragma mark UICollectionViewDelegate
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return _tasteItem.realShotPics? _tasteItem.realShotPics.count : 0;
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    UICollectionViewCell *thumbnailCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"thumbnailCell" forIndexPath:indexPath];
//    UIImageView *thumbnailView = (UIImageView *)[thumbnailCell viewWithTag:100];
//    [thumbnailView setImageWithURL:_tasteItem.realShotPics[indexPath.row] placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectFade];
//    return thumbnailCell;
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (_inputView.textView.isFirstResponder) {
//        return;
//    }
//    
//    _thumbnailShotView = [[UIImageView alloc] initWithFrame:collectionView.frame];
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    _thumbnailShotView.image = ((UIImageView *)[cell.contentView viewWithTag:100]).image;
//    [_thumbnailContainerView addSubview:_thumbnailShotView];
//    [self viewPhotos:indexPath.row];
//}

#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = [_contentCellClassArray count];
    if (_tasteItem.comments) {
        count += _tasteItem.comments.count;
    }
    
    return count;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < _contentCellClassArray.count) {
        CGFloat height = 0;
        Class cellClass = _contentCellClassArray[indexPath.row];
        if (cellClass == [TextContentCell class]) {
            PostContentItem *item = _tasteItem.contentItems[indexPath.row];
            height = [TextContentCell  heightForText:item.text];
        }else if(cellClass == [TagListCell class]) {
            height = [TagListCell heightForTags:_tasteItem.tags];
        }else if (cellClass == [EditorCommentCell class]) {
            height = [EditorCommentCell heightForText:_tasteItem.editorComment.comment];
        }else {
            height = [cellClass height];
        }
        
        return height;
    }else {
        Comment *comment = _tasteItem.comments[indexPath.row - _contentCellClassArray.count];
        NSString *commentContent = @"";
        if (comment.receiverPhone && comment.receiverNick && comment.receiverPhone.length > 0 && comment.receiverNick.length > 0) {
            commentContent = [NSString stringWithFormat:@"回复%@: %@", comment.receiverNick, comment.comment];
        }else {
            commentContent = comment.comment;
        }
        return [CommentCell heightForComment:commentContent];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > _contentCellClassArray.count - 1) {
        return YES;
    }else {
        return NO;
    }
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return [NSStringFromSelector(action) isEqualToString:@"copy:"];
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    NSString *actionName = NSStringFromSelector(action);
    if ([actionName isEqualToString:@"copy:"]) {
        if (indexPath.row > _contentCellClassArray.count - 1) {
            CommentCell *cell = (CommentCell *)[tableView cellForRowAtIndexPath:indexPath];
            if (cell) {
                [UIPasteboard generalPasteboard].string = cell.contentLabel.attributedText.string;
            }
        }
    }
}
 

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > _contentCellClassArray.count - 1) {
        Comment *comment = _tasteItem.comments[indexPath.row - _contentCellClassArray.count];
        return comment.commentId != nil;
    }else {
        return NO;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > _contentCellClassArray.count - 1) {
        Comment *comment = _tasteItem.comments[indexPath.row - _contentCellClassArray.count];
        return (JTGlobalCache.currentUser && [comment.extraId isEqualToString:JTGlobalCache.currentUser.phone])? @"删除":@"举报";
    }else {
        return @"";
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > _contentCellClassArray.count - 1) {
        Comment *comment = _tasteItem.comments[indexPath.row - _contentCellClassArray.count];
        _toActionComment = comment;
        if(JTGlobalCache.currentUser && [comment.extraId isEqualToString:JTGlobalCache.currentUser.phone]){
            [self deleteComment];
        }else {
            [self showCommentReportMenu];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < _contentCellClassArray.count) {
        Class cellClass = _contentCellClassArray[indexPath.row];
        NSString *methodName = [NSString stringWithFormat:@"%@ForTableView:atIndexPath:", NSStringFromClass(cellClass)];
        PostContentCell *cell = (PostContentCell *)[self performSelector:NSSelectorFromString(methodName) withObject:tableView withObject:indexPath];
        [cell updateLayout];
        
        return cell;
    }else {
        return [self commentCellForTableView:tableView atIndexPath:indexPath];
    }
}

- (TextContentCell *)TextContentCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    TextContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextContentCell" forIndexPath:indexPath];
    PostContentItem *item = _tasteItem.contentItems[indexPath.row];
    [cell setText:item.text];
    return cell;
}

- (ImageContentCell *)ImageContentCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    ImageContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageContentCell" forIndexPath:indexPath];
    PostContentItem *item = _tasteItem.contentItems[indexPath.row];
    [cell.thumnailView setImageWithURL:item.image placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectFade];
    return cell;
}

- (TagListCell *)TagListCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    TagListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TagListCell" forIndexPath:indexPath];
    
    cell.tagView.tagDelegate = self;
    
    NSMutableArray *tagItems = [[NSMutableArray alloc] init];
    for (NSString *tag in _tasteItem.tags) {
        BOOL isSystemTag = [tag hasSuffix:@"-sys"];
        NSString *tagTitle = isSystemTag ? [tag substringToIndex:tag.length - 4] : tag;
        if (!isSystemTag) {
            [tagItems addObject:[NSString stringWithFormat:@"#%@", tagTitle]];
        }else {
            [tagItems addObject:[NSString stringWithFormat:@"#%@", tagTitle]];
        }
    }
    
    [cell.tagView setTags:tagItems];
    return cell;
}

- (GoodsInfoCell *)GoodsInfoCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    GoodsInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsInfoCell" forIndexPath:indexPath];
    cell.titleLabel.text = _tasteItem.name;
    cell.priceLabel.text = [NSString stringWithFormat:@"· ￥%@",_tasteItem.price];
  
    NSString *sourceName = @"";
    if (_tasteItem.goods.source != GoodsSourceOthers) {
        NSString *sourceIcon = @"";
        switch (_tasteItem.goods.source) {
            case GoodsSourceTaobao:
                sourceIcon = @"icon_taobao_b";
                sourceName = @"淘宝";
                break;
            case GoodsSourceJD:
                sourceIcon = @"icon_jingdong_b";
                sourceName = @"京东";
                break;
            case GoodsSourceDangdang:
                sourceIcon = @"icon_dangdang_b";
                sourceName = @"当当";
                break;
            case GoodsSourceAmazonCN:
                sourceIcon = @"icon_amazon_b";
                sourceName = @"亚马逊";
                break;
            case GoodsSourceYiHaoDian:
                sourceIcon = @"icon_1haodian_b";
                sourceName = @"1号店";
                break;
            default:
                break;
        }
        
        cell.iconView.image = [UIImage imageNamed:sourceIcon];
        cell.iconView.hidden = NO;
    }else {
        cell.iconView.hidden = YES;
    }
    
    if ([_tasteItem.goods.buylink hasPrefix:@"http"]) {
        cell.buyLinkLabel.textColor = [UIColor colorWithRed:0.31 green:0.7 blue:0.96 alpha:1];
        cell.buyLinkLabel.text = @"TA提供的购买地址";
        cell.arrowLabel.hidden = NO;
    }else {
        cell.arrowLabel.hidden = YES;
        cell.buyLinkLabel.textColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1];
        
        if (_tasteItem.goods.source != GoodsSourceOthers){
            cell.buyLinkLabel.text = [NSString stringWithFormat:@"%@搜索 “%@”", sourceName, _tasteItem.goods.buylink];
        }else {
            cell.buyLinkLabel.text = _tasteItem.goods.buylink;
        }
    }
    
    return cell;
}

- (AskPurchaseInfoCell *)AskPurchaseInfoCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    AskPurchaseInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AskPurchaseInfoCell" forIndexPath:indexPath];
    cell.arrowView.hidden = _hasQueryGoodsInfo;
    cell.tipLabel.hidden = !_hasQueryGoodsInfo;
    return cell;
}

- (ProvidePurchaseInfoCell *)ProvidePurchaseInfoCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    ProvidePurchaseInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProvidePurchaseInfoCell" forIndexPath:indexPath];
    if (_queryUserList && _queryUserList.count > 0) {
        cell.tipLabel.text = [NSString stringWithFormat:@"已有%ld人询问", _queryUserList.count];
    }else {
        cell.tipLabel.text = @"";
    }
    
    return cell;
}

- (PraiseListCell *)PraiseListCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    PraiseListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PraiseListCell" forIndexPath:indexPath];
    if (!cell.target) {
        cell.target = self;
        cell.avatarSelector = @selector(viewPinUserInfo:);
        [cell.praiseButton addTarget:self action:@selector(praise:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [cell.praiseButton setTitle:_tasteItem.pincount == 0 ? @"赞" : [NSString stringWithFormat:@"%d", _tasteItem.pincount] forState:UIControlStateNormal];
    [cell.praiseButton showHightlighted:_tasteItem.pin animate:NO];

    if (_tasteItem.pinUsers && _tasteItem.pinUsers.count > 0) {
        NSInteger topCount = _tasteItem.pinUsers.count > 5 ? 5 : _tasteItem.pinUsers.count;
        NSArray *topPinUsers = [_tasteItem.pinUsers subarrayWithRange:NSMakeRange(0, topCount)];
        NSArray *avatarURLs = [topPinUsers valueForKeyPath:@"avatar"];
        [cell setAvatars:avatarURLs];
    }
    
    [cell updateLayout];
    
    return cell;
}

- (CommentCountCell *)CommentCountCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    CommentCountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCountCell" forIndexPath:indexPath];
    cell.countLabel.text = [NSString stringWithFormat:@" %d·评论  ", _tasteItem.comments.count];
    return cell;
}

- (EditorCommentCell *)EditorCommentCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    EditorCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditorCommentCell" forIndexPath:indexPath];
    [cell.avatarView setImageWithURL:_tasteItem.editorComment.headImage placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    cell.titleLabel.text = _tasteItem.editorComment.nick;
    cell.contentLabel.text = _tasteItem.editorComment.comment;
    return cell;
}

- (CommentCell *)commentCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell" forIndexPath:indexPath];
    Comment *comment = _tasteItem.comments[indexPath.row - _contentCellClassArray.count];
    
    if (cell.avatarView.tag == 0) {
        [cell.avatarView addTarget:self action:@selector(viewCommentPersonInfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.avatarView.tag = indexPath.row - _contentCellClassArray.count + 1;
    [cell.avatarView setImage:nil forState:UIControlStateNormal];
    if (comment.headImage && comment.headImage.length > 0) {
        [cell.avatarView setImageWithURL: comment.headImage];
    }
    
    NSMutableAttributedString *commentStr = nil;
    
    if (comment.receiverPhone && comment.receiverNick && comment.receiverPhone.length > 0 && comment.receiverNick.length > 0) {
        NSString *responseName = [comment responseDisplayNameWithPosterId:_tasteItem.userId];
        commentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"回复%@: %@", responseName, comment.comment]];
        [commentStr addAttributes:@{NSForegroundColorAttributeName: MAIN_CELL_COMMENT_NAME_COLOR} range:NSMakeRange(2, responseName.length + 1)];
    }else {
        commentStr = [[NSMutableAttributedString alloc] initWithString:comment.comment];
    }
    
    [commentStr addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12], NSForegroundColorAttributeName:MainCellTitleColor} range:NSMakeRange(0, commentStr.length)];
    
    cell.contentLabel.attributedText = commentStr;
    cell.infoLabel.text = [NSString stringWithFormat:@"%@:", [comment displayNameWithPosterId:_tasteItem.userId]];
    cell.timeLabel.text = [[NSDate dateWithTimeIntervalSince1970:comment.createTime/1000] prettyFullString];
    [cell updateLayout];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row >= _contentCellClassArray.count) {
        _isReplyComment = YES;
        Comment *selectedComment = _tasteItem.comments[indexPath.row - _contentCellClassArray.count];
        if (_toActionComment != selectedComment) {
            _toActionComment = selectedComment;
            [self showCommentPublishView:nil];
        }
    }else if(indexPath.row < _tasteItem.contentItems.count) {
        if (_contentCellClassArray[indexPath.row] == [ImageContentCell class]) {
            ImageContentCell *cell = (ImageContentCell *)[_tableView cellForRowAtIndexPath:indexPath];
            PostContentItem *imageItem = _tasteItem.contentItems[indexPath.row];
            if (cell && cell.thumnailView.image) {
                _thumbnailShotView = [[UIImageView alloc] initWithFrame:cell.thumnailView.frame];
                _thumbnailShotView.image = cell.thumnailView.image;
                [cell.contentView addSubview:_thumbnailShotView];
                NSInteger imageIndex = [_tasteItem.realShotPics indexOfObject:imageItem.image];
                [self viewPhotos:imageIndex];
            }
        }
    }else if(_contentCellClassArray[indexPath.row] == [AskPurchaseInfoCell class]){
        if (!_hasQueryGoodsInfo) {
            [self queryGoodsInfo];
        }
    }else if(_contentCellClassArray[indexPath.row] == [ProvidePurchaseInfoCell class]){
        PurchaseInfoController *purchaseInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PurchaseInfoController"];
        purchaseInfoController.delegate = self;
        purchaseInfoController.goods = _tasteItem.goods;
        purchaseInfoController.userList = _queryUserList;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:purchaseInfoController];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }else if(_contentCellClassArray[indexPath.row] == [GoodsInfoCell class]){
        if ([_tasteItem.goods.buylink hasPrefix:@"http"]) {
            BannerViewController *webController = [[BannerViewController alloc] init];
            webController.URL = [NSURL URLWithString:_tasteItem.goods.buylink];
        
            [self.navigationController  pushViewController:webController animated:YES];
        }
    }
}

@end
