//
//  ShaibaCollectionViewController.h
//  youpinwei
//
//  Created by tmy on 15/1/16.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShaibaCollectionViewController : UICollectionViewController
@property (nonatomic) BOOL autoCancelRequestWhenDisappear;

@end
