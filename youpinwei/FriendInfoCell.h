//
//  FriendInfoCell.h
//  youpinwei
//
//  Created by tmy on 15/1/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendInfoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *avatarBtn;
@property (strong, nonatomic) IBOutlet UILabel *nameTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *addBtn;
@property (strong, nonatomic) IBOutlet UILabel *introduceLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;

@property (nonatomic) BOOL showAvatar;

+ (CGFloat)heightWithSubtitleContent:(NSString *)content;

- (void)updateLayout;

@end
