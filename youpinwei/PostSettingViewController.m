//
//  PostSettingViewController.m
//  youpinwei
//
//  Created by tmy on 15/2/2.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PostSettingViewController.h"

@interface PostSettingViewController ()

@end

@implementation PostSettingViewController
{
    UIImageView     *_navLineView;
    Goods       *_goods;
    NSArray     *_tags;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _goods = [PublishParamSet sharedInstance].goods;
        _tags = [PublishParamSet sharedInstance].tags;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _navLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 8)];
    _navLineView.image = [UIImage imageNamed:@"bg_release_right"];
    [self.view addSubview:_navLineView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self.navigationController.navigationBar addSubview:_navLineView];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    [_navLineView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)publish:(id)sender {
    if (!_tags || _tags.count == 0) {
        Alert(@"至少要添加一个标签哦～", @"", @"确定");
    }else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(postSettingViewControllerDidFinish:)]) {
            [self.delegate postSettingViewControllerDidFinish:self];
        }
    }
}

- (void)selectTags {
    TagSelectionViewController *tagSelectionController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagSelectionViewController"];
    tagSelectionController.existTags = _tags;
    tagSelectionController.delegate = self;
    [self.navigationController pushViewController: tagSelectionController animated:YES];
}

- (void)selectGoods {
    PurchaseInfoController *purchaseInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PurchaseInfoController"];
    purchaseInfoController.delegate = self;
    purchaseInfoController.goods = _goods;
    [self.navigationController pushViewController:purchaseInfoController animated:YES];
}


- (void)tagSelectionViewController:(TagSelectionViewController *)controller didSelectTags:(NSArray *)tags {
    _tags = tags;
    [PublishParamSet sharedInstance].tags = _tags;
    [self.tableView reloadData];
}

- (void)purchaseInfoController:(PurchaseInfoController *)controller didFillGoods:(Goods *)goods {
    _goods = goods;
    [PublishParamSet sharedInstance].goods = _goods;
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    if (indexPath.section == 0) {
        if (!_tags) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultLabelCell" forIndexPath:indexPath];
        }else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"LabelCell" forIndexPath:indexPath];
            NSString *tagStr = [_tags componentsJoinedByString:@" #"];
            tagStr = [NSString stringWithFormat:@"#%@", tagStr];
            cell.textLabel.text = tagStr;
        }
    }else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultTaoBaoCell" forIndexPath:indexPath];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self selectTags];
    }else if(indexPath.section == 1){
        [self selectGoods];
    }
}



@end
