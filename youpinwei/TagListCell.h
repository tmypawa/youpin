//
//  TagListCell.h
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "PostContentCell.h"

@interface TagListCell : PostContentCell
@property (strong, nonatomic) IBOutlet DWTagList *tagView;

+ (CGFloat)heightForTags:(NSArray *)tags;

@end
