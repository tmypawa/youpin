//
//  YouPinBaseViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-29.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"
#import "UIImage+ImageEffects.h"
#import "LoginViewController.h"
#import "MMPopLabel.h"

@interface ShaibaBaseViewController ()

@end

@implementation ShaibaBaseViewController
{
    UIButton   *_maskBtn;
    NSMutableArray  *_tutorialPopLabels;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.autoCancelRequestWhenDisappear = YES;
        _tutorialPopLabels = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id)init {
    if (self = [super init]) {
        self.autoCancelRequestWhenDisappear = YES;
        _tutorialPopLabels = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = LIST_BG_COLOR;
    
   self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.autoCancelRequestWhenDisappear) {
        if ([self.parentViewController isKindOfClass:[UINavigationController class]] && ![self.navigationController.viewControllers containsObject:self]) {
            [[YouPinAPI sharedAPI] cancelRequestForSender:self];
        }else if (self.parentViewController && self.parentViewController.navigationController && ![self.parentViewController.navigationController.viewControllers containsObject:self.parentViewController]) {
            [[YouPinAPI sharedAPI] cancelRequestForSender:self];
        }else if(!self.parentViewController){
            [[YouPinAPI sharedAPI] cancelRequestForSender:self];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dimissTutorial:(id)sender {
    for (MMPopLabel *label in _tutorialPopLabels) {
        [label dismiss];
    }
    
    [_tutorialPopLabels removeAllObjects];
    
    if (_maskBtn) {
        [_maskBtn removeFromSuperview];
        _maskBtn = nil;
    }
}

- (void)showTutorialWithAttachedView:(UIView *)attachedView title:(NSString *)title offset:(CGPoint)offset showConfirmButton:(BOOL)showButton {
    MMPopLabel *label = [MMPopLabel popLabelWithText: title];
    label.labelFont = [UIFont systemFontOfSize:13];
    
//    if (showButton) {
//        UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [confirmBtn setTitle:@"我知道了" forState:UIControlStateNormal];
//        [confirmBtn addTarget:self action:@selector(dimissTutorial:) forControlEvents:UIControlEventTouchUpInside];
//        confirmBtn.backgroundColor = [UIColor redColor];
//        [label addButton:confirmBtn];
//    }
    
    [_tutorialPopLabels addObject:label];
    
    [attachedView.superview addSubview:label];
    [attachedView bringSubviewToFront:label];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [label popAtView:attachedView];
        CGPoint center = label.center;
        center.x += offset.x;
        center.y += offset.y;
        label.center = center;
        
        _maskBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _maskBtn.frame = screenFrame;
        [[UIApplication sharedApplication].keyWindow addSubview:_maskBtn];
      
        [_maskBtn addTarget:self action:@selector(dimissTutorial:) forControlEvents:UIControlEventTouchUpInside];
//        if (!showButton) {
//            _maskBtn.userInteractionEnabled = YES;
//            [_maskBtn addTarget:self action:@selector(dimissTutorial:) forControlEvents:UIControlEventTouchUpInside];
//        }else {
//            _maskBtn.userInteractionEnabled = NO;
//        }
    });
}

- (void)showTutorialWithAttachedView:(UIView *)attachedView title:(NSString *)title offset:(CGPoint)offset {
    [self showTutorialWithAttachedView:attachedView title:title offset:offset showConfirmButton:NO];
}


@end
