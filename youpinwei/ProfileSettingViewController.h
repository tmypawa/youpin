//
//  ProfileSettingViewController.h
//  youpinwei
//
//  Created by tmy on 14-9-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileSettingViewControllerDelegate;

@interface ProfileSettingViewController : ShaibaBaseViewController<UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>

@property (nonatomic, weak) id<ProfileSettingViewControllerDelegate> delegate;
@property (nonatomic) BOOL isEditNickname;
@property (nonatomic, strong) User *existUser;

@end

@protocol ProfileSettingViewControllerDelegate <NSObject>

- (void)profileSettingViewController:(ProfileSettingViewController *)controller didSetProfileInfo:(User *)profileInfo;
- (void)profileSettingViewControllerDidCancel:(ProfileSettingViewController *)controller;

@end
