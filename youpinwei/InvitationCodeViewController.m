//
//  InvitationCodeViewController.m
//  youpinwei
//
//  Created by tmy on 14/12/5.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "InvitationCodeViewController.h"
#import "LoginViewController.h"

@interface InvitationCodeViewController ()

@end

@implementation InvitationCodeViewController
{
    
    IBOutlet UITextField *_codeTextFiled;
    IBOutlet UIScrollView *_scrollView;
    IBOutlet UIImageView *_bgImageView;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:237/255.0 green:237/255.0 blue:237/255.0 alpha:255/255.0];
    _codeTextFiled.tintColor = NavigationBarTintColor;
    _bgImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"invitation_bg" ofType:@"png"]];
    _scrollView.contentSize = CGSizeMake(screenWidth, _bgImageView.frame.size.height + 60);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillAppear:(NSNotification *)notification {
    CGRect frame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    SetFrameY(_scrollView,  - frame.size.height);
    [UIView commitAnimations];
}

- (void)keyboardWillDisappear:(NSNotification *)notification {
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    SetFrameY(_scrollView, 0);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirm:(id)sender {
    NSString *code = [_codeTextFiled.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    
    if (code.length == 0) {
        Alert(@"请输入晒吧邀请码", @"", @"确定");
        return;
    }
    
    [_codeTextFiled resignFirstResponder];
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [[YouPinAPI sharedAPI] applyInvitationCode:code sender:self callback:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (!error) {
            LoginViewController *loginController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            loginController.showGuide = YES;
            
            [self.navigationController setViewControllers:@[loginController]];
        }else {
            Alert(@"邀请码无效", @"", @"确定");
        }
    }];
}


@end
