//
//  TagQRViewController.h
//  youpinwei
//
//  Created by tmy on 15/3/24.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"

@interface TagQRViewController : ShaibaBaseViewController<UIActionSheetDelegate>

@property (nonatomic, strong) NSString *QRCodeUrl;

@end
