//
//  GlobalCacheDefine.h
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JTCacheKit.h"

@class User;

JTCache_Value(isReturnUser, IsReturnUser, BOOL)
JTCache_Value(isLogin, IsLogin, BOOL)
JTCache_Value(isBinded, IsBinded, BOOL)
JTCache_Value(isBindedMore, IsBindedMore, BOOL)
JTCache_Value(hasShowAddressBookTip, HasShowAddressBookTip, BOOL)
JTCache_Value(hasDeclinedWithCustomDialog, HasDeclinedWithCustomDialog, BOOL)
JTCache_Value(hasDeclinedWithSystemDialog, HasDeclinedWithSystemDialog, BOOL)
JTCache_Value(hasAllowedSyncAddressBook, HasAllowedSyncAddressBook, BOOL)
JTCache_Value(bannerSwitch, BannerSwitch, BOOL)
JTCache_Value(hotTagSwitch, HotTagSwitch, BOOL)
JTCache_Value(bannerHeight, BannerHeight, float)
JTCache_Value(tagHeight, TagHeight, float)
JTCache_Value(mode, Mode, NSInteger)
JTCache_Value(showInvitation, ShowInvitation, BOOL)
JTCache_Value(publishImageTutorialTimes, PublishImageTutorialTimes, NSInteger)

JTCache_Value(lastAddressBookSyncTime, LastAddressBookSyncTime, NSTimeInterval)
JTCache_Value(lastNotificationUpdateTime, LastNotificationUpdateTime, NSTimeInterval)
JTCache_Value(lastFriendNotificationTime, LastFriendNotificationTime, NSTimeInterval)
JTCache_Value(friendNotificationBadge, FriendNotificationBadge, NSInteger)
JTCache_Value(friendTimelineBadge, FriendTimelineBadge, NSInteger)

JTCache_Object(lastVersion, LastVersion, NSString)
JTCache_Object(currentUser, CurrentUser, User)
JTCache_Object(recentTastes, RecentTastes, NSMutableArray)
JTCache_Object(publicTastes, PublicTastes, NSMutableArray)
JTCache_Object(recommendTags, RecommendTags, NSArray)
JTCache_Object(discoveryRecommendTags, DiscoveryRecommendTags, NSMutableArray)
JTCache_Object(discoveryBanners, DiscoveryBanners, NSMutableArray)
JTCache_Object(goodsList, GoodsList, NSMutableArray)
JTCache_Object(draftBoxList, DraftBoxList, NSMutableArray)
JTCache_Object(publicBanners, PublicBanners, NSArray)
JTCache_Object(lastNotificationList, LastNotificationList, NSMutableArray)
JTCache_Object(notificationList, NotificationList, NSMutableArray)
JTCache_Object(tutorialSet, TutorialSet, NSMutableSet)
JTCache_Object(wxShare, WxShare, NSDictionary)
JTCache_Object(smsShare, SmsShare, NSString)
JTCache_Object(publishPlaceHolders, PublishPlaceHolders, NSArray)
JTCache_Object(tagHistoryList, TagHistoryList, NSMutableArray)
JTCache_Object(discoveryHistoryList, DiscoveryHistoryList, NSMutableArray)
JTCache_Object(friendInvitationCode, FriendInvitationCode, NSString)
JTCache_Object(externalPostId, ExternalPostId, NSString)

//temp cache
JTCache_Value(currentLoginType, CurrentLoginType, int)
JTCache_Object(thirdLoginData, ThirdLoginData, NSDictionary)