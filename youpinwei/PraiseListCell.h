//
//  PraiseListCell.h
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostContentCell.h"

@interface PraiseListCell : PostContentCell
 
@property (strong, nonatomic) IBOutlet UIButton *nameButton;
@property (strong, nonatomic) IBOutlet JTAnimationButton *praiseButton;

@property (nonatomic, weak) id target;
@property (nonatomic) SEL avatarSelector;

- (void)setAvatars:(NSArray *)avatarURLs;

@end
