//
//  GuideViewController.h
//  youpinwei
//
//  Created by tmy on 14/11/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GuideViewControllerDelegate;

@interface GuideViewController : ShaibaBaseViewController<UIScrollViewDelegate>

@property (nonatomic, weak) id<GuideViewControllerDelegate> delegate;

@end

@protocol GuideViewControllerDelegate <NSObject>

- (void)guideViewControllerDidFinishGuiding:(GuideViewController *)controller;

@end