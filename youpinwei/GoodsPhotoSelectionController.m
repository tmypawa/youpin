//
//  GoodsPhotoSelectionController.m
//  youpinwei
//
//  Created by tmy on 14/12/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "GoodsPhotoSelectionController.h"
#import "GoodsPhotoCell.h"
#import "PublishParamSet.h"
#import "PublishViewController.h"

@interface GoodsPhotoSelectionController ()

@end

@implementation GoodsPhotoSelectionController
{
    NSMutableArray  *_selectedIndexSet;
    NSArray        *_photoURLs;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _selectedIndexSet = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"宝贝照片";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"宝贝照片" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[YouPinAPI sharedAPI] fetchMultipleGoodsPicWithItemId:self.itemId sender:self callback:^(NSArray *picURLs, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        if (!error) {
            _photoURLs = picURLs;
        }
        
        [self.collectionView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)next:(id)sender {
    NSArray *selectedPhotoURLs = _selectedIndexSet;
    [PublishParamSet sharedInstance].goodsPhotoURLs = selectedPhotoURLs;
    PublishViewController *publishController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishViewController"];
    [self.navigationController pushViewController:publishController animated:YES];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photoURLs ? _photoURLs.count : 0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"GoodsPhotoHeader" forIndexPath:indexPath];
    }else {
        return nil;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GoodsPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsPhotoCell" forIndexPath:indexPath];
    NSString *photoURL = _photoURLs[indexPath.row];
    
    [cell.photoView setImageWithURL:photoURL effect:SDWebImageEffectFade];
    
    BOOL isSelected = [_selectedIndexSet containsObject:_photoURLs[indexPath.row]];
    cell.selectionView.image = [UIImage imageNamed:isSelected ? @"btn_wishlist_push" : @"btn_wishlist_normal"];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GoodsPhotoCell *cell = (GoodsPhotoCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if ([_selectedIndexSet containsObject:_photoURLs[indexPath.row]]) {
        [_selectedIndexSet removeObject:_photoURLs[indexPath.row]];
        cell.selectionView.image = [UIImage imageNamed:@"btn_wishlist_normal"];
    }else {
        [_selectedIndexSet addObject:_photoURLs[indexPath.row]];
        cell.selectionView.image = [UIImage imageNamed:@"btn_wishlist_push"];
    }
    
    if (_selectedIndexSet.count > 0) {
        self.title = [NSString stringWithFormat:@"%ld/%ld", _selectedIndexSet.count, _photoURLs.count];
    }else {
        self.title = @"宝贝照片";
    }
}


@end
