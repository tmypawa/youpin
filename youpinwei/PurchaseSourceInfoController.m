//
//  PurchaseSourceInfoController.m
//  youpinwei
//
//  Created by tmy on 15/3/25.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PurchaseSourceInfoController.h"
#import "JTFixedTextView.h"

@implementation PurchaseSourceInfoController
{
    
    IBOutlet UIImageView *_iconView;
    IBOutlet UIView *_containerView;
    IBOutlet JTFixedTextView *_inputTextView;
    IBOutlet UIButton *_confirmBtn;
    IBOutlet UIImageView *_inputBgView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    _inputTextView.delegate = self;
    _inputTextView.tintColor = NavigationBarTintColor;
    _inputTextView.placeholderText = @"粘贴链接或输入搜索关键字";
    _containerView.layer.cornerRadius = 4;
    _inputBgView.image = [[UIImage imageNamed:@"bg_inputView"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
    [_confirmBtn setTitleColor:[UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1] forState:UIControlStateNormal];
    _confirmBtn.enabled = NO;
    
    if (self.iconImage) {
        _iconView.image = self.iconImage;
    }
    
    [_inputTextView becomeFirstResponder];
}

- (IBAction)confirm:(id)sender {
    NSString *value = [_inputTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (self.delegate && [self.delegate respondsToSelector:@selector(PurchaseSourceInfoController:didFillInfo:)]) {
        [self.delegate PurchaseSourceInfoController:self didFillInfo:value];
    }
    
    [_inputTextView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)cancel:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(PurchaseSourceInfoControllerDidCancel:)]) {
        [self.delegate PurchaseSourceInfoControllerDidCancel:self];
    }
    
    [_inputTextView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *value = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    _confirmBtn.enabled = (value.length > 0);
    [_confirmBtn setTitleColor:_confirmBtn.enabled ? [UIColor colorWithRed:0.31 green:0.7 blue:0.96 alpha:1] : [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1] forState:UIControlStateNormal];
}

@end
