//
//  DiscoveryBanner.m
//  youpinwei
//
//  Created by tmy on 15/4/1.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "DiscoveryBanner.h"

@implementation DiscoveryBanner

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.backgroundUrl forKey:@"backgroundUrl"];
    [encoder encodeObject:self.htmlUrl forKey:@"htmlUrl"];
    [encoder encodeInteger:self.index forKey:@"index"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.requestId forKey:@"requestId"];
    [encoder encodeInteger:self.type forKey:@"type"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.backgroundUrl = [decoder decodeObjectForKey:@"backgroundUrl"];
        self.htmlUrl = [decoder decodeObjectForKey:@"htmlUrl"];
        self.index = [decoder decodeIntegerForKey:@"index"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.requestId = [decoder decodeObjectForKey:@"requestId"];
        self.type = [decoder decodeIntegerForKey:@"type"];
    }
    return self;
}

@end
