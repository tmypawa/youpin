//
//  FriendInfoCell.m
//  youpinwei
//
//  Created by tmy on 15/1/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "FriendInfoCell.h"
#import "UIKit+RoundImage.h"

@implementation FriendInfoCell

- (void)awakeFromNib {
    [self.avatarBtn makeRound];
    self.avatarBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.addBtn setBackgroundImage:[[UIImage imageNamed:@"btn-add friends-normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 10, 5, 10)] forState:UIControlStateNormal];
    self.showAvatar = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)heightWithSubtitleContent:(NSString *)content {
    CGFloat height = [content boundingRectWithSize:CGSizeMake(191, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:13]} context:nil].size.height;
    height += 27;
    if (height < 50) {
        height = 50;
    }
    
    return height;
}

- (void)updateLayout {
    if (self.subtitleLabel.text.length > 0) {
        SetFrameY(self.subtitleLabel, 27);
        AdjustLabelHeight(self.subtitleLabel);
        SetFrameY(self.nameTitleLabel, 8);
    }else {
        SetFrameY(self.subtitleLabel, 24);
        SetFrameY(self.nameTitleLabel, 15);
    }
    
    if (self.showAvatar) {
        SetFrameX(self.nameTitleLabel, 56);
        SetFrameX(self.subtitleLabel, 56);
    }else {
        SetFrameX(self.nameTitleLabel, 15);
        SetFrameX(self.subtitleLabel, 15);
    }
}

@end
