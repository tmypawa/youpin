//
//  PhotoEditViewController.m
//  youpinwei
//
//  Created by tmy on 14-9-16.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PhotoFilterViewController.h"
#import "PhotoEditViewController.h"
#import "PhotoEffectViewCell.h"
#import "GPUImage.h"
#import "UIImage+Utility.h"

@interface PhotoFilterViewController ()

@end

@implementation PhotoFilterViewController
{
    __weak IBOutlet UIButton *_filterBtn;
    __weak IBOutlet UIButton *_microspurBtn;
    __weak IBOutlet UICollectionView *_effectsCollectionView;
    __weak IBOutlet UIView *_indicatorView;
    UIImage         *_originalImage;
    GPUImageView    *_photoView;
    GPUImagePicture *_editedImage;
    NSArray     *_filters;
    NSArray     *_filterNames;
    NSArray     *_microspurs;
    NSArray     *_microspurNames;
    GPUImageOutput  *_finalFilter;
    NSMutableArray  *_appliedFilters;
    int         _selectEffectType;
    NSIndexPath     *_selectedIndexPath;
    int             _randomEffectExampleIndex;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        _selectEffectType = 0;
        _selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        _appliedFilters = [[NSMutableArray alloc] initWithArray:@[[NSNull null], [NSNull null]]];
        _randomEffectExampleIndex = arc4random() % 4 + 1;
        [self initFilterList];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    _originalImage = [self cropImage:self.photo];

    self.view.backgroundColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.16 alpha:1];
    _effectsCollectionView.backgroundColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.16 alpha:1];
    [_filterBtn setTitleColor:LoginNavigationBarTintColor forState:UIControlStateNormal];
    
    _photoView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    [self.view addSubview:_photoView];
    
    _editedImage = [[GPUImagePicture alloc] initWithImage:_originalImage smoothlyScaleOutput:YES];

    [_editedImage addTarget:_photoView];
 
    
    [_editedImage processImage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)cropImage:(UIImage *)originImage {
    float fixedSize = 720;
    float zoomSize = originImage.size.height/originImage.size.width * fixedSize;
    UIImage *cropImage = [originImage zoomImageWithSize:CGSizeMake(zoomSize, zoomSize)];
    cropImage = [cropImage clipImageWithRect:CGRectMake(0, 84 * 2.25, fixedSize, fixedSize)];
    return cropImage;
}

- (void)initFilterList {
    _filters = @[[GPUImageAmatorkaFilter class], [GPUImageMissEtikateFilter class], [GPUImageSoftEleganceFilter class]];
    _microspurs = @[[GPUImageTiltShiftFilter class], [GPUImageGaussianSelectiveBlurFilter class]];
    _filterNames = @[@"无", @"", @"", @""];
    _microspurNames = @[@"无", @"平行", @"径向"];
}

- (GPUImageOutput *)filterWithFilterClass:(Class)filterClass {
    GPUImageOutput *filter = [[filterClass alloc] init];
    
    if (filterClass == [GPUImageTiltShiftFilter class]) {
        [(GPUImageTiltShiftFilter *)filter setTopFocusLevel:0.4];
        [(GPUImageTiltShiftFilter *)filter setBottomFocusLevel:0.6];
        [(GPUImageTiltShiftFilter *)filter setFocusFallOffRate:0.2];
    }else if(filterClass == [GPUImageGaussianSelectiveBlurFilter class]){
        [(GPUImageGaussianSelectiveBlurFilter *)filter setExcludeCircleRadius:40.0/320.0];
    }
    
    return filter;
}

- (void)generateActiveFilterChain {
    [_editedImage removeAllTargets];
    NSMutableArray *activeFilters = [[NSMutableArray alloc] init];
    for (id filterClass in _appliedFilters) {
        if (filterClass != [NSNull null]) {
            GPUImageOutput *filter = [self filterWithFilterClass:filterClass];
            [filter useNextFrameForImageCapture];
            [activeFilters addObject:filter];
        }
    }
    
    if (activeFilters.count > 0) {
        [_editedImage removeTarget:_photoView];
    }else {
        [_editedImage useNextFrameForImageCapture];
        [_editedImage addTarget:_photoView];
        _finalFilter = nil;
    }
    
    for (int i = 0; i < activeFilters.count; i++) {
        if (i == 0) {
            [_editedImage addTarget:activeFilters[i]];
        }
        
        if(i < activeFilters.count - 1) {
            [(GPUImageOutput *)activeFilters[i] addTarget:activeFilters[i + 1]];
        }
        
        if(i == activeFilters.count - 1) {
            _finalFilter = activeFilters[i];
            [_finalFilter addTarget:_photoView];
        }
    }
}

- (IBAction)changeEffectsType:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        if (sender == _filterBtn) {
            _selectEffectType = 0;
            [_filterBtn setTitleColor:LoginNavigationBarTintColor forState:UIControlStateNormal];
            _filterBtn.enabled = NO;
            [_microspurBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _microspurBtn.enabled = YES;
            CGRect indicatorFrame = _indicatorView.frame;
            indicatorFrame.origin.x = 4;
            _indicatorView.frame = indicatorFrame;
        }else {
            _selectEffectType = 1;
            [_filterBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _filterBtn.enabled = YES;
            [_microspurBtn setTitleColor:LoginNavigationBarTintColor forState:UIControlStateNormal];
            _microspurBtn.enabled = NO;
            CGRect indicatorFrame = _indicatorView.frame;
            indicatorFrame.origin.x = 60;
            _indicatorView.frame = indicatorFrame;
        }
    }];
    
    _selectedIndexPath = nil;
    [_effectsCollectionView reloadData];
}

- (IBAction)back:(id)sender {
    if (self.navigationController.viewControllers[0] == self) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)next:(id)sender {
    
    UIImage *finalImage = [self fetchEditedImage];
    
    PhotoEditViewController *watermarkController = [self.storyboard instantiateViewControllerWithIdentifier:@"WatermarkViewController"];
    watermarkController.photo = finalImage;
    [self.navigationController pushViewController:watermarkController animated:YES];
 
}

- (UIImage *)fetchEditedImage {
    if (_finalFilter) {
        return [_finalFilter imageFromCurrentFramebuffer];
    }else {
        
        return _originalImage;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _selectEffectType == 0 ? _filters.count + 1 : _microspurs.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotoEffectViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoEffectViewCell" forIndexPath:indexPath];
    
    id filterClass = _appliedFilters[_selectEffectType];
    int selectedIndex = 0;
    if (filterClass != [NSNull null]) {
        selectedIndex = _selectEffectType == 0 ?[_filters indexOfObject:filterClass] : [_microspurs indexOfObject:filterClass];
        selectedIndex ++;
    }
    
    if (indexPath.row == selectedIndex) {
        _selectedIndexPath = indexPath; 
        [cell setSelectStatus:YES];
    }else {
        [cell setSelectStatus:NO];
    }
    
    if (_selectEffectType == 0) {
        if (indexPath.row == 0) {
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", _randomEffectExampleIndex]];
        }else {
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d_0_%d", _randomEffectExampleIndex, indexPath.row]];
        }
        
        cell.titleLabel.text = _filterNames[indexPath.row];
    }else {
        cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"microspur_%d", indexPath.row]];
        cell.titleLabel.text = _microspurNames[indexPath.row];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_selectedIndexPath) {
        PhotoEffectViewCell *previousCell = (PhotoEffectViewCell *)[collectionView cellForItemAtIndexPath:_selectedIndexPath];
        if (previousCell) {
            [previousCell setSelectStatus:NO];
        }
    }
    
    _selectedIndexPath = indexPath;
    PhotoEffectViewCell *selectedCell = (PhotoEffectViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [selectedCell setSelectStatus:YES];
    
    if (indexPath.row == 0) {
        id toCancelFilter = _appliedFilters[_selectEffectType];
        if (toCancelFilter != [NSNull null]) {
            _appliedFilters[_selectEffectType] = [NSNull null];
        }
    }else {
        Class filterClass = _selectEffectType == 0 ? _filters[indexPath.row - 1] : _microspurs[indexPath.row - 1];
        _appliedFilters[_selectEffectType] = filterClass;
    }
    
    [self generateActiveFilterChain];

    [_editedImage processImage];
}

@end
