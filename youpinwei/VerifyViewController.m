//
//  VerifyViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "VerifyViewController.h"
#import "FriendAddingViewController.h"
#import "CategoryChooseViewController.h"

#define CHECK_TIME 60

@implementation VerifyViewController
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UITextField *_verifyCodeField;
    __weak IBOutlet UIButton *_resendBtn;
    
    __weak IBOutlet UIButton *_confirmBtn;
    __weak NSTimer *_resendTimer;
    IBOutlet UILabel *_phoneLabel;
    int             _time;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.tintColor = GlobalTintColor;
    
    _verifyCodeField.textColor = GlobalTintColor;
    _verifyCodeField.tintColor = GlobalTintColor;
    [_verifyCodeField setValue:GlobalTintColor
               forKeyPath:@"_placeholderLabel.textColor"];
    
    _phoneLabel.text = self.phone;
    [_verifyCodeField becomeFirstResponder];
    
    [self beginTimeCheck];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self adjustForiPhone4];
}

- (void)adjustForiPhone4 {
    if (isIPhone4) {
        [UIView animateWithDuration:0.3 animations:^{
            self.view.transform = CGAffineTransformMakeTranslation(0, -60);
        }];
    }
}

- (void)beginTimeCheck {
    _time = 0;
    _resendTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeCheck) userInfo:nil repeats:YES];
}

- (void)timeCheck {
    _time ++ ;
    [_resendBtn setTitle:[NSString stringWithFormat:@"%d秒", 60 - _time] forState:UIControlStateNormal];
    
    if (_time == CHECK_TIME) {
        [_resendTimer invalidate];
        _resendTimer = nil;
        [_resendBtn setTitle:@"重发" forState:UIControlStateNormal];
    }
}

- (IBAction)resendCode:(id)sender {
    if (_time < CHECK_TIME)  return;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[YouPinAPI sharedAPI] registerWithPhone:self.phone loginType:JTGlobalCache.currentLoginType sender:self callback:^(NSError *error) {
        if(!error){
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [self beginTimeCheck];
        }else {
            Alert(NSLocalizedString(@"注册失败", nil), error.localizedDescription, NSLocalizedString(@"确定", nil));
        }
    }];
    
}

- (IBAction)submitCode:(id)sender {
    //submit code
    if(!_verifyCodeField.text || _verifyCodeField.text.length == 0){
        Alert(@"提示", @"请输入验证码", @"确定");
        return;
    }
    
    [_verifyCodeField resignFirstResponder];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[YouPinAPI sharedAPI] verifyCode:_verifyCodeField.text phone:self.phone sender:self callback:^(User *existUser, NSArray *knownFriendList, NSError *error) {
        if (!error) {
            if (![existUser checkValid]) {
                if (JTGlobalCache.currentLoginType == LoginTypeWeixin && JTGlobalCache.thirdLoginData) {
                    int gender = [JTGlobalCache.thirdLoginData[@"gender"] intValue];
                    NSString *location = JTGlobalCache.thirdLoginData[@"location"];
                    NSString *openid = JTGlobalCache.thirdLoginData[@"openid"];
                    NSString *headUrl = JTGlobalCache.thirdLoginData[@"profile_image_url"];
                    NSString *nickname = JTGlobalCache.thirdLoginData[@"screen_name"];
                    
                    [[YouPinAPI sharedAPI] setProfileWithNickname:nickname gender:gender avatar:nil phone:self.phone headUrl:headUrl location:location userOpenId:openid sender:self callback:^(User *userInfo, NSError *error) {
                        if (!error) {
                            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                            [[YouPin sharedInstance] loginWithUser:userInfo];

//                            if (knownFriendList && knownFriendList.count > 0) {
//                                [self goToFriendsViewWithFriendList:knownFriendList];
//                            }else {
//                                [self finishLoginFlow];
//                            }
                            [self goToChooseCategoryView];
                        }
                    }];
                }else {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [self goToProfileViewWithExistUser:existUser];
                }
            }else {
                [[YouPin sharedInstance] loginWithUser:existUser];
//                if (knownFriendList && knownFriendList.count > 0) {
//                    [self goToFriendsViewWithFriendList:knownFriendList];
//                }else {
//                    [self finishLoginFlow];
//                }
                [self goToChooseCategoryView];
            }
        }else {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            _verifyCodeField.text = @"";
            if (error.code == 1000009) {
                Alert(@"提示", @"验证码不正确", @"确定");
            }else {
                Alert(error.localizedDescription, @"", @"确定");
            }
        }
    }];
}

- (void)goToChooseCategoryView {
    CategoryChooseViewController *categoryChooseController = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryChooseViewController"];
    [self.navigationController pushViewController:categoryChooseController animated:YES];
}

- (void)goToFriendsViewWithFriendList:(NSArray *)friendList {
    FriendAddingViewController *friendAddingController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendAddingViewController"];
    friendAddingController.knownFriendList = friendList;
    friendAddingController.showBackButton = NO;
    [self.navigationController pushViewController:friendAddingController animated:YES];
}

- (void)finishLoginFlow {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationUserDidLogin object:nil userInfo:nil];
    }];
}

- (void)goToProfileViewWithExistUser:(User *)existUser {
    ProfileSettingViewController *profileController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSettingViewController"];
    profileController.delegate = self;
    profileController.existUser = existUser;
    [self.navigationController pushViewController:profileController animated:YES];
}
 

@end
