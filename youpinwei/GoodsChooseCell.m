//
//  GoodsChooseCell.m
//  youpinwei
//
//  Created by tmy on 14-8-22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "GoodsChooseCell.h"

@implementation GoodsChooseCell

- (void)awakeFromNib {
    self.titleLabel.textColor = GoodsCellTitleColor;
    self.headerView.backgroundColor = DEFAULT_IMAGE_BG_COLOR;
    self.headerView.clipsToBounds = YES;
    self._timeLabel.textColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
    self.backgroundColor = [UIColor clearColor];
}

- (void)updateTimelineViewWithTimeShowing:(BOOL)isTimeShow {
    if (isTimeShow) {
        self._timeLabel.hidden = NO;
        SetFrameHeight(self.timelineCircleView, 39);
    }else {
        self._timeLabel.hidden = YES;
        SetFrameHeight(self.timelineCircleView, 22);
    }
}
 
@end
