//
//  GoodsChooseController.h
//  youpinwei
//
//  Created by tmy on 14-8-22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoSelectionController.h"
#import "CameraViewController.h"
#import "TaobaoURLProtocol.h"

@protocol GoodsChooseControllerDelegate;

@interface GoodsChooseController : UIViewController<UITableViewDataSource, UITableViewDelegate, PhotoSelectionControllerDelegate, UIWebViewDelegate, TaobaoURLProtocolDelegate, CameraViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) id<GoodsChooseControllerDelegate> delegate;
@property (nonatomic) BOOL isPublishEntrance;

@end


@protocol GoodsChooseControllerDelegate <NSObject>

- (void)goodsChooseController:(GoodsChooseController *)controller didSelectGoods:(Goods *)goods;

@end
