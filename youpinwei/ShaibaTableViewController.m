//
//  ShaibaTableViewController.m
//  youpinwei
//
//  Created by tmy on 15/1/16.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaTableViewController.h"

@implementation ShaibaTableViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.autoCancelRequestWhenDisappear = YES;
    }
    
    return self;
}

- (id)init {
    if (self = [super init]) {
        self.autoCancelRequestWhenDisappear = YES;
    }
    
    return self;
} 

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = LIST_BG_COLOR;
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.autoCancelRequestWhenDisappear) {
        if ([self.parentViewController isKindOfClass:[UINavigationController class]] && ![self.navigationController.viewControllers containsObject:self]) {
            [[YouPinAPI sharedAPI] cancelRequestForSender:self];
        }else if (self.parentViewController && self.parentViewController.navigationController && ![self.parentViewController.navigationController.viewControllers containsObject:self.parentViewController]) {
            [[YouPinAPI sharedAPI] cancelRequestForSender:self];
        }else if(!self.parentViewController){
            [[YouPinAPI sharedAPI] cancelRequestForSender:self];
        }
    }
}

@end
