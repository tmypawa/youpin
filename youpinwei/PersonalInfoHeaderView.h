//
//  PersonalInfoHeaderView.h
//  youpinwei
//
//  Created by tmy on 14/12/9.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "M13BadgeView.h"

@interface PersonalInfoHeaderView : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UIButton *avatarView;
@property (strong, nonatomic) IBOutlet UIButton *nicknameBtn;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIView *infoContainerView;
@property (strong, nonatomic) IBOutlet UILabel *notificationCountLabel;
@property (strong, nonatomic) IBOutlet UIView *totalCountView;
@property (strong, nonatomic) IBOutlet UILabel *totalCountLabel;
@property (strong, nonatomic) IBOutlet UIImageView *sexIconView;
@property (strong, nonatomic) IBOutlet UILabel *pointsLabel;
@property (strong, nonatomic) IBOutlet UIView *levelView;
@property (strong, nonatomic) IBOutlet UIView *leverProgressView;
@property (strong, nonatomic) IBOutlet UIImageView *wishListThumbnailView;
@property (strong, nonatomic) IBOutlet UIButton *addFriendButton;
@property (strong, nonatomic) IBOutlet UIView *friendStatusView;
@property (strong, nonatomic) IBOutlet UIImageView *friendStatusIconView;
@property (strong, nonatomic) IBOutlet UILabel *friendStatusLabel;
@property (strong, nonatomic) IBOutlet M13BadgeView *friendBadgeView;
@property (strong, nonatomic) IBOutlet M13BadgeView *notificationBadgeView;

- (void)updateLayout;
- (void)showLevelProgress:(CGFloat)progress currentLevel:(NSInteger)currentLevel nextLevel:(NSInteger)nextLevel;

@end
