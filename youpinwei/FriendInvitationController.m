//
//  FriendInvitationController.m
//  youpinwei
//
//  Created by tmy on 15/1/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "FriendInvitationController.h"

@interface FriendInvitationController ()

@end

@implementation FriendInvitationController
{
    IBOutlet UITextField *_invitationContentField;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _invitationContentField.tintColor = NavigationBarTintColor;
    if (JTGlobalCache.isLogin) {
        _invitationContentField.text = [NSString stringWithFormat:@"我是%@", JTGlobalCache.currentUser.nickname];
    }
    
    [_invitationContentField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)send:(id)sender {
    NSString *invitationContent = [_invitationContentField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (invitationContent.length == 0) {
        Alert(@"请填写验证信息", @"", @"确定");
    }else {
        [_invitationContentField resignFirstResponder];
        MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[YouPinAPI sharedAPI] sendFriendInvitationToUser:self.userId introduce:invitationContent callback:^(NSError *error) {
            if (!error) {
                [hudView showResultWithText:@"已发送" imageName:@"icon-correct-Add friends"];
                if (self.delegate && [self.delegate respondsToSelector:@selector(friendInvitationControllerDidSendInvitation:)]) {
                    [self.delegate friendInvitationControllerDidSendInvitation:self];
                }
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else {
                [hudView showResultWithText:@"发送失败" imageName:@"icon-error-add friends"];
            }
        }];
    }
}



@end
