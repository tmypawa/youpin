//
//  PublishPhotoBrowerController.h
//  youpinwei
//
//  Created by tmy on 14/12/22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "JTImageBrowerController.h"
#import "JTImageZoomAnimationController.h"

@protocol PublishPhotoBrowerControllerDelegate;

@interface PublishPhotoBrowerController : JTImageBrowerController
@property (nonatomic, weak) id<PublishPhotoBrowerControllerDelegate> publishPhotoDelegate;

@end

@protocol PublishPhotoBrowerControllerDelegate <NSObject>

- (void)publishPhotoBrowerController:(PublishPhotoBrowerController *)controller didDeletePhotoItemAtIndex:(NSInteger)itemIndex;

@end