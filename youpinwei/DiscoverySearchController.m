//
//  DiscoverySearchController.m
//  youpinwei
//
//  Created by tmy on 15/3/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "DiscoverySearchController.h"
#import "TagViewController.h"
#import "PostSearchResultController.h"

@implementation DiscoverySearchController
{
    IBOutlet UIImageView *_searchBgView;
    
    IBOutlet UITextField *_searchTextField;
    IBOutlet UIButton *_deleteBtn;
    IBOutlet UITableView *_searchTableView;
    IBOutlet UIView *_footerView;
    BOOL       _hasInput;
    NSArray     *_relatedTagsList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _searchBgView.image = [[UIImage imageNamed:@"bg_search"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
    _searchTextField.tintColor = NavigationBarTintColor;
    [_searchTextField becomeFirstResponder];
    
    if (JTGlobalCache.discoveryHistoryList.count == 0) {
        [_searchTableView setTableFooterView:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeText:) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

- (IBAction)cancel:(id)sender {
    [_searchTextField resignFirstResponder];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(discoverySearchControllerDidCancel:)]) {
        [self.delegate discoverySearchControllerDidCancel:self];
    }
}

- (IBAction)deleteInput:(id)sender {
    _searchTextField.text = @"";
    _hasInput = NO;
    [self showHistory];
}

- (IBAction)deleteHistory:(id)sender {
    [JTGlobalCache.discoveryHistoryList removeAllObjects];
    [self showHistory];
}

- (void)showSearchResultViewWithKeyword:(NSString *)keyword {
    PostSearchResultController *searchResultController = [self.storyboard instantiateViewControllerWithIdentifier:@"PostSearchResultController"];
    searchResultController.keyword = keyword;
    [self.navigationController pushViewController:searchResultController animated:YES];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)refreshRelatedTagsWithInputValue:(NSString *)inputValue {
    [[YouPinAPI sharedAPI] cancelRequestForSender:self];
    [[YouPinAPI sharedAPI] queryTagsWithKeyword:inputValue sender:self callback:^(NSArray *tags, NSError *error) {
        if (!error) {
            _relatedTagsList = tags;
            if (_hasInput) {
                [_searchTableView reloadData];
            }
        }
    }];
}

- (void)goToTagViewWithTag:(NSString *)tag {
    TagViewController *tagController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
    tagController.tagName = tag;
    [self.navigationController pushViewController:tagController animated:YES];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)showHistory {
    [_searchTableView reloadData];
    if (JTGlobalCache.discoveryHistoryList.count > 0) {
        [_searchTableView setTableFooterView:_footerView];
    }else {
        [_searchTableView setTableFooterView:nil];
    }
}

- (void)textFieldDidChangeText:(NSNotification *)notification {
    NSString *textValue = [_searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @" "]];
    if (NotEmptyValue(textValue)) {
        NSString *currentInputName = [_searchTextField textInputMode].primaryLanguage;
        BOOL isChineseInput = [currentInputName hasPrefix:@"zh"];
        if(!isChineseInput){
            [self refreshRelatedTagsWithInputValue:textValue];
        }else {
            UITextRange *range = _searchTextField.markedTextRange;
            if (!range || range.isEmpty) {
                [self refreshRelatedTagsWithInputValue:textValue];
            }
        }
    }else {
        [self showHistory];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
    _hasInput = NotEmptyValue(value);
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *textValue = [_searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @" "]];
    if (NotEmptyValue(textValue)) {
        [JTGlobalCache.discoveryHistoryList insertObject:textValue atIndex:0];
        if (JTGlobalCache.discoveryHistoryList.count > MAX_TAG_HISTORY_SIZE) {
            [JTGlobalCache.discoveryHistoryList removeLastObject];
        }
        
        [self showSearchResultViewWithKeyword:textValue];
    }
    
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!_hasInput) {
        return  JTGlobalCache.discoveryHistoryList ? JTGlobalCache.discoveryHistoryList.count : 0;
    }else {
        return _relatedTagsList ? _relatedTagsList.count : 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DiscoverySearchCell" forIndexPath:indexPath];
    if (cell.tag == 0) {
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
    }
    
    cell.tag = indexPath.row + 1;
    
    if (_hasInput && _relatedTagsList) {
        cell.textLabel.text = _relatedTagsList[indexPath.row];
    }else if(JTGlobalCache.discoveryHistoryList){
        cell.textLabel.text = JTGlobalCache.discoveryHistoryList[indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_hasInput && _relatedTagsList) {
        NSString *tag = _relatedTagsList[indexPath.row];
        [self goToTagViewWithTag:tag];
    }else {
        NSString *historyValue = JTGlobalCache.discoveryHistoryList[indexPath.row];
        [self showSearchResultViewWithKeyword:historyValue];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
