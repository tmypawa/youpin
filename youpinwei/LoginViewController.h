//
//  LoginViewController.h
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GuideViewController.h"

@interface LoginViewController : LoginBaseViewController<GuideViewControllerDelegate>

@property (nonatomic)  BOOL showGuide;
@property (nonatomic) BOOL showLogoAnimation;

- (void)hideLoginView;

@end
