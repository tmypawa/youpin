//
//  PublishTextItem.h
//  youpinwei
//
//  Created by tmy on 15/1/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishItem.h"

@interface PublishTextItem : PublishItem

@property (nonatomic, strong) NSString *text;

@end
