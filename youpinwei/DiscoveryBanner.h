//
//  DiscoveryBanner.h
//  youpinwei
//
//  Created by tmy on 15/4/1.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiscoveryBanner : NSObject

@property (nonatomic, strong) NSString *backgroundUrl;
@property (nonatomic, strong) NSString *htmlUrl;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *requestId;
@property (nonatomic, assign) DiscoveryBannerType type;

@end
