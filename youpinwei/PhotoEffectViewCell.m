//
//  PhotoEffectViewCell.m
//  youpinwei
//
//  Created by tmy on 14-9-16.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PhotoEffectViewCell.h"

@implementation PhotoEffectViewCell

- (void)awakeFromNib {
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)setSelectStatus:(BOOL)isSelect {
    if (isSelect) {
        self.imageView.layer.borderColor = LoginNavigationBarTintColor.CGColor;
        self.imageView.layer.borderWidth = 2;
        self.titleLabel.textColor = [UIColor whiteColor];
    }else {
        self.imageView.layer.borderWidth = 0;
        self.titleLabel.textColor = [UIColor whiteColor];
    }
}

@end
