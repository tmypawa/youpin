//
//  DetailCells.h
//  youpinwei
//
//  Created by tmy on 15/2/5.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#ifndef youpinwei_DetailCells_h
#define youpinwei_DetailCells_h

#import "CommentCell.h"
#import "TextContentCell.h"
#import "ImageContentCell.h"
#import "TagListCell.h"
#import "GoodsInfoCell.h"
#import "PraiseListCell.h"
#import "CommentCountCell.h"
#import "EditorCommentCell.h"
#import "PostContentCell.h"

#endif
