//
//  RecommendTagItem.m
//  youpinwei
//
//  Created by tmy on 15/3/27.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "RecommendTagItem.h"

@implementation RecommendTagItem

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.productId forKey:@"productId"];
    [encoder encodeObject:self.thumbnail forKey:@"thumbnail"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.productId = [decoder decodeObjectForKey:@"productId"];
        self.thumbnail = [decoder decodeObjectForKey:@"thumbnail"];
    }
    return self;
}

@end
