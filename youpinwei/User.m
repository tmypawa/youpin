//
//  User.m
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "User.h"
#import "JTAddressBookManager.h"

@implementation User

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeBool:self.hasProfile forKey:@"hasProfile"];
    [encoder encodeObject:self.avatar forKey:@"avatar"];
    [encoder encodeObject:self.nickname forKey:@"nickname"];
    [encoder encodeInteger:self.gender forKey:@"gender"];
    [encoder encodeInteger:self.type forKey:@"type"];
    [encoder encodeInteger:self.points forKey:@"points"];
    [encoder encodeInteger:self.degree forKey:@"degree"];
    [encoder encodeDouble:self.opUpdateTime forKey:@"opUpdateTime"];
    [encoder encodeInteger:self.relation forKey:@"relation"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
        self.hasProfile = [decoder decodeBoolForKey:@"hasProfile"];
        self.avatar = [decoder decodeObjectForKey:@"avatar"];
        self.nickname = [decoder decodeObjectForKey:@"nickname"];
        self.gender = [decoder decodeIntegerForKey:@"gender"];
        self.type = [decoder decodeIntegerForKey:@"type"];
        self.points = [decoder decodeIntegerForKey:@"points"];
        self.degree = [decoder decodeIntegerForKey:@"degree"];
        self.opUpdateTime = [decoder decodeDoubleForKey:@"opUpdateTime"];
        self.relation = [decoder decodeIntegerForKey:@"relation"];
    }
    return self;
}

- (NSString *)displayName{
    NSString *displayName = @"";
    if (JTGlobalCache.currentUser && [JTGlobalCache.currentUser.userId isEqualToString:self.userId]) {
        displayName = self.nickname;
    }else {
        if (self.relation == UserRelationTypeMyFriend || self.relation == UserRelationTypeFriend) {
            JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:self.phone];
            if (person) {
                displayName = person.fullName;
            }else {
                displayName = self.nickname;
            }
        }else {
            displayName = self.nickname;
        }
    }
    
    return displayName;
}

- (BOOL)checkValid {
    return ![SafeValue(self.avatar) isEqualToString:@""] && ![SafeValue(self.nickname) isEqualToString:@""];
}

@end
