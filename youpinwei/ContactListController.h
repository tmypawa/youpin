//
//  ContactListController.h
//  youpinwei
//
//  Created by tmy on 15/3/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaTableViewController.h"
@import MessageUI;

@interface ContactListController : ShaibaTableViewController<MFMessageComposeViewControllerDelegate>

@end
