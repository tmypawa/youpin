//
//  PraiseViewCell.m
//  youpinwei
//
//  Created by tmy on 14/12/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PraiseViewCell.h"
#import "UIKit+RoundImage.h"

@implementation PraiseViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.separatorInset = UIEdgeInsetsZero;
    [self.avatarView makeRound];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

- (void)updateLayout {
    [self.nicknameLabel sizeToFit];
    SetFrameX(self.levelconView, CGRectGetMaxX(self.nicknameLabel.frame) + 5);
    self.levelconView.center = CGPointMake(self.levelconView.center.x, self.nicknameLabel.center.y);
    self.infoLabel.center = CGPointMake(self.infoLabel.center.x, self.nicknameLabel.center.y);
}

@end
