//
//  NotificationViewController.m
//  youpinwei
//
//  Created by tmy on 14/10/22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "NotificationViewController.h"
#import "FriendInfoViewController.h"
#import "DetailViewController.h"
#import "NotificationCell.h"

#define UNREAD_DURATION 600

@interface NotificationViewController ()

@end

@implementation NotificationViewController
{
    NSMutableArray  *_notificationList;
    UIRefreshControl *_refreshControl;
    NSTimeInterval   _showTime;
    NSTimeInterval   _latestNotificationTime;
    IBOutlet UIView *_loadMoreFooterView;
    IBOutlet UIButton *_loadMoreBtn;
    IBOutlet UIActivityIndicatorView *_loadMoreIndicator;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _notificationList = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_loadMoreIndicator stopAnimating];
    _loadMoreIndicator.hidden = YES;
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)]];
    
    [self refreshDataWithLastTimestamp:JTGlobalCache.lastNotificationUpdateTime];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_showTime == 0) {
        _showTime = [[NSDate date] timeIntervalSince1970];
    }else {
        NSTimeInterval duration = [[NSDate date] timeIntervalSince1970] - _showTime;
        if (duration >= UNREAD_DURATION) {
            if (_notificationList && _notificationList.count > 0) {
                _latestNotificationTime = [_notificationList[0] createTime]/1000;
            }
            
            [self.tableView reloadData];
        }
    }
}
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refresh:(id)sender {
    [self refreshDataWithLastTimestamp:JTGlobalCache.lastNotificationUpdateTime];
}

- (IBAction)clear:(id)sender {
    [JTGlobalCache.notificationList removeAllObjects];
    [JTGlobalCache.lastNotificationList removeAllObjects];
    [JTGlobalCache synchronize];
    
    [_notificationList removeAllObjects];
    [self.tableView reloadData];
}
- (IBAction)loadMore:(id)sender {
    _loadMoreBtn.hidden = YES;
    [_notificationList removeAllObjects];
    [_notificationList addObjectsFromArray:JTGlobalCache.lastNotificationList];
    [_notificationList addObjectsFromArray:JTGlobalCache.notificationList];
    [self.tableView reloadData];
}

- (void)refreshDataWithLastTimestamp:(NSTimeInterval )lastTimestamp {
    if (_notificationList.count == 0 && !_refreshControl.isRefreshing) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    [[YouPinAPI sharedAPI] fetchNotificationListWithUpdateTime:lastTimestamp sender:self callback:^(NSArray *notificationList, NSInteger notificationCount, NSError *error) {
        if (_refreshControl.isRefreshing) {
            [_refreshControl endRefreshing];
        }else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
        if (!error) {
            [_notificationList removeAllObjects];
            if (notificationList.count > 0) {
                JTGlobalCache.lastNotificationUpdateTime = [notificationList[0] updateTime];
                
                if (JTGlobalCache.lastNotificationList.count > 0) {
                    [JTGlobalCache.notificationList insertObjects:JTGlobalCache.lastNotificationList atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, JTGlobalCache.lastNotificationList.count)]];
                }
                
                [JTGlobalCache.lastNotificationList removeAllObjects];
                [JTGlobalCache.lastNotificationList addObjectsFromArray:notificationList];
            }
            
            [_notificationList addObjectsFromArray:JTGlobalCache.lastNotificationList];
            
            if (_notificationList.count == 0) {
                [self showNoItemsViewWithTitle:@"没有新的通知"];
            }else {
                [self dimissNoItemsView];
            }
            
            [self.tableView reloadData];
            
            
            [JTGlobalCache synchronize];
            
            if (_loadMoreIndicator.isAnimating) {
                [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
            }else if(_notificationList.count > 0){
                [self.tableView setTableFooterView:_loadMoreFooterView];
            }else {
                [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
            }
            
            [_loadMoreIndicator stopAnimating];
            _loadMoreIndicator.hidden = YES;
            _loadMoreBtn.hidden = NO;
        }
        
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _notificationList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Notification *notification = _notificationList[indexPath.row];
    if ([notification.type isEqualToString:@"2"]) {
        return [NotificationCell heightWithTitle:notification.content];
    }else {
        return [NotificationCell heightWithTitle:nil];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCell" forIndexPath:indexPath];
    
    Notification *notification = _notificationList[indexPath.row];
    NSString *nicknameStr = nil;
    NSArray *nickArrays = [notification.nick componentsSeparatedByString:@","];
    NSInteger endIndex = [notification.tips rangeOfString:[nickArrays lastObject]].location;
    if (endIndex != NSNotFound) {
        endIndex += [[nickArrays lastObject] length];
    }else {
        endIndex = [nickArrays[0] length];
    }
    
    if (notification.headImageList.count > 1 ) {
        nicknameStr = [NSString stringWithFormat:@"等%ld人", notification.headImageList.count];
    }else {
        nicknameStr = notification.nick;
    }
   
    NSMutableAttributedString *actionAttributeStr = [[NSMutableAttributedString alloc] initWithString:notification.tips];
    
    [actionAttributeStr addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1], NSFontAttributeName : [UIFont systemFontOfSize:15]} range:NSMakeRange(0, endIndex)];
    
    [actionAttributeStr addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.61 green:0.62 blue:0.64 alpha:1], NSFontAttributeName : [UIFont systemFontOfSize:12]} range:NSMakeRange(endIndex , actionAttributeStr.length - endIndex)];
    
    cell.actionLabel.attributedText = actionAttributeStr;
    [cell.thumbnailView setImageWithURL:notification.productImage effect:SDWebImageEffectNone];
    
    [cell setAvatarViewsWithURLs:notification.headImageList];
    
    if([notification.type integerValue] == NotificationTypeComment) {
        cell.contentLabel.text = notification.content;
        cell.contentLabel.hidden = NO;
        cell.thumbnailView.hidden = NO;
        
    }else if ([notification.type integerValue] == NotificationTypeNewUserJoin){
        cell.contentLabel.hidden = YES;
        cell.thumbnailView.hidden = YES;
    }else {
        cell.contentLabel.text = @"";
        cell.contentLabel.hidden = YES;
        cell.thumbnailView.hidden = NO;
    }
    
    cell.timeLabel.text = [[NSDate dateWithTimeIntervalSince1970:notification.createTime/1000] prettyShortString];
    
    [cell updateLayout];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Notification *notification = _notificationList[indexPath.row];
    if ([notification.type integerValue] == NotificationTypeNewUserJoin) {
        NSString *userId = notification.userId;
        FriendInfoViewController *friendController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
        friendController.userId = userId;
        [self.navigationController pushViewController:friendController animated:YES];
    }else {
        DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
        detailController.tasteId = notification.productId;
        [self.navigationController pushViewController:detailController animated:YES];
    }
}

@end
