//
//  CategoryChooseViewController.h
//  youpinwei
//
//  Created by tmy on 15/3/21.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"

@protocol CategoryChooseViewControllerDelegate;

@interface CategoryChooseViewController : ShaibaBaseViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) BOOL forSingleUse;
@property (nonatomic, weak) id<CategoryChooseViewControllerDelegate> delegate;

@end


@protocol CategoryChooseViewControllerDelegate <NSObject>

- (void)categoryChooseViewControllerDidFinish:(CategoryChooseViewController *)controller;

@end
