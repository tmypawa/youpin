//
//  PublishTextItem.m
//  youpinwei
//
//  Created by tmy on 15/1/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishTextItem.h"

@implementation PublishTextItem

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.text forKey:@"text"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.text = [decoder decodeObjectForKey:@"text"];
    }
    return self;
}

@end
