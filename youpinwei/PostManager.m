//
//  PostManager.m
//  youpinwei
//
//  Created by tmy on 15/2/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PostManager.h"
#import "PublishItem.h"
#import "PublishImageItem.h"
#import "PublishTextItem.h"

@implementation PostManager
{
    NSString        *_cacheDir;
}

+ (instancetype)defaultManager {
    static PostManager *DefaultManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        DefaultManager = [[self alloc] init];
    });
    
    return DefaultManager;
}

- (id)init {
    if (self = [super init]) {
        _cacheDir = [NSString stringWithFormat:@"%@/Library/Caches/%@",NSHomeDirectory(), PUBLISH_IMAGE_CACHE_DIR];
    }
    
    return self;
}

- (void)publishPost:(TasteItem *)post {
    [[YouPinAPI sharedAPI] publishPost:post sender:nil callback:^(TasteItem *newTaste, NSError *error) {
        if (!error) {            
            [self clearAllPhotosForPost:post];
            
            if (newTaste.draftIdentifier) {
                [self deleteLocalDraftForDraftIdentifier:newTaste.draftIdentifier];
            }
            
            [self handleForTasteDidPublishSuccess:newTaste];
        }else {
            [self handleForTasteFailedToPublish:post];
        }
    }];
    
    [[PublishParamSet sharedInstance] reset];
    [self handleForTasteDidSend:post];
}

- (void)updatePost:(TasteItem *)post callback:(void(^)(TasteItem *newTaste, NSError *error))callback {
    [[YouPinAPI sharedAPI] publishPost:post sender:nil callback:^(TasteItem *newTaste, NSError *error) {
        if (!error) {
            [self clearAllPhotosForPost:post];
            
            if (newTaste.draftIdentifier) {
                [self deleteLocalDraftForDraftIdentifier:newTaste.draftIdentifier];
            }
            
            [self handleForTasteDidEditSuccess:newTaste];
            
            if (callback) {
                callback(newTaste, nil);
            }
        }else {
            [self handleForTasteFailedToEdit:post];
            if (callback) {
                callback(nil, error);
            }
        }
    }];
    
    [[PublishParamSet sharedInstance] reset];
    [self handleForTasteDidEdit:post];
}

- (void)updatePost:(TasteItem *)post {
    [self updatePost:post callback:nil];
}

- (NSString *)postImageFileWithImageName:(NSString *)imageName {
    return [self localFilePathWithFileName:imageName];
}

- (void)handleForTasteDidSend:(TasteItem *)taste {
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationNewTasteItemDidSend object:self userInfo:@{YouPinNotificationPublishTasteItemKey: taste}];
}

- (void)handleForTasteDidPublishSuccess:(TasteItem *)taste {
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationNewTasteItemDidPublish object:self userInfo:@{YouPinNotificationPublishTasteItemKey: taste}];
    NSLog(@"publish success!");
}

- (void)handleForTasteFailedToPublish:(TasteItem *)taste  {
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationNewTasteItemFailedToPublish object:self userInfo:@{YouPinNotificationPublishTasteItemKey: taste}];
    NSLog(@"publish failed...");
}

- (void)handleForTasteDidEdit:(TasteItem *)taste {
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationPhotoDidFinishEdit object:self userInfo:@{YouPinNotificationPublishTasteItemKey: taste}];
}

- (void)handleForTasteDidEditSuccess:(TasteItem *)taste {
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationTasteItemDidEditedSuccess object:self userInfo:@{YouPinNotificationPublishTasteItemKey: taste}];
    NSLog(@"edit success!");
}

- (void)handleForTasteFailedToEdit:(TasteItem *)taste  {
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationTasteItemFailedToEdit object:self userInfo:@{YouPinNotificationPublishTasteItemKey: taste}];
    NSLog(@"eidt failed...");
}

- (void)cancelPublishForPost:(TasteItem *)post {
    [self clearAllPhotosForPost:post];
    [self deleteLocalDraftForDraftIdentifier:post.draftIdentifier];
}

- (void)deleteLocalDraftForDraftIdentifier:(NSString *)draftIdentifier {
    if (!draftIdentifier)  return;
    
    NSUInteger existIndex = [JTGlobalCache.draftBoxList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [((TasteItem *)obj).draftIdentifier isEqualToString:draftIdentifier];
    }];
    
    if (existIndex != NSNotFound) {
        TasteItem *existDraftItem = JTGlobalCache.draftBoxList[existIndex];
        NSString *zipFilePath = [self localFilePathWithFileName:existDraftItem.photosZipFilePath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:zipFilePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:zipFilePath error:nil];
        }
        
        [JTGlobalCache.draftBoxList removeObject:existDraftItem];
    }
}

- (NSString *)localFilePathWithFileName:(NSString *)fileName {
    if (NotEmptyValue(fileName)) {
        return [NSString stringWithFormat:@"%@/%@", _cacheDir, fileName];
    }else {
        return @"";
    }
}

- (void)clearAllPhotosForPost:(TasteItem *)post {
    if (post.publishItems) {
        for (PublishItem *item in post.publishItems) {
            if ([item isKindOfClass:[PublishImageItem class]] && ((PublishImageItem *)item).source == PublishImageSourceLocal) {
                NSString *filePath = [self localFilePathWithFileName:((PublishImageItem *)item).imagePath];
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            }
        }
    }
    
    if (post.littleThumbnailImage && ![post.littleThumbnailImage hasPrefix:@"http"]) {
        NSString *path = [self localFilePathWithFileName:post.littleThumbnailImage];
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
}

- (void)clearDrafts {
    for (TasteItem *draftTaste in JTGlobalCache.draftBoxList) {
        if (draftTaste.photosZipFilePath) {
            NSString *zipFilePath = [self localFilePathWithFileName:draftTaste.photosZipFilePath];
            if ([[NSFileManager defaultManager] fileExistsAtPath:zipFilePath]) {
                [[NSFileManager defaultManager] removeItemAtPath:zipFilePath error:nil];
            }
        }
        
        [self clearAllPhotosForPost:draftTaste];
    }
    
    [JTGlobalCache.draftBoxList removeAllObjects];
    [JTGlobalCache synchronize];
}

@end
