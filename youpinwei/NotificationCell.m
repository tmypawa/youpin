//
//  NotificationCell.m
//  youpinwei
//
//  Created by tmy on 14/10/22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "NotificationCell.h"
#import "UIKit+RoundImage.h"

@implementation NotificationCell
{
    NSMutableArray  *_avatarViews;
}

- (void)awakeFromNib {
    _avatarViews = [[NSMutableArray alloc] init];
    
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.backgroundColor = DEFAULT_IMAGE_BG_COLOR;
    self.actionLabel.textColor = [UIColor colorWithRed:0 green:0.59 blue:0.56 alpha:1];
    self.titleBgView.image = [[UIImage imageNamed:@"bg_notification_product"] stretchableImageWithLeftCapWidth:30 topCapHeight:0];
    self.contentLabel.textColor = [UIColor colorWithRed:0.36 green:0.36 blue:0.36 alpha:1];
    self.timeLabel.textColor = [UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1];
    self.goodsTitleLabel.textColor = [UIColor colorWithRed:0.57 green:0.57 blue:0.57 alpha:1];
    [self.avatarView makeRound];
    UIView *hightlightBgView = [[UIView alloc] initWithFrame:CGRectZero];
    hightlightBgView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
    self.selectedBackgroundView = hightlightBgView;
    self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
}

- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}
 

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)heightWithTitle:(NSString *)title {
    CGFloat titleHeight = 0;
    if (title) {
        titleHeight = [title boundingRectWithSize:CGSizeMake(197, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
        titleHeight += 70;
    }else {
        titleHeight = 80;
    }
   
    return titleHeight;
}

- (void)setAvatarViewsWithURLs:(NSArray *)URLs {
    for (UIImageView *avatarView in _avatarViews) {
        [avatarView removeFromSuperview];
    }
    
    [_avatarViews removeAllObjects];
    NSInteger index = 0;
    for (NSString *url in URLs) {
        UIImageView *avatarView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + index * 37, 23, 32, 32)];
        avatarView.backgroundColor = [UIColor lightTextColor];
        avatarView.contentMode = UIViewContentModeScaleAspectFill;
        [avatarView makeRound];
        if ((NSNull *)url != [NSNull null]) {
            [avatarView setImageWithURL:url effect:SDWebImageEffectNone];
        }
        
        [self.contentView addSubview:avatarView];
        [_avatarViews addObject:avatarView];
        index++;
    }
}

- (void)updateLayout {
    if (_avatarViews && _avatarViews.count > 0) {
        UIImageView *lastAvatarView = [_avatarViews lastObject];
        SetFrameX(self.actionLabel, CGRectGetMaxX(lastAvatarView.frame) + 10);
        SetFrameX(self.timeLabel, CGRectGetMaxX(lastAvatarView.frame) + 10);
    }else {
        SetFrameX(self.actionLabel, 52);
        SetFrameX(self.timeLabel, 52);
    }
    
    SetFrameX(self.contentLabel, self.actionLabel.frame.origin.x);
    SetFrameY(self.contentLabel, CGRectGetMaxY(self.actionLabel.frame) + 5);
    
    if (!self.contentLabel.hidden) {
        AdjustLabelHeight(self.contentLabel);
        SetFrameY(self.timeLabel, CGRectGetMaxY(self.contentLabel.frame) + 6);
    }else {
        SetFrameY(self.timeLabel, CGRectGetMaxY(self.actionLabel.frame) + 6);
    }
}

@end
