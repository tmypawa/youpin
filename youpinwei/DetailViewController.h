//
//  DetailViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTInputView.h"
#import "ProfileSettingViewController.h"
#import "PublishCommentViewController.h"
#import "PurchaseInfoController.h"
#import "DWTagList.h"

@class TasteItem;

@interface DetailViewController : ShaibaBaseViewController <UITableViewDataSource, UITableViewDelegate, JTInputViewDelegate, UITextViewDelegate, ProfileSettingViewControllerDelegate, UIViewControllerTransitioningDelegate, DWTagListDelegate, UIActionSheetDelegate, UIAlertViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, PublishCommentViewControllerDelegate, PurchaseInfoControllerDelegate>

@property (nonatomic, strong) NSString *tasteId;
@property (nonatomic, strong) TasteItem *relatedTasteItem;
@property (nonatomic) BOOL shouldCommentImmediately;

@end
