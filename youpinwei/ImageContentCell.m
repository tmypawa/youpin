//
//  ImageContentCell.m
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ImageContentCell.h"

@implementation ImageContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.thumnailView.contentMode = UIViewContentModeScaleAspectFit;
    self.thumnailView.clipsToBounds = YES;
    SetFrameSize(self.thumnailView, 310, 310);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateLayout {
    SetFrameY(self.thumnailView, 15);
}

+ (CGFloat)height {
    return 325;
}

@end
