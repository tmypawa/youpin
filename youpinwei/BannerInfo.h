//
//  BannerInfo.h
//  youpinwei
//
//  Created by tmy on 14/12/12.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BannerInfo : NSObject<NSCoding>

@property (nonatomic, strong) NSString *picUrl;
@property (nonatomic, strong) NSString *gotoUrl;
@property (nonatomic, strong) NSString *tag;

@end
