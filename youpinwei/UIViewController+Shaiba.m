//
//  UIViewController+Shaiba.m
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "UIViewController+Shaiba.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "PublishViewController.h"
#import "PostContentItem.h"

@implementation UIViewController (Shaiba)

- (void)presentLoginController {
    if(JTGlobalCache.mode != InitModeReview) {
        LoginViewController *loginController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        loginController.showGuide = YES;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginController];
        [self presentViewController:navController animated:YES completion:nil];
    }else {
        RegisterViewController *registerController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
        [self presentViewController:navController animated:YES completion:nil];
    }
}

- (void)presentPublishViewController {
    PublishViewController *publishController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:publishController];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)showToastWithTitle:(NSString *)title {
    MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hudView.userInteractionEnabled = NO;
    hudView.mode = MBProgressHUDModeText;
    hudView.labelText = title;
    [hudView hide:YES afterDelay:1];
}

- (void)showHUDViewWithTitle:(NSString *)title {
    MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hudView.userInteractionEnabled = NO;
    hudView.labelText = title;
}

- (void)hideHUDView {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)showGlobalHUDViewWithTitle:(NSString *)title {
    MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
    hudView.userInteractionEnabled = YES;
    hudView.labelText = title;
}

- (void)hideGlobalHUDView {
    [MBProgressHUD hideAllHUDsForView:self.view.window animated:YES];
}

- (void)showSuccessHUDWithTitle:(NSString *)title {
    MBProgressHUD *hudView = [MBProgressHUD  showHUDAddedTo:self.view animated:YES];
   
    [hudView showResultWithText:title imageName:@"icon-correct-Add friends"];
}

- (void)showFailHUDWithTitle:(NSString *)title {
    MBProgressHUD *hudView = [MBProgressHUD  showHUDAddedTo:self.view animated:YES];
    [hudView showResultWithText:title imageName:@"icon-error-add friends"];
}

- (void)showNoItemsViewWithTitle:(NSString *)title {
    UILabel *noItemsLabel = (UILabel *)[self.view viewWithTag:10001];
    if (!noItemsLabel) {
        noItemsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
        noItemsLabel.backgroundColor = [UIColor clearColor];
        noItemsLabel.textColor = [UIColor lightGrayColor];
        noItemsLabel.font = [UIFont systemFontOfSize:17];
        noItemsLabel.textAlignment = NSTextAlignmentCenter;
        noItemsLabel.numberOfLines = 0;
        noItemsLabel.tag = 10001;
    }
    
    noItemsLabel.text = title;
    AdjustLabelHeight(noItemsLabel);
    noItemsLabel.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    [self.view addSubview:noItemsLabel];
    if ([self.view isKindOfClass:[UIScrollView class]]) {
        ((UIScrollView *)self.view).scrollEnabled = NO;
    }
}

- (void)dimissNoItemsView {
    UILabel *noItemsLabel = (UILabel *)[self.view viewWithTag:10001];
    if (noItemsLabel) {
        [noItemsLabel removeFromSuperview];
    }
}

- (void)showNavigationToastWithTitle:(NSString *)title image:(UIImage *)image actionView:(UIView *)actionView {
    NSMutableDictionary *options = [@{kCRToastTextKey: title} mutableCopy];
    options[kCRToastNotificationTypeKey] = @(CRToastTypeNavigationBar);
    
    options[kCRToastInteractionRespondersKey] = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTap
                                                                                                  automaticallyDismiss:YES
                                                                                                                 block: nil]];
    
    if (image) {
        options[kCRToastImageKey] = image;
    }
    
    if (actionView) {
        options[kCRToastShowActionView] = @(YES);
        options[kCRToastActionView] = actionView;
    }
    
    [CRToastManager showNotificationWithOptions:options completionBlock:nil];
}

- (void)showStatusBarToastWithTitle:(NSString *)title {
    NSMutableDictionary *options = [@{kCRToastTextKey: title} mutableCopy];
    options[kCRToastNotificationTypeKey] = @(CRToastTypeStatusBar);
    
    options[kCRToastInteractionRespondersKey] = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTap
                                                                                                  automaticallyDismiss:YES
                                                                                                                 block: nil]];
    options[kCRToastFontKey] = [UIFont systemFontOfSize:12];
    options[kCRToastTextAlignmentKey] = @(NSTextAlignmentCenter);
    
    [CRToastManager showNotificationWithOptions:options completionBlock:nil];
}


- (void)showShareMenuWithTasteItem:(TasteItem *)tasteItem {
    NSString *imageUrl = [tasteItem.realShotPics count] > 0 ? tasteItem.realShotPics[0] : @"";
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url: imageUrl];
    
    NSString *description = @"";
    for (PostContentItem *item in tasteItem.contentItems) {
        if (item.type == PostContentTypeText) {
            description = item.text;
            break;
        }
    }
    
    BOOL isSelfPost = JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:tasteItem.userId];
    NSString *title = isSelfPost ? @"我在【晒吧】里分享了好多败来的宝贝" : @"我在【晒吧】里看到一个很棒的晒单";
    [UMSocialData defaultData].extConfig.wechatSessionData.title = title;
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;
    
    NSString *userId = JTGlobalCache.isLogin ? JTGlobalCache.currentUser.userId : @"";
    NSString *url = [NSString stringWithFormat:@"http://shai.ba/ypw/act/shareWX.json?userId=%@&productId=%@&type=%d", userId, tasteItem.productId, 1];
    [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = url;
    
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UMENG_ID
                                      shareText:description
                                     shareImage:nil
                                shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline]
                                       delegate:nil];
}

@end
