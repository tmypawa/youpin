//
//  GoodsPhotoCell.h
//  youpinwei
//
//  Created by tmy on 14/12/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsPhotoCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) IBOutlet UIImageView *selectionView;

@end
