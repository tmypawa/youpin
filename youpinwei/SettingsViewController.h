//
//  SettingsViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-29.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShaibaBaseViewController.h"

@interface SettingsViewController : ShaibaTableViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting, UIAlertViewDelegate>

@end
