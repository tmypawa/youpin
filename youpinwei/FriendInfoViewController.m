//
//  FriendInfoViewController.m
//  youpinwei
//
//  Created by tmy on 14/12/25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "FriendInfoViewController.h"
#import "JTImageZoomAnimationController.h"
#import "JTImageBrowerController.h"

@interface FriendInfoViewController ()

@end

@implementation FriendInfoViewController
{
    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _tasteItems = [[NSMutableArray alloc] init];
        _currentOperationItemIndex = -1;
        _user = nil;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人主页";
    _placeHolderView.hidden = YES;
    
    [self loadPullRereshAnimation];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self refreshData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initViews {
    
}

- (IBAction)showProfile:(id)sender {
    NSURL *url = [NSURL URLWithString:_user.avatar];
    JTImageBrowerController *imageController = [[JTImageBrowerController alloc] initWithImageURLs:@[url]];
    imageController.transitioningDelegate = self;
    [self presentViewController:imageController animated:YES completion:nil];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    JTImageZoomAnimationController *animationController = [[JTImageZoomAnimationController alloc] initWithReferenceView:_headerView.avatarView.imageView];
    return animationController;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    JTImageZoomAnimationController *animationController = [[JTImageZoomAnimationController alloc] initWithReferenceView:_headerView.avatarView.imageView];
    return animationController;
}

- (void)showUserProfile {
    if (_user) {
        _headerView.infoContainerView.hidden = NO;
        [self showUserInfoHeader];
    }
}

- (void)showUserInfoHeader {
    _headerView.hidden = NO;
    [_headerView.avatarView setImageWithURL:_user.avatar placeholderImage:[UIImage imageNamed:@"icon_mine_default_Head"] effect:SDWebImageEffectNone];
    
    [_headerView.nicknameBtn setTitle:[_user displayName] forState:UIControlStateNormal];
    [_headerView.nicknameBtn setTitleColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1] forState:UIControlStateNormal];
    [_headerView.nicknameBtn setBackgroundImage:nil forState:UIControlStateNormal];
    NSMutableAttributedString *levelTitleStr = nil;
    if(_user.degree > 0) {
        levelTitleStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"体验家%ld级, %ld 米阳光", _user.degree, _user.points]];
        NSRange levelRange = [levelTitleStr.string rangeOfString:[NSString stringWithFormat:@"%ld", _user.degree]];
        [levelTitleStr addAttributes:@{NSForegroundColorAttributeName: LevelColor(_user.degree)} range:levelRange];
    }else {
        if (_user.points > 0) {
            levelTitleStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld 米阳光", _user.points]];
        }else {
            levelTitleStr = [[NSMutableAttributedString alloc] initWithString:@"你还没有阳光"];
        }
    }
    _headerView.pointsLabel.attributedText = levelTitleStr;
    if (_user.gender != 0) {
        _headerView.sexIconView.image = [UIImage imageNamed:_user.gender == 1?@"icon_boy":@"icon_girl"];
    }
    
    [self updateFriendStatus];
    
    [_headerView updateLayout];
}

- (void)updateFriendStatus {
    if (_user.relation == 0) {
        _headerView.addFriendButton.hidden = NO;
        _headerView.friendStatusView.hidden = YES;
    }else {
        _headerView.addFriendButton.hidden = YES;
        _headerView.friendStatusView.hidden = NO;
        if (_user.relation == 4) {
            _headerView.friendStatusIconView.image = [UIImage imageNamed:@"icon_friends_mine"];
            _headerView.friendStatusLabel.text = @"互为好友";
        }else if (_user.relation == 5){
            _headerView.friendStatusIconView.image = [UIImage imageNamed:@"icon_time"];
            _headerView.friendStatusLabel.text = @"等待验证";
        }
    }
}

- (IBAction)showWishList:(id)sender {
    WishListViewController *wishListController = [self.storyboard instantiateViewControllerWithIdentifier:@"WishListViewController"];
    wishListController.userId = _user.userId;
    [self.navigationController pushViewController:wishListController animated:YES];
}

- (void)goToInvitationView:(UIButton *)sender {
    if (JTGlobalCache.isLogin) {
        FriendInvitationController *invitationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInvitationController"];
        invitationController.userId = _user.userId;
        invitationController.delegate = self;
        [self.navigationController pushViewController:invitationController animated:YES];
    }else {
        [self showRegisterAlert];
    }
}

- (void)initForHeaderView {
    [super initForHeaderView];
    [_headerView.addFriendButton addTarget:self action:@selector(goToInvitationView:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)refreshData {
    [[YouPinAPI sharedAPI] fetchFriendTimelineWithStartTimestamp:[self startTimestamp] endTimestamp:0 userId:self.userId Sender:self callback:^(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User *userInfo, NSDictionary *levelDict, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.collectionView stopRefreshAnimation];
        if (!error) {
            _levelDict = levelDict;
            [_tasteItems removeAllObjects];
            [_tasteItems addObjectsFromArray:items];
            
            _headerView.totalCountView.hidden = _tasteItems.count == 0;
            _placeHolderView.hidden = YES;
            self.collectionView.backgroundView.backgroundColor = [UIColor whiteColor];
            
            _headerView.totalCountLabel.text = [NSString stringWithFormat:@"共%ld个宝贝, %ld个精品", totalCount, expressCount];
            
            if (userInfo) {
                _user = userInfo;
                if (!_hasLoadRefreshView) {
                    [self loadPullRereshAnimation];
                    [self addLoadMoreView];
                    _hasLoadRefreshView = YES;
                }
                [self showUserProfile];
            }
            
            [self.collectionView reloadData];
        }else {
            
        }
    }];
}

- (void)loadMoreData {
    [[YouPinAPI sharedAPI] fetchFriendTimelineWithStartTimestamp:0 endTimestamp:[self endTimestamp] userId:self.userId Sender:self callback:^(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User *userInfo, NSDictionary *levelDict, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            if (items.count == 0) {
                _hasNext = NO;
            }
            [_tasteItems addObjectsFromArray:items];
            [self.collectionView endLoadMoreWithHandler:^(UIView *customView) {
                [((UIImageView *)customView) stopAnimating];
//                customView.hidden = YES;
            }];
            [self.collectionView reloadData];
        }else {
            
        }
    }];
}

#pragma mark FriendInvitationControllerDelegate

- (void)friendInvitationControllerDidSendInvitation:(FriendInvitationController *)controller {
    _user.relation = 5;
    [self updateFriendStatus];
}

@end
