//
//  RecommendTagViewCell.m
//  youpinwei
//
//  Created by tmy on 15/3/27.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "RecommendTagViewCell.h"
#import "RecommendTagItem.h"

@implementation RecommendTagViewCell
{
    IBOutlet UICollectionView *_itemsCollectionView;
    NSArray     *_items;
}

- (void)awakeFromNib {
    self.backgroundColor = MAIN_CELL_BG_COLOR;
    _itemsCollectionView.delegate = self;
    _itemsCollectionView.dataSource = self;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItems:(NSArray *)items {
    _items = items;
    [_itemsCollectionView reloadData];
}

- (IBAction)showMore:(id)sender {
    if (self.showMoreHandler) {
        self.showMoreHandler();
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _items ? _items.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TagItemCell" forIndexPath:indexPath];
    UIImageView *thumbnailsView = (UIImageView *)[cell.contentView viewWithTag:1000];
    
    RecommendTagItem *item = _items[indexPath.row];
    
    [thumbnailsView setImageWithURL:item.thumbnail placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectFade];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemSelectHandler) {
        self.itemSelectHandler(indexPath.row);
    }
}

@end
