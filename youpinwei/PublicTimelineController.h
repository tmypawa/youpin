//
//  PublicTimelineController.h
//  youpinwei
//
//  Created by tmy on 14/11/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "MainViewController.h"

@protocol CategoryChooseViewControllerDelegate;

@interface PublicTimelineController : MainViewController<UICollectionViewDataSource, UICollectionViewDelegate, CategoryChooseViewControllerDelegate>

@end
