//
//  YouPin.m
//  youpinwei
//
//  Created by tmy on 14-9-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "YouPin.h"
#import "JTAddressBookManager.h"
#import "YouPinAPI.h"
#import "BPush.h"
#import "TaobaoURLProtocol.h"
#import "UMFeedback.h"
#import "MMPopLabel.h"

@implementation YouPin

+ (instancetype)sharedInstance {
    static YouPin *SharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SharedInstance = [[self alloc] init];
    });
    
    return SharedInstance;
}

- (id)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:YouPinNotificationUserDidLogin object:nil];
    }
    
    return self;
}

- (void)initGlobalCache {
    JTGlobalCache.isReturnUser = NO;
    JTGlobalCache.currentUser = nil;
    JTGlobalCache.isLogin = NO;
    JTGlobalCache.hasAllowedSyncAddressBook = NO;
    JTGlobalCache.hasDeclinedWithCustomDialog = NO;
    JTGlobalCache.hasDeclinedWithSystemDialog = NO;
    JTGlobalCache.hasShowAddressBookTip = NO;
    JTGlobalCache.lastAddressBookSyncTime = 0;
    JTGlobalCache.lastFriendNotificationTime = 0;
    JTGlobalCache.lastNotificationUpdateTime = 0;
    JTGlobalCache.friendNotificationBadge = 0;
    JTGlobalCache.friendTimelineBadge = 0;
    JTGlobalCache.bannerHeight = 100;
    JTGlobalCache.bannerSwitch = NO;
    JTGlobalCache.mode = 1;
    JTGlobalCache.publishImageTutorialTimes = 0;
    JTGlobalCache.lastVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    JTGlobalCache.recentTastes = [[NSMutableArray alloc] init];
    JTGlobalCache.publicTastes = [[NSMutableArray alloc] init];
    JTGlobalCache.discoveryRecommendTags = [[NSMutableArray alloc] init];
    JTGlobalCache.discoveryBanners = [[NSMutableArray alloc] init];
    JTGlobalCache.draftBoxList = [[NSMutableArray alloc] init];
    JTGlobalCache.lastNotificationList = [[NSMutableArray alloc] init];
    JTGlobalCache.notificationList = [[NSMutableArray alloc] init];
    JTGlobalCache.tutorialSet = [[NSMutableSet alloc] init];
    JTGlobalCache.tagHistoryList = [[NSMutableArray alloc] init];
    JTGlobalCache.discoveryHistoryList = [[NSMutableArray alloc] init];
    [JTGlobalCache synchronize];
}

- (void)clearGlobalCacheForLogOut {
    JTGlobalCache.isReturnUser = NO;
    JTGlobalCache.currentUser = nil;
    JTGlobalCache.isLogin = NO;
    JTGlobalCache.hasAllowedSyncAddressBook = NO;
    JTGlobalCache.hasDeclinedWithCustomDialog = NO;
    JTGlobalCache.hasDeclinedWithSystemDialog = NO;
    JTGlobalCache.hasShowAddressBookTip = NO;
    JTGlobalCache.lastAddressBookSyncTime = 0;
    JTGlobalCache.lastFriendNotificationTime = 0;
    JTGlobalCache.lastNotificationUpdateTime = 0;
    JTGlobalCache.friendNotificationBadge = 0;
    JTGlobalCache.friendTimelineBadge = 0;
    JTGlobalCache.bannerSwitch = NO;
    JTGlobalCache.recentTastes = [[NSMutableArray alloc] init];
    JTGlobalCache.draftBoxList = [[NSMutableArray alloc] init];
    JTGlobalCache.lastNotificationList = [[NSMutableArray alloc] init];
    JTGlobalCache.notificationList = [[NSMutableArray alloc] init];
    JTGlobalCache.tagHistoryList = [[NSMutableArray alloc] init];
    JTGlobalCache.discoveryHistoryList = [[NSMutableArray alloc] init];
    [JTGlobalCache synchronize];
}

- (void)doInitialCheck {
    [MobClick startWithAppkey:UMENG_ID reportPolicy:REALTIME channelId:@"内测"];
    [MobClick setAppVersion:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    [UMFeedback setAppkey:UMENG_ID];
    [UMSocialData setAppKey:UMENG_ID];
    [UMSocialWechatHandler setWXAppId:WEIXIN_APP_ID appSecret:WEIXIN_APP_SECRET url:nil];
    
    [NSURLProtocol registerClass:[TaobaoURLProtocol class]];
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    if (!JTGlobalCache.isReturnUser) {
        [self initGlobalCache];
    }
    
    [self compatibleInitForOldVersion];
    
    [self checkDirs];
    
    [self setAppearance];
    
    [self initConfig];
    
    if (JTGlobalCache.isLogin && JTGlobalCache.hasAllowedSyncAddressBook) {
        [self syncAddressBook];
    }
    
    if(JTGlobalCache.isLogin){
        [self uploadDeviceInfo];
        [self bindPushInfo];
    }
    
    if (JTGlobalCache.recentTastes && JTGlobalCache.recentTastes.count > 25) {
        [JTGlobalCache.recentTastes removeObjectsInRange:NSMakeRange(25, JTGlobalCache.recentTastes.count - 25)];
    }
    
    if (JTGlobalCache.publicTastes && JTGlobalCache.publicTastes.count > 25) {
        [JTGlobalCache.publicTastes removeObjectsInRange:NSMakeRange(25, JTGlobalCache.publicTastes.count - 25)];
    }
}

- (void)compatibleInitForOldVersion {
    if (NotEmptyValue(JTGlobalCache.lastVersion )) {
        NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        NSDecimalNumber *currentVersionNumber = [NSDecimalNumber decimalNumberWithString:currentVersion];
        NSDecimalNumber *lastVersionNumber = [NSDecimalNumber decimalNumberWithString:JTGlobalCache.lastVersion];
        
        if ([lastVersionNumber compare:currentVersionNumber] == NSOrderedAscending ) {
            JTGlobalCache.lastNotificationList = [[NSMutableArray alloc] init];
            JTGlobalCache.notificationList = [[NSMutableArray alloc] init];
            JTGlobalCache.recentTastes = [[NSMutableArray alloc] init];
            JTGlobalCache.publicTastes = [[NSMutableArray alloc] init];
            JTGlobalCache.draftBoxList = [[NSMutableArray alloc] init];
            JTGlobalCache.tutorialSet = [[NSMutableSet alloc] init];
            JTGlobalCache.tagHistoryList = [[NSMutableArray alloc] init];
            JTGlobalCache.discoveryRecommendTags = [[NSMutableArray alloc] init];
            JTGlobalCache.discoveryBanners = [[NSMutableArray alloc] init];
            JTGlobalCache.discoveryHistoryList = [[NSMutableArray alloc] init];
            JTGlobalCache.lastVersion = currentVersion;
            [JTGlobalCache synchronize];
        }
    }
}

- (void)loginWithUser:(User *)user {
    JTGlobalCache.currentUser = user;
    JTGlobalCache.isLogin = YES;
    if (!JTGlobalCache.isReturnUser) {
        JTGlobalCache.isReturnUser = YES;
    }
    [JTGlobalCache synchronize];
}

- (void)logout {
    [self clearGlobalCacheForLogOut];
    [self clearCookieCache];
}

- (void)userDidLogin:(NSNotification *)notification {
    if(JTGlobalCache.isLogin){
        [self uploadDeviceInfo];
        [self bindPushInfo];
    }
}

- (void)clearCookieCache {
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:@"http://h5.m.taobao.com"]];
    for (NSHTTPCookie *cookie in cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
}

- (void)onMethod:(NSString*)method response:(NSDictionary*)data {
    
    NSDictionary* res = [[NSDictionary alloc] initWithDictionary:data];
    if ([BPushRequestMethod_Bind isEqualToString:method]) {
        NSString *userid = [res valueForKey:BPushRequestUserIdKey];
        NSString *channelid = [res valueForKey:BPushRequestChannelIdKey];
        int returnCode = [[res valueForKey:BPushRequestErrorCodeKey] intValue];
        
        if (returnCode == BPushErrorCode_Success) {
            [[YouPinAPI sharedAPI] uploadPushInfoWithUserId:userid channelId:channelid callback:^(NSError *error) {
                
            }];
        }
    }
}

- (void)setAppearance {
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: GlobalTintColor}];
    [UINavigationBar appearance].barTintColor = NavigationBarTintColor;
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageFromColor:NavigationBarTintColor] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage imageFromColor:NavigationBarTintColor]];
    [UINavigationBar appearance].tintColor = GlobalTintColor;
    [[UINavigationBar appearance] setBackIndicatorImage: [UIImage imageNamed:@"btn_back"]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"btn_back"]];
    
    [[MMPopLabel appearance] setLabelColor:[UIColor blackColor]];
     [[MMPopLabel appearance] setLabelTextColor:[UIColor whiteColor]];
     [[MMPopLabel appearance] setLabelTextHighlightColor:[UIColor greenColor]];
     [[MMPopLabel appearance] setLabelFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f]];
     [[MMPopLabel appearance] setButtonFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
    
    [CRToastManager setDefaultOptions:@{
                                        kCRToastTimeIntervalKey : @(8),
                                        kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                                        kCRToastBackgroundColorKey: [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1],
                                        kCRToastTextColorKey : [UIColor whiteColor],
                                        kCRToastFontKey : [UIFont systemFontOfSize:14],
                                        kCRToastTextAlignmentKey: @(NSTextAlignmentLeft),
                                        kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                                        kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover)
                                        }];
}

- (void)checkDirs {
    NSString *publishCacheDir = [NSString stringWithFormat:@"%@/Library/Caches/%@",NSHomeDirectory(), PUBLISH_IMAGE_CACHE_DIR];
    BOOL isDir = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:publishCacheDir isDirectory:&isDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:publishCacheDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

- (void)initConfig {
    [[YouPinAPI sharedAPI] fecthInitConfigInfo:^(NSDictionary *configDict, NSError *error) {
        if (!error) {
            BOOL bannerSwitch = [configDict[@"bannerSwitch"] boolValue];
            NSInteger mode = [configDict[@"mode"] integerValue];
            float bannerHeight = [configDict[@"banner"][@"height"] floatValue]/ [UIScreen mainScreen].scale;
            JTGlobalCache.bannerSwitch = bannerSwitch;
            JTGlobalCache.mode = mode;
            JTGlobalCache.bannerHeight = bannerHeight;
            JTGlobalCache.tagHeight = [configDict[@"hotHeight"] floatValue];
            JTGlobalCache.showInvitation = [configDict[@"showPills"] boolValue];
            JTGlobalCache.wxShare = configDict[@"wxShare"];
            JTGlobalCache.smsShare = configDict[@"smsShare"];
            JTGlobalCache.publishPlaceHolders = configDict[@"publishTextHints"];
        }else {
            JTGlobalCache.bannerSwitch = NO;
            JTGlobalCache.mode = InitModeReview;
            JTGlobalCache.showInvitation = NO;
            JTGlobalCache.bannerHeight = 100;
            JTGlobalCache.tagHeight = 100;
        }
        
        [JTGlobalCache synchronize];
    }];
}

- (void)syncAddressBook {
    [[JTAddressBookManager defaultManager] fetchAllContacts:^(NSArray *allContacts, NSArray *updatedContacts) {
        if (JTGlobalCache.lastAddressBookSyncTime == 0 || (updatedContacts && updatedContacts.count > 0)) {
            [[YouPinAPI sharedAPI] uploadAddressBook:JTGlobalCache.lastAddressBookSyncTime == 0? allContacts : updatedContacts callback:^(NSError *error) {
                if (!error) {
//                    NSLog(@"uploaded address book %ld", JTGlobalCache.lastAddressBookSyncTime == 0? allContacts.count : updatedContacts.count);
                    JTGlobalCache.lastAddressBookSyncTime = [[NSDate date] timeIntervalSince1970];
                }
            }];
        }else {
            NSLog(@"no updated address info");
        }
    }];
}

- (void)uploadDeviceInfo {
    NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    if (![currentVersion isEqualToString:JTGlobalCache.lastVersion]) {
        [[YouPinAPI sharedAPI] updateDeviceInfoWithCallback:^(NSError *error) {
            if (!error) {
                JTGlobalCache.lastVersion = currentVersion;
                [JTGlobalCache synchronize];
            }
        }];
    }
}

- (void)bindPushInfo {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:myTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }else
    {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
}

- (void)handleForAppActive {
    NSString *inviteCode = [UIPasteboard generalPasteboard].string;
    if (NotEmptyValue(inviteCode) && [inviteCode hasPrefix:APP_INVITATION_PREFIX]) {
        [UIPasteboard generalPasteboard].string = @"";
        JTGlobalCache.friendInvitationCode = inviteCode;
        [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationAppDidOpenURL object:nil];
    }
}

- (void)handleForForeground {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if (JTGlobalCache.isLogin && JTGlobalCache.hasAllowedSyncAddressBook) {
        [[YouPin sharedInstance] syncAddressBook];
    }
}

- (void)handleForReceivedRemoteNotification:(NSDictionary *)userInfo {
    if([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        self.receivedRemoteNotification = userInfo;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationDidReceiveRemoteNotification object:nil userInfo:nil];
    
    
    [BPush handleNotification:userInfo];
}

- (BOOL)handleForOpenURL:(NSURL*)url sourceApplication:(NSString *)application annotation:(id)annotation {
    if ([url.scheme isEqualToString:APP_SCHEME]) {
        if(!JTGlobalCache.isLogin) return NO;
        
        NSArray *params = [url.query componentsSeparatedByString:@"&"];
        if (params.count > 0) {
            BOOL hasValidParam = NO;
            if ([params[0] hasPrefix:@"inviteCode"]) {
                NSString *inviteCode = [[params[0] componentsSeparatedByString:@"="] lastObject];
                if (NotEmptyValue(inviteCode)) {
                    JTGlobalCache.friendInvitationCode = inviteCode;
                    hasValidParam = YES;
                }
            }else if ([params[0] hasPrefix:@"productId"]) {
                NSString *productId = [[params[0] componentsSeparatedByString:@"="] lastObject];
                if (NotEmptyValue(productId)) {
                    JTGlobalCache.externalPostId = productId;
                    hasValidParam = YES;
                }
            }
            
            if (hasValidParam) {
                [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationAppDidOpenURL object:nil];
            }
            
            return hasValidParam;
        }else {
            return NO;
        }
    }else {
        return [UMSocialSnsService handleOpenURL:url];
    }

}

@end
