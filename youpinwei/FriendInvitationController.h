//
//  FriendInvitationController.h
//  youpinwei
//
//  Created by tmy on 15/1/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FriendInvitationControllerDelegate;

@interface FriendInvitationController : ShaibaBaseViewController

@property (nonatomic,strong) NSString *userId;
@property (nonatomic, weak) id<FriendInvitationControllerDelegate> delegate;

@end


@protocol FriendInvitationControllerDelegate <NSObject>

- (void)friendInvitationControllerDidSendInvitation:(FriendInvitationController *)controller;

@end