//
//  PurchaseInfoController.h
//  youpinwei
//
//  Created by tmy on 15/3/25.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"
#import "PurchaseSourceInfoController.h"

@protocol PurchaseInfoControllerDelegate;
@protocol GoodsChooseControllerDelegate;

@interface PurchaseInfoController : ShaibaBaseViewController<UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate, PurchaseSourceInfoControllerDelegate, GoodsChooseControllerDelegate>

@property (nonatomic, weak) id<PurchaseInfoControllerDelegate> delegate;
@property (nonatomic, strong) Goods *goods;
@property (nonatomic, strong) NSArray   *userList;

@end

@protocol PurchaseInfoControllerDelegate <NSObject>

- (void)purchaseInfoController:(PurchaseInfoController *)controller didFillGoods:(Goods *)goods;

@end
