//
//  TextContentCell.m
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "TextContentCell.h"

@implementation TextContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textContentLabel.font = [UIFont systemFontOfSize:15];
    SetFrameY(self.textContentLabel, 10);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)heightForText:(NSString *)text {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:LINE_SPACE];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName: paragraphStyle}];

    CGFloat titleHeight = [attributedString boundingRectWithSize:CGSizeMake(screenWidth - 20, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    titleHeight = ceilf(titleHeight);

    return titleHeight + 15;
}

- (void)setText:(NSString *)text {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:LINE_SPACE];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSParagraphStyleAttributeName: paragraphStyle}];
    
    self.textContentLabel.attributedText = attributedString;
}

- (void)updateLayout {
    SetFrameY(self.textContentLabel, 15);
     CGFloat titleHeight = [self.textContentLabel.attributedText boundingRectWithSize:CGSizeMake(screenWidth - 20, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    titleHeight = ceilf(titleHeight);
    CGRect frame = self.textContentLabel.frame;
    frame.size.height = titleHeight;
    self.textContentLabel.frame = frame;
}

@end
