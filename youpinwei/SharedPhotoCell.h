//
//  SharedPhotoCell.h
//  youpinwei
//
//  Created by tmy on 14-8-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharedPhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end
