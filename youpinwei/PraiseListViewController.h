//
//  PraiseListViewController.h
//  youpinwei
//
//  Created by tmy on 14/12/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TasteItem.h"

@interface PraiseListViewController : ShaibaTableViewController

@property (nonatomic, retain) TasteItem *tasteItem;

@end
