//
//  PublishStatusItem.h
//  youpinwei
//
//  Created by tmy on 15/2/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TasteItem.h"
#import "PublishStatusCell.h"

@interface PublishStatusItem : NSObject

@property (nonatomic, strong) TasteItem *post;
@property (nonatomic) PublishStatusCellType status;

@end
