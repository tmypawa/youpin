//
//  MainViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShaibaViewController.h"
#import "DialogViewController.h"
#import "DetailViewController.h"
#import "UITableViewCollection+LoadMore.h"
#import "UIScrollView+UzysAnimatedGifPullToRefresh.h"
#import "DWTagList.h"
#import "PublishStatusController.h"

@class TagViewController;

@interface MainViewController : ShaibaBaseViewController<UIActionSheetDelegate, UIAlertViewDelegate, DWTagListDelegate, DialogViewControllerDelegate, UIScrollViewDelegate, PublishStatusControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    PublishStatusController *_publishStatusController;
    IBOutlet UIBarButtonItem *_addFriendItem;
    IBOutlet UIView *_navTitleView;
    IBOutlet UIView *_bindingFooterView;
    __weak IBOutlet UIButton *_bindBtn;
    IBOutlet UIView *_registerView;
    IBOutlet UIButton *_registerBtn;
    IBOutlet UIView *_noItemsView;
    IBOutlet UIView *_addressTipView;
    IBOutlet UIView *_addressTurnOnView;
    IBOutlet UIButton *_addFriendBtn;
    UIView     *_statusBannerView;
    NSMutableArray  *_tasteItems;
    int             _currentOperationItemIndex;
    BOOL             _hasNext;
    CGFloat _lastContentOffsetY;
    NSMutableIndexSet   *_scaledRowIndexSet;
    BOOL            _scrollCheckLock;
    BOOL            _isViewBeingShow;
    NSTimeInterval  _todayTimestamp;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (void)internalInit;
- (void)loadPullRereshAnimation;
- (void)handleForDoubleTapTab;
- (void)resetNavigationBar;
- (void)showNavigationBar:(BOOL)show;
- (NSTimeInterval)startTimestamp;
- (NSTimeInterval)endTimestamp;
- (NSTimeInterval)dayTimestampFromDate:(NSDate *)date;
- (void)addLoadMoreView;
- (void)refreshData;
- (void)loadNextData;
- (void)reloadItems:(NSArray *)items hasNext:(BOOL)hasNext error:(NSError *)error;
- (void)loadNextItems:(NSArray *)items hasNext:(BOOL)hasNext error:(NSError *)error;

@end
