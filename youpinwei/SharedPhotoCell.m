//
//  SharedPhotoCell.m
//  youpinwei
//
//  Created by tmy on 14-8-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "SharedPhotoCell.h"

@implementation SharedPhotoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib {
    self.photoView.backgroundColor = [UIColor lightGrayColor];
}

@end
