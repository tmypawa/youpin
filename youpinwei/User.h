//
//  User.h
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    UserRelationTypeNotFriend = 0,
    UserRelationTypeMyFriend = 1,
    UserRelationTypeThirdFriend = 2,
    UserRelationTypeMe = 3,
    UserRelationTypeFriend = 4,
    UserRelationTypeWaitAccept = 5
} UserRelationType;

@interface User : NSObject<NSCoding>

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic) NSInteger gender;
@property (nonatomic) NSInteger type;
@property (nonatomic) BOOL hasProfile;
@property (nonatomic) NSInteger points;
@property (nonatomic) NSInteger degree;
@property (nonatomic) NSTimeInterval opUpdateTime;
@property (nonatomic) BOOL newUser;
@property (nonatomic) UserRelationType relation;

- (NSString *)displayName;

- (BOOL)checkValid;

@end
