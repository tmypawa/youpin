//
//  YouPinBaseViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-29.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICSDrawerController.h"
#import "CRProductTour.h"

@interface ShaibaBaseViewController : UIViewController
@property (nonatomic) BOOL autoCancelRequestWhenDisappear;

- (void)showTutorialWithAttachedView:(UIView *)attachedView title:(NSString *)title offset:(CGPoint)offset;
- (void)showTutorialWithAttachedView:(UIView *)attachedView title:(NSString *)title offset:(CGPoint)offset showConfirmButton:(BOOL)showButton ;

@end
