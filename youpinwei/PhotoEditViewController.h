//
//  WatermarkViewController.h
//  youpinwei
//
//  Created by tmy on 14-9-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTInputView.h"
#import "GPUImagePicture.h"

@interface PhotoEditViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, JTInputViewDelegate, UITextViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, strong) GPUImagePicture *internalSourcePicture1;
@property (nonatomic, strong) GPUImagePicture *internalSourcePicture2;

@end
