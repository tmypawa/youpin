//
//  MainItemCell.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "SlideImageView.h"

@interface MainItemCell : UITableViewCell<UICollectionViewDataSource, UICollectionViewDelegate, SlideImageViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *avatarView;
@property (strong, nonatomic) IBOutlet UILabel *friendLabel;
@property (strong, nonatomic) UIButton *titleBtn;
@property (strong, nonatomic) IBOutlet UIImageView *levelIconView;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet JTAnimationButton *praiseBtn;
@property (strong, nonatomic) IBOutlet JTAnimationButton *favBtn;
@property (strong, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet UILabel *personLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentCountLabel;
@property (weak, nonatomic) IBOutlet DWTagList *tagsView;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *timeIconView;
@property (strong, nonatomic) IBOutlet UIImageView *contactIconView;
@property (strong, nonatomic) IBOutlet UIButton *photoCountBtn;
@property (strong ,nonatomic) void(^onThumbnailSelected)();
@property (nonatomic) BOOL isScaled;

+ (CGFloat)heightForTitle:(NSString *)title thumbnailCount:(NSInteger)thumbnailCount hasComments:(BOOL)hasComments hasTags:(BOOL)hasTags scaled:(BOOL)isScaled;

- (void)showTagView:(BOOL)isShow;
- (void)updateLayout:(BOOL)shouldScale;
- (void)showThumbnails:(NSArray *)thumbnailsArray;
- (void)setText:(NSString *)text;
- (void)enableScrolling:(BOOL)isEnable;

@end
