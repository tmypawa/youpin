//
//  MainItemCell.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "MainItemCell.h"
#import "UIKit+RoundImage.h"

@implementation MainItemCell
{
    
    IBOutlet UIPageControl *_pageControl;
    IBOutlet SlideImageView *_thumbnailCollectionView;
    IBOutlet UIView *_lineView;
    __weak IBOutlet UIView *_actionBarView;
    NSArray  *_thumbnails;
    IBOutlet UIImageView *_bottomShadowView;
    IBOutlet UIView *_bgView;
    UIImageView *_animatedThumbnailView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    self.backgroundColor = MAIN_CELL_BG_COLOR;
    self.contentView.backgroundColor = MAIN_CELL_BG_COLOR;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _bottomShadowView.image = [[UIImage imageNamed:@"bg_shadow"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    
    [self.avatarView makeRound];
    self.avatarView.backgroundColor = [UIColor whiteColor];
    
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.numberOfLines = 0;
    self.personLabel.textColor = MainCellPersonTitleColor;
    self.titleLabel.textColor = MainCellTitleColor;
    self.commentLabel.textColor = MainCellInfoColor;
    self.thumbnailView.backgroundColor = DEFAULT_IMAGE_BG_COLOR;
    self.timeLabel.textColor = MainCellTitleColor;
    
    self.titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.titleBtn.backgroundColor = [UIColor clearColor];
    self.titleBtn.frame = self.personLabel.bounds;
    self.titleBtn.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight;
    self.titleBtn.adjustsImageWhenHighlighted = NO;
    [self.personLabel addSubview:self.titleBtn];
    self.personLabel.userInteractionEnabled = YES;
    
    self.praiseBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.praiseBtn.normalImage = [UIImage imageNamed:@"icon_like_normal"];
    self.praiseBtn.highlightImage = [UIImage imageNamed:@"icon_like_done"];
    self.praiseBtn.highlightScale = 1;
    
    self.favBtn.normalImage = [UIImage imageNamed:@"icon_dreamlist_normal"];
    self.favBtn.highlightImage = [UIImage imageNamed:@"icon_dreamlist_done"];
    self.favBtn.highlightScale = 1;
    
    [self.photoCountBtn setBackgroundImage:[[UIImage imageNamed:@"btn_numofpics"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    
    self.tagsView.font = [UIFont systemFontOfSize:14];
    self.tagsView.scrollEnabled = NO;
    self.tagsView.backgroundColor = [UIColor clearColor];
    [self.tagsView setTagBackgroundColor:[UIColor clearColor]];
    self.tagsView.borderWidth = 0;
    self.tagsView.labelMargin = 0;
    self.tagsView.horizontalPadding = 3;
    self.tagsView.verticalPadding = 0;
    self.tagsView.bottomMargin = 0;
    [self.tagsView setCornerRadius:4];
    [self.tagsView setTextColor:TAG_TEXT_COLOR];
    self.tagsView.highlightedBackgroundColor = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1];
    self.tagsView.textShadowOffset = CGSizeMake(0, 0);
    self.tagsView.automaticResize = YES;
    
    _thumbnailCollectionView._zMarginValue = 1;
    _thumbnailCollectionView._xMarginValue = 20;
    _thumbnailCollectionView.delegate = self;
    _thumbnailCollectionView.borderColor = [UIColor whiteColor];
    _thumbnailCollectionView.delegate = self;
    _thumbnailCollectionView.applyAlpha = YES;
    _thumbnailCollectionView.numberOfdisplayLayers = 3;
    _thumbnailCollectionView.imageViewBackgroundColor = [UIColor lightGrayColor];
    
    _animatedThumbnailView = [[UIImageView alloc] initWithFrame:_thumbnailCollectionView.frame];
    _animatedThumbnailView.backgroundColor = [UIColor lightGrayColor];
    _animatedThumbnailView.layer.borderColor = _thumbnailCollectionView.borderColor.CGColor;
    _animatedThumbnailView.layer.borderWidth = 2.f;
    _animatedThumbnailView.contentMode = UIViewContentModeScaleAspectFill;
    _animatedThumbnailView.clipsToBounds = YES;
    _animatedThumbnailView.hidden = YES;
    [self.contentView addSubview:_animatedThumbnailView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (CGFloat)heightForTitle:(NSString *)title thumbnailCount:(NSInteger)thumbnailCount hasComments:(BOOL)hasComments hasTags:(BOOL)hasTags scaled:(BOOL)isScaled  {
    NSString *trimmedTitle = [title stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" \n"]];
    CGFloat maxHeight = ([UIFont systemFontOfSize:14].lineHeight + LINE_SPACE) * 5;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:LINE_SPACE];
//    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:trimmedTitle attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName: paragraphStyle}];
    
    CGFloat titleHeight = [attributedString boundingRectWithSize:CGSizeMake(screenWidth - 20, maxHeight) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine context:nil].size.height;
    titleHeight = ceilf(titleHeight);
//    CGFloat titleHeight = [trimmedTitle boundingRectWithSize:CGSizeMake(300, maxHeight) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    if (titleHeight > maxHeight) {
        titleHeight = maxHeight;
    }
    
    CGFloat height = titleHeight + 408.5f;
    
    height += 10;
    
    if (hasTags) {
        height += 33;
    }
    
    height += 5;
    
    return height;
}

- (void)showTagView:(BOOL)isShow {
    self.tagsView.hidden = !isShow;
}

- (void)showThumbnails:(NSArray *)thumbnailsArray {
    if (_thumbnails != thumbnailsArray) {
        _thumbnails = nil;
        [_thumbnailCollectionView reLoadUIview];
        [_thumbnailCollectionView bringSubviewToFront:self.photoCountBtn];
        _thumbnails = thumbnailsArray;
    }
}

- (void)enableScrolling:(BOOL)isEnable {
    _thumbnailCollectionView.userInteractionEnabled = isEnable;
}

- (void)setText:(NSString *)text {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:LINE_SPACE];
//    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName: paragraphStyle}];
    
    self.titleLabel.attributedText = attributedString;
}

- (void)updateLayout:(BOOL)shouldScale {
    [self.contentView.layer removeAllAnimations];
    [_animatedThumbnailView.layer removeAllAnimations];
    [self enableScrolling:YES];
    self.isScaled = shouldScale;
    
//    void(^animatedUpdateLayoutBlock)(void) = ^(){
//        if (_thumbnails) {
//            if (_thumbnails.count > 1 && !shouldScale) {
//                _thumbnailCollectionView._xMarginValue = 20;
//                SetFrameSize(_thumbnailCollectionView, 300, 300);
//            }else {
//                _thumbnailCollectionView._xMarginValue = 30;
//                SetFrameSize(_thumbnailCollectionView, 310, 310);
//            }
//        }
//        
//        if (_thumbnails && _thumbnails.count > 0) {
//            [_thumbnailCollectionView reLoadUIview];
//        }
//    };

//    if (shouldScale) {
//        [self enableScrolling:NO];
//        SetFrameSize(_animatedThumbnailView, 300, 300);
//        [_animatedThumbnailView setImageWithURL:_thumbnails[0] placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectNone];
//        _animatedThumbnailView.hidden = NO;
//        
//        [UIView animateWithDuration:0.5f delay:0 usingSpringWithDamping:0.8f initialSpringVelocity:0 options:0 animations:^{
//            SetFrameSize(_animatedThumbnailView, 310, 310);
//        } completion:^(BOOL finished) {
//            animatedUpdateLayoutBlock();
//            _thumbnailCollectionView.hidden = NO;
//            _animatedThumbnailView.hidden = YES;
//            _animatedThumbnailView.image = nil;
//            [self enableScrolling:YES];
//        }];
//    }else {
//        animatedUpdateLayoutBlock();
//    }
    
    if (_thumbnails && _thumbnails.count > 0) {
        [_thumbnailCollectionView reLoadUIview];
    }
    
    CGFloat titleLabelY = 357;
    
    SetFrameY(self.titleLabel, titleLabelY);
    
    CGFloat maxHeight = (self.titleLabel.font.lineHeight + LINE_SPACE) * 5;
    
    CGFloat titleHeight = [self.titleLabel.attributedText boundingRectWithSize:CGSizeMake(screenWidth - 20, maxHeight) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingTruncatesLastVisibleLine context:nil].size.height;
    titleHeight = ceilf(titleHeight);
    CGRect frame = self.titleLabel.frame;
    frame.size.height = titleHeight;
    self.titleLabel.frame = frame;
    
    
//    CGFloat titleHeight = [self.titleLabel.text boundingRectWithSize:CGSizeMake(self.titleLabel.frame.size.width, maxHeight) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
//    SetFrameHeight(self.titleLabel, titleHeight);
    if (!self.tagsView.hidden) {
        SetFrameY(self.tagsView, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 10);
        SetFrameHeight(self.tagsView, 18);
        SetFrameY(_actionBarView, CGRectGetMaxY(self.tagsView.frame) + 10);
    }else {
        SetFrameY(_actionBarView, CGRectGetMaxY(self.titleLabel.frame) + 10);
    }
    
    SetFrameHeight(_bgView, CGRectGetMaxY(_actionBarView.frame));
    
    [self.personLabel sizeToFit];
    
    [self.timeLabel sizeToFit];
    CGRect fitTimeSize = self.timeLabel.frame;
    SetFrameX(self.timeLabel, screenFrame.size.width - fitTimeSize.size.width - 7);
    SetFrameX(self.timeIconView, self.timeLabel.frame.origin.x - 17);
    self.timeLabel.center = CGPointMake(self.timeLabel.center.x, self.avatarView.center.y);
    self.timeIconView.center = CGPointMake(self.timeIconView.center.x, self.avatarView.center.y);
    
    if (self.friendLabel.hidden) {
        CGPoint nameCenter = self.personLabel.center;
        nameCenter.y = self.avatarView.center.y;
        self.personLabel.center = nameCenter;
    }else {
        SetFrameY(self.personLabel, 5);
    }
    
    SetFrameX(self.levelIconView, CGRectGetMaxX(self.personLabel.frame) + 5);
    self.levelIconView.center = CGPointMake(self.levelIconView.center.x, self.personLabel.center.y);
}


- (NSInteger)numberOfImagesInSlideImageView:(SlideImageView *)slideImageView {
    return _thumbnails ? _thumbnails.count : 0;
}

- (NSURL *)slideImageView:(SlideImageView *)slideImageView imageUrlForIndex:(int)index {
    NSString *urlStr = _thumbnails[index];
    NSURL *url = [NSURL URLWithString:urlStr];
    return url;
}

- (void)SlideImageViewDidClickWithIndex:(int)index
{
    if (self.onThumbnailSelected) {
        self.onThumbnailSelected();
    }
}


@end
