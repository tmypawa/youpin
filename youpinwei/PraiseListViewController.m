//
//  PraiseListViewController.m
//  youpinwei
//
//  Created by tmy on 14/12/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PraiseListViewController.h"
#import "FriendInfoViewController.h"
#import "PraiseViewCell.h"
#import "User.h"

@interface PraiseListViewController ()

@end

@implementation PraiseListViewController
{
    NSMutableArray *_userList;
}

- (void)dealloc {
    [[YouPinAPI sharedAPI] cancelRequestForSender:self];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _userList = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[YouPinAPI sharedAPI] fetchPraiseListWithTatesId:_tasteItem.productId sender:self callback:^(NSArray *users, BOOL hasNext, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        if (!error) {
            [_userList addObjectsFromArray:users];
            [self.tableView reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewFriendInfo:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    User *user = _userList[index];
    
    if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:user.userId]) {
        PersonalInfoViewController *personInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
        [self.navigationController pushViewController:personInfoController animated:YES];
    }else {
        FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
        friendInfoController.userId = user.userId;
        [self.navigationController pushViewController:friendInfoController animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _userList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PraiseViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PraiseViewCell" forIndexPath:indexPath];
    User *user = _userList[indexPath.row];
    
    if (cell.avatarView.tag == 0) {
        [cell.avatarView addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.avatarView.tag = indexPath.row + 1;
    [cell.avatarView setImageWithURL:user.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    cell.nicknameLabel.text = [user displayName];
    cell.timeLabel.text = [[NSDate dateWithTimeIntervalSince1970:user.opUpdateTime/1000] prettyShortString];
    cell.levelconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_lv_%ld", user.degree]];
    if (user.degree > 0) {
        cell.infoLabel.hidden = NO;
        NSMutableAttributedString *levelTitleStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"体验家%ld级", user.degree]];
        [levelTitleStr addAttributes:@{NSForegroundColorAttributeName: LevelColor(user.degree)} range:NSMakeRange(3, levelTitleStr.length - 4)];
        cell.infoLabel.attributedText = levelTitleStr;
    }else {
        cell.infoLabel.hidden = YES;
    }
    
    [cell updateLayout];
    
    return cell;
}


@end
