//
//  PostSearchResultController.m
//  youpinwei
//
//  Created by tmy on 15/3/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PostSearchResultController.h"

@implementation PostSearchResultController
{
    NSInteger   _itemStart;
}

- (void)dealloc {
    [self.tableView removePullToRefreshActionHandler];
}


- (void)internalInit {
    _tasteItems = [[NSMutableArray alloc] init];
    _currentOperationItemIndex = -1;
    _hasNext = YES;
    _scaledRowIndexSet = [[NSMutableIndexSet alloc] init];
}

- (void)awakeFromNib {
    [self internalInit];
}

- (void)viewDidLayoutSubviews {
//    self.tableView.frame = CGRectMake(0, 0, screenWidth, screenHeight - 64);
//    CGFloat bottomInset = _tag && _tag.showPhoto ? _actionView.frame.size.height : 0;
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottomInset, 0);
//    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)registerNotifications {
    
}

- (void)initView {
    self.title = self.keyword;
    [self showHUDViewWithTitle:@""];
    [self refreshData];
}

- (void)loadPullRereshAnimation {
    [super loadPullRereshAnimation];
    self.tableView.pullToRefreshView.originalTopInset = 0;
}

- (void)handleForNavigationAppear:(UIScrollView *)scrollView {
    
}

- (void)refreshData {
    _itemStart = 0;
    [[YouPinAPI sharedAPI] queryPostWithKeyword:self.keyword start:_itemStart sender:self callback:^(NSArray *items, NSInteger start, NSError *error) {
        [self hideHUDView];
        if (!error) {
            _itemStart = start;
            [self reloadItems:items hasNext:_hasNext error:error];
            self.tableView.hidden = NO;
        }
    }];
}


- (void)loadNextData {
    [[YouPinAPI sharedAPI] queryPostWithKeyword:self.keyword start:_itemStart sender:self callback:^(NSArray *items, NSInteger start, NSError *error) {
        if (!error) {
            _itemStart = start;
            [self loadNextItems:items hasNext:_hasNext error:error];
        }
        
    }];
}

- (void)loadNextItems:(NSArray *)items hasNext:(BOOL)hasNext error:(NSError *)error {
    if (!error) {
        if (items.count > 0) {
            [_tasteItems addObjectsFromArray:items];
            [self.tableView reloadData];
        }else {
            _hasNext = NO;
            [self.tableView dismissLoadMore];
        }
        
        [self.tableView endLoadMoreWithHandler:^(UIView *customView) {
            [((UIImageView *)customView) stopAnimating];
            customView.hidden = YES;
        }];
        
    }else {
        
    }
}

@end
