//
//  MainViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "MainViewController.h"
#import "TagViewController.h"
#import "GoodsChooseController.h"
#import "LoginViewController.h"
#import "AlbumPhotoViewController.h"
#import "PhotoEditViewController.h"
#import "PhotoCropViewController.h"
#import "PublishViewController.h"
#import "PBWebViewController.h"
#import "FriendInfoViewController.h"
#import "FriendAddingMethodController.h"
#import "MainItemCell.h"
#import "TasteItem.h"
#import "YouPin.h"
#import "JTAddressBookManager.h"
#import "NSDate+Utility.h"


@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.autoCancelRequestWhenDisappear = NO;
    }
    return self;
}

- (void)dealloc {
    [_tableView removePullToRefreshActionHandler];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)internalInit {
    _tasteItems = JTGlobalCache.recentTastes;
    _currentOperationItemIndex = -1;
    _hasNext = YES;
    
    _scaledRowIndexSet = nil;
    _scaledRowIndexSet = [[NSMutableIndexSet alloc] init];
}

- (void)awakeFromNib {
    [self internalInit];
    [self registerNotifications];
    
    _publishStatusController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishStatusController"];
    _publishStatusController.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     
    self.view.backgroundColor = LIST_BG_COLOR;
    
    _todayTimestamp = [self dayTimestampFromDate:[NSDate date]];
    
    if (_tasteItems.count > 0) {
        [self loadPullRereshAnimation];
        [self addLoadMoreView];
    }
    
    [self initView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    _isViewBeingShow = NO;
    
    self.rdv_tabBarController.navigationItem.rightBarButtonItem = nil;
    self.tableView.scrollsToTop = NO;
    [self resetNavigationBar];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _isViewBeingShow = YES;
    
    [((ShaibaViewController *)self.rdv_tabBarController) setNavigationTitle:@"朋友们"];
    if (JTGlobalCache.isLogin) {
        self.rdv_tabBarController.navigationItem.rightBarButtonItem = _addFriendItem;
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    if ([UIApplication sharedApplication].statusBarHidden) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    
    if (_currentOperationItemIndex != -1) {
        [self updateActionBarForCellWithRowIndex:_currentOperationItemIndex];
    }
    
    _currentOperationItemIndex = -1;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.tableView.scrollsToTop = YES;
    
    if ([self class] == [MainViewController class]) {
        if (JTGlobalCache.isLogin && (!_tasteItems || _tasteItems.count == 0) && _tableView.hidden) {
            [_noItemsView removeFromSuperview];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self refreshData];
        }
    }
    
    if (!_tableView.tableHeaderView) {
        [_tableView setTableHeaderView:_publishStatusController.view];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGFloat fixedHeight = screenHeight - 48;
    self.tableView.frame = CGRectMake(0, self.view.frame.size.height - fixedHeight, screenWidth, fixedHeight);
}

- (void)handleForDoubleTapTab {
    CGFloat baseOffset = -self.tableView.contentInset.top;
    if (self.tableView.contentOffset.y > baseOffset) {
        [self showNavigationBar:YES];
        [self.tableView setContentOffset:CGPointMake(0, -64) animated:YES];
    }else if(!self.tableView.pullToRefreshView.state != UZYSGIFPullToRefreshStateLoading){
        [self.tableView triggerPullToRefresh];
    }
}

- (void)addLoadMoreView {
    UIImageView *loadingMoreView = [[UIImageView alloc] initWithImage:[UIImage animatedImageNamed:@"R" duration:1.6]];
    loadingMoreView.frame = CGRectMake(0, 0, screenWidth, 56);
    loadingMoreView.center = CGPointMake(screenWidth/2, loadingMoreView.center.y);
    loadingMoreView.contentMode = UIViewContentModeCenter;
    loadingMoreView.backgroundColor = MAIN_CELL_BG_COLOR;
    [loadingMoreView stopAnimating];
    loadingMoreView.hidden = YES;
    
    self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 0, 0);
    self.tableView.backgroundColor = LIST_BG_COLOR;
    self.tableView.backgroundView = nil;
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.tableView setupLoadMoreWithCustomView:loadingMoreView];
    [self.tableView addTarget:self forLoadMoreAction:@selector(loadNext:)];
}

- (void)loadPullRereshAnimation {
    NSMutableArray *pullAnimations = [[NSMutableArray alloc] init];
    NSMutableArray *loadingAnimations = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 23; ++i) {
        [pullAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
    for (int i = 0; i < 45; ++i) {
        [loadingAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
    
    __weak  MainViewController *_self = self;
    __weak  UITableView *__tableView = _tableView;
    [_tableView addPullToRefreshActionHandler:^{
        if (__tableView.superview) {
            [_self refreshData];
        }
    } ProgressImages:pullAnimations LoadingImages:loadingAnimations ProgressScrollThreshold:20 LoadingImagesFrameRate:32];
    _tableView.showAlphaTransition = NO;
    _tableView.showVariableSize = NO;
    _tableView.showPullToRefresh = YES;
    _tableView.showTitle = NO;
 
    [_tableView addTopInsetInPortrait:0 TopInsetInLandscape:0];
    _tableView.pullToRefreshView.originalTopInset = 64;
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidBindAccount:) name:YouPinNotificationUserDidBindAccount object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:YouPinNotificationUserDidLogin object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogout:) name:YouPinNotificationUserDidLogout object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTasteItemDidPublish:) name:YouPinNotificationNewTasteItemDidPublish object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTasteItemFailedToPublish:) name:YouPinNotificationNewTasteItemFailedToPublish object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tasteItemDidRemove:) name:YouPinNotificationDidRemoveTasteItem object:nil];
}

- (void)initView {
    [_registerBtn setBackgroundImage:[[UIImage imageNamed:@"btn_profile_ok_normal"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    _noItemsView.center = CGPointMake(self.view.center.x, self.view.center.y - 54);
    
    [_addFriendBtn setBackgroundImage:[[UIImage imageNamed:@"btn_red_normal"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    [_addFriendBtn setBackgroundImage:[[UIImage imageNamed:@"btn_red_highlight"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateHighlighted];
    
    SetFrameSize(_publishStatusController.view, screenWidth, 0);
    
    [_registerView removeFromSuperview];
    [_addressTipView removeFromSuperview];
    [_addressTurnOnView removeFromSuperview];
    [_noItemsView removeFromSuperview];
    
    if (JTGlobalCache.isLogin) {
        if (_tasteItems && _tasteItems.count > 0) {
            [self.tableView reloadData];
            if (JTGlobalCache.friendTimelineBadge > 0) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.tableView triggerPullToRefresh];
                });
            }
        }else {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self refreshData];
        }
    }else {
        self.tableView.hidden = YES;
        [self.view addSubview:_registerView];
        
        _registerView.center = CGPointMake(self.view.center.x, self.view.center.y - 64);
    }
}

- (void)showAddFriendTip:(BOOL)show {
    if (show) {
        _noItemsView.center = CGPointMake(screenWidth / 2, (screenHeight - 64 - self.rdv_tabBarController.tabBar.frame.size.height) / 2);
        [self.view addSubview:_noItemsView];
        self.rdv_tabBarController.navigationItem.rightBarButtonItem = _addFriendItem;
    }else {
        self.tableView.hidden = NO;
        [_noItemsView removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showSyncAddressBookTipView {
    DialogViewController *dialogController = [self.storyboard instantiateViewControllerWithIdentifier:@"DialogViewController"];
    [dialogController setTitle:@"启用通讯录匹配" subtitle:@"" icon:[UIImage imageNamed:@"icon_Mail list"] descriptionText:@"启用手机通讯录匹配，才能时刻关注好友买了什么宝贝~您的通讯录将以加密形式保存在服务器，不做其他任何用途" okButtonTitle:@"立即启用" cancelButtonTitle:@"以后再说"];
    dialogController.delegate = self;
    dialogController.tag = 100;
    [self.navigationController presentViewController:dialogController animated:NO completion:nil];
}

- (void)showPublishSuccessToast {
    UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    shareBtn.frame = CGRectMake(0, 0, 101, 29);
    shareBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [shareBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [shareBtn setTitle:@"分享给好友" forState:UIControlStateNormal];
    [shareBtn setImage:[UIImage imageNamed:@"icon_share_s_normal"] forState:UIControlStateNormal];
    [shareBtn setBackgroundImage:[[UIImage imageNamed:@"btn_blue_normal"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    [shareBtn setBackgroundImage:[[UIImage imageNamed:@"btn_blue_highlight"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateHighlighted];
    [shareBtn addTarget:self action:@selector(shareToFriend:) forControlEvents:UIControlEventTouchUpInside];
 
    [self showNavigationToastWithTitle:@"宝贝发布成功" image:[UIImage imageNamed:@"icon_released"] actionView:shareBtn];
}

- (void)showPublishFailToast {
    [self showStatusBarToastWithTitle:@"发布失败，请检查网络设置"];
}

- (void)newTasteItemDidPublish:(NSNotification *)notification {
    TasteItem *newTasteItem = notification.userInfo[YouPinNotificationPublishTasteItemKey];
    if (newTasteItem && _tasteItems) {
        [_tasteItems insertObject:newTasteItem atIndex:0];

        if (_tableView) {
            if (_isViewBeingShow) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (_tasteItems.count > 1) {
                        [_tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                    }else {
                        [_tableView reloadData];
                    }
                });
            }else {
                [_tableView reloadData];
            }
        }
        
        [self showPublishSuccessToast];
    }
}

- (void)newTasteItemFailedToPublish:(NSNotification *)notification {
    [self showPublishFailToast];
}

- (void)tasteItemDidRemove:(NSNotification *)notification {
    if (_currentOperationItemIndex != -1) {
        [_tasteItems removeObjectAtIndex:_currentOperationItemIndex];
        [self.tableView reloadData];
        _currentOperationItemIndex = -1;
        [JTGlobalCache synchronize];
    }
}

- (void)userDidLogin:(NSNotification *)notification {
    if (JTGlobalCache.currentUser && self.isViewLoaded) {
        [self initView];
    }
}

- (void)userDidLogout:(NSNotification *)notification {
    [[YouPinAPI sharedAPI] cancelRequestForSender:self];
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    
    [self internalInit];
    [self initView];
}

- (void)userDidBindAccount:(NSNotification *)notification {
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (NSTimeInterval)startTimestamp {
//    NSTimeInterval startTimestamp = 0;
//    if (_tasteItems && _tasteItems.count > 0) {
//        startTimestamp = [_tasteItems[0] createTime];
//    }
    
    return 0;
}

- (void)shareToFriend:(UIButton *)sender {
    [CRToastManager dismissNotification:YES];
    
    TasteItem *tasteItem = _tasteItems[0];
    [self showShareMenuWithTasteItem:tasteItem];
}

- (NSTimeInterval)endTimestamp {
    NSTimeInterval endTimestamp = 0;
    if (_tasteItems.count > 0) {
        endTimestamp = [[_tasteItems lastObject] updateTime];
    }
    return endTimestamp;
}

- (NSTimeInterval)dayTimestampFromDate:(NSDate *)date {
    NSDateComponents *todayComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    NSTimeInterval dayTimestamp = [[[NSCalendar currentCalendar] dateFromComponents:todayComponents] timeIntervalSince1970];
    return dayTimestamp;
}


- (IBAction)refresh:(id)sender {
    [self refreshData];
}

- (void)refreshData {
    [[YouPinAPI sharedAPI] fetchTasteTimelineWithStartTimestamp:[self startTimestamp] endTimestamp:0 Sender:self callback:^(NSArray *items, BOOL hasNext, NSError *error) {
        [self reloadItems:items hasNext:hasNext error:error];
        
        if (JTGlobalCache.friendTimelineBadge > 0) {
            JTGlobalCache.friendTimelineBadge = 0;
            [JTGlobalCache synchronize];
            self.rdv_tabBarItem.badgeValue = @"";
        }
    }];
}

- (void)loadNext:(UIImageView *)loadingView {
    if (!_hasNext) {
        return;
    }
    
    loadingView.hidden = NO;
    [loadingView startAnimating];
    [self.tableView showLoadMoreView];
    
    [self loadNextData];
}

- (void)loadNextData {
    [[YouPinAPI sharedAPI] fetchTasteTimelineWithStartTimestamp:0 endTimestamp:[self endTimestamp] Sender:self callback:^(NSArray *items, BOOL hasNext, NSError *error) {
        [self loadNextItems:items hasNext:hasNext error:error];
    }];
}

- (void)reloadItems:(NSArray *)items hasNext:(BOOL)hasNext error:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    
    _todayTimestamp = [self dayTimestampFromDate:[NSDate date]];
    
    if (!error) {
        [_tasteItems removeAllObjects];
        [_tasteItems addObjectsFromArray:items];
        
        [JTGlobalCache synchronize];
        
        [self.tableView reloadData];
        
        if (!_hasNext && _tasteItems.count > 0) {
            _hasNext = YES;
        }
        
        [self showAddFriendTip:(_tasteItems.count == 0)];
    }else {
        
    }
    
    if (!self.tableView.pullToRefreshView) {
        [self loadPullRereshAnimation];
    }else {
        [self.tableView stopRefreshAnimation];
    }
    
    if (!self.tableView.loadMoreView) {
        [self addLoadMoreView];
    }
    
    if (_hasNext) {
        [self.tableView showLoadMoreView];
    }
}

- (void)loadNextItems:(NSArray *)items hasNext:(BOOL)hasNext error:(NSError *)error {
    if (!error) {
        if (items.count > 0) {
            [_tasteItems addObjectsFromArray:items];
            [JTGlobalCache synchronize];
            [self.tableView reloadData];
        }else {
            _hasNext = NO;
            [self.tableView dismissLoadMore];
        }
        
        [self.tableView endLoadMoreWithHandler:^(UIView *customView) {
            [((UIImageView *)customView) stopAnimating];
            customView.hidden = YES;
        }];
    }else {
        
    }
}

- (void)resetNavigationBar {
    _lastContentOffsetY = 0;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    ((ShaibaViewController *)self.rdv_tabBarController).navigationTitleView.alpha = 1;
    self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    
}

- (void)showNavigationBar:(BOOL)show {
    if (self.navigationController.navigationBar.hidden == !show) {
        return;
    }
    
    [self.navigationController setNavigationBarHidden:!show animated:YES];
    
    
    [UIView animateWithDuration:show? 0.3f : 0.15f animations:^{
        self.tableView.contentInset = UIEdgeInsetsMake((show? 64 : 0), 0, 0, 0);
        ((ShaibaViewController *)self.rdv_tabBarController).navigationTitleView.alpha = show;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)handleForNavigationAppear:(UIScrollView *)scrollView {
    
    CGFloat maxContentOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
    if (_lastContentOffsetY != 0 && scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < maxContentOffsetY) {
        CGFloat offsetY = scrollView.contentOffset.y - _lastContentOffsetY;
        if (scrollView.contentOffset.y >= 0) {
            if (offsetY >= 0) {
                [self showNavigationBar:NO];
            }else if(offsetY < - 20){
                [self showNavigationBar:YES];
            }
        }
    }else if (scrollView.contentOffset.y < 0 && self.navigationController.navigationBar.hidden) {
        [self showNavigationBar:YES];
    }
    
    if (scrollView.contentOffset.y >= 0) {
        _lastContentOffsetY = scrollView.contentOffset.y;
    }
}

- (void)scaleCellThumbnailAtIndex:(NSInteger)rowIndex {
    if (rowIndex != NSNotFound) {
        MainItemCell *scaleCell = (MainItemCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]];
        
        if (![_scaledRowIndexSet containsIndex:rowIndex] && !scaleCell.isScaled) {
            [scaleCell enableScrolling:NO];
            [_scaledRowIndexSet addIndex:rowIndex];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                TasteItem *item = _tasteItems[rowIndex];
                if (item.realShotPics && item.realShotPics.count > 1) {
                    MainItemCell *scaleCell = (MainItemCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]];
                    [scaleCell updateLayout:YES];
                }
                [_scaledRowIndexSet removeIndex:rowIndex];
            });
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(!self.view.superview)
        return;
    
    [self handleForNavigationAppear:scrollView];
    
    if (_tasteItems.count > 0 && !self.tableView.isLoadingMore && _hasNext) {
        float maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
        float offsetY = scrollView.contentOffset.y;
        if (offsetY >= maxOffsetY - 170) {
            [self.tableView startLoadMore];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
}

- (void)showRegisterAlert {
    AlertWithActionsAndTag(1000, @"登录晒吧", @"继续操作之前先登录一下吧~~", @"看看再说", @"马上GO~~");
}


- (IBAction)addFriend:(id)sender {
    FriendAddingMethodController *addingMethodController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendAddingMethodController"];
    [self.navigationController pushViewController:addingMethodController animated:YES];
}

- (IBAction)publish:(id)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"从淘宝选择宝贝",@"拍照", nil];
        actionSheet.tag = 2000;
        [actionSheet showInView:self.navigationController.view];
    }
}

- (IBAction)registerAccount:(id)sender {
    [self bindTaobao];
}

- (void)bindTaobao {
    [self presentLoginController];
}

- (void)viewFriendInfo:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    TasteItem *tastItem = _tasteItems[index];
    
    if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:tastItem.userId]) {
        PersonalInfoViewController *personInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
        [self.navigationController pushViewController:personInfoController animated:YES];
    }else {
        FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
        friendInfoController.userId = tastItem.userId;
        [self.navigationController pushViewController:friendInfoController animated:YES];
    }
}


- (void)fav:(UIButton *)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else {
        NSInteger index = sender.tag - 1;
        TasteItem *tastItem = _tasteItems[index];
        tastItem.addWishList = !tastItem.addWishList;
        if (tastItem.addWishList) {
            tastItem.wishcount ++;
        }else {
            tastItem.wishcount --;
        }
        
        [((JTAnimationButton *)sender) showHightlighted:tastItem.addWishList animate:YES];
        if (tastItem.wishcount > 0) {
            [((JTAnimationButton *)sender) setTitle:[NSString stringWithFormat:@"%d", tastItem.wishcount] forState:UIControlStateNormal];
        }else {
            [((JTAnimationButton *)sender) setTitle:@"心愿单" forState:UIControlStateNormal];
        }
        
        NSString *toastTitle = tastItem.addWishList ? @"已加入心愿单" : @"已移除心愿单";
        [self showToastWithTitle:toastTitle];
        
        [[YouPinAPI sharedAPI] addWishListWithTaste:tastItem.productId add:tastItem.addWishList callback:^(int wishCount, NSArray *currentWishListUsers, NSError *error) {
            if (!error) {
 
            }
        }];
    }
}

- (void)praise:(UIButton *)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else {
        NSInteger index = sender.tag - 1;
        TasteItem *tastItem = _tasteItems[index];
        if (!tastItem.pin) {
            tastItem.pin = !tastItem.pin;
            tastItem.pincount++;
            [sender setTitle:[NSString stringWithFormat:@"%d", tastItem.pincount] forState:UIControlStateNormal];
            [((JTAnimationButton *)sender) showHightlighted:tastItem.pin animate:YES];
            
            NSString *toastTitle = tastItem.pincount > 0? @"谢谢你送我1米阳光" : @"第一个点赞，奖励你1米阳光";
            [self showToastWithTitle:toastTitle];
            
            [[YouPinAPI sharedAPI] praiseTaste:tastItem.productId value:tastItem.pin? 1 : 2 callback:^(int pinCount, NSArray *pinUsers, NSError *error) {
                if (!error) {
                    tastItem.pincount = pinCount;
                    tastItem.pinUsers = pinUsers;
                }
            }];
        }
    }
}

- (void)comment:(UIButton *)sender {
    _currentOperationItemIndex = sender.tag - 1;
    TasteItem *item = _tasteItems[sender.tag - 1];
    DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
    detailController.tasteId = item.productId;
    detailController.relatedTasteItem = item;
    detailController.shouldCommentImmediately = YES;
    [self.navigationController pushViewController:detailController animated:YES];
}

- (void)updateActionBarForCellWithRowIndex:(NSInteger)rowIndex {
    TasteItem *item = _tasteItems[rowIndex];
    MainItemCell *cell = (MainItemCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]];
    if (cell) {
        [cell.praiseBtn showHightlighted:item.pin animate:NO];
        if (item.pincount > 0) {
            [cell.praiseBtn setTitle:[NSString stringWithFormat:@"%d", item.pincount] forState:UIControlStateNormal];
        }else {
            [cell.praiseBtn setTitle:@"赞" forState:UIControlStateNormal];
        }
        
        [cell.favBtn showHightlighted:item.addWishList animate:NO];
        [cell.commentBtn setTitle:[NSString stringWithFormat:@"%d", item.commentCount] forState:UIControlStateNormal];
    }
}

- (void)more:(UIButton *)sender {
    _currentOperationItemIndex = (int)sender.tag - 1;
    TasteItem *currentItem = _tasteItems[_currentOperationItemIndex];
    
    UIActionSheet *moreActionSheet = [[UIActionSheet alloc] initWithTitle:@"更多" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"查看宝贝详情", @"举报不良内容", nil];
    if (JTGlobalCache.currentUser && [currentItem.extraId isEqualToString:JTGlobalCache.currentUser.phone]) {
        [moreActionSheet addButtonWithTitle:@"删除"];
    }
    
    moreActionSheet.tag = 100;
    [moreActionSheet showInView:self.navigationController.view];
}


#pragma mark publishStatusControllerDelegate
- (void)publishStatusControllerDidChangeContentHeight:(PublishStatusController *)controller {
    if (_tasteItems.count == 0) {
        [self showAddFriendTip:NO];
    }
    
    [UIView animateWithDuration:0.35f animations:^{
        [_tableView setTableHeaderView:nil];
        [_tableView setTableHeaderView:_publishStatusController.view];
    }];
}

- (void)updateFriendInfoViewForCell:(MainItemCell *)cell withTasteItem:(TasteItem *)item {
    if (item.relation != UserRelationTypeThirdFriend) {
        cell.personLabel.text = [item displayName];
        if (item.relation == UserRelationTypeFriend || item.relation == UserRelationTypeMyFriend) {
            JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:item.extraId];
            if (person) {
                cell.friendLabel.hidden = NO;
                cell.friendLabel.text = person.fullName;
                cell.contactIconView.hidden = NO;
                cell.contactIconView.image = [UIImage imageNamed:@"icon_contact"];
            }else {
                cell.friendLabel.hidden = YES;
                cell.contactIconView.hidden = YES;
            }
        }else {
            cell.friendLabel.hidden = YES;
            cell.contactIconView.hidden = YES;
        }
    }else {
        cell.personLabel.text = item.nick;
        cell.friendLabel.text = [item displayName];
        cell.friendLabel.hidden = NO;
        cell.contactIconView.hidden = NO;
        cell.contactIconView.image = [UIImage imageNamed:@"icon_friendofriend"];
    }
}

#pragma mark DWTagListDelegate 
- (void)DWTagList:(DWTagList *)tagList willDisplayTagView:(DWTagView *)tagView atTagIndex:(NSInteger)tagIndex {
    NSInteger rowIndex = tagList.tag - 1;
    if (rowIndex >=0 && rowIndex < _tasteItems.count) {
        TasteItem *item = _tasteItems[rowIndex];
        if (item) {
            if (tagIndex < item.tags.count) {
                NSString *tag = item.tags[tagIndex];
                BOOL isSystemTag = [tag hasSuffix:@"-sys"];
                
                if (isSystemTag) {
                    tagView.button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
                }
            }
        }
    }
}

- (void)DWTagList:(DWTagList *)tagList selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex {
    TasteItem *item = _tasteItems[tagList.tag - 1];
    TagViewController *tagViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
    tagViewController.tagName = item.tags[tagIndex];
    [self.navigationController pushViewController:tagViewController animated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _tasteItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    TasteItem *item = _tasteItems[indexPath.row];
    return [MainItemCell heightForTitle:[item displayTitle] thumbnailCount:item.realShotPics.count hasComments:(item.comments && item.comments.count > 0) hasTags:(item.tags && item.tags.count > 0 ) scaled:[_scaledRowIndexSet containsIndex:indexPath.row]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" forIndexPath: indexPath];
    
    if (cell.praiseBtn.tag == 0) {
        [cell.praiseBtn addTarget:self action:@selector(praise:) forControlEvents:UIControlEventTouchUpInside];
        cell.tagsView.tagDelegate = self;
        [cell.favBtn addTarget:self action:@selector(fav:) forControlEvents:UIControlEventTouchUpInside];
        [cell.commentBtn addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        [cell.avatarView addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
        [cell.titleBtn addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.praiseBtn.tag = indexPath.row + 1;
    cell.favBtn.tag = indexPath.row + 1;
    cell.commentBtn.tag = indexPath.row + 1;
    cell.avatarView.tag = indexPath.row + 1;
    cell.titleBtn.tag = indexPath.row + 1;
    cell.tagsView.tag = indexPath.row + 1;
    
    __weak MainViewController *__self = self;
    cell.onThumbnailSelected = ^() {
        [__self tableView:tableView didSelectRowAtIndexPath:indexPath];
    };
    
    TasteItem *item = _tasteItems[indexPath.row];
    
    [cell showThumbnails:item.realShotPics];
    
    [self updateFriendInfoViewForCell:cell withTasteItem:item];
    
    [cell.avatarView setImageWithURL:item.headImage placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    
    [cell setText:[item displayTitle]];
    
    [cell.praiseBtn showHightlighted:item.pin animate:NO];
    if (item.pincount > 0) {
        [cell.praiseBtn setTitle:[NSString stringWithFormat:@"%d", item.pincount] forState:UIControlStateNormal];
    }else {
        [cell.praiseBtn setTitle:@"赞" forState:UIControlStateNormal];
    }
    
    if (item.wishcount > 0) {
        [cell.favBtn setTitle:[NSString stringWithFormat:@"%d", item.wishcount] forState:UIControlStateNormal];
    }else {
        [cell.favBtn setTitle:@"心愿单" forState:UIControlStateNormal];
    }
    
    [cell.favBtn showHightlighted:item.addWishList animate:NO];
    
    [cell.commentBtn setTitle:[NSString stringWithFormat:@"%d", item.commentCount] forState:UIControlStateNormal];
    
    NSTimeInterval dayTimestamp = [self dayTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:item.updateTime/1000]];
    if (_todayTimestamp - dayTimestamp <= 1 * 24 * 3600) {
        cell.timeLabel.hidden = NO;
        cell.timeIconView.hidden = NO;
        cell.timeLabel.text = [[NSDate dateWithTimeIntervalSince1970:item.updateTime/1000] prettyShortString];
    }else {
        cell.timeLabel.hidden = YES;
        cell.timeIconView.hidden = YES;
    }
    
    
    if (item.degree > 0) {
        cell.levelIconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_lv_%ld", item.degree]];
        cell.levelIconView.hidden = NO;
    }else {
        cell.levelIconView.hidden = YES;
    }
    
    
    if (item.tags && item.tags.count > 0) {
        NSMutableArray *tagItems = [[NSMutableArray alloc] init];
        for (NSString *tag in item.tags) {
            BOOL isSystemTag = [tag hasSuffix:@"-sys"];
            NSString *tagTitle = isSystemTag ? [tag substringToIndex:tag.length - 4] : tag;
            if (!isSystemTag) {
                [tagItems addObject:[NSString stringWithFormat:@"#%@", tagTitle]];
            }else {
                [tagItems addObject:[NSString stringWithFormat:@"#%@", tagTitle]];
            }
        }
        
        [cell.tagsView setTags:tagItems];
        
        [cell showTagView:YES];
    }else {
        [cell.tagsView setTags:nil];
        [cell showTagView:NO];
    }
    
    [cell.photoCountBtn setTitle:[NSString stringWithFormat:@"%ld", item.realShotPics.count] forState:UIControlStateNormal];
 
    [cell updateLayout:NO];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TasteItem *taste = _tasteItems[indexPath.row];
    if (IsSafeValue(taste.productId)) {
        _currentOperationItemIndex = (int)indexPath.row;
        DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
        
        detailController.tasteId = taste.productId;
        detailController.relatedTasteItem = taste;
        [self.navigationController pushViewController:detailController animated:YES];
    }
}


#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 100) {
        if (buttonIndex == 0) {
            TasteItem *item = _tasteItems[_currentOperationItemIndex];
            NSString *buyPageUrl = item.buyLink;
            PBWebViewController *webViewController = [[PBWebViewController alloc] init];
            webViewController.URL = [NSURL URLWithString:buyPageUrl];
            [self.navigationController pushViewController:webViewController animated:YES];
            _currentOperationItemIndex = 0;
        }else if(buttonIndex == 1) {
            UIActionSheet *reportSheet = [[UIActionSheet alloc] initWithTitle:@"举报" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"内容与宝贝不符", @"内容不健康", @"有广告嫌疑", @"垃圾信息", nil];
            reportSheet.tag = 200;
            [reportSheet showInView:self.navigationController.view];
        }else if(buttonIndex == 3 ){
            AlertWithActionsAndTag(500, @"删除宝贝", @"您确定要删除此宝贝吗?", @"取消", @"确定");
        }
    }else if(actionSheet.tag == 200) {
        if (buttonIndex != 4) {
            NSString *reportContent = [actionSheet buttonTitleAtIndex:buttonIndex];
            TasteItem *currentItem = _tasteItems[_currentOperationItemIndex];
            [[YouPinAPI sharedAPI] reportTasteItem:currentItem content:reportContent sender:self callback:^(NSError *error) {
                if (!error) {
                    
                }
            }];
        }
        
        _currentOperationItemIndex = -1;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 500 && buttonIndex == 1) {
        MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hudView.labelText = @"正在删除...";
        
        TasteItem *item = _tasteItems[_currentOperationItemIndex];
        int rowIndex = _currentOperationItemIndex;
        [[YouPinAPI sharedAPI] deleteTasteWithProductId:item.productId sender:self callback:^(NSError *error) {
            [hudView hide:YES];
            if (!error) {
                [_tasteItems removeObjectAtIndex:rowIndex];
                [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            }else {
                Alert(error.localizedDescription, @"", @"确定");
            }
        }];
    }else if (alertView.tag == 1000) {
        if (buttonIndex == 1) {
            [self bindTaobao];
        }
    }
    
    _currentOperationItemIndex = -1;
}


@end
