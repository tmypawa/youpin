//
//  WatermarkViewController.m
//  youpinwei
//
//  Created by tmy on 14-9-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PhotoEditViewController.h"
#import "UIImage+Utility.h"
#import "PhotoEffectViewCell.h"
#import "SmallPhotoEffectViewCell.h"
#import "JTWatermarkView.h"
#import "JTObjectMapping.h"
#import "Watermark.h"
#import "WatermarkPart.h"
#import "GPUImage.h"
#import "PhotoCropViewController.h"
#import "InstaFilters.h"


@interface PhotoEditViewController ()

@end

@implementation PhotoEditViewController
{
    __weak IBOutlet UIButton         *_labelBtn;
    __weak IBOutlet UIButton         *_watermarkBtn;
    __weak IBOutlet UIButton *_microspurBtn;
    __weak IBOutlet UIView           *_indicatorView;
    __weak IBOutlet UICollectionView *_watermarkCollectionView;
    IBOutlet UIView *_actionView;
    IBOutlet UIView *_effectsTagView;
    float                             _photoSize;
    int                               _selectEffectType;
    NSMutableArray                    *_selectedRowIndexes;
    JTWatermarkView                   *_currentWatermarkView;
    NSArray                           *_decorate;
    NSArray                           *_watermarks;
    NSMutableArray                    *_watermarkComponents;
    NSMutableDictionary               *_watermarkComponentDict;
    UIView                            *_currentEditingComponent;
    JTInputView                       *_inputView;
    BOOL                              _isEditActive;
    UIButton                          *_inputMaskBtn;
    
    GPUImageView    *_photoView;
    GPUImagePicture *_editedImage;
    UIImageView     *_originalPhotoView;
    UIImageView     *_microspurMaskView;
    NSArray     *_filters;
    NSArray     *_filterNames;
    NSArray     *_microspurs;
    NSArray     *_microspurNames;
    GPUImageOutput  *_finalFilter;
    NSMutableArray  *_appliedFilters;
    int             _randomEffectExampleIndex;
    
    UIView          *_currentPanView;
    CGPoint         _originPanPoint;
    
    CGPoint         _currentMicrospurMovePoint;
    CGPoint         _microspurMovePoint;
    CGFloat         _currentMicrospurScale;
    CGFloat         _totalMicrospurScale;
 
    UIPanGestureRecognizer *_microspurPanRecognizer;
    UIPinchGestureRecognizer  *_microspurPinchRecognizer;
    //microspur params
    float           _defaultTiltShiftFilterRadius;
    float           _maxTiltShiftFilterRadius;
    float           _minTiltShiftFilterRadius;
    
    float           _defaultGaussianBlurFilterRadius;
    float           _maxGaussianBlurFilterRadius;
    float           _minGaussianBlurFilterRadius;
}

- (void)dealloc {
    int a = 1;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _photoSize = 320;
        _totalMicrospurScale = 1;
        _currentMicrospurScale = 1;
        
        _defaultTiltShiftFilterRadius = 0.3f;
        _maxTiltShiftFilterRadius = 0.6f;
        _minTiltShiftFilterRadius = 0.1f;
        
        _defaultGaussianBlurFilterRadius = 0.3f;
        _maxGaussianBlurFilterRadius = 0.5f;
        _minGaussianBlurFilterRadius = 0.1f;
        
        [self initDecorateAndWatermarks];
        _watermarkComponents = [[NSMutableArray alloc] init];
        _watermarkComponentDict = [[NSMutableDictionary alloc] init];
        
        [self initFilterList];
        _appliedFilters = [[NSMutableArray alloc] initWithArray:@[[NSNull null], [NSNull null], [NSNull null]] ];
        _selectedRowIndexes = [NSMutableArray arrayWithObjects:@(0), @(0), @(0), nil];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    BOOL isPhotoCroped = [self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2] isKindOfClass:[PhotoCropViewController class]];
//    if (!isPhotoCroped && self.photo) {
//        self.photo = [self cropImage:self.photo];
//    }
    
    self.view.backgroundColor = [UIColor blackColor];
    _actionView.backgroundColor = [UIColor colorWithRed:0.16 green:0.16 blue:0.16 alpha:1];
    _watermarkCollectionView.backgroundColor = [UIColor clearColor];
    
    _originalPhotoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _photoSize, _photoSize)];
    _originalPhotoView.backgroundColor = [UIColor clearColor];
    _originalPhotoView.image = self.photo;
    
    _microspurMaskView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _photoSize * 2, _photoSize * 2)];
    _microspurMaskView.backgroundColor = [UIColor clearColor];
    _microspurMaskView.center = CGPointMake(_photoSize / 2, _photoSize / 2);
    
    [self initFilterView];

    if (isIPhone4) {
        SetFrameY(_effectsTagView, _photoView.frame.size.height + 10)
        SetFrameY(_watermarkCollectionView, _effectsTagView.frame.size.height + _effectsTagView.frame.origin.y + 10);
        SetFrameHeight(_watermarkCollectionView, 70);
    }else {
        SetFrameY(_effectsTagView, _photoView.frame.size.height + 25)
        SetFrameY(_watermarkCollectionView, _effectsTagView.frame.size.height + _effectsTagView.frame.origin.y + 15);
        SetFrameHeight(_watermarkCollectionView, 120);
    }
    
    SetFrameY(_actionView, screenFrame.size.height - _actionView.frame.size.height);
    
//    _inputView = [[JTInputView alloc] initWithScrollView:nil];
//    _inputView.delegate = self;
//    _inputView.textView.delegate = self;
//    _inputView.backgroundColor = [UIColor whiteColor];
//    [_inputView.sendButton setTitle:@"完成" forState:UIControlStateNormal];
//    [_inputView.sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
//    SetFrameOrigin(_inputView, 0, screenFrame.size.height);
    
//    [self.view addSubview:_inputView];
    
    _inputMaskBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _inputMaskBtn.frame = screenFrame;
    [_inputMaskBtn addTarget:self action:@selector(cancelEdit:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
    if (self.navigationController) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)canBecomeFirstResponder {
    return _isEditActive;
}

- (UIView *)inputAccessoryView {
    return _inputView;
}

- (void)initDecorateAndWatermarks {
    _decorate = @[];
    NSString *watermarksStr = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"watermarks" ofType:@"json"] encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *watermarkDict = [watermarksStr objectFromJSONString];
    NSArray *watermarksArray = watermarkDict[@"watermakers"];
    JTObjectMappingItem *watermarkItem = [JTObjectMappingItem itemWithKeyPath:nil mappingClass:[Watermark class]];
    JTObjectMappingItem *watermarkPartItem = [JTObjectMappingItem itemWithKeyPath:@"makers" mappingClass:[WatermarkPart class]];
    [watermarkItem addDependentMappingItems:@[watermarkPartItem]];
    _watermarks = [[JTObjectMapping mappingWithMappingItems:@[watermarkItem]] fetchObjectsFromArray:watermarksArray];
}

- (IBAction)changeEffectsType:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        if (sender == _watermarkBtn) {
            _selectEffectType = 0;
            [_watermarkBtn setTitleColor:LoginNavigationBarTintColor forState:UIControlStateNormal];
            _watermarkBtn.enabled = NO;
            [_labelBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _labelBtn.enabled = YES;
            [_microspurBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _microspurBtn.enabled = YES;
            CGRect indicatorFrame = _indicatorView.frame;
            indicatorFrame.origin.x = _watermarkBtn.frame.origin.x;
            _indicatorView.frame = indicatorFrame;
        }else if(sender == _labelBtn){
            _selectEffectType = 1;
            [_watermarkBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _watermarkBtn.enabled = YES;
            [_labelBtn setTitleColor:LoginNavigationBarTintColor forState:UIControlStateNormal];
            _labelBtn.enabled = NO;
            [_microspurBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _microspurBtn.enabled = YES;
            CGRect indicatorFrame = _indicatorView.frame;
            indicatorFrame.origin.x = _labelBtn.frame.origin.x;
            _indicatorView.frame = indicatorFrame;
        }else {
            _selectEffectType = 2;
            [_watermarkBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _watermarkBtn.enabled = YES;
            [_labelBtn setTitleColor:PHOTO_DEFAULT_TITLE_COLOR forState:UIControlStateNormal];
            _labelBtn.enabled = YES;
            [_microspurBtn setTitleColor:LoginNavigationBarTintColor forState:UIControlStateNormal];
            _microspurBtn.enabled = NO;
            CGRect indicatorFrame = _indicatorView.frame;
            indicatorFrame.origin.x = _microspurBtn.frame.origin.x;
            _indicatorView.frame = indicatorFrame;
        }
    }];
    
    [_watermarkCollectionView reloadData];
    [_watermarkCollectionView setContentOffset:CGPointMake(0, 0) animated:NO];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)done:(id)sender {
    UIImage *fileredImage = [self fetchFilteredImage];
    UIImage *finalEditedPhoto = [self processWatermarkForImage:fileredImage];
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationPhotoDidFinishEdit object:self userInfo:@{YouPinNotificationEditPhotoKey: finalEditedPhoto}];
}


#pragma mark Filter
- (void)initFilterView {
    _photoView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    _photoView.userInteractionEnabled = YES;
    _photoView.clipsToBounds = YES;
    [self.view insertSubview:_photoView atIndex:0];
    
    _editedImage = [[GPUImagePicture alloc] initWithImage:self.photo smoothlyScaleOutput:YES];
    
    [_editedImage addTarget:_photoView];
    
    [_editedImage processImage];
}

- (void)initFilterList {
    //GPUImageTiltShiftFilter radius: 0.2 - 0.6
    //GPUImageGaussianSelectiveBlurFilter radius: 0.1 - 0.5
    _microspurs = @[[GPUImageTiltShiftFilter class], [GPUImageGaussianSelectiveBlurFilter class]];
    _filterNames = @[@"无", @""];
    _microspurNames = @[@"无", @"平行", @"径向"];
}

//焦距
- (GPUImageOutput *)filterWithFilterClass:(Class)filterClass {
    GPUImageOutput<GPUImageInput> *filter = [[filterClass alloc] init];
    [filter useNextFrameForImageCapture];
    if(filterClass == [GPUImageGaussianSelectiveBlurFilter class]) {
        [(GPUImageGaussianSelectiveBlurFilter *)filter setExcludeCircleRadius:0.3];
    }else if (filterClass == [GPUImageTiltShiftFilter class]) {
        [(GPUImageTiltShiftFilter *)filter setFocusFallOffRate:0.04];
        [(GPUImageTiltShiftFilter *)filter setTopFocusLevel:0.35];
        [(GPUImageTiltShiftFilter *)filter setBottomFocusLevel:0.65];
    }
    
    return filter;
}

- (void)adjustForCurrentMicrospurFilter {
    id microspurFilter = _appliedFilters[2];
    if ((NSNull *)microspurFilter != [NSNull null]) {
        NSInteger microspurIndex = [_microspurs indexOfObject:[microspurFilter class]];
        CGPoint circlePoint = CGPointMake(_photoSize / 2 + _microspurMovePoint.x,  _photoSize / 2 + _microspurMovePoint.y);
        
        if (microspurIndex == 0) {
            float centerFousLevel = (1 / _photoSize) * circlePoint.y;
            float radius = _defaultTiltShiftFilterRadius * _totalMicrospurScale / 2;
            float topFocusLevel = centerFousLevel - radius;
            float bottomFocusLevel = centerFousLevel + radius;
            [(GPUImageTiltShiftFilter *)microspurFilter setTopFocusLevel:topFocusLevel];
            [(GPUImageTiltShiftFilter *)microspurFilter setBottomFocusLevel:bottomFocusLevel];
        }else if(microspurIndex == 1) {
            
            [(GPUImageGaussianSelectiveBlurFilter *)microspurFilter setExcludeCirclePoint:CGPointMake(circlePoint.x / _photoSize, circlePoint.y / _photoSize)];
            CGFloat circleRadius = _totalMicrospurScale * (0.5 / 2);
            [(GPUImageGaussianSelectiveBlurFilter *)microspurFilter setExcludeCircleRadius:circleRadius];
        }
        
        [self processImage];
    }
}

- (void)addMicrospurMaskView {
    id microspurFilter = _appliedFilters[2];
    if ((NSNull *)microspurFilter != [NSNull null]) {
        NSInteger microspurIndex = [_microspurs indexOfObject:[microspurFilter class]];
        NSString *maskName = @"";
        if (microspurIndex == 0) {
            maskName = @"parallel";
        }else {
            maskName = @"radial";
        }
        
        _microspurMaskView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:maskName ofType:@"png"]];
        _microspurMaskView.transform = CGAffineTransformIdentity;
        [_photoView addSubview:_microspurMaskView];
        _microspurMaskView.alpha = 1;
        
        _microspurPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRecognizerDidRecognize:)];
        _microspurPanRecognizer.maximumNumberOfTouches = 1;
        
        [_photoView addGestureRecognizer:_microspurPanRecognizer];
        
        _microspurPinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureRecognizerDidRecognize:)];
        [_photoView addGestureRecognizer:_microspurPinchRecognizer];
        
        [UIView animateWithDuration:0.5 delay:0.7 options:0 animations:^{
            _microspurMaskView.alpha = 0;
        } completion:nil];
    }
}

- (void)resetMicrospurMaskView {
    _totalMicrospurScale = 1;
    _currentMicrospurScale = 1;
    _currentMicrospurMovePoint = CGPointZero;
    _microspurMovePoint = CGPointZero;
    _microspurMaskView.image = nil;
    _microspurMaskView.transform = CGAffineTransformIdentity;
    [_microspurMaskView removeFromSuperview];
    [_photoView removeGestureRecognizer:_microspurPanRecognizer];
    _microspurPanRecognizer = nil;
}

- (void)showMicrospurMaskView {
    [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
        _microspurMaskView.alpha = 1;
    } completion:nil];

}

- (void)hideMicrospurMaskView {
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        _microspurMaskView.alpha = 0;
    } completion:nil];
    
}

- (void)applyFilterForSelectIndex:(NSInteger)selectIndex {
    [_editedImage removeAllTargets];
//    _finalFilter = nil;
    
    for (GPUImageOutput *filter in _appliedFilters) {
        if ((NSNull *)filter != [NSNull null]) {
            [filter removeAllTargets];
        }
    }
    
    NSMutableArray *activeFilters = [[NSMutableArray alloc] init];
    if (selectIndex == 0) {
        _appliedFilters[_selectEffectType] = [NSNull null];
    }else {
        if (_selectEffectType == 1) {
            IFImageFilter *filter = [IFImageFilter filterAtIndex:selectIndex - 1];
            _appliedFilters[_selectEffectType] = filter;
        }else if(_selectEffectType == 2) {
            Class filterClass = _microspurs[selectIndex - 1];
            GPUImageOutput *filter = [self filterWithFilterClass:filterClass];
            _appliedFilters[_selectEffectType] = filter;
        }
    }
    
    for (GPUImageOutput *filter in _appliedFilters) {
        if ((NSNull *)filter != [NSNull null]) {
            [activeFilters addObject:filter];
        }
    }
    
    if (activeFilters.count > 0) {
        [_editedImage removeTarget:_photoView];
    }else {
        [_editedImage useNextFrameForImageCapture];
        [_editedImage addTarget:_photoView];
        _finalFilter = nil;
    }
    
    for (int i = 0; i < activeFilters.count; i++) {
        if (i == 0) {
            [_editedImage addTarget:activeFilters[i] atTextureLocation:0];
        }
        
        if(i < activeFilters.count - 1) {
            [(GPUImageOutput *)activeFilters[i] addTarget:activeFilters[i + 1]];
        }
        
        if(i == activeFilters.count - 1) {
            _finalFilter = activeFilters[i];
            [_finalFilter addTarget:_photoView];
        }
    }
}

- (void)processImage {
    [_editedImage processImage];
}

- (UIImage *)fetchFilteredImage {
    if (_finalFilter) {
        return [_finalFilter imageFromCurrentFramebuffer];
    }else {
        
        return self.photo;
    }
}

#pragma mark Watermark
- (UIImage *)processWatermarkForImage:(UIImage *)image {
    UIImage *editedPhoto = nil;
    UIGraphicsBeginImageContextWithOptions(_photoView.frame.size, YES, 0);
    [image drawInRect:_photoView.frame];
    
    for (UIView *watermarkComponent in _watermarkComponents) {
        if ([watermarkComponent isKindOfClass:[UIImageView class]]) {
            [((UIImageView *)watermarkComponent).image drawInRect:watermarkComponent.frame];
        }else if([watermarkComponent isKindOfClass:[UIButton class]]) {
            UILabel *textLabel = ((UIButton *)watermarkComponent).titleLabel;
            [textLabel.text drawWithRect:watermarkComponent.frame options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSForegroundColorAttributeName : textLabel.textColor, NSFontAttributeName: textLabel.font} context:nil];
        }
    }
    
    if (_currentWatermarkView) {
        UIImage *decorateImage = [_currentWatermarkView exportTransformImage];
        [decorateImage drawAtPoint:_currentWatermarkView.transformedLocation];
    }
    
    editedPhoto = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    
    return editedPhoto;
}

- (void)clearWatermark {
    for (UIView *conponent in _watermarkComponents) {
        [conponent removeFromSuperview];
    }
    
    [_watermarkComponents removeAllObjects];
    [_watermarkComponentDict removeAllObjects];
}

- (void)showWatermark:(Watermark *)watermark {
    for (UIView *watermarkView in _watermarkComponents) {
        [watermarkView removeFromSuperview];
    }
    
    [_watermarkComponents removeAllObjects];
    [_watermarkComponentDict removeAllObjects];
    
    float referenceSize = 640;
    float factor = screenFrame.size.width / referenceSize;
    
    int index = 1;
    for (WatermarkPart *part in watermark.makers) {
        if (part.type == 2) {
            NSString *filename = [part.pic lastPathComponent];
            NSString *imageName = [filename componentsSeparatedByString:@"."][0];
            NSString *extension = [filename pathExtension];
            UIImage *image = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:extension]];
           
            UIImageView *imagePart = [[UIImageView alloc] initWithFrame:CGRectMake(referenceSize * [part.x floatValue] * factor, referenceSize * [part.y floatValue] * factor, image.size.width * factor, image.size.height * factor)];
            imagePart.image = image;
            imagePart.backgroundColor = [UIColor clearColor];
            imagePart.tag = index * 100;
            imagePart.userInteractionEnabled = YES;
            UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(watermarkViewDidPan:)];
            panRecognizer.delegate = self;
            panRecognizer.maximumNumberOfTouches = 1;
            [imagePart addGestureRecognizer:panRecognizer];
            
            [_watermarkComponents addObject:imagePart];
            _watermarkComponentDict[@(imagePart.tag)] = part;
            [_photoView addSubview:imagePart];
        }else if(part.type == 1){
            
            float x = referenceSize * [part.x floatValue] * factor;
            float y = referenceSize * [part.y floatValue] * factor;
            UIButton *textPart = [UIButton buttonWithType:UIButtonTypeCustom];
            textPart.frame = CGRectMake(x, y, 0, 0);
            textPart.titleLabel.font = [UIFont systemFontOfSize:part.fontsize];
            textPart.tag = index * 100;
        
            textPart.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
            textPart.titleLabel.layer.shadowOpacity = 0.9f;
            textPart.titleLabel.layer.shadowOffset = CGSizeMake(0, 1);
            textPart.titleLabel.layer.shadowRadius = 2;
            [textPart setTitleColor:[UIColor colorWithString:part.fontcolor] forState:UIControlStateNormal];
            textPart.backgroundColor =[UIColor clearColor];
            [textPart setTitle:part.text forState:UIControlStateNormal];
            [textPart addTarget:self action:@selector(editTextComponent:) forControlEvents:UIControlEventTouchUpInside];

            UILabel *testLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
            testLabel.numberOfLines = 1;
            testLabel.text = part.text;
            testLabel.font = [UIFont systemFontOfSize:part.fontsize];
            [testLabel sizeToFit];
            
            CGRect testFrame = testLabel.frame;
            SetFrameWidth(textPart, testFrame.size.width);
            SetFrameHeight(textPart, testFrame.size.height);
            
            [_watermarkComponents addObject:textPart];
             _watermarkComponentDict[@(textPart.tag)] = part;
            [_photoView addSubview:textPart];
        }
        
        index ++;
    }
}

- (void)showDecorateWitgIndex:(NSInteger)decorateIndex {
    if (_currentWatermarkView) {
        [_currentWatermarkView removeFromSuperview];
        _currentWatermarkView = nil;
    }
    
    UIImage *decorateImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"decorate_%ld", decorateIndex] ofType:@"png"]];
    CGSize decorateSize = CGSizeMake(decorateImage.size.width, decorateImage.size.height);
    JTWatermarkView *watermarkView = [[JTWatermarkView alloc] initWithFrame:CGRectMake((_photoView.frame.size.width - decorateSize.width)/2, (_photoView.frame.size.height - decorateSize.height)/2, decorateSize.width, decorateSize.height) watermarkImage:decorateImage containerView:_photoView];
    [_photoView addSubview:watermarkView];
    _currentWatermarkView = watermarkView;
}

- (void)cancelEdit:(UIButton *)sender {
    [self finishEdit];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer != _microspurPanRecognizer) {
        _currentPanView = gestureRecognizer.view;
        _originPanPoint = _currentPanView.frame.origin;
    }
    
    return YES;
}

- (void)watermarkViewDidPan:(UIPanGestureRecognizer *)panGesture {
    if (panGesture.view == _currentPanView) {
        CGPoint movePoint = [panGesture translationInView:_photoView];
        CGRect targetRect = CGRectMake(_originPanPoint.x + movePoint.x, _originPanPoint.y + movePoint.y, _currentPanView.frame.size.width, _currentPanView.frame.size.height);
        CGFloat maxX = _photoView.frame.size.width - _currentPanView.frame.size.width;
        CGFloat maxY = _photoView.frame.size.height - _currentPanView.frame.size.height;
        
        targetRect.origin.x = MAX(0, targetRect.origin.x);
        targetRect.origin.x = MIN(maxX, targetRect.origin.x);
        targetRect.origin.y = MAX(0, targetRect.origin.y);
        targetRect.origin.y = MIN(maxY, targetRect.origin.y);
        SetFrameOrigin(_currentPanView, targetRect.origin.x, targetRect.origin.y);
    }
}

- (void)gestureRecognizerDidRecognize:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer == _microspurPanRecognizer) {
        CGPoint translationPoint = [_microspurPanRecognizer translationInView:_microspurPanRecognizer.view];
        CGFloat moveX = _microspurMovePoint.x + translationPoint.x;
        CGFloat moveY = _microspurMovePoint.y + translationPoint.y;
        
        if (moveX > -_photoSize/2 & moveX < _photoSize/2) {
            _currentMicrospurMovePoint.x = moveX;
        }
        
        if (moveY > -_photoSize/2 & moveY < _photoSize/2) {
            _currentMicrospurMovePoint.y = moveY;
        }
        
        if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
            [self transformMicrospurMaskView];
        }
        else if(gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            [_photoView insertSubview:_originalPhotoView belowSubview:_microspurMaskView];
            [self showMicrospurMaskView];
        }else if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
            _microspurMovePoint = _currentMicrospurMovePoint;
            [self hideMicrospurMaskView];
            [self adjustForCurrentMicrospurFilter];
            [_originalPhotoView removeFromSuperview];
        }
    }
}

- (void)pinchGestureRecognizerDidRecognize:(UIPinchGestureRecognizer *)gestureRecognizer {
//    NSLog(@"scale %f", gestureRecognizer.scale);
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [_photoView insertSubview:_originalPhotoView belowSubview:_microspurMaskView];
        [self showMicrospurMaskView];

    }else if(gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGFloat scale = _totalMicrospurScale * gestureRecognizer.scale;
        if (scale >= 1 && scale <= 6) {
            _currentMicrospurScale = scale;
        }
        
        [self transformMicrospurMaskView];
    }else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        _totalMicrospurScale = _currentMicrospurScale;
        [self hideMicrospurMaskView];
        [self adjustForCurrentMicrospurFilter];
        [_originalPhotoView removeFromSuperview];
    }
}

- (void)transformMicrospurMaskView {
    CGAffineTransform translation = CGAffineTransformMakeTranslation(_currentMicrospurMovePoint.x, _currentMicrospurMovePoint.y);
    CGAffineTransform finalTransform = CGAffineTransformScale(translation, _currentMicrospurScale, _currentMicrospurScale);
    _microspurMaskView.transform = finalTransform;
}

- (void)finishEdit {
    _isEditActive = NO;
    [self resignFirstResponder];
    [_inputView clearInput];

    [_inputMaskBtn removeFromSuperview];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SetFrameOrigin(_inputView, 0, screenFrame.size.height);
        [self.view addSubview:_inputView];
    });
}

- (void)editTextComponent:(UIButton *)sender {
    _currentEditingComponent = sender;
    [_inputView.textView becomeFirstResponder];
}

- (void)JTInputViewDidPressSendButton:(JTInputView *)inputView {
    NSString *text = inputView.inputText;
    if (_currentEditingComponent) {
        int tag = _currentEditingComponent.tag;
        WatermarkPart *editedPart = _watermarkComponentDict[@(tag)];
        if (text.length > editedPart.textlength) {
            text = [text substringWithRange:NSMakeRange(0, editedPart.textlength)];
            text = [text stringByAppendingString:@"..."];
        }
        
        if ([_currentEditingComponent isKindOfClass:[UIButton class]]) {
            [((UIButton *)_currentEditingComponent) setTitle:text forState:UIControlStateNormal];
        }
        
        [_currentEditingComponent sizeToFit];
    }
    
    [self finishEdit];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self.view addSubview:_inputMaskBtn];
    _isEditActive = YES;
    [_inputView removeFromSuperview];
    [self becomeFirstResponder];
    return YES;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (isIPhone4) {
        return CGSizeMake(70, 70);
    }else {
        return CGSizeMake(90, 120);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return isIPhone4 ? 5 : 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    int count = 0;
    switch (_selectEffectType) {
        case 0:
            count = (int)_watermarks.count + 1;
            break;
        case 1:
            count = (int)[IFImageFilter filterList].count + 1;
            break;
        case 2:
            count = (int)_microspurs.count + 1;
        default:
            break;
    }

    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotoEffectViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:(!isIPhone4)? @"PhotoEffectViewCell" : @"SmallPhotoEffectViewCell" forIndexPath:indexPath];
    NSInteger selectedRowIndex = [_selectedRowIndexes[_selectEffectType] integerValue];
    [cell setSelectStatus:selectedRowIndex == indexPath.row];
    
    if(indexPath.row == 0){
        cell.titleLabel.text = @"无";
        cell.imageView.image = [UIImage imageNamed:@"microspur_0"];
    }else {
        if (_selectEffectType == 0) {
            Watermark *watermark = _watermarks[indexPath.row - 1];
            NSString *filename = [[watermark.icon lastPathComponent] componentsSeparatedByString:@"."][0];
            if (cell.titleLabel) {
                cell.titleLabel.text = watermark.name;
            }
            
            cell.imageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filename ofType:@"png"]];
        }else if(_selectEffectType == 1){
            NSDictionary *filterDict = [IFImageFilter filterList][indexPath.row - 1];
            if (cell.titleLabel) {
                cell.titleLabel.text = filterDict[@"name"];
            }
            
            NSString *thumbnailName = filterDict[@"thumbnail"];
            if(thumbnailName && thumbnailName.length > 0) {
                cell.imageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:thumbnailName  ofType:@"png"]];
            }
        }else {
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"microspur_%d", (int)indexPath.row]];
            cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
            if (cell.titleLabel) {
                cell.titleLabel.text = _microspurNames[indexPath.row];
            }
        }

    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger selectedRowIndex = [_selectedRowIndexes[_selectEffectType] integerValue];
    if (selectedRowIndex != -1) {
        PhotoEffectViewCell *lastSelectedCell = (PhotoEffectViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:selectedRowIndex inSection:0]];
        if (lastSelectedCell) {
            [lastSelectedCell setSelectStatus:NO];
        }
    }
    
    PhotoEffectViewCell *cell = (PhotoEffectViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (cell) {
        [cell setSelectStatus:YES];
    }
    
    _selectedRowIndexes[_selectEffectType] = @(indexPath.row);
    
    if (_selectEffectType == 0) {
        if (indexPath.row == 0) {
            [self clearWatermark];
        }else {
            Watermark *watermark = _watermarks[indexPath.row - 1];
            [self showWatermark:watermark];
        }
    }else if(_selectEffectType == 1) {
        [self applyFilterForSelectIndex:indexPath.row];
        [self processImage];
    }else if(_selectEffectType == 2){
        [self applyFilterForSelectIndex:indexPath.row];
        [self processImage];
        
        [self resetMicrospurMaskView];
        [self addMicrospurMaskView];
    }else if(_selectEffectType == 3){
        [self showDecorateWitgIndex:indexPath.row];
    }
}


@end
