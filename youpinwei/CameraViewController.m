//
//  CameraViewController.m
//  youpinwei
//
//  Created by tmy on 14-9-11.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "CameraViewController.h"
#import "AlbumPhotoViewController.h"
#import "PhotoCropViewController.h"
#import "PhotoEditViewController.h"
#import "NYXImagesKit.h"
#import "UIImage+Utility.h"

@interface CameraViewController ()

@end

@implementation CameraViewController
{
    UIButton        *_flashBtn;
    UIButton        *_cameraBtn;
    UIButton        *_takePhotoBtn;
    UIButton        *_cancelBtn;
    BOOL            _isFlashOn;
}

- (id)init {
    if (self = [super init]) {
        self.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadView {
    [super loadView];
    
    if (self.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, screenFrame.size.height)];
        overlayView.backgroundColor = [UIColor clearColor];
        self.cameraOverlayView = overlayView;
        
        UIView *toolBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 360, screenFrame.size.width, screenFrame.size.height - 360)];
        toolBarView.clipsToBounds = NO;
        toolBarView.backgroundColor = [UIColor blackColor];
        [overlayView addSubview:toolBarView];
       
        
        UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, 40)];
        topBarView.backgroundColor = [UIColor blackColor];
        [overlayView addSubview:topBarView];
        
        _cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _cameraBtn.frame = CGRectMake(screenFrame.size.width - 55, 0, 40, 40);
        [_cameraBtn setImage:[UIImage imageNamed:@"switch-camera_normal"] forState:UIControlStateNormal];
        [_cameraBtn setImage:[UIImage imageNamed:@"switch-camera_press"] forState:UIControlStateHighlighted];
        [_cameraBtn addTarget:self action:@selector(cameraDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [overlayView addSubview:_cameraBtn];
        
        _flashBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _flashBtn.frame = CGRectMake((screenFrame.size.width - 90)/2, 0, 90, 40);
        _flashBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -70, 0, 0);
        _flashBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
        _flashBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_flashBtn setImage:[UIImage imageNamed:@"flash-lamp_normal"] forState:UIControlStateNormal];
        [_flashBtn setImage:[UIImage imageNamed:@"flash-lamp_press"] forState:UIControlStateHighlighted];
        [_flashBtn addTarget:self action:@selector(flashDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [_flashBtn setTitle:@"打开" forState:UIControlStateNormal];
        [_flashBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _flashBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [overlayView addSubview:_flashBtn];
        
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelBtn.frame = CGRectMake(0, 0, 60, 40);
        [_cancelBtn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_cancelBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
        [overlayView addSubview:_cancelBtn];
        
        _takePhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _takePhotoBtn.frame = CGRectMake((screenFrame.size.width - 72)/2, toolBarView.frame.size.height - 118, 72, 72);
        [_takePhotoBtn setImage:[UIImage imageNamed:@"take-picture_normal"] forState:UIControlStateNormal];
        [_takePhotoBtn setImage:[UIImage imageNamed:@"take-picture_press"] forState:UIControlStateHighlighted];
        [_takePhotoBtn addTarget:self action:@selector(takePhoto:) forControlEvents:UIControlEventTouchUpInside];
        [toolBarView addSubview:_takePhotoBtn];
        
        UIButton *albumBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        albumBtn.frame = CGRectMake((_takePhotoBtn.frame.origin.x - 22)/2, 0, 22, 40);
        albumBtn.center = CGPointMake(albumBtn.center.x, _takePhotoBtn.center.y);
        [albumBtn setImage:[UIImage imageNamed:@"icon_album"] forState:UIControlStateNormal];
        [albumBtn setImageEdgeInsets:UIEdgeInsetsMake(-10, 0, 0, 0)];
        albumBtn.adjustsImageWhenHighlighted = NO;
        [albumBtn addTarget:self action:@selector(showPhotoAlbum:) forControlEvents:UIControlEventTouchUpInside];
        //[toolBarView addSubview:albumBtn];
        
        UILabel *albumTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, albumBtn.frame.size.width, 20)];
        albumTitleLabel.center = CGPointMake(albumBtn.center.x, albumBtn.center.y + 15);
        albumTitleLabel.backgroundColor =[UIColor clearColor];
        albumTitleLabel.textColor = [UIColor whiteColor];
        albumTitleLabel.font =[UIFont systemFontOfSize:11];
        albumTitleLabel.textAlignment = NSTextAlignmentCenter;
        albumTitleLabel.text = @"相册";
        //[toolBarView addSubview:albumTitleLabel];
        
        UIImageView *tipView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_photo_tip"]];
        tipView.center = CGPointMake(_takePhotoBtn.center.x, _takePhotoBtn.center.y - _takePhotoBtn.frame.size.height/2 - 40);
        [toolBarView addSubview:tipView];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(photoEditFinishNotification:) name:YouPinNotificationPhotoDidFinishEdit object:nil];
    
    if (self.sourceType == UIImagePickerControllerSourceTypeCamera) {
        self.showsCameraControls = NO;
        self.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        _isFlashOn = YES;
    }
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)flashDidClick:(UIButton *)sender {
    _isFlashOn = !_isFlashOn;
    self.cameraFlashMode = _isFlashOn ? UIImagePickerControllerCameraFlashModeAuto : UIImagePickerControllerCameraFlashModeOff;
    [_flashBtn setTitle: _isFlashOn ? @"打开" : @"关闭" forState:UIControlStateNormal];
}

- (void)cameraDidClick:(UIButton *)sender {
    self.cameraDevice = self.cameraDevice ==  UIImagePickerControllerCameraDeviceRear ? UIImagePickerControllerCameraDeviceFront : UIImagePickerControllerCameraDeviceRear;
}

- (void)cancel:(UIButton *)sender {
    if (self.cameraDelegate && [self.cameraDelegate respondsToSelector:@selector(cameraViewControllerDidCancel:)]) {
        [self.cameraDelegate cameraViewControllerDidCancel:self];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showPhotoAlbum:(UIButton *)sender {
    AlbumPhotoViewController *albumController = [[AlbumPhotoViewController alloc] init];
    albumController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    albumController.delegate = self;
    [self presentViewController:albumController animated:YES completion:nil];
}

- (void)takePhoto:(UIButton *)sender {
    [self takePicture];
}

- (void)photoEditFinishNotification:(NSNotification *)notification {
    UIImage *photo = notification.userInfo[YouPinNotificationEditPhotoKey];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.cameraDelegate && [self.cameraDelegate respondsToSelector:@selector(cameraViewController:didPickImage:)]) {
            [self.cameraDelegate cameraViewController:self didPickImage:photo];
        }
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    });
}


- (UIImage *)cropImage:(UIImage *)originImage {
    float fixedSize = CROP_IMAGE_SIZE;
    float zoomSize = originImage.size.height/originImage.size.width * fixedSize;
    UIImage *cropImage = [originImage zoomImageWithSize:CGSizeMake(zoomSize, zoomSize)];
    cropImage = [cropImage clipImageWithRect:CGRectMake(0, 40 * fixedSize / screenWidth, fixedSize, fixedSize)];
    return cropImage;
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *photo = info[UIImagePickerControllerOriginalImage];
    photo = [self cropImage:photo];
    //photo = [photo scaleToSize:CGSizeMake(CROP_IMAGE_SIZE, CROP_IMAGE_SIZE) usingMode:NYXResizeModeAspectFit];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        PhotoEditViewController *photoEditController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoEditViewController"];
        photoEditController.photo = photo;
        [self pushViewController:photoEditController animated:YES];
    }else {
        PhotoCropViewController *cropViewController = [storyboard instantiateViewControllerWithIdentifier:@"PhotoCropViewController"];
        cropViewController.originalImage = photo;
        cropViewController.delegate = self;
        [self pushViewController:cropViewController animated:YES];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropViewController:(RSKImageCropViewController *)controller didCropImage:(UIImage *)croppedImage {
    PhotoEditViewController *editController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PhotoEditViewController"];
    editController.photo = croppedImage;
    [self pushViewController:editController animated:YES];
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller {
    [self popViewControllerAnimated:YES];
}

@end
