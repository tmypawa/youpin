//
//  PublishViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <ImageIO/ImageIO.h>
#import "PublishViewController.h"
#import "CameraViewController.h"
#import "PublishImageCell.h"
#import "PublishTextCell.h"
#import "JTInputView.h"
#import "TasteItem.h"
#import "UIImage+Utility.h"
#import "ZipFile.h"
#import "ZipWriteStream.h"
#import "PhotoEditViewController.h"
#import "PhotoCropViewController.h"
#import "AlbumPhotoViewController.h"
#import "YouPin.h"

#define MAX_TITLE_LENGTH 20
#define MIN_DESCRIPTION_TEXT_LENGTH 15
#define LITTLE_THUMBNAIL_SIZE 120

@interface PublishViewController ()

@end

@implementation PublishViewController
@synthesize draftTasteItem = _snapshotTaste;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.autoSaveSnapshot = YES;
        _actionRowIndex = -1;
        _editingCellIndex = -1;
        _publishContentItems = [[NSMutableArray alloc] init];
        _publishTextItems = [[NSMutableArray alloc] init];
        _publishImageItems = [[NSMutableArray alloc] init];
        _cacheDir = [NSString stringWithFormat:@"%@/Library/Caches/%@",NSHomeDirectory(), PUBLISH_IMAGE_CACHE_DIR];
    }
    return self;
}

- (void)dealloc {
    [[PublishParamSet sharedInstance] reset];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)cancel:(id)sender {
    if (_titleTextField.text.length > 0 || _publishContentItems.count > 0) {
        AlertWithActionsAndTag(1000, @"确定要取消发布吗?", @"", @"取消",@"确定");
    }else {
        [self doCancel];
    }
}

- (void)doCancel {
    [_titleTextField resignFirstResponder];
    [self.view endEditing:YES];
    
    [[PublishParamSet sharedInstance] reset];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            [self clearAllPhotos];
            [self clearDrafts];
        }
    });
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)closeInstruction:(id)sender {
    [_instructionView removeFromSuperview];
    _instructionView = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:PUBLISH_NAV_BG_COLOR] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageFromColor:[UIColor clearColor]]];
    self.navigationController.navigationBar.tintColor = PUBLISH_TITLE_COLOR;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : PUBLISH_TITLE_COLOR};
    
    _titleTextField.tintColor = NavigationBarTintColor;
    _titleTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _titleTextField.returnKeyType = UIReturnKeyDone;
    _titleTextField.delegate = self;
    
    _titleBgView.layer.borderColor = PUBLISH_BORDER_COLOR.CGColor;
    _titleBgView.layer.borderWidth = 0.5f;
    _toolBgView.layer.borderColor = PUBLISH_BORDER_COLOR.CGColor;
    _toolBgView.layer.borderWidth = 0.5f;
    
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [_maskBtn removeFromSuperview];
    [_instructionView removeFromSuperview];
    
   
    [self updateViewFromExternalParams];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
    if (_thumbnailShotView) {
        [_thumbnailShotView removeFromSuperview];
        _thumbnailShotView = nil;
    }
    
    if (self.autoSaveSnapshot && !_snapshotTimer) {
        [self turnOnSnapshotTimer:YES];
    }
    
    if (![JTGlobalCache.tutorialSet containsObject:TUTORIAL_PUBLISH_INSTRUCTION]) {
        [JTGlobalCache.tutorialSet addObject:TUTORIAL_PUBLISH_INSTRUCTION];
        [[UIApplication sharedApplication].keyWindow addSubview:_instructionView];
    }else {
        [_instructionView removeFromSuperview];
        _instructionView = nil;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.navigationController.isBeingDismissed) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWillAppear:(NSNotification *)notification {
    if (!_isEditingText)  return;
    
    CGRect startFrame = [notification.userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    if(startFrame.origin.y != [UIScreen mainScreen].bounds.size.height) return;
    
    CGRect frame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (frame.size.height && frame.size.width > 0) {
        UIEdgeInsets insets = _tableView.contentInset;
        insets.bottom = frame.size.height;
        _tableView.contentInset = insets;
        _tableView.scrollIndicatorInsets = insets;
        
        if (_editingCellIndex != -1) {
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_editingCellIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        //[_tableView setContentOffset:CGPointMake(0, _tableView.contentSize.height + _tableView.contentInset.bottom - _tableView.frame.size.height) animated:YES];
    }
}

- (void)keyboardWillDisappear:(NSNotification *)notification {
    UIEdgeInsets insets = _tableView.contentInset;
    insets.bottom = 0;
    _tableView.contentInset = insets;
    _tableView.scrollIndicatorInsets = insets;
    
    _editingCellIndex = -1;
}

- (void)updateViewFromExternalParams {
    //restore from draft taste item
    if (_snapshotTaste) {
        [self restoreViewFromSnapshot];
    }else {
        if ([PublishParamSet sharedInstance].goods) {
            self.goods = [PublishParamSet sharedInstance].goods;
        }
        
        NSArray *photoURLs = [PublishParamSet sharedInstance].goodsPhotoURLs;
        if (photoURLs) {
            for (NSString *imageURL in photoURLs) {
                [self addImageItemWithURL:imageURL];
            }
        }
        
        NSArray *photos = [PublishParamSet sharedInstance].photos;
        __weak UITableView *__tableView = _tableView;
        if (photos) {
            NSInteger index = 0;
            for (UIImage *photo in photos) {
                [self addImageItem:photo completionHandler:^{
                    if (index == photos.count - 1) {
                        [__tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                    }
                }];
                index ++;
            }
        }
        
        NSArray *texts = [PublishParamSet sharedInstance].texts;
        if (texts) {
            for (NSString *text in texts) {
                [self addTextItem:text];
            }
        }
    }
    
    [_tableView reloadData];
}

- (void)restoreViewFromSnapshot {
    _tags = _snapshotTaste.tags;
    [PublishParamSet sharedInstance].tags = _tags;
    [PublishParamSet sharedInstance].goods = _snapshotTaste.goods;
    
    _titleTextField.text = _snapshotTaste.title;
    
    [self fillPhotoItemsArrayFromSnapshot];
    
    [_tableView reloadData];
}

- (void)fillPhotoItemsArrayFromSnapshot {
    if (_snapshotTaste.publishItems) {
        [_publishContentItems addObjectsFromArray:_snapshotTaste.publishItems];
        NSArray *textItems = [_publishContentItems filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            return [evaluatedObject isKindOfClass:[PublishTextItem class]];
        }]];
        
        NSArray *imageItems = [_publishContentItems filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            return [evaluatedObject isKindOfClass:[PublishImageItem class]];
        }]];
        
        [_publishTextItems addObjectsFromArray:textItems];
        [_publishImageItems addObjectsFromArray:imageItems];
    }
    
    for (PublishImageItem *imageItem in _publishImageItems) {
        if (imageItem.source == PublishImageSourceLocal) {
            NSString *filePath = [self localFilePathWithFileName:imageItem.imagePath];
            UIImage *thumbnail = [self generateThumbnailFromPath:filePath atSize:ZOOM_SIZE];
            imageItem.thumbnail = thumbnail;
        }
    }
}

- (UIImage*)generateThumbnailFromPath:(NSString *)imagePath atSize:(CGFloat)maxSize
{
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((CFURLRef)[NSURL fileURLWithPath:imagePath], NULL);
    if (!imageSource)
        return nil;
    
    CFDictionaryRef options = (__bridge CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                (id)kCFBooleanTrue, (id)kCGImageSourceCreateThumbnailWithTransform,
                                                (id)kCFBooleanTrue, (id)kCGImageSourceCreateThumbnailFromImageIfAbsent,
                                                (id)[NSNumber numberWithFloat:maxSize], (id)kCGImageSourceThumbnailMaxPixelSize,
                                                nil];
    CGImageRef imgRef = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options);
    
    UIImage* thumbnailImage = [UIImage imageWithCGImage:imgRef];
    
    CGImageRelease(imgRef);
    CFRelease(imageSource);
    
    return thumbnailImage;
}

- (void)turnOnSnapshotTimer:(BOOL)isTurnOn {
    if (isTurnOn) {
        if (_snapshotTimer) {
            [_snapshotTimer invalidate];
        }
        
        _snapshotTimer = [NSTimer timerWithTimeInterval:SNAPSHOT_INTERVAL target:self selector:@selector(saveTasteSnapshot) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_snapshotTimer forMode:NSRunLoopCommonModes];
    }else {
        if (_snapshotTimer) {
            [_snapshotTimer invalidate];
        }
        
        _snapshotTimer = nil;
    }
}

- (void)saveTasteSnapshot {
    if (!_snapshotTaste) {
        _snapshotTaste = [[TasteItem alloc] init];
        _snapshotTaste.extraId = JTGlobalCache.currentUser.phone;
        _snapshotTaste.nick = JTGlobalCache.currentUser.nickname;
        _snapshotTaste.headImage = JTGlobalCache.currentUser.avatar;
        _snapshotTaste.name = @"";
        _snapshotTaste.price = @"";
        _snapshotTaste.itemId = @"";
        _snapshotTaste.draftIdentifier = [[NSUUID UUID] UUIDString];
     
        [JTGlobalCache.draftBoxList addObject:_snapshotTaste];
    }
    
    _snapshotTaste.goods = [PublishParamSet sharedInstance].goods;
    _snapshotTaste.tags = [PublishParamSet sharedInstance].tags;
    _snapshotTaste.title = _titleTextField.text;
    _snapshotTaste.publishItems = [_publishContentItems copy];
    
    [JTGlobalCache synchronize];
}

- (NSString *)localFilePathWithFileName:(NSString *)fileName {
    return [NSString stringWithFormat:@"%@/%@", _cacheDir, fileName];
}


- (NSArray *)filenamesFromFilePathes:(NSArray *)filePathes {
    NSMutableArray *filenames = [[NSMutableArray alloc] init];
    for (NSString *path in filePathes) {
        [filenames addObject: path.lastPathComponent];
    }
    
    return filenames;
}

- (NSArray *)filePathesFromFilenames:(NSArray *)filenames {
    NSMutableArray *filePathes = [[NSMutableArray alloc] init];
    for (NSString *filename in  filenames) {
        [filePathes addObject: [self localFilePathWithFileName:filename]];
    }
    
    return filePathes;
}


- (NSArray *)photoItemPathesFromCurrentPhotos {
    NSMutableArray *photoItemPathes = [[NSMutableArray alloc] init];
    for (PublishImageItem *photoItem in _publishImageItems) {
        if (photoItem.source == PublishImageSourceLocal) {
            NSString *filePath = photoItem.imagePath;
            [photoItemPathes addObject:filePath.lastPathComponent];
        }else {
            [photoItemPathes addObject:photoItem.imagePath];
        }
    }
    
    return photoItemPathes;
}

- (IBAction)cancelInput:(id)sender {
    [_titleTextField resignFirstResponder];
    [_maskBtn removeFromSuperview];
}


- (void)processPhoto:(UIImage *)image completionHandler:(void(^)(UIImage *thumbnail, NSString *imagePath))completionHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *uuid = [[NSUUID UUID] UUIDString];
        UIImage *scaledImage = [image zoomImageWithSize:CGSizeMake(720, 720)];
        NSData *imageData = UIImageJPEGRepresentation(scaledImage, 0.7f);
        NSString *fullPath = [NSString stringWithFormat:@"%@/%@.jpg", _cacheDir, uuid];
        [imageData writeToFile:fullPath atomically:YES];
        UIImage *thumbnail = [image zoomImageWithSize:CGSizeMake(ZOOM_SIZE, ZOOM_SIZE)];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionHandler) {
                completionHandler(thumbnail, fullPath);
            }
        });
    });
}


- (IBAction)publish:(id)sender {
    [self.view endEditing:YES];
    
    BOOL isPublishValid = YES;
    
    if (_publishTextItems.count > 0) {
        NSInteger totalTextLength = 0;
        for (PublishTextItem *item in _publishTextItems) {
            totalTextLength += item.text.length;
        }
        
        if (totalTextLength == 0) {
            _hasShowTextTutorial = YES;
            
            NSInteger rowIndex = [_publishContentItems indexOfObject:_publishTextItems[0]];
            CGFloat maxOffset = _tableView.contentSize.height - _tableView.frame.size.height;
            if (maxOffset > 0) {
                [_tableView setContentOffset:CGPointMake(0, maxOffset) animated:YES];
            }
            
            PublishTextCell *textCell = (PublishTextCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]];
            if (textCell) {
                [self showTutorialWithAttachedView:textCell.inputView title:TUTORIAL_PUBLISH_TEXT_TAG offset:CGPointMake(-40, -70)];
            }else {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    PublishTextCell *textCell = (PublishTextCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]];
                    if (textCell) {
                        [self showTutorialWithAttachedView:textCell.inputView title:TUTORIAL_PUBLISH_TEXT_TAG offset:CGPointMake(-40, -70)];
                    }
                });
            }
            
            isPublishValid = NO;
        }else if (totalTextLength < MIN_DESCRIPTION_TEXT_LENGTH) {
            NSString *title = [NSString stringWithFormat:@"宝贝描述文字不得少于%d字", MIN_DESCRIPTION_TEXT_LENGTH];
            Alert(title, @"", @"确定");
            isPublishValid = NO;
        }
    }else {
        CGFloat maxOffset = _tableView.contentSize.height - _tableView.frame.size.height;
        if (maxOffset > 0) {
            [_tableView setContentOffset:CGPointMake(0, maxOffset) animated:YES];
        }
        
         [self showTutorialWithAttachedView:_toolBgView title:TUTORIAL_PUBLISH_TEXT_TAG offset:CGPointMake(-60, -10)];
        _hasShowTextTutorial = YES;
        isPublishValid = NO;
    }
    
    if (!isPublishValid) {
        return;
    }
    
    if (_publishImageItems.count == 0) {
        Alert(@"无图无真相，至少添加一张实拍~~", @"", @"确定");
        
        isPublishValid = NO;
    }else if(!_hasShowImageTutorial && _publishImageItems.count == 1 && !_hasShowTextTutorial && JTGlobalCache.publishImageTutorialTimes < 2) {
        JTGlobalCache.publishImageTutorialTimes += 1;
        _hasShowImageTutorial = YES;
        [self showTutorialWithAttachedView:_addImageBtn title:TUTORIAL_PUBLISH_IMAGE_TAG offset:CGPointZero];
        
        isPublishValid = NO;
    }
   
    if (!isPublishValid) {
        return;
    }
    
    [self.view endEditing:YES];
    
    [self saveTasteSnapshot];
    
    PostSettingViewController *postSettingController = [self.storyboard instantiateViewControllerWithIdentifier:@"PostSettingViewController"];
    postSettingController.delegate = self;
    [self.navigationController pushViewController:postSettingController animated:YES];
}

- (void)publishPost {
    _tags  = [PublishParamSet sharedInstance].tags;
    self.goods = [PublishParamSet sharedInstance].goods;
    
    if (self.autoSaveSnapshot) {
        [self turnOnSnapshotTimer:NO];
    }
    
    [self saveTasteSnapshot];
    
    TasteItem *taste = [self generateTasteItem];
    
    [self startPublishFlowWithTaste:taste];
}

- (NSString *)generateLittleThumbnail {
    NSString *imagePath = nil;
    NSString *thumbnailName = nil;
    for (PublishImageItem *item in _publishImageItems) {
        if (item.source == PublishImageSourceLocal) {
            imagePath = [self localFilePathWithFileName:item.imagePath];
            break;
        }
    }
    
    if (!imagePath) {
        if (_publishImageItems.count > 0) {
            thumbnailName = [_publishImageItems[0] imagePath];
        }
    }else {
       UIImage *thumbnail = [self generateThumbnailFromPath:imagePath atSize:LITTLE_THUMBNAIL_SIZE];
        NSString *filename = [NSString stringWithFormat:@"%@.jpg", [[NSUUID UUID] UUIDString]];
        NSString *path = [self localFilePathWithFileName:filename];
        thumbnailName = filename;
        NSData *imageData = UIImageJPEGRepresentation(thumbnail, 0.8f);
        [imageData writeToFile:path atomically:YES];
    }
    
    return thumbnailName;
}

- (TasteItem *)generateTasteItem {
    TasteItem *taste = [[TasteItem alloc] init];
    taste.extraId = JTGlobalCache.currentUser.phone;
    taste.nick = JTGlobalCache.currentUser.nickname;
    taste.headImage = JTGlobalCache.currentUser.avatar;
    taste.title = _titleTextField.text;
    taste.publishItems = _publishContentItems;
    if (_publishTextItems.count > 0) {
        taste.tasteDescription = [_publishTextItems[0] text];
    }else {
        taste.tasteDescription = taste.title;
    }
    
    if (_publishImageItems.count > 0) {
        taste.littleThumbnailImage = [self generateLittleThumbnail];
    }
    
    taste.createTime = [[NSDate date] timeIntervalSince1970] * 1000;
    taste.updateTime = taste.createTime;
    taste.source = self.goods ? [@(TasteSourceTypeTaobao) stringValue] : [@(TasteSourceTypePhoto) stringValue];
    taste.draftIdentifier = [[NSUUID UUID] UUIDString];
    taste.photosZipFilePath = @"";
    taste.requestId = _snapshotTaste.draftIdentifier;
    taste.draftIdentifier = _snapshotTaste.draftIdentifier;
    
    if (_tags) {
        taste.tags = _tags;
    }
    
    if (self.goods) {
        taste.billId = self.goods.orderId;
        taste.price = self.goods.price;
        taste.itemId = self.goods.itemId;
        taste.name = self.goods.title;
        taste.buyLink = self.goods.buylink;
    }else {
        taste.price = @"";
        taste.itemId = @"";
        taste.name = @"";
        taste.buyLink = @"";
    }
    
    return taste;
}

- (void)startPublishFlowWithTaste:(TasteItem *)taste {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @autoreleasepool {
            [self prepareUploadFilesForTaste:taste];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self doPublishWithTaste:taste];
            [self dismiss];
        });
    });
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareUploadFilesForTaste:(TasteItem *)taste {
    __block BOOL hasFilesToUpload = NO;
    [_publishImageItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (((PublishImageItem *)obj).source == PublishImageSourceLocal) {
            hasFilesToUpload = YES;
            *stop = YES;
        }
    }];
    
    if (hasFilesToUpload) {
        NSString *zipFilePath = [NSString stringWithFormat:@"%@/%@.zip", _cacheDir, [[NSUUID UUID] UUIDString]];
        ZipFile *zipFile = [[ZipFile alloc] initWithFileName:zipFilePath mode:ZipFileModeCreate];
        int index = 1;
        for (PublishImageItem *imageItem in _publishImageItems) {
            if (imageItem.source == PublishImageSourceLocal) {
                NSData *photoData = [[NSData alloc] initWithContentsOfFile:[self localFilePathWithFileName:imageItem.imagePath]];
                if(!photoData) continue;
                
                NSString *imageName = [imageItem.imagePath lastPathComponent];
                ZipWriteStream *writeStream = [zipFile writeFileInZipWithName:imageName compressionLevel:ZipCompressionLevelBest];
                [writeStream writeData:photoData];
                [writeStream finishedWriting];
                photoData = nil;
            }
            
            index ++;
        }
        
        [zipFile close];
        
        taste.photosZipFilePath = zipFilePath;
        _snapshotTaste.photosZipFilePath = zipFilePath.lastPathComponent;
        [JTGlobalCache synchronize];
    }
}

- (void)doPublishWithTaste:(TasteItem *)taste {
    [[PostManager defaultManager] publishPost:taste];
}

- (void)addTextItem:(NSString *)text {
    PublishTextItem *textItem = [[PublishTextItem alloc] init];
    if (text) {
        textItem.text = text;
    }
    [_publishContentItems addObject:textItem];
    [_publishTextItems addObject:textItem];
}

- (IBAction)addText:(id)sender {
    [self addTextItem:@""];
    _isEditingText = YES;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_publishContentItems.count - 1 inSection:0];
    [_tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    PublishTextCell *insertedCell = (PublishTextCell *)[_tableView cellForRowAtIndexPath:indexPath];
    
    [insertedCell.inputView becomeFirstResponder];
}


- (IBAction)addPhoto:(UIButton *)sender {
    if (_publishImageItems.count >= MAX_PHOTO_NUMBER) {
        NSString *title = [NSString stringWithFormat:@"最多只能添加%d张宝贝照片", MAX_PHOTO_NUMBER];
        Alert(@"提示", title, @"好的");
    }else {
        PhotoSelectionController *photoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoSelectionController"];
        photoController.photoDelegate = self;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:photoController];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
    
}

- (void)addImageItemWithURL:(NSString *)imageURL {
    PublishImageItem *imageItem = [[PublishImageItem alloc] init];
    imageItem.source = PublishImageSourceOnline;
    imageItem.imagePath = imageURL;
    [_publishContentItems addObject:imageItem];
    [_publishImageItems addObject:imageItem];
}

- (void)addImageItem:(UIImage *)photo completionHandler:(void(^)())completionHandler {
    [self processPhoto:photo completionHandler:^(UIImage *thumbnail, NSString *imagePath) {
        PublishImageItem *imageItem = [[PublishImageItem alloc] init];
        imageItem.source = PublishImageSourceLocal;
        imageItem.thumbnail = thumbnail;
        imageItem.imagePath = [imagePath lastPathComponent];
        [_publishContentItems addObject:imageItem];
        [_publishImageItems addObject:imageItem];
        
        if (completionHandler) {
            completionHandler();
        }
    }];
}


- (void)clearAllPhotos {
    for (PublishImageItem *imageItem in _publishImageItems) {
        [[NSFileManager defaultManager] removeItemAtPath:[self localFilePathWithFileName:imageItem.imagePath] error:nil];
    }
    
    [_publishContentItems removeObjectsInArray:_publishImageItems];
    [_publishImageItems removeAllObjects];
}

- (void)clearDrafts {
    [[PostManager defaultManager] clearDrafts];
}

- (void)removePhotoAtIndex:(NSInteger)photoIndex {
    PublishImageItem *photoItem = _publishImageItems[photoIndex];
    if (photoItem.source == PublishImageSourceLocal) {
        [[NSFileManager defaultManager] removeItemAtPath:photoItem.imagePath error:nil];
    }
    
    [_publishImageItems removeObject:photoItem];
    [_publishContentItems removeObject:photoItem];
}

- (void)removeText:(UIButton *)sender {
    UITableViewCell *cell = [self textCellForCellElement:sender];
    NSInteger row = [_tableView indexPathForCell:cell].row;
    if (row != NSNotFound) {
        _actionRowIndex = row;
        AlertWithActionsAndTag(2000, @"确定要删除这段文字？ ", @"", @"取消", @"确定");
    }
}

- (PublishTextCell *)textCellForCellElement:(UIView *)view {
    PublishTextCell *cell = nil;
    if (iOSVersion < 8.0f) {
        cell = (PublishTextCell *)view.superview.superview.superview;
    }else {
        cell = (PublishTextCell *)view.superview.superview;
    }
    
    return cell;
}

- (void)beginInputText:(UIButton *)sender {
    _isEditingText = YES;
    
    PublishTextCell *cell = [self textCellForCellElement:sender];
    _editingCellIndex = [_tableView indexPathForCell:cell].row;
    
    cell.maskButton.hidden = YES;
    [cell.inputView becomeFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    _isEditingText = YES;
    
    UITableViewCell *cell = [self textCellForCellElement:textView];
    if (cell) {
        NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
        [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    _isEditingText = NO;
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    UITableViewCell *cell = [self textCellForCellElement:textView];
    if (cell) {
        NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
        NSString *text = textView.text ? [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]] : @"";
        PublishItem *item = _publishContentItems[indexPath.row];
        if ([item isKindOfClass:[PublishTextItem class]]) {
            ((PublishTextItem *)item).text = text;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    PublishTextCell *cell = [self textCellForCellElement:textView];
    cell.maskButton.hidden = NO;
    if (_publishContentItems.count >= 3 && ![JTGlobalCache.tutorialSet containsObject:TUTORIAL_PUBLISH_SORT]) {
        [self showTutorialWithAttachedView:cell.inputView title:TUTORIAL_PUBLISH_SORT offset:CGPointMake(0, - cell.frame.size.height / 2)];
        [JTGlobalCache.tutorialSet addObject:TUTORIAL_PUBLISH_SORT];
        [JTGlobalCache synchronize];
    }
}

#pragma mark - PostSettingViewControllerDelegate
- (void)postSettingViewControllerDidFinish:(PostSettingViewController *)controller {
    [self publishPost];
}

#pragma mark - CameraViewControllerDelegate
//- (void)cameraViewController:(CameraViewController *)controller didPickImage:(UIImage *)image {
//   
//}
//
//- (void)cameraViewControllerDidCancel:(CameraViewController *)controller {
//
//}


- (void)photoSelectionController:(PhotoSelectionController *)controller didSelectAlbumPhoto:(UIImage *)photoImage {
    __weak UITableView *__tableView = _tableView;
    __weak NSArray *__items = _publishContentItems;
    __weak PublishViewController *__self = self;
    [self addImageItem:photoImage completionHandler:^{
        if (__tableView) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:__items.count - 1 inSection:0];
            [__tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [__tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            if (__items.count >= 3 && ![JTGlobalCache.tutorialSet containsObject:TUTORIAL_PUBLISH_SORT]) {
                PublishImageCell *imageCell = (PublishImageCell *)[__tableView cellForRowAtIndexPath:indexPath];
                if (imageCell) {
                    [__self showTutorialWithAttachedView:imageCell.thumbnailView title:TUTORIAL_PUBLISH_SORT offset:CGPointMake(0, - imageCell.frame.size.height / 2)];
                    [JTGlobalCache.tutorialSet addObject:TUTORIAL_PUBLISH_SORT];
                    [JTGlobalCache synchronize];
                }
            }
        }
    }];
}


#pragma mark - UITableViewDelegate 
- (NSInteger)tableView:(FMMoveTableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = _publishContentItems.count;
    
    if (tableView.movingIndexPath && tableView.movingIndexPath.section != tableView.initialIndexPathForMovingRow.section)
    {
        if (section == tableView.movingIndexPath.section) {
            numberOfRows++;
        }
        else if (section == tableView.initialIndexPathForMovingRow.section) {
            numberOfRows--;
        }
    }
    
    return numberOfRows;
}

- (CGFloat)tableView:(FMMoveTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    indexPath = [tableView adaptedIndexPathForRowAtIndexPath:indexPath];
    
    PublishItem *publishItem = _publishContentItems[indexPath.row];
    if ([publishItem isKindOfClass:[PublishImageItem class]]) {
        return 130;
    }else {
        return 120;
    }
}

- (UITableViewCell *)tableView:(FMMoveTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *adaptedIndexPath = indexPath;
    if (![tableView indexPathIsMovingIndexPath:indexPath]) {
        if (tableView.movingIndexPath != nil) {
            adaptedIndexPath = [tableView adaptedIndexPathForRowAtIndexPath:indexPath];
        }
    }
    
    PublishItem *publishItem = _publishContentItems[adaptedIndexPath.row];
    if ([publishItem isKindOfClass:[PublishImageItem class]]) {
        return [self tableView:tableView imageCellForRowAtIndexPath:indexPath];
    }else {
        return [self tableView:tableView textCellForRowAtIndexPath:indexPath];
    }
}


- (PublishTextCell *)tableView:(FMMoveTableView *)tableView textCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublishTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PublishTextCell" forIndexPath:indexPath];
    if (cell.closeButton.tag == 0) {
        [cell.closeButton addTarget:self action:@selector(removeText:) forControlEvents:UIControlEventTouchUpInside];
        [cell.maskButton addTarget:self action:@selector(beginInputText:) forControlEvents:UIControlEventTouchUpInside];
        cell.inputView.delegate = self;
    }
    
    if ([tableView indexPathIsMovingIndexPath:indexPath])
    {
        [cell prepareForMove];
    }
    else
    {
        [cell prepareForShow];
        
        if (tableView.movingIndexPath != nil) {
            indexPath = [tableView adaptedIndexPathForRowAtIndexPath:indexPath];
        }
        
        cell.closeButton.tag = indexPath.row + 1;
        cell.maskButton.tag = indexPath.row + 1;
        cell.inputView.tag = indexPath.row + 1;
        
        PublishTextItem *textItem = _publishContentItems[indexPath.row];
        cell.inputView.text = textItem.text;
        
        NSInteger textItemIndex = [_publishTextItems indexOfObject:textItem];
        if (JTGlobalCache.publishPlaceHolders &&  textItemIndex < JTGlobalCache.publishPlaceHolders.count) {
            cell.inputView.placeholderText = JTGlobalCache.publishPlaceHolders[textItemIndex];
        }else {
            cell.inputView.placeholderText = @"添加宝贝描述";
        }
    }
    
    return cell;
}

- (PublishImageCell *)tableView:(FMMoveTableView *)tableView imageCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublishImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PublishImageCell" forIndexPath:indexPath];
    
    if ([tableView indexPathIsMovingIndexPath:indexPath])
    {
        [cell prepareForMove];
    }
    else
    {
        [cell prepareForShow];
        
        if (tableView.movingIndexPath != nil) {
            indexPath = [tableView adaptedIndexPathForRowAtIndexPath:indexPath];
        }
        
        PublishImageItem *imageItem = _publishContentItems[indexPath.row];
        if (imageItem.source == PublishImageSourceLocal) {
            cell.thumbnailView.image = imageItem.thumbnail;
        }else {
            [cell.thumbnailView setImageWithURL:imageItem.imagePath placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectFade];
        }

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PublishItem *item = _publishContentItems[indexPath.row];
    NSInteger imageIndex = [_publishImageItems indexOfObject:item];
    if (imageIndex == NSNotFound)  return;
    
    if ([item isKindOfClass:[PublishImageItem class]]) {
        PublishImageCell *cell = (PublishImageCell *)[tableView cellForRowAtIndexPath:indexPath];
        _thumbnailShotView = [[UIImageView alloc] initWithFrame:cell.thumbnailView.frame];
        _thumbnailShotView.contentMode = UIViewContentModeScaleAspectFill;
        _thumbnailShotView.clipsToBounds = YES;
        _thumbnailShotView.image = cell.thumbnailView.image;
        [cell.bgView addSubview:_thumbnailShotView];
        
        NSMutableArray *photoURLS = [[NSMutableArray alloc] init];
        for (PublishImageItem *imageItem in _publishImageItems) {
            if (imageItem.source == PublishImageSourceLocal) {
                NSString *filePath = [self localFilePathWithFileName:imageItem.imagePath];
                [photoURLS addObject:[NSURL fileURLWithPath:filePath]];
            }else {
                [photoURLS addObject:[NSURL URLWithString:imageItem.imagePath]];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _imageBrowerController = [[PublishPhotoBrowerController alloc] initWithImageURLs: photoURLS];
            _imageBrowerController.publishPhotoDelegate = self;
            _imageBrowerController.transitioningDelegate = self;
            _imageBrowerController.showPageIndex = imageIndex;
            [self presentViewController:_imageBrowerController animated:YES completion:nil];
        });
    }
}

- (NSIndexPath *)moveTableView:(FMMoveTableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    //	Uncomment these lines to enable moving a row just within it's current section
    //	if ([sourceIndexPath section] != [proposedDestinationIndexPath section]) {
    //		proposedDestinationIndexPath = sourceIndexPath;
    //	}
    
    return proposedDestinationIndexPath;
}


- (BOOL)moveTableView:(FMMoveTableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)moveTableView:(FMMoveTableView *)tableView moveRowFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    PublishItem *sourceItem = _publishContentItems[fromIndexPath.row];

    [_publishContentItems removeObject:sourceItem];
    if ([sourceItem isKindOfClass:[PublishImageItem class]]) {
        [_publishImageItems removeObject:sourceItem];
    }else if([sourceItem isKindOfClass:[PublishTextItem class]]) {
        [_publishTextItems removeObject:sourceItem];
    }
    
    [_publishContentItems insertObject:sourceItem atIndex:toIndexPath.row];
    
    if ([sourceItem isKindOfClass:[PublishImageItem class]]) {
        NSInteger toInsertImageItemsIndex = 0;
        for (int i = (int)toIndexPath.row - 1; i >= 0; i--) {
            PublishItem *item = _publishContentItems[i];
            if ([item isKindOfClass:[PublishImageItem class]]) {
                toInsertImageItemsIndex = [_publishImageItems indexOfObject:item] + 1;
                break;
            }
        }
        
        [_publishImageItems insertObject:sourceItem atIndex:toInsertImageItemsIndex];
    }else if([sourceItem isKindOfClass:[PublishTextItem class]]) {
        NSInteger toInsertTextItemsIndex = 0;
        for (int i = (int)toIndexPath.row - 1; i >= 0; i--) {
            PublishItem *item = _publishContentItems[i];
            if ([item isKindOfClass:[PublishTextItem class]]) {
                toInsertTextItemsIndex = [_publishTextItems indexOfObject:item] + 1;
                break;
            }
        }
        
        [_publishTextItems insertObject:sourceItem atIndex:toInsertTextItemsIndex];
    }
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    JTImageZoomAnimationController *animationController = [[JTImageZoomAnimationController alloc] initWithReferenceView:_thumbnailShotView];
    return animationController;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    if (_imageBrowerController.numberOfImages > 0) {
        NSInteger imageIndex = _imageBrowerController.numberOfImages > 0 ? _imageBrowerController.currentImageIndex : 0;
        NSInteger cellIndex = [_publishContentItems indexOfObject:_publishImageItems[imageIndex]];
        
        PublishImageCell *cell = (PublishImageCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndex inSection:0]];
        if (cell) {
            _thumbnailShotView.frame = cell.thumbnailView.frame;
            _thumbnailShotView.image = _publishImageItems.count > 0? cell.thumbnailView.image : nil;
            [cell.bgView addSubview:_thumbnailShotView];
        }
    }
   
    JTImageZoomAnimationController *animationController = [[JTImageZoomAnimationController alloc] initWithReferenceView:_thumbnailShotView];
    return animationController;
}

- (void)publishPhotoBrowerController:(PublishPhotoBrowerController *)controller didDeletePhotoItemAtIndex:(NSInteger)itemIndex {
    if (itemIndex < _publishImageItems.count && itemIndex >= 0) {
        [self removePhotoAtIndex:itemIndex];
        [_tableView reloadData];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1000 && buttonIndex == 1) {
        [self doCancel];
    }
    
    if (alertView.tag == 2000) {
        if (buttonIndex == 1) {
            PublishItem *item = _publishContentItems[_actionRowIndex];
            if ([item isKindOfClass:[PublishTextItem class]]) {
                [_publishContentItems removeObject:item];
                [_publishTextItems removeObject:item];
                [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_actionRowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            }
        }
        
        _actionRowIndex = -1;
    }
}

@end
