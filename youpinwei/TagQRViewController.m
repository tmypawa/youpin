//
//  TagQRViewController.m
//  youpinwei
//
//  Created by tmy on 15/3/24.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "TagQRViewController.h"

@interface TagQRViewController ()

@end

@implementation TagQRViewController
{
    
    IBOutlet UIImageView *_QRImageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [_QRImageView setImageWithURL:self.QRCodeUrl effect:SDWebImageEffectNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showActionMenu:(id)sender {
    if (!_QRImageView.image)  return;
        
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"保存到相册", nil];
    [actionSheet showInView:self.navigationController.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        UIImageWriteToSavedPhotosAlbum(_QRImageView.image, nil, nil, nil);
        Alert(@"二维码图片已保存到相册", @"", @"确定");
    }
}

@end
