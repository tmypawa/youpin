//
//  config.h
//  youpinwei
//
//  Created by tmy on 14-9-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#ifndef youpinwei_config_h
#define youpinwei_config_h

#define APP_SCHEME @"shaiba"
#define APP_INVITATION_PREFIX @"s_b_"

#define UMENG_ID @"5461ebedfd98c5e34a003f6b"
#define WEIXIN_APP_ID @"wx70671353e39961e4"
#define WEIXIN_APP_SECRET @"a16f5303621ef97171ef2c7bf513f113"

#define ALIPAY_CLIENT_ID @"2014071800007092"
#define PUBLISH_IMAGE_CACHE_DIR @"PublishCache"

#define ADDRESS_BOOK_SYNC_DURATION 3600*24
#define CROP_IMAGE_SIZE 1000
#define MAX_TAG_HISTORY_SIZE 10
#define LINE_SPACE 5

#endif
