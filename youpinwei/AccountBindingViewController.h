//
//  RegisterViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaobaoURLProtocol.h"

@interface AccountBindingViewController : UIViewController<UIWebViewDelegate, TaobaoURLProtocolDelegate>

@end
