//
//  RelatedTagsViewController.m
//  youpinwei
//
//  Created by tmy on 15/3/23.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "RelatedTagsViewController.h"
#import "UITableViewCollection+LoadMore.h"
#import "CategoryChooseCell.h"
#import "TagViewController.h"

@implementation RelatedTagsViewController
{
    NSMutableArray       *_categoryList;
    BOOL                 _hasNext;
    UICollectionReusableView       *_noMoreFooterView;
    UICollectionReusableView       *_loadMoreFooterView;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _categoryList = [[NSMutableArray alloc] init];
        _hasNext = YES;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter  withReuseIdentifier:@"LoadMoreFooterView"];
    [self addLoadMoreView];
    [self loadCategoryList];
}

- (void)addLoadMoreView {
    UIImageView *loadingMoreView = [[UIImageView alloc] initWithImage:[UIImage animatedImageNamed:@"R" duration:1.6]];
    loadingMoreView.frame = CGRectMake(0, 0, screenWidth, 56);
    loadingMoreView.center = CGPointMake(screenWidth/2, loadingMoreView.center.y);
    loadingMoreView.contentMode = UIViewContentModeCenter;
    loadingMoreView.backgroundColor = MAIN_CELL_BG_COLOR;
    [loadingMoreView stopAnimating];
    loadingMoreView.hidden = YES;
    
    [self.collectionView setupLoadMoreWithCustomView:loadingMoreView];
    [self.collectionView addTarget:self forLoadMoreAction:@selector(loadNext:)];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(!self.view.superview)
        return;
    
    if (!self.collectionView.isLoadingMore && _hasNext) {
        float maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
        float offsetY = scrollView.contentOffset.y;
        if (offsetY >= maxOffsetY - 170) {
            [self.collectionView startLoadMore];
        }
    }
}

- (NSTimeInterval)startTime {
    if (!_categoryList || _categoryList.count == 0) {
        return 0;
    }else {
        Tag *tag = _categoryList[0];
        return tag.updateTime;
    }
}

- (NSTimeInterval)endTime {
    if (!_categoryList || _categoryList.count == 0) {
        return 0;
    }else {
        Tag *tag = [_categoryList lastObject];
        return tag.updateTime;
    }
}

- (void)loadCategoryList {
    [self showHUDViewWithTitle:@""];
    
    [[YouPinAPI sharedAPI] fetchMoreRelatedTagListWithTag:self.tagName startTime:[self startTime] endTime:[self endTime] callback:^(NSArray *tags, NSError *error) {
        [self hideHUDView];
        if (!error) {
            [_categoryList addObjectsFromArray:tags];
            [self.collectionView reloadData];
        }
    }];
}

- (void)loadNext:(UIImageView *)loadingView {
    if (!_hasNext) {
        return;
    }
    
    loadingView.hidden = NO;
    [loadingView startAnimating];
    
    [self loadNextData];
}

- (void)loadNextData {
    [[YouPinAPI sharedAPI] fetchMoreRelatedTagListWithTag:self.tagName startTime:[self startTime] endTime:[self endTime] callback:^(NSArray *tags, NSError *error) {
        if (!error) {
            if (tags.count == 0) {
                _hasNext = NO;
            }
            
            [_categoryList addObjectsFromArray:tags];
            [self.collectionView endLoadMoreWithHandler:^(UIView *customView) {
                [((UIImageView *)customView) stopAnimating];
            }];
            
            [self.collectionView reloadData];
        }else {
            
        }
    }];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        if (!_hasNext) {
            if (!_noMoreFooterView) {
                _noMoreFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"NoMoreFooterView" forIndexPath:indexPath];
            }
            
            return _noMoreFooterView;
        }else {
            if (!_loadMoreFooterView) {
                _loadMoreFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"LoadMoreFooterView" forIndexPath:indexPath];
            }
            
            [_loadMoreFooterView addSubview:self.collectionView.loadMoreView];
            return _loadMoreFooterView;
        }
    }
    else {
        return nil;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _categoryList ? _categoryList.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryChooseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryChooseCell" forIndexPath:indexPath];
    Tag *tag = _categoryList[indexPath.row];
    cell.thumbnailView.backgroundColor = [UIColor colorWithString:tag.color];
    [cell.thumbnailView setImageWithURL:tag.thumbnailsUrl effect:SDWebImageEffectFade];
    cell.titleLabel.text = tag.name;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Tag *tag = _categoryList[indexPath.row];
    TagViewController *tagController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
    tagController.tagName = tag.name;
    [self.navigationController pushViewController:tagController animated:YES];
}

@end
