//
//  ShareAddingCell.m
//  youpinwei
//
//  Created by tmy on 14-8-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "ShareAddingCell.h"

@implementation ShareAddingCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithRed:0.84 green:0.84 blue:0.84 alpha:1].CGColor;
}

@end
