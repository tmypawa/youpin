//
//  GlobalCacheDefine.m
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "GlobalCacheDefine.h"

JTCache_Value_Synthesize(isReturnUser, IsReturnUser, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(isLogin, IsLogin, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(isBinded, IsBinded, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(isBindedMore, IsBindedMore, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(hasShowAddressBookTip, HasShowAddressBookTip, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(hasDeclinedWithCustomDialog, HasDeclinedWithCustomDialog, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(hasDeclinedWithSystemDialog, HasDeclinedWithSystemDialog, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(hasAllowedSyncAddressBook, HasAllowedSyncAddressBook, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(bannerSwitch, BannerSwitch, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(hotTagSwitch, HotTagSwitch, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(bannerHeight, BannerHeight, float, JTSyncTypePreference ,0)
JTCache_Value_Synthesize(tagHeight, TagHeight, float, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(mode, Mode, NSInteger, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(showInvitation, ShowInvitation, BOOL, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(publishImageTutorialTimes, PublishImageTutorialTimes, NSInteger, JTSyncTypePreference, 0)

JTCache_Value_Synthesize(lastAddressBookSyncTime, LastAddressBookSyncTime, NSTimeInterval, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(lastNotificationUpdateTime, LastNotificationUpdateTime, NSTimeInterval, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(lastFriendNotificationTime, LastFriendNotificationTime, NSTimeInterval, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(friendNotificationBadge, FriendNotificationBadge, NSInteger, JTSyncTypePreference, 0)
JTCache_Value_Synthesize(friendTimelineBadge, FriendTimelineBadge, NSInteger, JTSyncTypePreference, 0)

JTCache_Object_Synthesize(lastVersion, LastVersion, NSString, JTSyncTypePreference, 0)
JTCache_Object_Synthesize(currentUser, CurrentUser, User, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(recentTastes, RecentTastes, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(publicTastes, PublicTastes, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(recommendTags, RecommendTags, NSArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(discoveryRecommendTags, DiscoveryRecommendTags, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(discoveryBanners, DiscoveryBanners, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(goodsList, GoodsList, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(draftBoxList, DraftBoxList, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(publicBanners, PublicBanners, NSArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(lastNotificationList, LastNotificationList, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(notificationList, NotificationList, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(tutorialSet, TutorialSet, NSMutableSet, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(wxShare, WxShare, NSDictionary, JTSyncTypePreference, 0)
JTCache_Object_Synthesize(smsShare, SmsShare, NSString, JTSyncTypePreference, 0)
JTCache_Object_Synthesize(publishPlaceHolders, PublishPlaceHolders, NSArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(tagHistoryList, TagHistoryList, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(discoveryHistoryList, DiscoveryHistoryList, NSMutableArray, JTSyncTypeArchive, 0)
JTCache_Object_Synthesize(friendInvitationCode, FriendInvitationCode, NSString, JTSyncTypeNone, 0)
JTCache_Object_Synthesize(externalPostId, ExternalPostId, NSString, JTSyncTypeNone, 0)



//temp cache
JTCache_Value_Synthesize(currentLoginType, CurrentLoginType, int, JTSyncTypeNone, 0)
JTCache_Object_Synthesize(thirdLoginData, ThirdLoginData, NSDictionary, JTSyncTypeNone, 0)