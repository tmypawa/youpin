//
//  RootViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "RootViewController.h"
#import "InvitationCodeViewController.h"
#import "GuideViewController.h"
#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "MainViewController.h"
#import "PublicTimelineController.h"
#import "PersonalInfoViewController.h"
#import "SettingsViewController.h"
#import "NotificationViewController.h"
#import "CenterNavigationController.h"
#import "ShaibaViewController.h"
#import "RDVTabBarItem.h"
#import "PublishViewController.h"
#import "PostEditViewController.h"
#import "DiscoveryViewController.h"


@interface RootViewController ()

@end

@implementation RootViewController
{
    BOOL        _handleForAppLaunch;
    TasteItem   *_draftTaste;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PublicTimelineController *publicController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublicTimelineController"];
    DiscoveryViewController *discoveryController = [self.storyboard instantiateViewControllerWithIdentifier:@"DiscoveryViewController"];
    MainViewController *mainController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    PersonalInfoViewController *myInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
    UIViewController *placeholderControlelr  = [[UIViewController alloc] init];
    ShaibaViewController *tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"ShaibaViewController"];
    tabBarController.viewControllers  = @[publicController, discoveryController, placeholderControlelr, mainController, myInfoController];
    NSArray *tabItemIcons = @[@"icon_home_normal", @"icon_explore_normal", @"icon_friends_nromal", @"icon_mine_normal", @""];
    NSArray *tabItemHighlightIcons = @[@"icon_home_selected", @"icon_explore_highlight", @"icon_friends_highlight", @"icon_mine_normal", @""];
    NSArray *tabItemTitles = @[@"首页", @"发现",@"", @"朋友们", @"我的"];
    
    int index = 0;
    for (RDVTabBarItem *item in tabBarController.tabBar.items) {
        UIViewController *contentController = tabBarController.viewControllers[index];
        item.backgroundColor = [UIColor whiteColor];
        [item setTitle:tabItemTitles[index]];
        item.selectedTitleAttributes = @{NSForegroundColorAttributeName : NavigationBarTintColor, NSFontAttributeName : [UIFont systemFontOfSize:12]};
        item.unselectedTitleAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.61 green:0.61 blue:0.61 alpha:1], NSFontAttributeName : [UIFont systemFontOfSize:12]};
        if (index < 2) {
            [item setFinishedSelectedImage:[UIImage imageNamed:tabItemHighlightIcons[index]] withFinishedUnselectedImage:[UIImage imageNamed:tabItemIcons[index]]];
        }else if (index > 2) {
            [item setFinishedSelectedImage:[UIImage imageNamed:tabItemHighlightIcons[index - 1]] withFinishedUnselectedImage:[UIImage imageNamed:tabItemIcons[index - 1]]];
        }
        
        item.badgeBackgroundColor = nil;
        item.badgeBackgroundImage = [UIImage imageNamed:@"icon_mine_point"];
        
//        if (index < 2) {
//            [item setBackgroundSelectedImage:[UIImage imageNamed:@"bg_table_line"] withUnselectedImage:[UIImage imageNamed:@"bg_table_line"]];
//        }
        
        if (index != 2 ) {
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:contentController action:@selector( handleForDoubleTapTab)];
            tapGestureRecognizer.numberOfTapsRequired = 2;
            tapGestureRecognizer.delaysTouchesBegan = NO;
            tapGestureRecognizer.delaysTouchesEnded = NO;
            [item addGestureRecognizer:tapGestureRecognizer];
        }
        
        index++;
    }
    
    CenterNavigationController *navController = [[CenterNavigationController alloc] initWithRootViewController:tabBarController];
    
    [self addChildViewController:navController];
    [self.view addSubview:navController.view];
    [navController didMoveToParentViewController:self];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!_handleForAppLaunch) {
        _handleForAppLaunch = YES;
        
        if (!JTGlobalCache.isReturnUser) {
            UINavigationController *navController = nil;
            
            if (JTGlobalCache.mode == InitModeNormal) {
                LoginViewController *loginController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                loginController.showGuide = YES;
                navController = [[UINavigationController alloc] initWithRootViewController:loginController];
            }else if(JTGlobalCache.mode == InitModeReview) {
                RegisterViewController *registerController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
                navController = [[UINavigationController alloc] initWithRootViewController:registerController];
            }else if(JTGlobalCache.mode == InitModeCrash){
                LoginViewController *loginController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                loginController.showGuide = YES;
                navController = [[UINavigationController alloc] initWithRootViewController:loginController];
            }else {
                LoginViewController *loginController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                loginController.showGuide = YES;
                navController = [[UINavigationController alloc] initWithRootViewController:loginController];
            }

            
            navController.navigationBar.translucent = YES;
            [self presentViewController:navController animated:NO completion:nil];
        }
        
        if (JTGlobalCache.isLogin && JTGlobalCache.draftBoxList.count > 0) {
            TasteItem *draftTaste = JTGlobalCache.draftBoxList.lastObject;
            if (draftTaste) {
                _draftTaste = draftTaste;
                AlertWithActionsAndTag(1000, @"是否载入上次保存的草稿？", @"", @"放弃此草稿", @"载入");
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1000) {
        if (buttonIndex == 1) {
            PublishViewController *publishController = nil;
            if (NotEmptyValue(_draftTaste.productId)) {
                publishController = [self.storyboard instantiateViewControllerWithIdentifier:@"PostEditViewController"];
            }else {
                publishController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishViewController"];
            }
            
            publishController.draftTasteItem = _draftTaste;
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:publishController];
            [self presentViewController:navController animated:YES completion:nil];

        }else {
            [JTGlobalCache.draftBoxList removeAllObjects];
            [JTGlobalCache synchronize];
            _draftTaste = nil;
        }
    }
}

@end
