//
//  YouPinAPI.m
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "YouPinAPI.h"
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"
#import "JTObjectMapping.h"
#import "JTAddressBookManager.h"
#import "PublishItem.h"
#import "PublishTextItem.h"
#import "PublishImageItem.h"
#import "PostContentItem.h"
#import "RecommendTag.h"
#import "DiscoveryBanner.h"

#define PAGE_COUNT 25

@implementation YouPinAPI
{
    AFHTTPSessionManager    *_httpSessionManager;
}

+ (instancetype)sharedAPI {
    static YouPinAPI *SharedAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SharedAPI = [[self alloc] init];
    });
    
    return SharedAPI;
}

- (id)init {
    if (self = [super init]) {
        _httpSessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:HOST]];
        _httpSessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    }
    
    return self;
}

- (NSError *)checkErrorWithResponse:(id)responseObject {
    NSError *error = nil;
    if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {
        int resultCode = [responseObject[@"resultCode"] intValue];
        if (resultCode != 0) {
            NSString *errorMsg = responseObject[@"errorMsg"];
            error = [NSError errorWithDomain:NSURLErrorDomain code:resultCode userInfo:@{NSLocalizedDescriptionKey: errorMsg}];
        }
    }else {
        error = [NSError errorWithDomain:NSURLErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"出现未知错误，请稍后重试"}];
    }
    
    return error;
}

- (void)cancelRequestForSender:(UIViewController *)sender {
    [sender cancelTasksInSessionManager:_httpSessionManager];
}

- (NSString *)userAuthParams {
    if (JTGlobalCache.isLogin) {
        return [NSString stringWithFormat:@"phone=%@&userId=%@&v=%@",JTGlobalCache.currentUser.phone, JTGlobalCache.currentUser.userId, JTGlobalCache.lastVersion];
    }else {
        return [NSString stringWithFormat:@"v=%@", JTGlobalCache.lastVersion];
    }
}

- (void)fecthInitConfigInfo:(void(^)(NSDictionary *configDict, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST, INIT_CONFIG, [self userAuthParams]];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    request.timeoutInterval = 5;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!error && data) {
        NSString *configJson = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (configJson && configJson.length > 0) {
            NSDictionary *retDict = [configJson objectFromJSONString];
            NSError *codeError = [self checkErrorWithResponse:retDict];
            if (!codeError) {
                NSDictionary *configDict = retDict[@"data"];
                callback(configDict, nil);
            }else {
                callback(nil, codeError);
            }
        }else {
            callback(nil, [NSError errorWithDomain:NSURLErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"出现未知错误，请稍后重试"}]);
        }
    }else {
        callback(nil, error);
    }
}

- (void)applyInvitationCode:(NSString *)code sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@",HOST, INVITATION_CODE];
    NSDictionary *params = @{
                             @"code": code,
                             };
    
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            callback(nil);
        }else {
            callback(error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)registerWithPhone:(NSString *)phone loginType:(LoginType)loginType sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@",HOST,REGISTER];
    NSDictionary *params = @{
                             @"phone": phone,
                             @"userType": [NSString stringWithFormat:@"%d", loginType],
                             @"device": [UIDevice currentDevice].model,
                             @"platform": @"ios",
                             @"osVersion": [UIDevice currentDevice].systemVersion,
                             @"versionName": [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"],
                             @"versionCode": [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"],
                             @"deviceCode": [UIDevice currentDevice].identifierForVendor.UUIDString
                             };
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            callback(nil);
        }else {
            callback(error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
            callback(error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)logout:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,USER_LOGOUT, [self userAuthParams]];
    [_httpSessionManager GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            callback(nil);
        }else {
            callback(error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)updateDeviceInfoWithCallback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@",HOST,UPDATE_DEVICE_INFO];
    NSDictionary *params = @{
                             @"phone": JTGlobalCache.currentUser.phone,
                             @"userType": @"1",
                             @"device": [UIDevice currentDevice].model,
                             @"platform": @"ios",
                             @"osVersion": [UIDevice currentDevice].systemVersion,
                             @"versionName": [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"],
                             @"versionCode": [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"],
                             @"deviceCode": [UIDevice currentDevice].identifierForVendor.UUIDString
                             };
    [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            callback(nil);
        }else {
            callback(error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)verifyCode:(NSString *)code phone:(NSString *)phone sender:(UIViewController *)sender callback:(void(^)(User *existUser,  NSArray *knownFriendList,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?phone=%@",HOST,VERIFY_CODE, phone];
    NSDictionary *params = @{
                             @"code": code
                             };
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *userMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.user" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            JTObjectMappingItem *friendListMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.userList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[userMappingItem, friendListMappingItem]] fetchMappingResultFromDictionary:responseObject];
            User *existUser = ret[@"data.user"];
            NSArray *friendList = ret[@"data.userList"];
            callback(existUser, friendList, nil);
        }else {
            callback(nil, nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)setProfileWithNickname:(NSString *)nickname
                        gender:(int)gender
                        avatar:(UIImage *)avatar
                         phone:(NSString *)phone
                       headUrl:(NSString *)headUrl
                      location:(NSString *)location
                    userOpenId:(NSString *)userOpenId
                        sender:(UIViewController *)sender
                      callback:(void(^)(User *userInfo,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?phone=%@",HOST,PROFILE, phone];
    NSMutableDictionary *params = [@{
                             @"nick": nickname
                             } mutableCopy];
    if (gender != 0) {
        params[@"sex"] = @(gender);
    }
    
    if (IsSafeValue(headUrl)) {
        params[@"headUrl"] = headUrl;
    }
    
    if (IsSafeValue(location)) {
        params[@"location"] = location;
    }
    
    if (IsSafeValue(userOpenId)) {
        params[@"userOpenId"] = userOpenId;
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (avatar) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(avatar, 1) name:@"pic" fileName:@"avatar.jpg" mimeType:@"image/jpeg"];
        }
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *userMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.user" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[userMappingItem]] fetchMappingResultFromDictionary:responseObject];
            User *user = ret[@"data.user"];
            
            callback(user, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)uploadAddressBook:(NSArray *)addressBookContacts callback:(void(^)(NSError *error))callback {
    if (addressBookContacts) {
        NSMutableArray *contactDicts = [[NSMutableArray alloc] init];
        for (JTAddressBookPerson *person in addressBookContacts) {
            [contactDicts addObject:@{@"cphone": [person.phoneNumbers componentsJoinedByString:@","], @"cname": person.fullName, @"op": [NSNumber numberWithInt:0]}];
        }
        
        NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,UPLOAD_ADDRESS_BOOK, [self userAuthParams]];
        NSString *jsonValue = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:contactDicts options:0 error:nil] encoding:NSUTF8StringEncoding];
        
        NSDictionary *params = @{
                                 @"contacts": jsonValue
                                 };
        [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
            NSError *error = [self checkErrorWithResponse:responseObject];
            callback(error);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            callback(error);
        }];
    }
}

- (void)uploadPurchaseHistory:(NSString * )purchaseHistory account:(NSString *)account sender:(UIViewController *)sender callback:(void(^)(NSArray *purchaseItems ,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@",HOST,UPLOAD_PURCHASE];
    NSDictionary *params = @{
                             @"phone": JTGlobalCache.currentUser.phone,
                             @"source": @(1),
                             @"purchases": purchaseHistory,
                             @"bindingId": account,
                             @"bindingPlatform": @"1"
                             };
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.purchases" mappingClass:[Goods class] propertyMappingsFromDictionary:@{@"goodsDescription" : @"description"}];
            NSArray *purchaseItems = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject][@"data.purchases"];
            callback(purchaseItems, nil);
        }else {
            callback(nil, error); 
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchPurchaseHistory:(UIViewController *)sender callback:(void(^)(NSArray *purchaseItems ,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@",HOST,PURCAHSE_HISTORY];
    NSDictionary *params = @{
                             @"phone": JTGlobalCache.currentUser.phone,
                             };
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.purchases" mappingClass:[Goods class] propertyMappingsFromDictionary:@{@"goodsDescription" : @"description"}];
            NSArray *purchaseItems = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject][@"data.purchases"];
            callback(purchaseItems, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchTimelineWithUrlPath:(NSString *)path startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp listType:(int)listType userId:(NSString *)userId Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,path, [self userAuthParams]];
    NSMutableDictionary *params = [@{
                                     @"size": @(PAGE_COUNT),
                                     @"listType": @(listType)
                                     } mutableCopy];
    
    if (startTimestamp > 0) {
        params[@"startTimestamp"] = @(0);
    }
    
    if (endTimestamp > 0) {
        params[@"endTimestamp"] = @(endTimestamp);
    }
    
    if (userId) {
        params[@"friendId"] = userId;
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.products" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"tasteDescription" : @"description"}];
            JTObjectMappingItem *commentItem = [JTObjectMappingItem itemWithKeyPath:@"comments" mappingClass:[Comment class]];
            [mappingItem addDependentMappingItems:@[commentItem]];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *tasteItems = ret[@"data.products"];
            BOOL hasNext = NO;
            callback(tasteItems, hasNext, nil);
        }else {
            callback(nil, NO, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, NO ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchPersonalTimelineWithUrlPath:(NSString *)path startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp listType:(int)listType userId:(NSString *)userId Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,path, [self userAuthParams]];
    NSMutableDictionary *params = [@{
                                     @"size": @(PAGE_COUNT),
                                     @"listType": @(listType)
                                     } mutableCopy];
    
    if (startTimestamp > 0) {
        params[@"startTimestamp"] = @(0);
    }
    
    if (endTimestamp > 0) {
        params[@"endTimestamp"] = @(endTimestamp);
    }
    
    if (userId) {
        params[@"friendId"] = userId;
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.products" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"tasteDescription" : @"description"}];
            JTObjectMappingItem *commentItem = [JTObjectMappingItem itemWithKeyPath:@"comments" mappingClass:[Comment class]];
            [mappingItem addDependentMappingItems:@[commentItem]];
            
            JTObjectMappingItem *userMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.user" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem, userMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *tasteItems = ret[@"data.products"];
            User *user = ret[@"data.user"];
            NSInteger totalCount = [responseObject[@"data"][@"totalCount"] integerValue];
            NSInteger expressCount = [responseObject[@"data"][@"selfExpCount"] integerValue];
            NSDictionary *levelDict = responseObject[@"data"][@"degrees"];
            BOOL hasNext = NO;
            callback(tasteItems, hasNext, totalCount, expressCount, user, levelDict, nil);
        }else {
            callback(nil, NO, 0, 0, nil, nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, NO, 0, 0, nil, nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}


- (void)fetchTasteTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext,NSError *error))callback {
    [self fetchTimelineWithUrlPath:TIMELINE_LIST startTimestamp:startTimestamp endTimestamp:endTimestamp listType:0 userId:JTGlobalCache.currentUser.userId Sender:sender callback:callback];
}

- (void)fetchPublicTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, RecommendTag *recommendTag, BOOL hasNext,NSError *error))callback {
//    [self fetchTimelineWithUrlPath:TIMELINE_LIST startTimestamp:startTimestamp endTimestamp:endTimestamp listType:3 userId:nil Sender:sender callback:callback];
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,TIMELINE_LIST, [self userAuthParams]];
    NSMutableDictionary *params = [@{
                                     @"size": @(PAGE_COUNT),
                                     @"listType": @(3)
                                     } mutableCopy];
    
    if (startTimestamp > 0) {
        params[@"startTimestamp"] = @(0);
    }
    
    if (endTimestamp > 0) {
        params[@"endTimestamp"] = @(endTimestamp);
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.products" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"tasteDescription" : @"description"}];
            JTObjectMappingItem *commentItem = [JTObjectMappingItem itemWithKeyPath:@"comments" mappingClass:[Comment class]];
            [mappingItem addDependentMappingItems:@[commentItem]];
            
            JTObjectMappingItem *recommendTagMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.rtag" mappingClass:[RecommendTag class]];
            JTObjectMappingItem *recommendTagItemMappingItem = [JTObjectMappingItem itemWithKeyPath:@"productList" mappingClass:[RecommendTagItem class]];
            [recommendTagMappingItem addDependentMappingItems:@[recommendTagItemMappingItem]];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem, recommendTagMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *tasteItems = ret[@"data.products"];
            RecommendTag *tag = ret[@"data.rtag"];
            BOOL hasNext = NO;
            callback(tasteItems, tag, hasNext, nil);
        }else {
            callback(nil, nil, NO, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, nil, NO ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchPersonalTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error))callback {
    [self fetchPersonalTimelineWithUrlPath:TIMELINE_LIST startTimestamp:startTimestamp endTimestamp:endTimestamp listType:1 userId:JTGlobalCache.currentUser.userId Sender:sender callback:callback];
}

- (void)fetchWishListWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp userId:(NSString *)userId Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error))callback {
    [self fetchPersonalTimelineWithUrlPath:TIMELINE_LIST startTimestamp:startTimestamp endTimestamp:endTimestamp listType:2 userId:userId Sender:sender callback:callback];
}

- (void)fetchFriendTimelineWithStartTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp userId:(NSString *)userId Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error))callback {
    [self fetchPersonalTimelineWithUrlPath:TIMELINE_LIST startTimestamp:startTimestamp endTimestamp:endTimestamp listType:1 userId:userId Sender:sender callback:callback];
}

- (void)fetchFavoriteTagTimelineWithTagName:(NSString *)tagName startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, Tag *tag, BOOL hasNext,NSError *error)) callback{
    [self fetchTagTimelineWithTagName:tagName tagType:0 startTimestamp:startTimestamp endTimestamp:endTimestamp Sender:sender callback:callback];
}

- (void)fetchLatestTagTimelineWithTagName:(NSString *)tagName startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, Tag *tag, BOOL hasNext,NSError *error)) callback{
    [self fetchTagTimelineWithTagName:tagName tagType:1 startTimestamp:startTimestamp endTimestamp:endTimestamp Sender:sender callback:callback];
}

- (void)fetchTagTimelineWithTagName:(NSString *)tagName tagType:(NSInteger)type startTimestamp:(NSTimeInterval)startTimestamp endTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *items, Tag *tag, BOOL hasNext,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,TIMELINE_LIST, [self userAuthParams]];
    NSMutableDictionary *params = [@{
                                     @"size": @(PAGE_COUNT),
                                     @"tag": tagName,
                                     @"tagType": @(type)
                                     } mutableCopy];
    if (startTimestamp > 0) {
        params[@"startTimestamp"] = @(startTimestamp);
    }
    
    if (endTimestamp > 0) {
        params[@"endTimestamp"] = @(endTimestamp);
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.products" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"tasteDescription" : @"description"}];
            JTObjectMappingItem *commentItem = [JTObjectMappingItem itemWithKeyPath:@"comments" mappingClass:[Comment class]];
            [mappingItem addDependentMappingItems:@[commentItem]];
            
            JTObjectMappingItem *tagMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.tag" mappingClass:[Tag class] propertyMappingsFromDictionary:@{@"tagDescription": @"description"}];
            JTObjectMappingItem *relatedTagsItem = [JTObjectMappingItem itemWithKeyPath:@"relevantTags" mappingClass:[Tag class] propertyMappingsFromDictionary:@{@"tagDescription": @"description"}];
            [tagMappingItem addDependentMappingItems:@[relatedTagsItem]];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem, tagMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *tasteItems = ret[@"data.products"];
            Tag *tag = ret[@"data.tag"];
//            if (IsEmptyValue(tag.picUrl)) {
//                tag = nil;
//            }
            BOOL hasNext = NO;
            callback(tasteItems, tag, hasNext, nil);
        }else {
            callback(nil, nil, NO, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, nil, NO ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchHotTags:(void(^)(NSArray *tagList, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,HOT_TAG_LIST,[self userAuthParams]];
    [_httpSessionManager GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *tagMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.hotList" mappingClass:[Tag class] propertyMappingsFromDictionary:@{@"tagDescription": @"description"}];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[tagMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *tags = ret[@"data.hotList"];
            
            callback(tags, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil ,error);
    }];
}

- (void)fetchRelatedTagsWithTag:(NSString *)tag sender:(UIViewController *)sender callback:(void(^)(NSArray *tags, NSError *error))callback  {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,RELATED_TAG_LIST,[self userAuthParams]];
    NSDictionary *params = @{@"tag": tag};
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            NSArray *tags = [responseObject valueForKeyPath:@"data.tags"];
            callback(tags, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil ,error);
    }];
    
    if (sender) {
        [sender didSendTask:task.taskIdentifier];
    }
}

- (void)fetchTasteDetailWithTasteId:(NSString *)Id sender:(UIViewController *)sender callback:(void(^)(TasteItem *item, BOOL hasQuery, NSArray *queryUsers,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,DETAIL,[self userAuthParams]];
    NSMutableDictionary *params = [@{
                             @"pid": Id
                             } mutableCopy];
    
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *productMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.product" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"contentItems": @"showContent", @"editorComment":@"systemComment"}];
            JTObjectMappingItem * editorCommentMappingItem = [JTObjectMappingItem itemWithKeyPath:@"systemComment" mappingClass:[Comment class]];
            [productMappingItem addDependentMappingItems:@[editorCommentMappingItem]];
            
            JTObjectMappingItem *contentItemMappingItem = [JTObjectMappingItem itemWithKeyPath:@"showContent" mappingClass:[PostContentItem  class]];
            
            JTObjectMappingItem *pinUserMappingItem = [JTObjectMappingItem itemWithKeyPath:@"pinUsers" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            
            [productMappingItem addDependentMappingItems:@[contentItemMappingItem, pinUserMappingItem]];
            
            JTObjectMappingItem *commentsMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.comments" mappingClass:[Comment class]];
            
            JTObjectMappingItem *queryUserMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.product.queryUsers" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[productMappingItem, commentsMappingItem, queryUserMappingItem]] fetchMappingResultFromDictionary:responseObject];
            TasteItem *tasteItem = ret[@"data.product"];
            tasteItem.comments = ret[@"data.comments"];
            
            NSDictionary *productDict = responseObject[@"data"][@"product"];
            if (NotEmptyValue(productDict[@"price"])) {
                Goods *goods = [[Goods alloc] init];
                goods.price = productDict[@"price"];
                goods.title = productDict[@"name"];
                goods.buylink = productDict[@"buyLink"];
                goods.source = [productDict[@"source"] integerValue];
                tasteItem.goods = goods;
            }
            
            BOOL hasQuery = [[responseObject valueForKeyPath:@"data.product.queryBuyInfo"] boolValue];
            NSArray *queryUsers = ret[@"data.product.queryUsers"];
            callback(tasteItem, hasQuery, queryUsers, nil);
        }else {
            callback(nil, NO, nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, NO, nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchPraiseListWithTatesId:(NSString *)Id sender:(UIViewController *)sender callback:(void(^)(NSArray *users, BOOL hasNext, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,PRAISE_LIST, [self userAuthParams]];
    NSDictionary *params = @{@"pid": Id};
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *pinUserMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.userList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[pinUserMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *userList = ret[@"data.userList"];
            BOOL hasNext = NO;
            callback(userList, hasNext, nil);
        }else {
            callback(nil, NO, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, NO ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchMultipleGoodsPicWithItemId:(NSString *)itemId sender:(UIViewController *)sender callback:(void(^)(NSArray *picURLs, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@",HOST,GOODS_MULTIPLE_PIC];
    NSDictionary *params = @{
                             @"phone": JTGlobalCache.currentUser.phone,
                             @"itemId": itemId
                             };
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            NSArray *picURLs = responseObject[@"data"][@"itemImgList"];
            callback(picURLs, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)publishCommentWithTasteId:(NSString *)Id comment:(Comment *)comment replyTargetPhone:(NSString *)replyTargetPhone sender:(UIViewController *)sender callback:(void(^)(Comment *newComment,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,PUBLISH_COMMENT, [self userAuthParams]];
    NSMutableDictionary *params = [@{
                             @"pid": Id,
                             @"comment": comment.comment,
                             @"requestId": comment.requestId
                             } mutableCopy];
    if (replyTargetPhone && ![replyTargetPhone isEqualToString:@""]) {
        params[@"receiverPhone"] = replyTargetPhone;
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params constructingBodyWithBlock:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.comment" mappingClass:[Comment class]];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject];
            Comment *newComment = ret[@"data.comment"];
            callback(newComment, nil);
        }else {
            callback(nil, error);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)deleteComment:(Comment *)comment sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,DELETE_COMMENT,[self userAuthParams]];
    NSDictionary *params = @{@"pid": comment.productId,
                             @"cid": comment.commentId
                             };
 
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)publishTaste:(TasteItem *)taste tags:(NSArray *)tags sender:(UIViewController *)sender callback:(void(^)(TasteItem *newTaste,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,PUBLISH_TASTE, [self userAuthParams]];
    JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:nil mappingClass:[TasteItem class] propertiesForMapping:@[@"billId", @"name", @"price", @"originalPic", @"source", @"itemId", @"requestId"]];
    NSMutableDictionary *params = [[[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchDictionaryFromObject:taste] mutableCopy];

    params[@"description"] = taste.tasteDescription;
    params[@"fileType"] = @"zip";
    
    if (NotEmptyValue(taste.productId)) {
        params[@"productId"] = taste.productId;
    }
    
//    if (taste.picUrls) {
//        params[@"picUrls"] = [taste.picUrls componentsJoinedByString:@","];
//    }
    
    if (tags) {
        params[@"tags"] = [tags componentsJoinedByString:@"#"];
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (taste.photosZipFilePath && [[NSFileManager defaultManager] fileExistsAtPath:taste.photosZipFilePath]) {
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:taste.photosZipFilePath] name:@"file" fileName:[taste.photosZipFilePath lastPathComponent] mimeType:@"application/octet-stream" error:nil];
        }
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.product" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"tasteDescription" : @"description"}];
            JTObjectMappingItem *commentItem = [JTObjectMappingItem itemWithKeyPath:@"comments" mappingClass:[Comment class]];
            [mappingItem addDependentMappingItems:@[commentItem]];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject];
            TasteItem *newTaste = ret[@"data.product"];
            newTaste.draftIdentifier = [taste.draftIdentifier copy];
            callback(newTaste, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)publishPost:(TasteItem *)post sender:(UIViewController *)sender callback:(void(^)(TasteItem *newTaste,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,PUBLISH_POST_V2, [self userAuthParams]];
    JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:nil mappingClass:[TasteItem class] propertiesForMapping:@[@"title", @"billId", @"name", @"price", @"buyLink", @"originalPic", @"source", @"itemId", @"requestId"]];
    NSMutableDictionary *params = [[[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchDictionaryFromObject:post] mutableCopy];
    
    if (post.tags && post.tags.count > 0) {
        params[@"tags"] = [post.tags componentsJoinedByString:@"#"];
    }
    
    params[@"fileType"] = @"zip";
    
    if (NotEmptyValue(post.productId)) {
        params[@"productId"] = post.productId;
    }

    if (post.publishItems) {
        NSMutableArray *itemsOrder = [[NSMutableArray alloc] init];
        NSUInteger imageIndex = 0;
        NSUInteger textIndex = 0;
        for (PublishItem *item  in post.publishItems) {
            if ([item isKindOfClass:[PublishTextItem class]]) {
                NSString *key = [NSString stringWithFormat:@"text%ld", textIndex];
                [itemsOrder addObject:@{@"type": @"0", @"key": key}];
                params[key] = ((PublishTextItem *)item).text;
                textIndex++;
            }else if ([item isKindOfClass:[PublishImageItem class]]) {
                if (((PublishImageItem *)item).source == PublishImageSourceLocal) {
                    [itemsOrder addObject:@{@"type": @"1", @"key": [((PublishImageItem *)item).imagePath lastPathComponent]}];
                }else {
                    [itemsOrder addObject:@{@"type": @"1", @"key": ((PublishImageItem *)item).imagePath}];
                }
                
                imageIndex++;
            }
        }
        
        params[@"order"] = [itemsOrder JSONString];
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (post.photosZipFilePath && [[NSFileManager defaultManager] fileExistsAtPath:post.photosZipFilePath]) {
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:post.photosZipFilePath] name:@"file" fileName:[post.photosZipFilePath lastPathComponent] mimeType:@"application/octet-stream" error:nil];
        }
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.product" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"contentItems": @"showContent", @"tasteDescription": @"description"}];

            JTObjectMappingItem *contentItemMappingItem = [JTObjectMappingItem itemWithKeyPath:@"showContent" mappingClass:[PostContentItem  class]];
            
            JTObjectMappingItem *pinUserMappingItem = [JTObjectMappingItem itemWithKeyPath:@"pinUsers" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            
            JTObjectMappingItem *commentItem = [JTObjectMappingItem itemWithKeyPath:@"comments" mappingClass:[Comment class]];
            
            [mappingItem addDependentMappingItems:@[contentItemMappingItem, commentItem, pinUserMappingItem]];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject];
            TasteItem *newTaste = ret[@"data.product"];
            newTaste.draftIdentifier = [post.draftIdentifier copy];
            
            NSDictionary *productDict = responseObject[@"data"][@"product"];
            if (NotEmptyValue(productDict[@"price"])) {
                Goods *goods = [[Goods alloc] init];
                goods.price = productDict[@"price"];
                goods.title = productDict[@"name"];
                goods.buylink = productDict[@"buyLink"];
                goods.source = [productDict[@"source"] integerValue];
                newTaste.goods = goods;
            }
            
            callback(newTaste, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil ,error);
    }];
    
    if (sender) {
        [sender didSendTask:task.taskIdentifier];
    }
}

- (void)deleteTasteWithProductId:(NSString *)productId sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,DELETE_TASTE, [self userAuthParams]];
    NSDictionary *params = @{@"pid": productId};
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
    [sender didSendTask:task.taskIdentifier];
}

- (void)praiseTaste:(NSString *)Id value:(int)praiseValue callback:(void(^)(int pinCount, NSArray *currentPinUsers,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,PRAISE, [self userAuthParams]];
    NSDictionary *params = @{@"pid": Id, @"youpin": @(praiseValue)};
    [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            int pinCount = [[responseObject valueForKeyPath:@"data.pinCount"] intValue];
            JTObjectMappingItem *pinUserMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.userList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[pinUserMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *pinUsers = ret[@"data.userList"];
            callback(pinCount, pinUsers, nil);
        }else {
            callback(0, nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(0,nil,error);
    }];
}

- (void)queryGoodsInfoForTaste:(NSString *)Id callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,QUERY_GOODS_INFO, [self userAuthParams]];
    NSDictionary *params = @{@"pid": Id};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            callback(nil);
        }else {
            callback(error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)subscribeTag:(NSString *)tagName value:(BOOL)subscribe callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,SUBSCRIBE_TAG, [self userAuthParams]];
    NSDictionary *params = @{@"tag": tagName, @"op": @(subscribe)};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}


- (void)addWishListWithTaste:(NSString *)Id add:(BOOL)add callback:(void(^)(int wishCount, NSArray *currentWishListUsers,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,ADD_WISH_LIST, [self userAuthParams]];
    NSDictionary *params = @{@"pid": Id, @"add": (add? @(1) : @(2))};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            int wishCount = [[responseObject valueForKeyPath:@"data.wishCount"] intValue];
            JTObjectMappingItem *pinUserMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.userList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[pinUserMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *wishListUsers = ret[@"data.userList"];
            callback(wishCount, wishListUsers, nil);
        }else {
            callback(0, nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(0,nil,error);
    }];
}

- (void)uploadPushInfoWithUserId:(NSString *)userId channelId:(NSString *)channelId callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,PUSH_BIND, [self userAuthParams]];
    NSDictionary *params = @{@"yunUserId": userId, @"yunChannelId": channelId};
    [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            callback(nil);
        }else {
            callback(error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)fetchDefaultTags:(void(^)(NSArray *tags, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,DEFAULT_TAGS, [self userAuthParams]];
    [_httpSessionManager GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            NSArray *tags = responseObject[@"data"][@"tags"];
            callback(tags, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
}

- (void)reportTasteItem:(TasteItem *)item content:(NSString *)reportContent sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@", HOST, REPORT, [self userAuthParams]];
    NSString *extraId = SafeValue(item.extraId);
    NSString *productId = SafeValue(item.productId);
    NSString *content = SafeValue(reportContent);
    NSDictionary *params = @{@"toPhone": extraId, @"content": content, @"productId": productId};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)reportComment:(Comment *)comment content:(NSString *)reportContent sender:(UIViewController *)sender callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@", HOST, REPORT, [self userAuthParams]];
    NSDictionary *params = @{@"toPhone": comment.extraId, @"content": reportContent, @"commentId": comment.commentId};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)internalFetchNotificationListWithUpdateTime:(NSTimeInterval)updateTime category:(int)category sender:(UIViewController *)sender callback:(void(^)(NSArray *notificationList, NSInteger notificationCount, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@", HOST, NOTIFICATION_LIST, [self userAuthParams]];
    NSDictionary *params = @{@"updateTime": @(updateTime), @"category": @(category)};
    
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.notifys" mappingClass:[Notification class]];
            NSArray *notificationList = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject][@"data.notifys"];
            NSInteger notificationCount = [responseObject[@"data"][@"notifyCount"] integerValue];
            callback(notificationList, notificationCount, nil);
        }else {
            callback(nil, 0, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, 0, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchNotificationListWithUpdateTime:(NSTimeInterval)updateTime sender:(UIViewController *)sender callback:(void(^)(NSArray *notificationList, NSInteger notificationCount, NSError *error))callback {
    [self internalFetchNotificationListWithUpdateTime:updateTime category:1 sender:sender callback:callback];
}

- (void)fetchFriendNotificationListWithUpdateTime:(NSTimeInterval)updateTime sender:(UIViewController *)sender callback:(void(^)(NSArray *notificationList, NSInteger notificationCount, NSError *error))callback {
    [self internalFetchNotificationListWithUpdateTime:updateTime category:2 sender:sender callback:callback];
}

- (void)fetchPublicBannerInfoWithSender:(UIViewController *)sender callback:(void(^)(NSArray *banners, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@", HOST, PUBLIC_TIMELINE_BANNER];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if (JTGlobalCache.isLogin) {
        params[@"phone"] = JTGlobalCache.currentUser.phone;
        params[@"userId"] = JTGlobalCache.currentUser.userId;
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.banner" mappingClass:[BannerInfo class]];
            BannerInfo *banner = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject][@"data.banner"];
            NSArray *banners = @[banner];
            callback(banners, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchFriendTimelineBadgeWithUpdateTime:(NSTimeInterval)updateTime sender:(UIViewController *)sender callback:(void(^)( NSInteger newTimelineCount, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@", HOST, FRIEND_TIMELINE_BADGE_COUNT, [self userAuthParams]];
    NSDictionary *params = @{@"updateTime": @(updateTime)};

    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            NSInteger newTimelineCount = [responseObject[@"data"][@"newTimelineCount"] integerValue];
            callback(newTimelineCount, nil);
        }else {
            callback(0, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(0, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)searchUserByNickname:(NSString *)nickname sender:(UIViewController *)sender callback:(void(^)(NSArray *userList, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,SEARCH_FREIDN_BY_NICKNAME, [self userAuthParams]];
    NSDictionary *params = @{@"nick": nickname};
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *userListMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.userList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[userListMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *userList = ret[@"data.userList"];
            callback(userList, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)sendFriendInvitationToUser:(NSString *)userId introduce:(NSString *)introduce callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,ADD_FRIEND, [self userAuthParams]];
    NSDictionary *params = @{@"friendId": userId, @"introduce": introduce};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)fetchFriendList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,FRIEND_LIST, [self userAuthParams]];
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *userListMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.friendList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[userListMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *userList = ret[@"data.friendList"];
            callback(userList, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchRecommendFriendList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,RECOMMAND_FRIENDS, [self userAuthParams]];
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *userListMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.friendList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[userListMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *userList = ret[@"data.friendList"];
            callback(userList, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchContactListWithType:(NSInteger)type sender:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,CONTACT_FRIEND, [self userAuthParams]];
    NSDictionary *params = @{@"type": @(type)};
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *userListMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.friendList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[userListMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *userList = ret[@"data.friendList"];
            callback(userList, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchContactFriendList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback {
    [self fetchContactListWithType:1 sender:sender callback:callback];
}

- (void)fetchContactInvitationList:(UIViewController *)sender callback:(void(^)(NSArray *friendList, NSError *error))callback {
    [self fetchContactListWithType:2 sender:sender callback:callback];
}

- (void)acceptFriend:(NSString *)userId notifyId:(NSString *)notifyId callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,ACCEPT_FRIEND, [self userAuthParams]];
    NSDictionary *params = @{@"friendId": userId, @"notifyId": notifyId};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)deleteFriend:(NSString *)userId callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,DELETE_FRIEND, [self userAuthParams]];
    NSDictionary *params = @{@"friendId": userId};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        callback(error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)acceptFriendInvitationWithCode:(NSString *)invitationCode callback:(void(^)(BOOL success, User *inviter, NSArray *knownUserList, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,FRIEND_INVITATION, [self userAuthParams]];
    NSDictionary *params = @{@"inviteCode": invitationCode};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
         NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *userListMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.friendList" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            
            JTObjectMappingItem *inviterMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.inviter" mappingClass:[User class] propertyMappingsFromDictionary:@{@"phone" : @"extraId", @"avatar" : @"headImage", @"nickname" : @"nick", @"gender": @"sex", @"type": @"userType"}];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[userListMappingItem, inviterMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *userList = ret[@"data.friendList"];
            User *inviterUser = ret[@"data.inviter"];
            BOOL success = [[responseObject valueForKeyPath:@"data.addSuccess"] boolValue];
            
            callback(success, inviterUser, userList, nil);
        }else {
            callback(NO, nil, nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(NO, nil, nil, error);
    }];
}

- (void)fetchCategoryChooseListWithStartTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime callback:(void(^)(NSArray *tags, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,CATEGORY_CHOOSE_LIST, [self userAuthParams]];
    NSDictionary *params = @{@"size": @(20), @"startTimestamp": @(startTime), @"endTimestamp": @(endTime)};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *tagMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.tags" mappingClass:[Tag class] propertyMappingsFromDictionary:@{@"tagDescription": @"description"}];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[tagMappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *tagList = ret[@"data.tags"];
            
            callback(tagList, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];

}

- (void)fetchMoreRelatedTagListWithTag:(NSString *)tagName startTime:(NSTimeInterval)startTime endTime:(NSTimeInterval)endTime callback:(void(^)(NSArray *tags, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,MORE_RELATED_TAG_LIST, [self userAuthParams]];
    NSDictionary *params = @{@"tag": tagName, @"size": @(20), @"startTimestamp": @(startTime), @"endTimestamp": @(endTime)};
    [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *tagMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.tags" mappingClass:[Tag class] propertyMappingsFromDictionary:@{@"tagDescription": @"description"}];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[tagMappingItem]] fetchMappingResultFromDictionary:responseObject];
            Tag *tag = ret[@"data.tag"];
            NSArray *tagList = tag.relevantTags;

            callback(tagList, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
}

- (void)selectCategories:(NSArray *)categories callback:(void(^)(NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,SELECT_CATEGORY, [self userAuthParams]];
    NSString *tagsStr = [categories componentsJoinedByString:@"#"];
    NSDictionary *params = @{@"tag": tagsStr};
    [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            callback(nil);
        }else {
            callback(error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(error);
    }];
}

- (void)queryTagsWithKeyword:(NSString *)keyword sender:(UIViewController *)sender callback:(void(^)(NSArray *tags, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,QUERY_FUZZY_TAG, [self userAuthParams]];
    NSDictionary *params = @{@"tag": keyword};
    NSURLSessionDataTask *task = [_httpSessionManager GET:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            NSArray *tags = responseObject[@"data"][@"tags"];
            callback(tags, nil);
        }else {
            callback(nil, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)queryPostWithKeyword:(NSString *)keyword start:(NSInteger)start sender:(UIViewController *)sender callback:(void(^)(NSArray *items, NSInteger start,NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,QUERY_POST, [self userAuthParams]];
    NSDictionary *params = @{
                                     @"tag": keyword,
                                     @"start": @(start)
                                     };
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.products" mappingClass:[TasteItem class] propertyMappingsFromDictionary:@{@"tasteDescription" : @"description"}];
            JTObjectMappingItem *commentItem = [JTObjectMappingItem itemWithKeyPath:@"comments" mappingClass:[Comment class]];
            [mappingItem addDependentMappingItems:@[commentItem]];
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem]] fetchMappingResultFromDictionary:responseObject];
            NSArray *tasteItems = ret[@"data.products"];
            NSInteger nextStart = [[responseObject valueForKeyPath:@"data.end"] integerValue];
            callback(tasteItems, nextStart, nil);
        }else {
            callback(nil, 0, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, 0 ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

- (void)fetchDiscoveryListWithEndTimestamp:(NSTimeInterval)endTimestamp Sender:(UIViewController *)sender callback:(void(^)(NSArray *banners, NSArray *recommendTag, BOOL hasNext,NSError *error))callback {
    
    NSString *url = [NSString stringWithFormat:@"%@%@?%@",HOST,DISCOVERY_LIST, [self userAuthParams]];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if (endTimestamp > 0) {
        params[@"endTimestamp"] = @(endTimestamp);
    }
    
    NSURLSessionDataTask *task = [_httpSessionManager POST:url parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = [self checkErrorWithResponse:responseObject];
        if (!error) {
            JTObjectMappingItem *mappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.discoveryItems" mappingClass:[DiscoveryBanner class]];
            
            JTObjectMappingItem *recommendTagMappingItem = [JTObjectMappingItem itemWithKeyPath:@"data.recommendTags" mappingClass:[RecommendTag class]];
            JTObjectMappingItem *recommendTagItemMappingItem = [JTObjectMappingItem itemWithKeyPath:@"productList" mappingClass:[RecommendTagItem class]];
            [recommendTagMappingItem addDependentMappingItems:@[recommendTagItemMappingItem]];
            
            NSDictionary *ret = [[JTObjectMapping mappingWithMappingItems:@[mappingItem, recommendTagMappingItem]] fetchMappingResultFromDictionary:responseObject];
            
            NSArray *banners = ret[@"data.discoveryItems"];
            NSArray *tags = ret[@"data.recommendTags"];
            BOOL hasNext = NO;
            callback(banners, tags, hasNext, nil);
        }else {
            callback(nil, nil, NO, error);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callback(nil, nil, NO ,error);
    }];
    
    [sender didSendTask:task.taskIdentifier];
}

@end
