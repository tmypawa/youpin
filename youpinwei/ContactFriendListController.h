//
//  ContactFriendListController.h
//  youpinwei
//
//  Created by tmy on 15/1/17.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendInvitationController.h"

@interface ContactFriendListController : ShaibaTableViewController<FriendInvitationControllerDelegate>

@end
