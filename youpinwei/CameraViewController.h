//
//  CameraViewController.h
//  youpinwei
//
//  Created by tmy on 14-9-11.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CameraViewControllerDelegate;
@protocol RSKImageCropViewControllerDelegate;

@interface CameraViewController : UIImagePickerController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate>
@property (nonatomic, weak) id<CameraViewControllerDelegate> cameraDelegate;

@end


@protocol CameraViewControllerDelegate <NSObject>

- (void)cameraViewController:(CameraViewController *)controller didPickImage:(UIImage *)image;
- (void)cameraViewControllerDidCancel:(CameraViewController *)controller;

@end