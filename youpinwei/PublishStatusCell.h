//
//  PublishStatusCell.h
//  youpinwei
//
//  Created by tmy on 15/2/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    PublishStatusCellTypeSending,
    PublishStatusCellTypeSuccess,
    PublishStatusCellTypeFailed,
} PublishStatusCellType;

@interface PublishStatusCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *refteshButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic) PublishStatusCellType status;

- (void)updateViewForStatus;

@end
