//
//  FriendAddingMethodController.h
//  youpinwei
//
//  Created by tmy on 15/1/17.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendInvitationController.h"
#import "UMSocial.h"


@interface FriendAddingMethodController : ShaibaTableViewController<UISearchDisplayDelegate, UISearchBarDelegate, FriendInvitationControllerDelegate, UIActionSheetDelegate, UMSocialUIDelegate>

@end
