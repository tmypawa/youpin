//
//  ShaibaViewController.m
//  youpinwei
//
//  Created by tmy on 14/11/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "ShaibaViewController.h"
#import "PublishViewController.h"
#import "GoodsChooseController.h"
#import "UIImageView+AnimationCompletion.h"
#import "MainViewController.h"
#import "NotificationViewController.h"
#import "NewFriendViewController.h"
#import "FriendInfoViewController.h"
#import "DetailViewController.h"
#import "InvitationResultController.h"

#define PAPER_ANIMATION_INTERVAL 0.2

@interface ShaibaViewController ()

@end

@implementation ShaibaViewController
{
    IBOutlet UIView *_publishMenuView;
    IBOutlet UIButton *_maskBtn;
    IBOutlet UIButton *_caremaBtn;
    UIImageView     *_paperAnimationView;
    NSMutableArray  *_paperAnimations;
    NSMutableArray         *_paperReverseAnimation;
    NSMutableArray  *_menuItemBtns;
    NSArray  *_sourceItems;
    BOOL _isMenuShowing;
    UIButton   *_topNotificationBtn;
    IBOutlet UIView *_navTitleView;
    IBOutlet UILabel *_navTitleLabel;
}

- (UIView *)navigationTitleView {
    return _navTitleView;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _sourceItems = @[
                         @{@"title": @"淘宝晒单", @"icon": @"icon_taobao"}
                         ];
        _menuItemBtns = [[NSMutableArray alloc] init];
        
        _paperAnimations = [[NSMutableArray alloc] init];
        for (int i = 0; i < 1; ++i) {
            [_paperAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"paper_%d", (i + 1)]]];
        }
        
        _paperReverseAnimation = [[NSMutableArray alloc] init];
        NSEnumerator *enumerator = [_paperAnimations reverseObjectEnumerator];
        id image = nil;
        while ( (image = [enumerator nextObject])) {
            [_paperReverseAnimation addObject:image];
        }
    }
    
    return self;
}

- (void)awakeFromNib {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidOpenURL:) name:YouPinNotificationAppDidOpenURL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidReceiveRemoteNotification:) name:YouPinNotificationDidReceiveRemoteNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBar.backgroundColor = [UIColor whiteColor];
    self.delegate = self;
    [self.view bringSubviewToFront:_publishMenuView];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    [self loadMenuItems];
    [self initViews];
    
    if (JTGlobalCache.isLogin) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self updateTabBadgeCount];
            [self updateNotificationCount];
            [self updateFriendNotificationCount];
        });
    }
    
    [self handleRemoteNotification];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    for (UIButton *menuItem in _menuItemBtns) {
        menuItem.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    }
    
    [self updateAllTabBadgesFromLocalCache];
}
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initViews {
    self.navigationItem.titleView = _navTitleView;
    
    UIView *statusBannerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 20)];
    statusBannerView.backgroundColor = NavigationBarTintColor;
    [self.navigationController.view insertSubview:statusBannerView aboveSubview:self.navigationController.navigationBar];
    
    UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 0.5)];
    topLineView.backgroundColor = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1];
    [self.tabBar addSubview:topLineView];
    
    _topNotificationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _topNotificationBtn.frame = CGRectMake(0, 0, 81, 24);
    _topNotificationBtn.titleEdgeInsets = UIEdgeInsetsMake(2, 0, 0, 0);
    _topNotificationBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [_topNotificationBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _topNotificationBtn.backgroundColor = NavigationBarTintColor;
    _topNotificationBtn.layer.borderWidth = 1;
    _topNotificationBtn.layer.cornerRadius = 12;
    _topNotificationBtn.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.3f].CGColor;
    _topNotificationBtn.center = CGPointMake(_navTitleView.frame.size.width/2, _navTitleView.frame.size.height/2);
    [_topNotificationBtn addTarget:self action:@selector(goToNotificationView:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setNavigationTitle:(NSString *)title {
    _navTitleLabel.text = title;
}

- (void)handleExternalURLParams {
    NSString *invitationCode = JTGlobalCache.friendInvitationCode;
    if (NotEmptyValue(invitationCode)) {
        InvitationResultController *invitationController = [self.storyboard instantiateViewControllerWithIdentifier:@"InvitationResultController"];
        invitationController.invitationCode = invitationCode;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:invitationController];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
        
        JTGlobalCache.friendInvitationCode = nil;
    }
    
    NSString *postId = JTGlobalCache.externalPostId;
    if (NotEmptyValue(postId)) {
        DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
        detailController.tasteId = postId;
        [self.navigationController pushViewController:detailController animated:YES];
    }
}

- (void)handleRemoteNotification {
    NSDictionary *userInfo =  [YouPin sharedInstance].receivedRemoteNotification;
    if (userInfo) {
        NSString *postId = [NSString stringWithFormat:@"%@", userInfo[@"pid"]];
        NotificationType type = [userInfo[@"type"] intValue];
        if (type == NotificationTypeFriendInvitation || type == NotificationTypeFriendAccept) {
            NewFriendViewController *newFriendController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewFriendViewController"];
            [self.navigationController pushViewController:newFriendController animated:YES];
        }else if (type == NotificationTypeNewUserJoin) {
            NSString *userId = userInfo[@"userId"];
            FriendInfoViewController *friendController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
            friendController.userId = userId;
            [self.navigationController pushViewController:friendController animated:YES];
        }else if(type != NotificationTypeNone){
            DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
            detailController.tasteId = postId;
            [self.navigationController pushViewController:detailController animated:YES];
        }
        
        [YouPin sharedInstance].receivedRemoteNotification = nil;
    }
}

- (void)showTopNotificationWithTitle:(NSString *)notificationTitle {
    [_topNotificationBtn.layer removeAllAnimations];
    [_navTitleLabel.layer removeAllAnimations];
    
    [_topNotificationBtn setTitle:notificationTitle forState:UIControlStateNormal];
    CGSize fitSize = [_topNotificationBtn sizeThatFits:CGSizeMake(CGFLOAT_MAX, _topNotificationBtn.frame.size.height)];
    if (fitSize.width + 20 > 81) {
        fitSize.width += 20;
    }else {
        fitSize.width = 81;
    }
    
    SetFrameWidth(_topNotificationBtn, fitSize.width);
    CGPoint center = _topNotificationBtn.center;
    center.y = -_topNotificationBtn.frame.size.height/2;
    center.x = _navTitleView.frame.size.width/2;
    _topNotificationBtn.center = center;
    [_navTitleView addSubview:_topNotificationBtn];
    [UIView animateWithDuration:0.7f delay:0 usingSpringWithDamping:0.6f initialSpringVelocity:0 options:0 animations:^{
        _topNotificationBtn.center = CGPointMake(_navTitleView.frame.size.width/2, _navTitleView.frame.size.height/2);
        _navTitleLabel.alpha = 0;
    } completion:nil];
}

- (void)hideTopNotification {
    [_topNotificationBtn removeFromSuperview];
    _navTitleLabel.alpha = 1;
}

- (void)appDidOpenURL:(NSNotification *)notification {
    [self handleExternalURLParams];
}

- (void)appDidEnterForeground:(NSNotification *)notification {
    [self updateTabBadgeCount];
    [self updateNotificationCount];
    [self updateFriendNotificationCount];
}

- (void)appDidReceiveRemoteNotification:(NSNotification *)notification {
    [self updateNotificationCount];
    [self handleRemoteNotification];
}

- (void)updateTabBadgeCount {
    NSTimeInterval updateTime = 0;
    if (JTGlobalCache.recentTastes && JTGlobalCache.recentTastes.count > 0) {
        updateTime = [JTGlobalCache.recentTastes[0] updateTime];
    }
    
    [[YouPinAPI sharedAPI] fetchFriendTimelineBadgeWithUpdateTime:updateTime sender:self callback:^(NSInteger newTimelineCount, NSError *error) {
        if (!error) {
            if (newTimelineCount > 0) {
                RDVTabBarItem *friendsTabBarItem = self.tabBar.items[3];
                friendsTabBarItem.badgeValue = @" ";
                JTGlobalCache.friendTimelineBadge = newTimelineCount;
                [JTGlobalCache synchronize];
            }
        }
    }];
}

- (void)updateNotificationCount {
    [[YouPinAPI sharedAPI] fetchNotificationListWithUpdateTime:JTGlobalCache.lastNotificationUpdateTime sender:self callback:^(NSArray *notificationList, NSInteger notificationCount, NSError *error) {
        if (notificationCount > 0) {
            NSString *notificationTitle = [NSString stringWithFormat:@"新通知·%ld", notificationCount];
            [self showTopNotificationWithTitle:notificationTitle];
        }
    }];
}

- (void)updateFriendNotificationCount {
    [[YouPinAPI sharedAPI] fetchFriendNotificationListWithUpdateTime:JTGlobalCache.lastFriendNotificationTime sender:self callback:^(NSArray *notificationList, NSInteger notificationCount, NSError *error) {
        if (notificationCount > 0) {
            RDVTabBarItem *personInfoTabBarItem = self.tabBar.items[4];
            personInfoTabBarItem.badgeValue = @" ";
            JTGlobalCache.friendNotificationBadge = notificationCount;
            [JTGlobalCache synchronize];
        }
    }];
}

- (void)updateAllTabBadgesFromLocalCache {
    NSString *friendTimelineBadgeValue = JTGlobalCache.friendTimelineBadge > 0 ? @" " : @"";
    RDVTabBarItem *friendsTabBarItem = self.tabBar.items[3];
    friendsTabBarItem.badgeValue = friendTimelineBadgeValue;
    
    NSString *friendNotificationBadgeValue = JTGlobalCache.friendNotificationBadge > 0 ? @" " : @"";
    RDVTabBarItem *personInfoTabBarItem = self.tabBar.items[4];
    personInfoTabBarItem.badgeValue = friendNotificationBadgeValue;
}

- (void)loadMenuItems {
    _paperAnimationView = [[UIImageView alloc] initWithFrame:CGRectMake(_publishMenuView.frame.size.width - 81 - 1, -8,  82, 16)];
    _paperAnimationView.animationRepeatCount = 1;
    _paperAnimationView.animationDuration = PAPER_ANIMATION_INTERVAL;
    _paperAnimationView.backgroundColor =[UIColor clearColor];
//    _paperAnimationView.animationImages = _paperAnimations;
    _paperAnimationView.image = _paperAnimations[0];
    //[_publishMenuView addSubview:_paperAnimationView];
    
    float height = _publishMenuView.frame.size.height;
    float width = _publishMenuView.frame.size.width;
    float originY = contentHeight;
    
    SetFrameY(_caremaBtn, originY - _caremaBtn.frame.size.height);
    _caremaBtn.titleLabel.alpha = 0;
    _caremaBtn.adjustsImageWhenHighlighted = NO;
    [_caremaBtn addTarget:self action:@selector(menuItemDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    [_menuItemBtns addObject:_caremaBtn];
    
//    for (int i = 0; i<_sourceItems.count; ++i) {
//        NSDictionary *itemDict = _sourceItems[i];
//        UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        menuBtn.frame = CGRectMake(_caremaBtn.frame.origin.x, originY + i * height, width, height);
//        [menuBtn setTitleEdgeInsets:UIEdgeInsetsMake(40, -30, 0, 0)];
//        [menuBtn setImageEdgeInsets:UIEdgeInsetsMake(-10, 0, 0, -50)];
//        [menuBtn setContentEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 0)];
//        [menuBtn setImage:[UIImage imageNamed: itemDict[@"icon"]] forState:UIControlStateNormal];
//        menuBtn.adjustsImageWhenHighlighted = NO;
//        [menuBtn setTitle:itemDict[@"title"] forState:UIControlStateNormal];
//        menuBtn.titleLabel.font = [UIFont systemFontOfSize:12];
//        menuBtn.titleLabel.alpha = 0;
//        [menuBtn addTarget:self action:@selector(menuItemDidTouch:) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:menuBtn];
//        [_menuItemBtns addObject:menuBtn];
//    }
    
    SetFrameHeight(_publishMenuView, ((_sourceItems.count + 1) * height) + 25);
    [self.view bringSubviewToFront:_caremaBtn];
    [_maskBtn removeFromSuperview];
//    [self.view bringSubviewToFront:_maskBtn];
}

- (void)menuItemDidTouch:(UIButton *)sender {
    if (JTGlobalCache.isLogin) {
        NSInteger itemIndex = [_menuItemBtns indexOfObject:sender];
        switch (itemIndex) {
            case 0:
            {
                [self presentCameraViewControllerForPublish];
                break;
            }
            case 1:
            {
                GoodsChooseController *goodsChooseController = [self.storyboard instantiateViewControllerWithIdentifier:@"GoodsChooseController"];
                goodsChooseController.isPublishEntrance = YES;
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:goodsChooseController];
                [self presentViewController:navController animated:YES completion:nil];
                break;
            }
            default:
                break;
        }
    }else {
        [self showRegisterAlert];
    }
    
    //[self showMenu:nil];
}

- (void)presentCameraViewControllerForPublish {
    PhotoSelectionController *photoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoSelectionController"];
    photoController.photoDelegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:photoController];
//    CameraViewController *cameraController = [[CameraViewController alloc] init];
//    cameraController.cameraDelegate = self;
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

- (void)showRegisterAlert {
    AlertWithActionsAndTag(1000, @"登录晒吧", @"继续操作之前先登录一下吧~~", @"看看再说", @"马上GO~~");
}

- (IBAction)showMenu:(id)sender {
    if (!_isMenuShowing) {
        [self showMenu];
    }else {
        [self hideMenu];
    }
    
    _isMenuShowing = !_isMenuShowing;
}

- (void)showMenu {
    NSTimeInterval deplayTime = 0.1;
    float yOffset = _publishMenuView.frame.size.height - _caremaBtn.frame.size.height;
    
    [UIView animateWithDuration:0.45 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
        SetFrameY(_publishMenuView, _publishMenuView.frame.origin.y - yOffset);
    } completion:^(BOOL finished) {
        // _maskBtn.frame = self.view.bounds;
        //[self.view insertSubview:_maskBtn belowSubview:_publishMenuView];

        _paperAnimationView.animationImages = _paperAnimations;
        [_paperAnimationView startAnimatingWithCompletionBlock:^(BOOL success) {
            [_paperAnimationView stopAnimating];
            _paperAnimationView.image = [_paperAnimations lastObject];
        }];
    }];
    
    for (int i = 0; i < _menuItemBtns.count; ++i) {
        [UIView animateWithDuration:0.4 delay:deplayTime * (i + 1) usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
            UIButton *itemBtn = (UIButton *)_menuItemBtns[i];
            itemBtn.titleLabel.alpha = 1;
            SetFrameY(itemBtn, itemBtn.frame.origin.y - yOffset);
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (void)hideMenu {
    NSTimeInterval deplayTime = 0.1;
    float yOffset = _publishMenuView.frame.size.height - _caremaBtn.frame.size.height;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
        for (int i = 0; i < _menuItemBtns.count; ++i) {
            UIButton *itemBtn = (UIButton *)_menuItemBtns[i];
            itemBtn.titleLabel.alpha = 0;
            SetFrameY(itemBtn, itemBtn.frame.origin.y + yOffset);
        }
    } completion:^(BOOL finished) {
        
    }];
    
    [UIView animateWithDuration:0.45 delay:deplayTime usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
        SetFrameY(_publishMenuView, _publishMenuView.frame.origin.y + yOffset);
        //_maskBtn.frame = _publishMenuView.frame;
        //[self.view bringSubviewToFront:_maskBtn];
    } completion:^(BOOL finished) {
        _paperAnimationView.animationImages = _paperReverseAnimation;
        [_paperAnimationView startAnimatingWithCompletionBlock:^(BOOL success) {
            [_paperAnimationView stopAnimating];
            _paperAnimationView.image = [_paperReverseAnimation lastObject];
        }];
    }];
}

- (void)goToNotificationView:(UIButton *)sender {
    NotificationViewController *notificationController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    [self.navigationController pushViewController:notificationController animated:YES];
    [self hideTopNotification];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
   if (alertView.tag == 1000) {
        if (buttonIndex == 1) {
            [self presentLoginController];
        }
    }
}

- (NSInteger)numberOfImagesForAnimatedImageView:(UIImageView *)imageView {
    return _paperAnimations.count;
}

- (NSString *)imageNameAtIndex:(NSInteger)index forAnimatedImageView:(UIImageView *)imageView {
    return [NSString stringWithFormat:@"paper_%d.png", (int)index + 1];
}

- (void)tabBarController:(RDVTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
//    [((MainViewController *)viewController) resetNavigationBar];
}

#pragma mark CameraViewControllerDelegate
- (void)cameraViewController:(CameraViewController *)controller didPickImage:(UIImage *)image {
    [PublishParamSet sharedInstance].photos = @[image];
    
    [self presentPublishViewController];
}

- (void)cameraViewControllerDidCancel:(CameraViewController *)controller {
    
}

- (void)photoSelectionController:(PhotoSelectionController *)controller didSelectAlbumPhoto:(UIImage *)photoImage {
    [PublishParamSet sharedInstance].photos = @[photoImage];
    
    [self presentPublishViewController];
}

@end
