//
//  AlbumPhotoViewController.m
//  youpinwei
//
//  Created by tmy on 14/11/18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "AlbumPhotoViewController.h"

@interface AlbumPhotoViewController ()

@end

@implementation AlbumPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
