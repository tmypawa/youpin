//
//  PublishStatusController.h
//  youpinwei
//
//  Created by tmy on 15/2/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PublishStatusControllerDelegate;

@interface PublishStatusController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<PublishStatusControllerDelegate> delegate;

@end


@protocol PublishStatusControllerDelegate <NSObject>

- (void)publishStatusControllerDidChangeContentHeight:(PublishStatusController *)controller;

@end