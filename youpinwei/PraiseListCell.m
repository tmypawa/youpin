//
//  PraiseListCell.m
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PraiseListCell.h"
#import "UIKit+RoundImage.h"

@implementation PraiseListCell
{
    IBOutlet UIButton *_moreBtn;
    
    IBOutlet UIView *_avatarContainerView;
    IBOutlet UIImageView *_praiseIconView;
    NSArray     *_avatars;
    NSMutableArray  *_avatarBtns;
    BOOL        _shouldUpdateAvatars;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _avatarBtns = [[NSMutableArray alloc] init];
    
    _nameButton.layer.cornerRadius = 4;
    self.praiseButton.normalImage = [UIImage imageNamed:@"icon_like"];
    self.praiseButton.highlightImage = [UIImage imageNamed:@"icon_liked"];
    self.praiseButton.highlightScale = 1;
    [self.praiseButton setBackgroundImage:[[UIImage imageNamed:@"btn_like"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
//    self.praiseButton.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.praiseButton.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
//    self.praiseButton.contentEdgeInsets = UIEdgeInsetsMake(0, 2, 0, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setAvatars:(NSArray *)avatarURLs {
    if (_avatars != avatarURLs) {
        for (UIButton *avatarBtn in _avatarBtns) {
            [avatarBtn removeFromSuperview];
        }
        
        [_avatarBtns removeAllObjects];
        _avatars = avatarURLs;
        _shouldUpdateAvatars = YES;
    }
}

- (void)updateLayout {
    CGSize titleSize = [self.praiseButton.titleLabel.text boundingRectWithSize:CGSizeMake(68, self.praiseButton.frame.size.height) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.praiseButton.titleLabel.font} context:nil].size;
    
    SetFrameWidth(self.praiseButton, 40 + titleSize.width);
    SetFrameX(_avatarContainerView, CGRectGetMaxX(self.praiseButton.frame) + 8);
    
    if (_avatars && _avatars.count > 0) {
        _praiseIconView.hidden = YES;
        self.nameButton.hidden = NO;
        SetFrameX(self.nameButton, CGRectGetMaxX(_avatarContainerView.frame) + 8);
    }else {
        _praiseIconView.hidden = NO;
        self.nameButton.hidden = YES;
    }
    
    if (_shouldUpdateAvatars && _avatars) {
        _shouldUpdateAvatars = NO;
        CGFloat x = 0;
        for (NSString *avatarURL in _avatars) {
            UIButton *avatarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            avatarBtn.imageView.contentMode = UIViewContentModeScaleToFill;
            avatarBtn.tag = [_avatars indexOfObject:avatarURL];
            avatarBtn.frame = CGRectMake(x, 0, 32, 32);
            avatarBtn.backgroundColor = [UIColor clearColor];
            [avatarBtn addTarget:self.target action:self.avatarSelector forControlEvents:UIControlEventTouchUpInside];
            [avatarBtn makeRound];
            [avatarBtn setImageWithURL:avatarURL placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
            [_avatarContainerView addSubview:avatarBtn];
            [_avatarBtns addObject:avatarBtn];
            x += (avatarBtn.frame.size.width + 8);
        }
    }
}

+ (CGFloat)height {
    return 50;
}

@end
