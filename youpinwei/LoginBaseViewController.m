//
//  LoginBaseViewController.m
//  youpinwei
//
//  Created by tmy on 15/1/23.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "LoginBaseViewController.h"

@interface LoginBaseViewController ()

@end

@implementation LoginBaseViewController
{
    IBOutlet UIImageView *_backgroundView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *bgPath = [[NSBundle mainBundle] pathForResource:@"bg-login" ofType:@"png"];
    _backgroundView.image = [UIImage imageWithContentsOfFile:bgPath];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
