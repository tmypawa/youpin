//
//  PhotoCropViewController.h
//  youpinwei
//
//  Created by tmy on 14/10/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "RSKImageCropViewController.h"

@interface PhotoCropViewController : RSKImageCropViewController<RSKImageCropViewControllerDataSource>

@end
