//
//  FriendInfoViewController.h
//  youpinwei
//
//  Created by tmy on 14/12/25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PersonalInfoViewController.h"
#import "FriendInvitationController.h"

@interface FriendInfoViewController : PersonalInfoViewController<FriendInvitationControllerDelegate, UIViewControllerTransitioningDelegate>

@property (nonatomic, strong) NSString *userId;

@end
