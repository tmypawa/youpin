//
//  RecommendTagItem.h
//  youpinwei
//
//  Created by tmy on 15/3/27.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecommendTagItem : NSObject

@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *thumbnail;

@end
