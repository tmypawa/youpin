//
//  FriendAddingViewController.m
//  youpinwei
//
//  Created by tmy on 15/1/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "FriendAddingViewController.h"
#import "FriendInfoCell.h"
#import "FriendInfoViewController.h"

@interface FriendAddingViewController ()

@end

@implementation FriendAddingViewController
{
    IBOutlet UISearchBar *_searchBar;
    NSArray  *_searchUserList;
    NSMutableSet *_invitationSet;
    NSInteger   _currentActionIndex;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.showBackButton = YES;
        _currentActionIndex = -1;
        _invitationSet = [[NSMutableSet alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:255/255.0];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:NavigationBarTintColor] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageFromColor:NavigationBarTintColor]];
    
    self.navigationItem.hidesBackButton = !self.showBackButton;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    _searchBar.backgroundImage = [UIImage imageFromColor:[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:255/255.0]];
    _searchBar.tintColor = NavigationBarTintColor;
    _searchBar.translucent = NO;

    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"FriendHeaderView"];
    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"FriendSearchCell" bundle:nil] forCellReuseIdentifier:@"FriendInfoCell"];
    
    [self loadFriends];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirm:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationUserDidLogin object:nil userInfo:nil];
    }];
}

- (void)loadFriends {
    [self showHUDViewWithTitle:@""];
    [[YouPinAPI sharedAPI] fetchRecommendFriendList:self callback:^(NSArray *friendList, NSError *error) {
        [self hideHUDView];
        if (!error) {
            self.knownFriendList = friendList;
            [self.tableView reloadData];
        }
    }];
}

- (void)addFriend:(UIButton *)sender {
    User *friend = nil;
    NSInteger index = sender.tag - 1;
    _currentActionIndex = index;
    if(self.searchDisplayController.isActive) {
        friend = _searchUserList[index];
    }else {
        friend = self.knownFriendList[index];
    }
    
    FriendInvitationController *invitationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInvitationController"];
    invitationController.userId = friend.userId;
    invitationController.delegate = self;
    [self.navigationController pushViewController:invitationController animated:YES];
}

- (void)viewFriendInfo:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    User *friend = self.searchDisplayController.isActive? _searchUserList[index] : self.knownFriendList[index];
    
    if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:friend.userId]) {
        PersonalInfoViewController *personInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
        [self.navigationController pushViewController:personInfoController animated:YES];
    }else {
        FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
        friendInfoController.userId = friend.userId;
        [self.navigationController pushViewController:friendInfoController animated:YES];
    }
}

- (void)friendInvitationControllerDidSendInvitation:(FriendInvitationController *)controller {
    if (_currentActionIndex != -1) {
        User *friend = nil;
        if(self.searchDisplayController.isActive) {
            friend = _searchUserList[_currentActionIndex];
        }else {
            friend = self.knownFriendList[_currentActionIndex];
        }
        
        [_invitationSet addObject:friend.userId];
        
        UITableView *toRefreshTableView = self.searchDisplayController.isActive ? self.searchDisplayController.searchResultsTableView : self.tableView;
        [toRefreshTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_currentActionIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        _currentActionIndex = -1;
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        return self.knownFriendList ? self.knownFriendList.count : 0;
    }else {
        return _searchUserList ? _searchUserList.count : 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"FriendHeaderView"];
    UILabel *titleLabel = (UILabel *)[headerView.contentView viewWithTag:1000];
    if (!titleLabel) {
        titleLabel =[[UILabel alloc] initWithFrame:CGRectMake(10, 0, 200, 40)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont systemFontOfSize:14];
        titleLabel.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:255/255.0];
        titleLabel.tag = 1000;
        [headerView.contentView addSubview:titleLabel];
        
        headerView.contentView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:255/255.0];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5f, screenWidth, 0.5f)];
        lineView.backgroundColor = [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:255/255.0];
        [headerView.contentView addSubview:lineView];
    }
    
    titleLabel.text = @"来自通讯录";
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FriendInfoCell" forIndexPath:indexPath];
    User *friend = self.searchDisplayController.isActive ? _searchUserList[indexPath.row] : self.knownFriendList[indexPath.row];
    
    if (cell.addBtn.tag == 0) {
        [cell.addBtn addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
        [cell.avatarBtn addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.addBtn.tag = indexPath.row + 1;
    cell.avatarBtn.tag = indexPath.row + 1;
    
    [cell.avatarBtn setImageWithURL:friend.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    cell.nameTitleLabel.text = friend.nickname;
    
    if(friend.relation == UserRelationTypeWaitAccept || [_invitationSet containsObject:friend.userId]){
        cell.statusLabel.text = @"等待验证";
        cell.statusLabel.hidden = NO;
        cell.addBtn.hidden = YES;
    }else if(friend.relation == UserRelationTypeFriend) {
        cell.addBtn.hidden = YES;
        cell.statusLabel.hidden = NO;
        cell.statusLabel.text = @"已添加";
    }else if(friend.relation == UserRelationTypeNotFriend) {
        [cell.addBtn setTitle:@"添加" forState:UIControlStateNormal];
        [cell.addBtn setTitleColor:NavigationBarTintColor forState:UIControlStateNormal];
        cell.addBtn.hidden = NO;
        cell.statusLabel.hidden = YES;
    }
    
    [cell updateLayout];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *friend = nil;
    if (self.tableView == tableView) {
        friend = self.knownFriendList[indexPath.row];
    }else {
        friend = _searchUserList[indexPath.row];
    }
    
    if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:friend.userId]) {
        PersonalInfoViewController *personInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
        [self.navigationController pushViewController:personInfoController animated:YES];
    }else {
        FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
        friendInfoController.userId = friend.userId;
        [self.navigationController pushViewController:friendInfoController animated:YES];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *keyword = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (keyword.length > 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[YouPinAPI sharedAPI] searchUserByNickname:keyword sender:self callback:^(NSArray *userList, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            if (!error) {
                _searchUserList = userList;
                [self.searchDisplayController.searchResultsTableView reloadData];
            }
        }];
    }
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    UISearchBar *searchBar = controller.searchBar;
    UIView *superView = searchBar.superview;
    if (![superView isKindOfClass:[UITableView class]]) {
        [searchBar removeFromSuperview];
        [self.tableView addSubview:searchBar];
    }
}

@end
