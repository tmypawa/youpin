//
//  FriendListCell.m
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "FriendListCell.h"
#import "UIKit+RoundImage.h"

@implementation FriendListCell

- (void)awakeFromNib {
    [self.avatarBtn makeRound];
    self.avatarBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
}

@end
