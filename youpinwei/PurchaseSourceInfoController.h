//
//  PurchaseSourceInfoController.h
//  youpinwei
//
//  Created by tmy on 15/3/25.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"

@protocol PurchaseSourceInfoControllerDelegate;

@interface PurchaseSourceInfoController : ShaibaBaseViewController<UITextViewDelegate>

@property (nonatomic, weak) id<PurchaseSourceInfoControllerDelegate> delegate;
@property (nonatomic, strong) UIImage *iconImage;

@end

@protocol PurchaseSourceInfoControllerDelegate <NSObject>

- (void)PurchaseSourceInfoController:(PurchaseSourceInfoController *)controller didFillInfo:(NSString *)sourceInfoValue;
- (void)PurchaseSourceInfoControllerDidCancel:(PurchaseSourceInfoController *)controller;

@end
