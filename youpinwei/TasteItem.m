//
//  TasteItem.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "TasteItem.h"
#import "Comment.h"
#import "JTAddressBookManager.h"

@implementation TasteItem

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.buyLink forKey:@"buyLink"];
    [encoder encodeObject:self.billId forKey:@"billId"];
    [encoder encodeObject:self.itemId forKey:@"itemId"];
    [encoder encodeDouble:self.createTime forKey:@"createTime"];
    [encoder encodeObject:self.tasteDescription forKey:@"tasteDescription"];
    [encoder encodeObject:self.extraId forKey:@"extraId"];
    [encoder encodeObject:self.whoseFriendPhone forKey:@"whoseFriendPhone"];
    [encoder encodeObject:self.whoseFriendNick forKey:@"whoseFriendNick"];
    [encoder encodeObject:self.nick forKey:@"nick"];
    [encoder encodeInteger:self.id forKey:@"id"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.headImage forKey:@"headImage"];
    [encoder encodeObject:self.originalPic forKey:@"originalPic"];
    [encoder encodeInteger:self.pincount forKey:@"pincount"];
    [encoder encodeInteger:self.wishcount forKey:@"wishcount"];
    [encoder encodeObject:self.price forKey:@"price"];
    [encoder encodeObject:self.productId forKey:@"productId"];
    [encoder encodeObject:self.shareData forKey:@"shareData"];
    [encoder encodeObject:self.source forKey:@"source"];
    [encoder encodeInteger:self.state forKey:@"state"];
    [encoder encodeDouble:self.updateTime forKey:@"updateTime"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeInt:self.commentCount forKey:@"commentCount"];
    [encoder encodeInt:self.readCount forKey:@"readCount"];
    [encoder encodeInt:self.relation forKey:@"relation"];
    [encoder encodeObject:self.realShotPics forKey:@"realShotPics"];
    [encoder encodeBool:self.pin forKey:@"pin"];
    [encoder encodeBool:self.addWishList forKey:@"addWishList"];
    [encoder encodeObject:self.contentItems forKey:@"contentItems"];
    [encoder encodeObject:self.comments forKey:@"comments"];
    [encoder encodeObject:self.editorComment forKey:@"editorComment"];
    [encoder encodeObject:self.pinUsers forKey:@"pinUsers"];
    [encoder encodeObject:self.tags forKey:@"tags"];
    [encoder encodeObject:self.photosZipFilePath forKey:@"photosZipFilePath"];
    [encoder encodeObject:self.draftIdentifier forKey:@"draftIdentifier"];
    [encoder encodeObject:self.requestId forKey:@"requestId"];
    [encoder encodeInteger:self.degree forKey:@"degree"];
    [encoder encodeBool:self.experience forKey:@"experience"];
    [encoder encodeObject:self.publishItems forKey:@"publishItems"];
    [encoder encodeObject:self.goods forKey:@"goods"];
    [encoder encodeObject:self.littleThumbnailImage forKey:@"littleThumbnailImage"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.title = [decoder decodeObjectForKey:@"title"];
        self.buyLink = [decoder decodeObjectForKey:@"buyLink"];
        self.billId = [decoder decodeObjectForKey:@"billId"];
        self.itemId = [decoder decodeObjectForKey:@"itemId"];
        self.createTime = [decoder decodeDoubleForKey:@"createTime"];
        self.tasteDescription = [decoder decodeObjectForKey:@"tasteDescription"];
        self.extraId = [decoder decodeObjectForKey:@"extraId"];
        self.whoseFriendPhone = [decoder decodeObjectForKey:@"whoseFriendPhone"];
        self.whoseFriendNick = [decoder decodeObjectForKey:@"whoseFriendNick"];
        self.nick = [decoder decodeObjectForKey:@"nick"];
        self.id = [decoder decodeIntegerForKey:@"id"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.headImage = [decoder decodeObjectForKey:@"headImage"];
        self.originalPic = [decoder decodeObjectForKey:@"originalPic"];
        self.pincount = [decoder decodeIntegerForKey:@"pincount"];
        self.wishcount = [decoder decodeIntegerForKey:@"wishcount"];
        self.price = [decoder decodeObjectForKey:@"price"];
        self.productId = [decoder decodeObjectForKey:@"productId"];
        self.shareData = [decoder decodeObjectForKey:@"shareData"];
        self.source = [decoder decodeObjectForKey:@"source"];
        self.state = [decoder decodeIntegerForKey:@"state"];
        self.updateTime = [decoder decodeDoubleForKey:@"updateTime"];
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.commentCount = [decoder decodeIntForKey:@"commentCount"];
        self.readCount = [decoder decodeIntForKey:@"readCount"];
        self.relation = [decoder decodeIntForKey:@"relation"];
        self.realShotPics = [decoder decodeObjectForKey:@"realShotPics"];
        self.pin = [decoder decodeBoolForKey:@"pin"];
        self.addWishList = [decoder decodeBoolForKey:@"addWishList"];
        self.contentItems = [decoder decodeObjectForKey:@"contentItems"];
        self.comments = [decoder decodeObjectForKey:@"comments"];
        self.editorComment = [decoder decodeObjectForKey:@"editorComment"];
        self.pinUsers = [decoder decodeObjectForKey:@"pinUsers"];
        self.tags = [decoder decodeObjectForKey:@"tags"];
        self.photosZipFilePath = [decoder decodeObjectForKey:@"photosZipFilePath"];
        self.draftIdentifier = [decoder decodeObjectForKey:@"draftIdentifier"];
        self.requestId = [decoder decodeObjectForKey:@"requestId"];
        self.degree = [decoder decodeIntegerForKey:@"degree"];
        self.experience = [decoder decodeBoolForKey:@"experience"];
        self.publishItems = [decoder decodeObjectForKey:@"publishItems"];
        self.goods = [decoder decodeObjectForKey:@"goods"];
        self.littleThumbnailImage = [decoder decodeObjectForKey:@"littleThumbnailImage"];
    }
    return self;
}

- (id)init {
    if (self = [super init]) {
        self.comments = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)updateStatusFromItem:(TasteItem *)item {
    if (item) {
        self.commentCount = item.commentCount;
        self.pincount = item.pincount;
        self.pin = item.pin;
        self.addWishList = item.addWishList;
        if (!self.comments) {
            self.comments = [[NSMutableArray alloc] init];
        }
        
        [self.comments removeAllObjects];
        if (item.comments && item.comments.count > 0) {
            [self.comments addObject:item.comments.lastObject];
        }
    }
}

- (NSString *)displayName {
    NSString *displayName = @"";
    if (JTGlobalCache.currentUser && [self.extraId isEqualToString:JTGlobalCache.currentUser.phone]) {
        displayName = @"我";
    }else {
        switch (self.relation) {
            case 0:
            case 1:
                displayName = self.nick;
                break;
            case 2:
            {
                JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:self.whoseFriendPhone];
                if (person) {
                    displayName = [NSString stringWithFormat:@"%@的朋友", person.fullName];
                }else if(self.whoseFriendNick && self.whoseFriendNick.length > 0){
                    displayName = [NSString stringWithFormat:@"%@的朋友", self.whoseFriendNick];
                }else {
                    displayName = @"朋友的朋友";
                }
            }
                break;
            default:
                break;
        }
    }
    
    return displayName;
}

- (NSString *)displayTitle
{
    NSString *fullTitle = @"";
    
    if (NotEmptyValue(self.tasteDescription)) {
        fullTitle = [self.tasteDescription stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" \n"]];
    }
    
//    fullTitle = [fullTitle stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,!。，！"]];
//    fullTitle = [fullTitle stringByAppendingString:@"..."];
    
    return fullTitle;
}

@end
