//
//  DialogViewController.m
//  youpinwei
//
//  Created by tmy on 14/11/28.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "DialogViewController.h"

@interface DialogViewController ()

@end

@implementation DialogViewController
{
    IBOutlet UILabel *_titleLabel;
    
    IBOutlet UIView *_containerView;
    IBOutlet UIImageView *_dialogBgView;
    IBOutlet UIImageView *_bgView;
    IBOutlet UILabel *_subtitleLabel;
    IBOutlet UIImageView *_logoView;
    IBOutlet UILabel *_descriptionLabel;
    IBOutlet UIView *_lineView;
    IBOutlet UIButton *_cancelBtn;
    IBOutlet UIButton *_okBtn;
    
    NSString *_title;
    NSString *_subtitle;
    UIImage  *_iconImage;
    NSString *_descriptionText;
    NSString *_okTitle;
    NSString *_cancelTitle;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titleLabel.text = _title;
    if (_subtitle && ![_subtitle isEqualToString:@""]) {
        _subtitleLabel.text = _subtitle;
    }
    
    if (_iconImage) {
        _logoView.image = _iconImage;
    }
    
    _descriptionLabel.text = _descriptionText;
    [_okBtn setTitle:_okTitle forState:UIControlStateNormal];
    [_cancelBtn setTitle:_cancelTitle forState:UIControlStateNormal];
    _dialogBgView.layer.cornerRadius = 6;
    
    [self updateLayout];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UIViewController *parentController = self.presentingViewController;
    UIView *shotView = [parentController.view snapshotViewAfterScreenUpdates:YES];
    [self.view insertSubview:shotView belowSubview:_bgView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setTitle:(NSString *)title subtitle:(NSString *)subtitle icon:(UIImage *)iconImage descriptionText:(NSString *)descriptionText okButtonTitle:(NSString *)okTitle cancelButtonTitle:(NSString *)cancelTitle {
    _title = title;
    _subtitle = subtitle;
    _iconImage = iconImage;
    _descriptionText = descriptionText;
    _okTitle = okTitle;
    _cancelTitle = cancelTitle;
}

- (void)updateLayout {
    if (!_subtitle || _subtitle.length == 0) {
        _subtitleLabel.hidden = YES;
        SetFrameY(_logoView, CGRectGetMaxY(_titleLabel.frame) + 31);
    }else {
        SetFrameY(_logoView, CGRectGetMaxY(_subtitleLabel.frame) + 31);
    }
    
    SetFrameY(_descriptionLabel, CGRectGetMaxY(_logoView.frame) + 31);
    CGFloat descriptionHeight = [_descriptionLabel.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(_descriptionLabel.frame), CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: _descriptionLabel.font} context:nil].size.height;
    if (iOSVersion < 8.0f) {
        descriptionHeight += 10;
    }
    
    SetFrameHeight(_descriptionLabel, descriptionHeight);
    SetFrameY(_lineView, CGRectGetMaxY(_descriptionLabel.frame) + 32);
    SetFrameY(_cancelBtn, CGRectGetMaxY(_lineView.frame) + 15);
    SetFrameY(_okBtn, CGRectGetMaxY(_lineView.frame) + 15);
    SetFrameHeight(_dialogBgView, CGRectGetMaxY(_cancelBtn.frame) + 10);
    SetFrameHeight(_containerView, CGRectGetMaxY(_cancelBtn.frame) + 10);
    _containerView.center = CGPointMake(screenWidth/2, screenHeight/2);
}

- (IBAction)ok:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(dialogViewController:didFinishWithResult:)]) {
        [self.delegate dialogViewController:self didFinishWithResult:YES];
    }
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}
 
- (IBAction)cancel:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(dialogViewController:didFinishWithResult:)]) {
        [self.delegate dialogViewController:self didFinishWithResult:NO];
    }
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

@end
