//
//  AskPurchaseInfoCell.h
//  youpinwei
//
//  Created by tmy on 15/3/26.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostContentCell.h"

@interface AskPurchaseInfoCell : PostContentCell
@property (strong, nonatomic) IBOutlet UIImageView *bgView;
@property (strong, nonatomic) IBOutlet UIImageView *arrowView;
@property (strong, nonatomic) IBOutlet UILabel *tipLabel;

@end
