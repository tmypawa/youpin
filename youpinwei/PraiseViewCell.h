//
//  PraiseViewCell.h
//  youpinwei
//
//  Created by tmy on 14/12/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PraiseViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *avatarView;
@property (strong, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UIImageView *levelconView;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

- (void)updateLayout;

@end
