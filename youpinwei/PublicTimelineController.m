//
//  PublicTimelineController.m
//  youpinwei
//
//  Created by tmy on 14/11/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PublicTimelineController.h"
#import "BannerViewController.h"
#import "NewFriendViewController.h"
#import "FriendInfoViewController.h"
#import "TagViewController.h"
#import "HotTagCell.h"
#import "MainItemCell.h"
#import "RecommendTag.h"
#import "RecommendTagViewCell.h"
#import "CategoryChooseViewController.h"

#define BANNER_PADDING 5

@interface PublicTimelineController ()

@end

@implementation PublicTimelineController
{
    IBOutlet UIView *_activityBannerView;
    IBOutlet UICollectionView *_tagCollectionView;
    IBOutlet UIButton *_bannerBtn;
    IBOutlet UILabel *_tagTitleView;
    IBOutlet UILabel *_itemsTitleView;
    NSArray         *_hotTags;
}

- (void)internalInit {
    [super internalInit];
    _tasteItems = JTGlobalCache.publicTastes;
}

- (void)awakeFromNib {
    [self internalInit];
    [self registerNotifications];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tagCollectionView.scrollsToTop = NO;

//    if (JTGlobalCache.bannerSwitch) {
//        if(JTGlobalCache.isLogin) {
//            [self showBannerView:YES];
//            [self loadBanners];
//        }else {
//            [self showBannerView:NO];
//        }
//    }else {
//        [self showBannerView:NO];
//    }
    [self.tableView setTableHeaderView:nil];
}


- (void)showBannerView:(BOOL)show {
    CGFloat headerHeight = 0;
    if (show) {
        headerHeight = JTGlobalCache.bannerHeight + _tagCollectionView.frame.size.height + _tagTitleView.frame.size.height + _itemsTitleView.frame.size.height;
        _bannerBtn.hidden = NO;
        SetFrameHeight(_bannerBtn, JTGlobalCache.bannerHeight);
        SetFrameY(_tagTitleView, JTGlobalCache.bannerHeight);
        SetFrameY(_tagCollectionView, JTGlobalCache.bannerHeight + _tagTitleView.frame.size.height);
        SetFrameY(_itemsTitleView, CGRectGetMaxY(_tagCollectionView.frame));
        
        if (JTGlobalCache.publicBanners && JTGlobalCache.publicBanners.count > 0) {
            [self showTopBanners];
        }
    }else {
        headerHeight = _tagCollectionView.frame.size.height + _tagTitleView.frame.size.height + _itemsTitleView.frame.size.height;
        _bannerBtn.hidden = YES;
        SetFrameY(_tagTitleView, 0);
        SetFrameY(_tagCollectionView, _tagTitleView.frame.size.height);
        SetFrameY(_itemsTitleView, CGRectGetMaxY(_tagCollectionView.frame));
    }
    
    SetFrameHeight(_activityBannerView, headerHeight);
    [self.tableView setTableHeaderView:_activityBannerView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [((ShaibaViewController *)self.rdv_tabBarController) setNavigationTitle:@"首页"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTasteItemDidSend:) name:YouPinNotificationNewTasteItemDidSend object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:YouPinNotificationUserDidLogin object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogout:) name:YouPinNotificationUserDidLogout object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidVisitAnonymously:) name:YouPinNotificationUserDidVisitAnonymously object:nil];
}

- (void)initView {
    if (_tasteItems && _tasteItems.count > 0) {
        [self.tableView showLoadMoreView];
    }
    
    if (JTGlobalCache.isLogin) {
        if (_tasteItems.count == 0) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self refreshData];
        }
    }
    
    [self loadHotTagList];
}

- (void)loadBanners {
    [[YouPinAPI sharedAPI] fetchPublicBannerInfoWithSender:self callback:^(NSArray *banners, NSError *error) {
        if (!error) {
            JTGlobalCache.publicBanners = banners;
            [JTGlobalCache synchronize];
            
            [self showBannerView:YES];
        }
    }];
}

- (void)loadHotTagList {
    [[YouPinAPI sharedAPI] fetchHotTags:^(NSArray *tags, NSError *error) {
        if (!error) {
            _hotTags = tags;
            [_tagCollectionView reloadData];
        }
    }];
}

- (void)showTopBanners {
    if (JTGlobalCache.publicBanners.count > 0) {
        BannerInfo *banner = JTGlobalCache.publicBanners[0];
        [_bannerBtn setImageWithURL:banner.picUrl effect:SDWebImageEffectFade];
    }
}

- (IBAction)showBannerDetail:(id)sender {
    if (JTGlobalCache.publicBanners.count > 0) {
        BannerInfo *banner = JTGlobalCache.publicBanners[0];
        [self showTagListView:banner.tag];
//        BannerViewController *bannerViewControler = [[BannerViewController alloc] init];
//        NSString *gotoUrl = [NSString stringWithFormat:@"%@?userId=%@", banner.gotoUrl, JTGlobalCache.currentUser.userId];
//        bannerViewControler.URL = [NSURL URLWithString:gotoUrl];
//        bannerViewControler.view.tintColor = NavigationBarTintColor;
//        [self.navigationController pushViewController:bannerViewControler animated:YES];
    }
}

- (void)userDidVisitAnonymously:(NSNotification *)notification {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self refreshData];
    [self loadBanners];
}

- (void)userDidLogin:(NSNotification *)notification {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if(JTGlobalCache.bannerSwitch) {
        [self loadBanners];
    }
    
    [self refreshData];
}

- (void)userDidLogout:(NSNotification *)notification {
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self.rdv_tabBarController.navigationController popToRootViewControllerAnimated:NO];
    self.rdv_tabBarController.selectedIndex = 0;
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    [[YouPinAPI sharedAPI] cancelRequestForSender:self];
    [self internalInit];
}


- (void)newTasteItemDidSend:(NSNotification *)notification {
    if (self.rdv_tabBarController.selectedIndex != 3) {
        self.rdv_tabBarController.selectedIndex = 3;
    }
}

- (void)insertRecommendTags {
    if(!JTGlobalCache.recommendTags) return;
    
    for (RecommendTag *tag in JTGlobalCache.recommendTags) {
        if (![_tasteItems containsObject:tag]) {
            NSInteger recommendTagIndex = [_tasteItems indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return ((TasteItem *)obj).updateTime <= tag.updateTime;
            }];
            
            if (recommendTagIndex != NSNotFound) {
                [_tasteItems insertObject:tag atIndex:recommendTagIndex];
            }
        }
    }
}

- (void)goToCategoryView {
    CategoryChooseViewController *categoryController = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryChooseViewController"];
    categoryController.forSingleUse = YES;
    categoryController.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:categoryController];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

- (void)categoryChooseViewControllerDidFinish:(CategoryChooseViewController *)controller {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self refreshData];
}

- (void)refreshData {
    [[YouPinAPI sharedAPI] fetchPublicTimelineWithStartTimestamp:[self startTimestamp] endTimestamp:0 Sender:self callback:^(NSArray *items, RecommendTag *recommendTag, BOOL hasNext, NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        
        _todayTimestamp = [self dayTimestampFromDate:[NSDate date]];
        
        if (!error) {
            [_tasteItems removeAllObjects];
            [_tasteItems addObjectsFromArray:items];
            
            if (recommendTag) {
                JTGlobalCache.recommendTags = @[recommendTag];
                [self insertRecommendTags];
            }else {
                JTGlobalCache.recommendTags = nil;
            }
            
            [JTGlobalCache synchronize];
            
            [self.tableView reloadData];
            
            if (!_hasNext && _tasteItems.count > 0) {
                _hasNext = YES;
            }
            
        }else {
            if (error.code == NO_SUBSCRIPTION_CATEGORY) {
                [self goToCategoryView];
            }
        }
        
        if (!self.tableView.pullToRefreshView) {
            [self loadPullRereshAnimation];
        }else {
            [self.tableView stopRefreshAnimation];
        }
        
        if (!self.tableView.loadMoreView) {
            [self addLoadMoreView];
        }
        
        if (_hasNext) {
            [self.tableView showLoadMoreView];
        }
    }];
}

- (void)loadNextData {
    [[YouPinAPI sharedAPI] fetchPublicTimelineWithStartTimestamp:0 endTimestamp:[self endTimestamp] Sender:self callback:^(NSArray *items, RecommendTag *recommendTag, BOOL hasNext, NSError *error) {
        if (!error) {
            if (items.count > 0) {
                [_tasteItems addObjectsFromArray:items];
                [self insertRecommendTags];
                [JTGlobalCache synchronize];
                [self.tableView reloadData];
            }else {
                _hasNext = NO;
                [self.tableView dismissLoadMore];
            }
            
            [self.tableView endLoadMoreWithHandler:^(UIView *customView) {
                [((UIImageView *)customView) stopAnimating];
                customView.hidden = YES;
            }];
        }
    }];
}


- (void)updateFriendInfoViewForCell:(MainItemCell *)cell withTasteItem:(TasteItem *)item {
    cell.friendLabel.hidden = YES;
    cell.contactIconView.hidden = YES;
    cell.personLabel.text = item.nick;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [super scrollViewDidEndDecelerating:scrollView];

    if (![JTGlobalCache.tutorialSet containsObject:TUTORIAL_TIMELINE_TAG]) {
        if ([self.tableView.visibleCells count] > 0) {
            MainItemCell *cell = self.tableView.visibleCells[0];
            NSInteger rowIndex = [self.tableView indexPathForCell:cell].row;
            if(![_tasteItems[rowIndex] isKindOfClass:[TasteItem class]]) return;
            
            TasteItem *item = _tasteItems[rowIndex];
            if (item.tags.count > 0) {
               CGRect tagAreaFrame = [cell.tagsView convertRect:cell.tagsView.bounds toView:nil];
                
                CGFloat visibleHeight = self.rdv_tabBarController.navigationController.navigationBarHidden ? screenHeight - 20 - self.rdv_tabBarController.tabBar.frame.size.height : screenHeight - 64 - self.rdv_tabBarController.tabBar.frame.size.height;
                visibleHeight -= 60;
                
                CGFloat visibleY = self.rdv_tabBarController.navigationController.navigationBarHidden ? 20 : 64;
                visibleY += 60;
                
                CGRect visibleFrame = CGRectMake(0, visibleY, screenWidth, visibleHeight);
                if (CGRectContainsRect(visibleFrame, tagAreaFrame)) {
                    [self showTutorialWithAttachedView:cell.tagsView title:TUTORIAL_TIMELINE_TAG offset:CGPointZero showConfirmButton:YES];
                    
                    [JTGlobalCache.tutorialSet addObject:TUTORIAL_TIMELINE_TAG];
                    [JTGlobalCache synchronize];
                }
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.tableView) {
        [super scrollViewDidScroll:scrollView];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_tasteItems[indexPath.row] isKindOfClass:[TasteItem class]]) {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }else {
        return 150;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_tasteItems[indexPath.row] isKindOfClass:[TasteItem class]]) {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }else {
        RecommendTag *recommendTag = _tasteItems[indexPath.row];
        RecommendTagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendTagViewCell" forIndexPath:indexPath];
        __weak PublicTimelineController *__self = self;
        cell.tag = indexPath.row + 1;
        cell.showMoreHandler = ^(){
            TagViewController *tagController = [__self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
            tagController.tagName = recommendTag.tagName;
            [__self.navigationController pushViewController:tagController animated:YES];
        };
        
        cell.itemSelectHandler = ^(NSInteger itemIndex) {
            DetailViewController *detailController = [__self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
            detailController.tasteId = ((RecommendTagItem *)recommendTag.productList[itemIndex]).productId;
            [__self.navigationController pushViewController:detailController animated:YES];
        };
        
        cell.titleLabel.text = recommendTag.tagName;
        [cell setItems:recommendTag.productList];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_tasteItems[indexPath.row] isKindOfClass:[TasteItem class]]) {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}


#pragma mark UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _hotTags? _hotTags.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HotTagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HotTagCell" forIndexPath:indexPath];
    Tag *tag = _hotTags[indexPath.row];
    [cell.thumbnailView setImageWithURL:tag.thumbnailsUrl placeholderImage:nil effect:SDWebImageEffectFade];
    cell.titleLabel.text = [NSString stringWithFormat:@"#%@", tag.name];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Tag *tag = _hotTags[indexPath.row];
    [self showTagListView:tag.name];
}

- (void)showTagListView:(NSString *)tagName {
    TagViewController *tagViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
    tagViewController.tagName = tagName;
    [self.navigationController pushViewController:tagViewController animated:YES];
}

@end
