//
//  TasteItem.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    TasteSourceTypePhoto = 0,
    TasteSourceTypeTaobao
} TasteSourceType;

@class Comment;
@class Goods;
@interface TasteItem : NSObject<NSCoding>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *buyLink;
@property (strong, nonatomic) NSString *billId;
@property (strong, nonatomic) NSString *itemId;
@property (nonatomic) NSTimeInterval createTime;
@property (strong, nonatomic) NSString *tasteDescription;
@property (strong, nonatomic) NSString *extraId;
@property (strong, nonatomic) NSString *nick;
@property (strong, nonatomic) NSString *whoseFriendPhone;
@property (strong, nonatomic) NSString *whoseFriendNick;
@property (nonatomic) int id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *originalPic;
@property (nonatomic) int pincount;
@property (nonatomic) int wishcount;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *productId;
@property (strong, nonatomic) NSString *shareData;
@property (strong, nonatomic) NSString *source;
@property (nonatomic) int state;
@property (nonatomic) NSTimeInterval updateTime;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *headImage;
@property (nonatomic) int commentCount;
@property (nonatomic) int readCount;
@property (nonatomic) int relation;
@property (strong, nonatomic) NSArray *realShotPics;
@property (nonatomic) BOOL pin;
@property (nonatomic) BOOL addWishList;
@property (nonatomic, strong) NSArray *contentItems;
@property (strong, nonatomic) NSMutableArray *comments;
@property (strong, nonatomic) Comment *editorComment;
@property (strong, nonatomic) NSMutableArray *pinUsers;
@property (strong, nonatomic) NSArray  *tags;
@property (nonatomic) NSInteger degree;
@property (nonatomic) BOOL experience;

//extension for draft
@property (strong, nonatomic) NSString *photosZipFilePath;
@property (strong, nonatomic) NSString *draftIdentifier;
@property (strong, nonatomic) NSString *requestId;
@property (strong, nonatomic) NSString *littleThumbnailImage;
@property (strong, nonatomic) NSArray *publishItems;
@property (strong, nonatomic) Goods *goods;

- (void)updateStatusFromItem:(TasteItem *)item;
- (NSString *)displayTitle;
- (NSString *)displayName;

@end
