//
//  TagSelectionViewController.m
//  youpinwei
//
//  Created by tmy on 14-10-14.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "TagSelectionViewController.h"

@implementation TagSelectionViewController
{
    __weak IBOutlet DWTagList *_tagListView;
    IBOutlet DWTagList *_existTagListView;
    __weak IBOutlet UITextField *_tagNameField;
    IBOutlet UIScrollView *_tagListBgView;
    IBOutlet UITableView *_historyTagTableView;
    IBOutlet UIButton *_maskBtn;
    NSMutableArray      *_selectedTags;
    NSArray             *_hotTags;
    NSArray             *_relatedTags;
    BOOL                _showingHistoryView;
    BOOL                _isShowingRelatedTag;
    BOOL                _isInputChinese;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _selectedTags = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeText:) name:UITextFieldTextDidChangeNotification object:nil];
    
    _historyTagTableView.dataSource = self;
    _historyTagTableView.delegate = self;
    [_historyTagTableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    _historyTagTableView.alpha = 0;
    
    if(self.existTags.count == 0){
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    _tagNameField.tintColor = NavigationBarTintColor;
    _tagNameField.delegate = self;
    _tagNameField.returnKeyType = UIReturnKeyDone;
    
    _tagListView.tagDelegate = self;
    _tagListView.cornerRadius = 18;
    [_tagListView setTagBackgroundColor:[UIColor whiteColor]];
    _tagListView.horizontalPadding = 15;
    _tagListView.verticalPadding = 10;
    _tagListView.labelMargin = 10;
    [_tagListView setBorderColor: CGColorRetain([UIColor colorWithRed:0.82 green:0.82 blue:0.82 alpha:1].CGColor) ];
    _tagListView.borderWidth = 0.5;
    _tagListView.highlightedBackgroundColor = [UIColor colorWithRed:0.91 green:0.89 blue:0.89 alpha:1];
    _tagListView.backgroundColor = [UIColor clearColor];
    
    _existTagListView.automaticResize = YES;
    _existTagListView.tintColor = NavigationBarTintColor;
    _existTagListView.textColor = NavigationBarTintColor;
    _existTagListView.tagDelegate = self;
    _existTagListView.borderWidth = 0;
    [_existTagListView setTagBackgroundColor:[UIColor clearColor]];
    _existTagListView.labelMargin = 5;
    _existTagListView.verticalPadding = 0;
    _existTagListView.horizontalPadding = 0;
    _existTagListView.cornerRadius = 0;
    _existTagListView.backgroundColor = [UIColor clearColor];
    
    if (self.existTags) {
        [_selectedTags addObjectsFromArray:self.existTags];
        [self updateTagsView];
    }
    
    [[YouPinAPI sharedAPI] fetchDefaultTags:^(NSArray *tags, NSError *error) {
        if (!error) {
            _hotTags = tags;
            [_tagListView setTags:_hotTags];
            [_tagListBgView setNeedsDisplay];
        }else {
            
        }
    }];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [JTGlobalCache synchronize];
}


- (void)keyboardWillAppear:(NSNotification *)notification {
    CGRect frame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (frame.size.height && frame.size.width > 0) {
        UIEdgeInsets insets = _historyTagTableView.contentInset;
        insets.bottom = frame.size.height;
        _historyTagTableView.contentInset = insets;
        _historyTagTableView.scrollIndicatorInsets = insets;
    }
}

- (void)keyboardWillDisappear:(NSNotification *)notification {
    UIEdgeInsets insets = _historyTagTableView.contentInset;
    insets.bottom = 0;
    _historyTagTableView.contentInset = insets;
    _historyTagTableView.scrollIndicatorInsets = insets;
}


- (IBAction)cancelInput:(id)sender {
    [_tagNameField resignFirstResponder];
    [_maskBtn removeFromSuperview];
}

- (IBAction)addTag:(id)sender {
    NSString *tagName = [_tagNameField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (tagName.length > 0) {
//        [self updateTagHistoryWithTag:tagName];
        [self addNewTag:_tagNameField.text];
        _tagNameField.text = @"";
    }else {
        Alert(@"请输入标签名字", @"", @"确定");
    }
    
    if (_isShowingRelatedTag) {
        [self showOrHideRelatedTagView:NO];
    }
}
 

- (IBAction)confirm:(id)sender {
    if (_tagNameField.text.length > 0) {
        [self addNewTag:_tagNameField.text];
        [self showOrHideRelatedTagView:NO];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tagSelectionViewController:didSelectTags:)]) {
        [self.delegate tagSelectionViewController:self didSelectTags:_selectedTags];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateTagsView{
    NSMutableArray *tags = [[NSMutableArray alloc] init];
    [_selectedTags enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [tags addObject:[NSString stringWithFormat:@"#%@", obj]];
    }];
    
    [_existTagListView setTags:tags];
    SetFrameY(_tagListBgView, CGRectGetMaxY(_existTagListView.frame) + 5);
    SetFrameHeight(_tagListBgView, self.view.frame.size.height - _tagListBgView.frame.origin.y);
}

- (void)addNewTag:(NSString *)tagName {
    [_selectedTags addObject:tagName];
    
    [self updateTagsView];
}

- (void)updateTagHistoryWithTag:(NSString *)tagName {
    [JTGlobalCache.tagHistoryList insertObject:tagName atIndex:0];
    if (JTGlobalCache.tagHistoryList.count > MAX_TAG_HISTORY_SIZE) {
        [JTGlobalCache.tagHistoryList removeLastObject];
    }
}

- (void)DWTagList:(DWTagList *)tagList selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex {
    if (tagList == _tagListView) {
        [self addNewTag:tagName];
    }else {
        [_selectedTags removeObjectAtIndex:tagIndex];
        [self updateTagsView];
    }
    
    if (_tagNameField.text.length == 0 && _selectedTags.count == 0) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }else {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
}

- (void)showOrHideRelatedTagView:(BOOL)show {
    if (show) {
        _isShowingRelatedTag = YES;
        _historyTagTableView.alpha = 0;
        [self.view addSubview:_historyTagTableView];
        [UIView animateWithDuration:0.2f animations:^{
            _historyTagTableView.alpha = 1;
        }];
        
        [_historyTagTableView reloadData];
    }else {
        _isShowingRelatedTag = NO;
        [UIView animateWithDuration:0.2f animations:^{
            _historyTagTableView.alpha = 0;
        } completion:^(BOOL finished) {
            [_historyTagTableView removeFromSuperview];
        }];

    }
}

- (void)refreshRelatedTagsWithInputValue:(NSString *)inputValue {
    [[YouPinAPI sharedAPI] cancelRequestForSender:self];
    [[YouPinAPI sharedAPI] fetchRelatedTagsWithTag:inputValue sender:self callback:^(NSArray *tags, NSError *error) {
        if (!error) {
            _relatedTags = tags;
            [_historyTagTableView reloadData];
            if (!_isShowingRelatedTag) {
                [self showOrHideRelatedTagView:YES];
            }
        }
    }];
}

- (void)textFieldDidChangeText:(NSNotification *)notification {
    NSString *textValue = [_tagNameField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @" "]];
    if (NotEmptyValue(textValue)) {
        NSString *currentInputName = [_tagNameField textInputMode].primaryLanguage;
        BOOL isChineseInput = [currentInputName hasPrefix:@"zh"];
        if(!isChineseInput){
            [self refreshRelatedTagsWithInputValue:textValue];
        }else {
            UITextRange *range = _tagNameField.markedTextRange;
            if (!range || range.isEmpty) {
                [self refreshRelatedTagsWithInputValue:textValue];
            }
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
 
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.view insertSubview:_maskBtn belowSubview:_tagNameField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField.text stringByReplacingCharactersInRange:range withString:string].length == 0 && _selectedTags.count == 0) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }else {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self addTag:nil];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _relatedTags ? _relatedTags.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TagHistoryCell" forIndexPath:indexPath];
    cell.textLabel.text = _relatedTags[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _tagNameField.text = _relatedTags[indexPath.row];
}

@end
