//
//  FriendListController.m
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "FriendListController.h"
#import "M13BadgeView.h"
#import "FriendListCell.h"
#import "FriendInfoViewController.h"
#import "NewFriendViewController.h"
#import "FriendAddingViewController.h"

@implementation FriendListController
{
    IBOutlet M13BadgeView *_newFriendBadgeView;
    NSMutableArray *_friendList;
    NSInteger   _actionCellIndex;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _friendList = [[NSMutableArray alloc] init];
        _actionCellIndex = -1;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SetFrameOrigin(_newFriendBadgeView, 124, 17);
    _newFriendBadgeView.hidesWhenZero = YES;
    _newFriendBadgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentNone;
    _newFriendBadgeView.verticalAlignment = M13BadgeViewVerticalAlignmentNone;
    _newFriendBadgeView.badgeBackgroundColor = NavigationBarTintColor;
    _newFriendBadgeView.textColor = [UIColor whiteColor];
    _newFriendBadgeView.font = [UIFont systemFontOfSize:12];
    _newFriendBadgeView.text = @"0";
    
    if (JTGlobalCache.isLogin) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[YouPinAPI sharedAPI] fetchFriendList:self callback:^(NSArray *friendList, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            if (!error) {
                [_friendList removeAllObjects];
                [_friendList addObjectsFromArray:friendList];
                [self.tableView reloadData];
            }
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateFriendBadge];
}
 

- (IBAction)showFriendNotificationView:(id)sender {
    NewFriendViewController *newFriendController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewFriendViewController"];
    [self.navigationController pushViewController:newFriendController animated:YES];
}

- (void)updateFriendBadge {
    _newFriendBadgeView.text = [NSString stringWithFormat:@"%ld", JTGlobalCache.friendNotificationBadge];
    SetFrameOrigin(_newFriendBadgeView, 124, 17);
}

- (void)viewFriendInfo:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    User *friend = _friendList[index];
    FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
    friendInfoController.userId = friend.userId;
    [self.navigationController pushViewController:friendInfoController animated:YES];
}

- (void)deleteFriend {
    if (_actionCellIndex != -1) {
        User *friend = _friendList[_actionCellIndex];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[YouPinAPI sharedAPI] deleteFriend:friend.userId callback:^(NSError *error) {
            if (!error) {
                [self showSuccessHUDWithTitle:@"已删除"];
                [_friendList removeObjectAtIndex:_actionCellIndex];
                [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_actionCellIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            }else {
                [self showFailHUDWithTitle:@"删除失败"];
            }
            
            _actionCellIndex = -1;
        }];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 2000) {
        if (buttonIndex == 1) {
            [self deleteFriend];
        }else {
            _actionCellIndex = -1;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _friendList.count;
}
 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    _actionCellIndex = indexPath.row;
    AlertWithActionsAndTag(2000, @"确定要删除该好友吗?", @"", @"取消", @"确定");
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendListCell *cell = (FriendListCell *)[tableView dequeueReusableCellWithIdentifier:@"FriendListCell" forIndexPath:indexPath];
    if (cell.avatarBtn.tag == 0) {
        [cell.avatarBtn addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.avatarBtn.tag = indexPath.row + 1;
    
    User *friend = _friendList[indexPath.row];
    cell.nicknameLabel.text = [friend displayName];
    [cell.avatarBtn setImageWithURL:friend.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *friend = _friendList[indexPath.row];
    FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
    friendInfoController.userId = friend.userId;
    [self.navigationController pushViewController:friendInfoController animated:YES];
}

@end
