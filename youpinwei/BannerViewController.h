//
//  BannerViewController.h
//  youpinwei
//
//  Created by tmy on 14/12/12.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PBWebViewController.h"
#import "CameraViewController.h"

@interface BannerViewController : PBWebViewController<CameraViewControllerDelegate>

@end
