//
//  WatermarkPart.h
//  youpinwei
//
//  Created by tmy on 14-9-21.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WatermarkPart : NSObject

@property (nonatomic, strong) NSString *pic;
@property (nonatomic, strong) NSString *x;
@property (nonatomic, strong) NSString *y;
@property (nonatomic) NSInteger type;
@property (nonatomic) BOOL editable;
@property (nonatomic, strong) NSArray *pics;


@property (nonatomic, strong) NSString *text;
@property (nonatomic) NSInteger  textlength;
@property (nonatomic) NSInteger fontsize;
@property (nonatomic, strong) NSString *fontname;
@property (nonatomic, strong) NSString *fontcolor;
@property (nonatomic, strong) NSString *bgcolor;
@property (nonatomic) CGFloat paddingpercent;


@end
