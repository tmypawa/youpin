//
//  FriendAddingViewController.h
//  youpinwei
//
//  Created by tmy on 15/1/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendInvitationController.h"

@interface FriendAddingViewController : ShaibaTableViewController<UISearchControllerDelegate, UISearchBarDelegate, FriendInvitationControllerDelegate>

@property (nonatomic, strong) NSArray *knownFriendList;
@property (nonatomic) BOOL showBackButton;

@end
