//
//  PostSettingViewController.h
//  youpinwei
//
//  Created by tmy on 15/2/2.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagSelectionViewController.h"
#import "PurchaseInfoController.h"

@protocol PostSettingViewControllerDelegate;

@interface PostSettingViewController : UITableViewController<TagSelectionViewControllerDelegate, PurchaseInfoControllerDelegate>

@property (nonatomic, weak) id<PostSettingViewControllerDelegate> delegate;

@end

@protocol PostSettingViewControllerDelegate <NSObject>

- (void)postSettingViewControllerDidFinish:(PostSettingViewController *)controller;

@end