//
//  Tag.h
//  youpinwei
//
//  Created by tmy on 15/2/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tag : NSObject

@property (nonatomic, strong) NSString *bannerHeight;
@property (nonatomic, strong) NSString *bannerUrl;
@property (nonatomic, strong) NSString *bannerWidth;
@property (nonatomic, assign) NSTimeInterval createTime;
@property (nonatomic, strong) NSString *tagDescription;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *picHeight;
@property (nonatomic, strong) NSString *picWidth;
@property (nonatomic, strong) NSString *picUrl;
@property (nonatomic, strong) NSString *thumbnailsHeight;
@property (nonatomic, strong) NSString *thumbnailsUrl;
@property (nonatomic, strong) NSString *thumbnailsWidth;
@property (nonatomic, strong) NSString *interestUrl;
@property (nonatomic) NSTimeInterval updateTime;
@property (nonatomic) NSString  *color;
@property (nonatomic) BOOL showPhoto;
@property (nonatomic, strong) NSString *QRurl;
@property (nonatomic, strong) NSString *QRThumbnail;
@property (nonatomic) BOOL subscribe;
@property (nonatomic) NSInteger subscribeCount;
@property (nonatomic) NSInteger count;
@property (nonatomic, strong) NSArray *relevantTags;
@property (nonatomic) NSInteger listType;
@property (nonatomic) NSInteger tagType;


@end
