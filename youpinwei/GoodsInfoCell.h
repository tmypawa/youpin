//
//  GoodsInfoCell.h
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostContentCell.h"

@interface GoodsInfoCell : PostContentCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UIImageView *iconView;
@property (strong, nonatomic) IBOutlet UILabel *buyLinkLabel;
@property (strong, nonatomic) IBOutlet UIImageView *arrowLabel;

@end
