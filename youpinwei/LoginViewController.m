//
//  LoginViewController.m
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "UIImageView+AnimationCompletion.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
{
    IBOutlet UIButton *_weixinLoginBtn;
    IBOutlet UIImageView *_logoView;
    IBOutlet UIButton *_noRegisterBtn;
    GuideViewController *_guideController;
    IBOutlet UIImageView *_loginAnimationView;
    BOOL       _hasPlayedAnimation;
}

- (void)dealloc {
    int a = 1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.view.tintColor = GlobalTintColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: GlobalTintColor};
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageFromColor:[UIColor clearColor]]];
    
    [self loadAnimationImages];
    
    if (self.showGuide) {
        [self hideLoginView];
        _guideController = [self.storyboard instantiateViewControllerWithIdentifier:@"GuideViewController"];
        _guideController.delegate = self;
        [self addChildViewController:_guideController];
        [self.view addSubview:_guideController.view];
        [_guideController didMoveToParentViewController:self];
    }
    
    if (self.showLogoAnimation) {
        [self hideLoginView];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.showLogoAnimation) {
        [self showLoginAnimation];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadAnimationImages {
    NSMutableArray *animationImages = [[NSMutableArray alloc] init];
    for (int i = 0; i<60; ++i) {
        UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"logo_%d", i + 1] ofType:@"png"]];
        [animationImages addObject:image];
    }
    
    _loginAnimationView.animationImages = animationImages;
    _loginAnimationView.animationDuration = 2;
    _loginAnimationView.animationRepeatCount = 1;
}

- (void)hideLoginView {
    _logoView.hidden = YES;
    _weixinLoginBtn.hidden = YES;
    _noRegisterBtn.hidden = YES;
}

- (IBAction)noRegister:(id)sender {
    [_loginAnimationView.layer removeAllAnimations];
   
    JTGlobalCache.isReturnUser = YES;
    [JTGlobalCache synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationUserDidVisitAnonymously object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)weixinLogin:(id)sender {
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        //          获取用户名、uid、token等
        if (response.responseCode == UMSResponseCodeSuccess) {
            [MBProgressHUD showHUDAddedTo:self.view animated:NO];
            [[UMSocialDataService defaultDataService] requestSnsInformation:UMShareToWechatSession  completion:^(UMSocialResponseEntity *response){
                if (response && response.data) {
                    [MBProgressHUD hideHUDForView:self.view animated:NO];
                    
                    JTGlobalCache.currentLoginType = LoginTypeWeixin;
                    JTGlobalCache.thirdLoginData = response.data;
                    RegisterViewController *registerController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
                    
                    UINavigationController *navController = self.navigationController ? self.navigationController : self.parentViewController.navigationController;
                    [navController pushViewController:registerController animated:YES];
                }
            }];
        }});
}

- (void)showLoginAnimation {
    [_loginAnimationView startAnimatingWithCompletionBlock:^(BOOL success) {
        _weixinLoginBtn.hidden = NO;
        _weixinLoginBtn.alpha = 0;
        _noRegisterBtn.hidden = NO;
        _noRegisterBtn.alpha = 0;
        
        [UIView animateWithDuration:0.9f delay:0.5f usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
            _loginAnimationView.transform = CGAffineTransformMakeScale(0.7f, 0.7f);
            SetFrameY(_loginAnimationView, _loginAnimationView.frame.origin.y - 100);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.4f animations:^{
                _weixinLoginBtn.alpha = 1;
                _noRegisterBtn.alpha = 1;
            }];
        }];
    }];
}

- (void)guideViewControllerDidFinishGuiding:(GuideViewController *)controller {
    [_guideController willMoveToParentViewController:nil];
    [_guideController.view removeFromSuperview];
    [_guideController removeFromParentViewController];
    _guideController = nil;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showLoginAnimation];
    });
}


@end
