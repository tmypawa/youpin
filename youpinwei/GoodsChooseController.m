//
//  GoodsChooseController.m
//  youpinwei
//
//  Created by tmy on 14-8-22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "GoodsChooseController.h"
#import "PublishViewController.h"
#import "CameraViewController.h"
#import "Goods.h"
#import "GoodsChooseCell.h"
#import "PhotoFilterViewController.h"
#import "UIKit+RoundImage.h"
#import "JTStretchableTableHeaderView.h"
#import "GoodsPhotoSelectionController.h"

#define TAOBAO_ORDER_LIST_URL @"http://h5.m.taobao.com/awp/mtb/mtb.htm?sprefer=p23590#!/awp/mtb/olist.htm?sta=4"
#define TAOBAO_ORDER_LIST_MORE_URL @"http://h5.m.taobao.com/awp/mtb/mtb.htm?sprefer=p23590#!/awp/mtb/olist.htm?sta=8"
#define TAOBAO_LOGIN_URL @"http://login.m.taobao.com/login.htm?spm=0.0.0.0&tpl_redirect_url=http%3A%2F%2Fm.taobao.com%2F%3Fsprefer%3Dsypc00"


@interface GoodsChooseController ()

@end

@implementation GoodsChooseController
{
    NSDateFormatter     *_dateFormatter;
    Goods       *_selectedGoods;
    int         _selectedIndex;
    IBOutlet UIView *_footerView;
    IBOutlet UIImageView *_avatarView;
    IBOutlet UILabel *_nameLabel;
    UIView         *_headerSectionView;
    UIView         *_pinnedTimelineView;
    UIImageView    *_pinnedCircleView;
    UILabel        *_pinnedLabel;
    IBOutlet UIImageView *_headerBgView;
    JTStretchableTableHeaderView    *_stretchHeaderView;
    __weak IBOutlet UIActivityIndicatorView *_indicatorView;
    IBOutlet UIButton *_syncBtn;
    IBOutlet UIWebView *_webView;
    NSMutableArray  *_goodsList;
    NSArray         *_circleColors;
    NSMutableDictionary *_timelineDict;
    NSMutableArray      *_timelineArray;
    NSString    *_registerUrl;
    int         _pageIndex;
    int         _totalPageCount;
    BOOL        _isSyncingMoreGoods;
    NSString    *_preDateString;
    int         _pinnedRowIndex;
    int         _lastRowIndex;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _selectedIndex = -1;
        _pinnedRowIndex = -1;
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        _goodsList = [[NSMutableArray alloc] init];
        _timelineDict = [[NSMutableDictionary alloc] initWithCapacity:50];
        _timelineArray = [[NSMutableArray alloc] initWithCapacity:50];
        _circleColors = @[[UIColor colorWithRed:0.99 green:0.44 blue:0.35 alpha:1], [UIColor colorWithRed:0.74 green:0.82 blue:0.93 alpha:1], [UIColor colorWithRed:1 green:0.76 blue:0.3 alpha:1], [UIColor colorWithRed:0.74 green:0.84 blue:0.53 alpha:1]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    [_avatarView makeRound];
    
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)]];
    _stretchHeaderView = [[JTStretchableTableHeaderView alloc] init];
    [_stretchHeaderView stretchHeaderForTableView:_tableView withView:_headerBgView];
    
    _indicatorView.hidden = YES;
    
    [TaobaoURLProtocol setDelegate:self];
    _webView.hidden = YES;
    _tableView.hidden = YES;
    
    if (JTGlobalCache.currentUser) { 
        [_avatarView setImageWithURL:JTGlobalCache.currentUser.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectFade];
        _nameLabel.text = JTGlobalCache.currentUser.nickname;
    }else {
        _nameLabel.text = @"未登录";
    }
    
    UIView *bgView = [[UIView alloc] initWithFrame:screenFrame];
    bgView.backgroundColor = [UIColor whiteColor];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(38, 0, 1, bgView.frame.size.height)];
    lineView.backgroundColor = [UIColor colorWithRed:0.76 green:0.76 blue:0.76 alpha:1];
    [bgView addSubview:lineView];
    
    self.tableView.backgroundView = bgView;
    [self bindAccount];
//    if (!JTGlobalCache.isBinded) {
//        
//    }else {
//        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        [[YouPinAPI sharedAPI] fetchPurchaseHistory:self callback:^(NSArray *purchaseItems, NSError *error) {
//            if (!error) {
//                [self generateTimeLine:purchaseItems];
//                [_goodsList addObjectsFromArray:purchaseItems];
//                self.tableView.hidden = NO;
//                [self.tableView reloadData];
//                if (!JTGlobalCache.isBindedMore) {
//                    [self.tableView setTableFooterView:_footerView];
//                }
//            }else {
//                
//            }
//            
//            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
//        }];
//    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.tableView && self.tableView.indexPathForSelectedRow) {
        [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)generateTimeLine:(NSArray *)newGoodsList {
    NSDateComponents *preDateComponents = nil;
    for (Goods *goods in newGoodsList) {
        NSDate *orderDate = [_dateFormatter dateFromString:goods.billTime];
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:orderDate];
        if (!preDateComponents || ([preDateComponents year] != [dateComponents year] || [preDateComponents month] != [dateComponents month])) {
            preDateComponents = dateComponents;
            NSString *dateStr = [NSString stringWithFormat:@"%d.%02d", [dateComponents year], [dateComponents month]];
            int row = [newGoodsList indexOfObject:goods] + _goodsList.count;
            _timelineDict[@(row)] = dateStr;
            [_timelineArray addObject:@(row)];
        }
    }
}

- (void)bindAccount {
    NSURL *url = [NSURL URLWithString:TAOBAO_ORDER_LIST_URL];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (IBAction)syncMoreGoods:(id)sender {
    _isSyncingMoreGoods = YES;
    _indicatorView.hidden = NO;
    [_indicatorView startAnimating];
    _syncBtn.hidden = YES;
    NSURL *url = [NSURL URLWithString:TAOBAO_ORDER_LIST_MORE_URL];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)cameraViewController:(CameraViewController *)controller didPickImage:(UIImage *)image {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [PublishParamSet sharedInstance].photos = @[image];
        [PublishParamSet sharedInstance].goods = _selectedGoods;
        
        PublishViewController *publishController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishViewController"];
        [self.navigationController pushViewController:publishController animated:YES];
    });
}

- (void)cameraViewControllerDidCancel:(CameraViewController *)controller {
    
}

- (void)startPublishFlowWithSelectedGoods:(Goods *)goods {
    [PublishParamSet sharedInstance].goods = goods;
    
    PublishViewController *publishController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishViewController"];
    [self.navigationController pushViewController:publishController animated:YES];
}

- (void)loadNextPage {
    [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByClassName(\"c-btn c-btn-100 c-p-next\")[0].click()"];
}

- (int)getTotalPageCount {
    NSString *ret = [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByClassName('c-p-arrow')[0].getElementsByTagName('span')[0].textContent"];
    if ([ret isEqualToString:@""]) {
        return 1;
    }else {
        NSString *totalCountStr = [ret componentsSeparatedByString:@"/"][1];
        return [totalCountStr intValue];
    }
}

- (BOOL)hasNextPage {
    NSString *disabledNextBtnCount = [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByClassName('c-btn c-btn-100 c-p-next c-btn-off').length"];
    NSString *nextbtnCount = [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByClassName('c-btn c-btn-100 c-p-next').length"];
    return !([disabledNextBtnCount intValue] > 0 || [nextbtnCount intValue] == 0);
}

- (void)TaobaoURLProtocol:(TaobaoURLProtocol *)urlLProtocol didFetchOrderList:(NSString *)orderListData {
    if (orderListData) {

        NSInteger startIndex = [orderListData rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"("] options:0].location;
        orderListData = [orderListData substringWithRange:NSMakeRange(startIndex + 1, orderListData.length - startIndex - 2)];
        NSDictionary *jsonRet = [orderListData objectFromJSONString];
        NSArray *jsonItems = jsonRet[@"data"][@"cell"];
        if(jsonItems.count > 0) {
            NSString *account = jsonItems[0][@"buyerNick"];
            
            [[YouPinAPI sharedAPI] uploadPurchaseHistory:orderListData account:account sender:self callback:^(NSArray *purchaseItems, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self generateTimeLine:purchaseItems];
                    [_goodsList addObjectsFromArray:purchaseItems];
                    
                    _pageIndex++;
                    
                    if (_isSyncingMoreGoods) {
                        [self.tableView reloadData];
                    }
                    
                    if ([self hasNextPage]) {
                        [self loadNextPage];
                    }else {
                        [MBProgressHUD hideHUDForView:self.view animated:NO];
                        self.tableView.hidden = NO;
                        [self.tableView reloadData];
                        
                        if (_isSyncingMoreGoods) {
                            _isSyncingMoreGoods = NO;
                            JTGlobalCache.isBindedMore = YES;
                        }else {
                            JTGlobalCache.isBinded = YES;
                        }
                        
                        [JTGlobalCache synchronize];
                        
                        if (!JTGlobalCache.isBindedMore) {
                            [self.tableView setTableFooterView:_footerView];
                        }else {
                            [self.tableView setTableFooterView:nil];
                        }
                    }
                });
            }];
        }
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *url = request.URL.description;
        NSLog(@"----- %@", url);
    if ([url rangeOfString:@"login.m.taobao.com/welcome.htm"].location != NSNotFound) {
        _webView.hidden = YES;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }else if ([url rangeOfString:@"http://login.m.taobao.com/login.htm"].location != NSNotFound) {
        _webView.hidden = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_stretchHeaderView scrollViewDidScroll:scrollView];
    if (scrollView.contentOffset.y >= 225.5) {
        if (_pinnedRowIndex == -1) {
            _pinnedRowIndex = 0;
            _lastRowIndex = 0;
            _pinnedLabel.text = _timelineDict[_timelineArray[_pinnedRowIndex]];
            _pinnedCircleView.tintColor = _circleColors[0];
            _pinnedTimelineView.hidden = NO;
        }else {
            NSArray *visibleRows = [self.tableView indexPathsForVisibleRows];
            if (visibleRows.count > 0) {
                int topRow = [visibleRows[0] row];
                if (topRow != _lastRowIndex) {
                    _lastRowIndex = topRow;
                    _pinnedCircleView.tintColor = _circleColors[topRow % 4];
                }
                
                if (topRow != _pinnedRowIndex) {
                    int pinnedRow = -1;
                    if ((topRow > _pinnedRowIndex && _timelineDict[@(topRow)])) {
                        pinnedRow = topRow;
                    }else if (topRow < _pinnedRowIndex) {
                        pinnedRow = [_timelineArray[[_timelineArray indexOfObject:@(_pinnedRowIndex)] - 1] intValue];
                    }
                    
                    if (pinnedRow != -1) {
                        _pinnedLabel.text = _timelineDict[@(pinnedRow)];
                        _pinnedRowIndex = pinnedRow;
                    }
                }
            }
        }
    }else {
        _pinnedRowIndex = -1;
        _pinnedTimelineView.hidden = YES;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _goodsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 89;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (!_headerSectionView) {
        _headerSectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenFrame.size.width, 40)];
        _headerSectionView.backgroundColor = [UIColor clearColor];
        
        _pinnedTimelineView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 58, 40)];
        _pinnedTimelineView.backgroundColor = [UIColor whiteColor];
        _pinnedCircleView = [[UIImageView alloc] initWithFrame:CGRectMake(22, 5, 13, 13)];
        _pinnedCircleView.backgroundColor = [UIColor clearColor];
        _pinnedCircleView.image = [[UIImage imageNamed:@"icon_circle"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_pinnedTimelineView addSubview:_pinnedCircleView];
        
        _pinnedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 58, 21)];
        _pinnedLabel.backgroundColor = [UIColor whiteColor];
        _pinnedLabel.textAlignment = NSTextAlignmentCenter;
        _pinnedLabel.font = [UIFont systemFontOfSize:13];
        _pinnedLabel.textColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1];
        [_pinnedTimelineView addSubview:_pinnedLabel];
        
        _pinnedTimelineView.hidden = YES;
        
        [_headerSectionView addSubview:_pinnedTimelineView];
    }
    
    return _headerSectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GoodsChooseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsChooseCell" forIndexPath:indexPath];
    Goods *goods = _goodsList[indexPath.row];
    [cell.headerView setImageWithURL:goods.pic effect:SDWebImageEffectFade];
    cell.titleLabel.text = goods.title;
    cell.priceLabel.text = [NSString stringWithFormat:@"￥%@",goods.payPrice];
    [cell.circleView makeRoundWithBorderColor:_circleColors[indexPath.row % 4] borderWidth:4];
    
    NSString *timelineStr = _timelineDict[@(indexPath.row)];
    if (timelineStr) {
        cell._timeLabel.text = timelineStr;
        [cell updateTimelineViewWithTimeShowing:YES];
    }else {
        [cell updateTimelineViewWithTimeShowing:NO];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedGoods = _goodsList[indexPath.row];
    if (self.isPublishEntrance) {
        [PublishParamSet sharedInstance].goods = _selectedGoods;
        PublishViewController *publishController = [self.storyboard instantiateViewControllerWithIdentifier:@"PublishViewController"];
        [self.navigationController pushViewController:publishController animated:YES];
    }else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(goodsChooseController:didSelectGoods:)]) {
            [self.delegate goodsChooseController:self didSelectGoods:_selectedGoods];
        }
    }
}


@end
