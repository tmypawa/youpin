//
//  PostContentItem.m
//  youpinwei
//
//  Created by tmy on 15/1/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PostContentItem.h"

@implementation PostContentItem

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInteger:self.type forKey:@"type"];
    [encoder encodeObject:self.text forKey:@"text"];
    [encoder encodeObject:self.image forKey:@"image"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.type = [decoder decodeIntegerForKey:@"type"];
        self.text = [decoder decodeObjectForKey:@"text"];
        self.image = [decoder decodeObjectForKey:@"image"];
    }
    return self;
}

@end
