//
//  Goods.h
//  youpinwei
//
//  Created by tmy on 14-8-23.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    GoodsSourceNone = -1,
    GoodsSourceOthers = 0,
    GoodsSourceTaobao = 1,
    GoodsSourceJD,
    GoodsSourceDangdang,
    GoodsSourceAmazonCN,
    GoodsSourceYiHaoDian
} GoodsSource;

@interface Goods : NSObject<NSCoding>

@property (strong, nonatomic) NSString *billTime;
@property (strong, nonatomic) NSString *buyerNick;
@property (strong, nonatomic) NSString *buylink;
@property (strong, nonatomic) NSString *categoryId;
@property (nonatomic) NSTimeInterval createTime;
@property (strong, nonatomic) NSString *goodsDescription;
@property (strong, nonatomic) NSString *extraId;
@property (nonatomic) long long id;
@property (strong, nonatomic) NSString *itemId;
@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSString *payPrice;
@property (strong, nonatomic) NSString *pic;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *purchaseId;
@property (strong, nonatomic) NSString *quantity;
@property (nonatomic) int seazon;
@property (strong, nonatomic) NSString *sellerId;
@property (strong, nonatomic) NSString *sellerNick;
@property (strong, nonatomic) NSString *shopName;
@property (nonatomic) GoodsSource source;
@property (nonatomic) int state;
@property (strong, nonatomic) NSString *title;
@property (nonatomic) NSTimeInterval updateTime;
@property (strong, nonatomic) NSString *userId;

@end
