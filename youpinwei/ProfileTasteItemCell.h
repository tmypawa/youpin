//
//  ProfileTasteItemCell.h
//  youpinwei
//
//  Created by tmy on 14/12/8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTasteItemCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *selectionStateView;
@property (strong, nonatomic) IBOutlet UIImageView *experienceIconView;

@end
