//
//  PostContentItem.h
//  youpinwei
//
//  Created by tmy on 15/1/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSInteger {
    PostContentTypeText = 0,
    PostContentTypeImage
} PostContentType;

@interface PostContentItem : NSObject

@property (nonatomic) PostContentType type;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *image;

@end
