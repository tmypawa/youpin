//
//  ProvidePurchaseInfoCell.h
//  youpinwei
//
//  Created by tmy on 15/3/26.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostContentCell.h"

@interface ProvidePurchaseInfoCell : PostContentCell
@property (strong, nonatomic) IBOutlet UILabel *tipLabel;

@end
