//
//  api.h
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#ifndef youpinwei_api_h
#define youpinwei_api_h

//API
#define HOST @"http://pinv.me/ypw/"

#define INIT_CONFIG @"config/getInitConfigs.json"

#define REGISTER @"user/register.json"
#define UPDATE_DEVICE_INFO @"user/update.json"
#define PROFILE @"user/setUserInfo.json"
#define UPLOAD_ADDRESS_BOOK @"contact/upload.json"

#define CATEGORY_CHOOSE_LIST @"product/getIntersetTags.json"
#define SELECT_CATEGORY @"product/saveIntersetTags.json"

#define TIMELINE_LIST @"product/list.json"
#define TIMELINE_EXPIRIENCE @"product/listExperience.json"
#define DETAIL @"product/detail.json"
#define PRAISE_LIST @"product/getPinUserListByPid.json"

#define PUBLISH_COMMENT @"product/comment.json"
#define PUBLISH_TASTE @"product/share.json"
#define GOODS_MULTIPLE_PIC @"product/getImagesByItemId.json"
#define DELETE_TASTE @"product/delete.json"
#define PRAISE @"product/youpin.json"
#define ADD_WISH_LIST @"product/addWishList.json"
#define UPLOAD_PURCHASE @"purchase/upload.json"
#define PURCAHSE_HISTORY @"purchase/list.json"
#define PUSH_BIND @"user/bindPushId.json"
#define DEFAULT_TAGS @"product/getDefaultTags.json"
#define VERIFY_CODE @"user/validate.json"
#define REPORT @"user/report.json"
#define NOTIFICATION_LIST @"notify/getNotifyList.json"
#define DELETE_COMMENT @"product/deleteComment.json"

#define QUERY_GOODS_INFO @"product/queryBuyinfo.json"

#define INVITATION_CODE @"user/invitedCode.json"

#define PUBLIC_TIMELINE_BANNER @"act/getTimelineBanner.json"

#define INIT_CONFIG @"config/getInitConfigs.json"

#define FRIEND_TIMELINE_BADGE_COUNT @"notify/getTimelineNewCount.json"

#define ADD_FRIEND @"friend/addFriend.json"
#define SEARCH_FREIDN_BY_NICKNAME @"user/getUserByNick.json"

#define FRIEND_LIST @"friend/list.json"
#define ACCEPT_FRIEND @"friend/accept.json"
#define DELETE_FRIEND @"friend/deleteFriend.json"
#define CONTACT_FRIEND @"friend/contactFriends.json"
#define FRIEND_INVITATION @"friend/getFriendsByInviteCode.json"
#define RECOMMAND_FRIENDS @"user/getRecommendFriends.json"

#define PUBLISH_POST_V2 @"product/advancedShare.json"

#define HOT_TAG_LIST @"act/getHotTagList.json"
#define RELATED_TAG_LIST @"product/getAssociativeTags.json"

#define SUBSCRIBE_TAG @"product/subscribeTag.json"

#define MORE_RELATED_TAG_LIST @"product/getMoreRelevantTags.json"

#define USER_LOGOUT @"user/exit.json"

#define DISCOVERY_LIST @"product/discover.json"

#define QUERY_FUZZY_TAG @"product/queryFuzzyTag.json"

#define QUERY_POST @"product/queryFuzzyProductTag.json"

//Error Code
#define ERROR_NOT_EXIST 2000003

#endif
