//
//  TaobaoURLProtocol.h
//  youpinwei
//
//  Created by tmy on 14-8-13.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TaobaoURLProtocolDelegate;

@interface TaobaoURLProtocol : NSURLProtocol<NSURLConnectionDataDelegate, NSURLConnectionDelegate>

+ (void)setDelegate:(id<TaobaoURLProtocolDelegate>)delegate;

@end

@protocol TaobaoURLProtocolDelegate <NSObject>

- (void)TaobaoURLProtocol:(TaobaoURLProtocol *)urlLProtocol didFetchOrderList:(NSString *)orderListData;

@end
