//
//  PhotoCropViewController.m
//  youpinwei
//
//  Created by tmy on 14/10/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PhotoCropViewController.h"
#import "PhotoEditViewController.h"
#import "RSKImageScrollView.h"
#import "UIImage+Rotating.h"

@implementation PhotoCropViewController
{
    NSInteger _rotateAngle;
    IBOutlet UIButton *_frameBtn;
    IBOutlet UIButton *_rotateBtn;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
     
    }
    
    return self;
}


- (void)awakeFromNib {
    self.dataSource = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController.navigationBar setHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    self.cropMode = RSKImageCropModeCustom;
    self.applyMaskToCroppedImage = YES;
    self.moveAndScaleLabel.text = @"";
    [self.chooseButton setTitleColor:NavigationBarTintColor forState:UIControlStateNormal];
    [self.chooseButton setTitle:@"选取" forState:UIControlStateNormal];
    [self.cancelButton setImage:[UIImage imageNamed:@"back_normal"] forState:UIControlStateNormal];
    [self.cancelButton setTitle:@"" forState:UIControlStateNormal];
    self.imageScrollView.backgroundColor = [UIColor whiteColor];
    
    [self.view bringSubviewToFront:_rotateBtn];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeFrameColor:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.imageScrollView addGestureRecognizer:tapRecognizer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    if (![self.navigationController.viewControllers containsObject:self]) {
//        
//        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
//        [self.navigationController.navigationBar setHidden:NO];
//    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [super handleDoubleTap:gestureRecognizer];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(doChangeFrameColor) object:nil];
}


- (IBAction)changeFrameColor:(id)sender {
    [self performSelector:@selector(doChangeFrameColor) withObject:nil afterDelay:0.2f];
}

- (void)doChangeFrameColor {
    self.imageScrollView.backgroundColor = self.imageScrollView.backgroundColor == [UIColor whiteColor] ? [UIColor blackColor] : [UIColor whiteColor];
}

- (IBAction)rotate:(id)sender {
    _rotateAngle += 90;
    [UIView animateWithDuration:0.3f animations:^{
        self.imageScrollView.transform = CGAffineTransformMakeRotation(_rotateAngle * M_PI / 180);
    }];
}

- (void)cropImage
{
    if ([self.delegate respondsToSelector:@selector(imageCropViewController:willCropImage:)]) {
        [self.delegate imageCropViewController:self willCropImage:self.originalImage];
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            UIImage *croppedImage = [self croppedImage:self.originalImage cropRect:[self cropRect]];
                    croppedImage = [croppedImage rotateInDegrees:-_rotateAngle];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegate respondsToSelector:@selector(imageCropViewController:didCropImage:)]) {
                    [self.delegate imageCropViewController:self didCropImage:croppedImage];
                }
            });
        }
    });
}
- (CGRect)imageCropViewControllerCustomMaskRect:(RSKImageCropViewController *)controller {
    return CGRectMake(0, (screenHeight - screenWidth)/2, screenWidth, screenWidth);
}

- (UIBezierPath *)imageCropViewControllerCustomMaskPath:(RSKImageCropViewController *)controller {
    return [UIBezierPath bezierPathWithRect:CGRectMake(0, (screenHeight - screenWidth)/2, screenWidth, screenWidth)];
}

@end
