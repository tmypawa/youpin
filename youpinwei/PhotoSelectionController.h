//
//  PhotoSelectionController.h
//  youpinwei
//
//  Created by tmy on 14-10-13.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Goods.h"
#import "CameraViewController.h"
#import "PhotoCropViewController.h"

@protocol PhotoSelectionControllerDelegate;

@interface PhotoSelectionController : UICollectionViewController<CameraViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate>
@property (nonatomic, strong) Goods *goods;
@property (nonatomic, assign) id<PhotoSelectionControllerDelegate> photoDelegate;
@property (nonatomic) BOOL fromPublishView;

@end


@protocol PhotoSelectionControllerDelegate <NSObject>

- (void)photoSelectionController:(PhotoSelectionController *)controller didSelectAlbumPhoto:(UIImage *)photoImage;
- (void)photoSelectionController:(PhotoSelectionController *)controller didSelectGoodsPhoto:(NSURL *)photoURL;

@end