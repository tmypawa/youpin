//
//  PublishImageItem.m
//  youpinwei
//
//  Created by tmy on 15/1/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishImageItem.h"

@implementation PublishImageItem

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInteger:self.source forKey:@"source"];
    [encoder encodeObject:self.imagePath forKey:@"imagePath"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.source = [decoder decodeIntegerForKey:@"source"];
        self.imagePath = [decoder decodeObjectForKey:@"imagePath"];
    }
    return self;
}

- (NSString *)imageName {
    if (self.imagePath) {
        if ([self.imagePath hasPrefix:@"http"]) {
            return self.imagePath;
        }else {
          return [self.imagePath lastPathComponent];
        }
    }else{
        return @"";
    }
}

@end
