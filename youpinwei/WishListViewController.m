//
//  WishListViewController.m
//  youpinwei
//
//  Created by tmy on 14/12/23.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "WishListViewController.h"
#import "DetailViewController.h"
#import "ProfileTasteItemCell.h"

@interface WishListViewController ()

@end

@implementation WishListViewController
{
    UICollectionReusableView *_headerView;
    IBOutlet UIView *_actionView;
    IBOutlet UICollectionView *_wishCollectionView;
    UIBarButtonItem  *_doneItem;
    IBOutlet UIBarButtonItem *_editItem;
    NSMutableArray      *_tasteItems;
    NSMutableIndexSet   *_selectedSet;
    NSInteger           _currentOperationItemIndex;
    BOOL            _isEdit;
    IBOutlet UILabel *_noResultLabel;
}
 
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _tasteItems = [[NSMutableArray alloc] init];
        _selectedSet = [[NSMutableIndexSet alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tasteItemDidRemove:) name:YouPinNotificationDidRemoveTasteItem object:nil];
    
    _doneItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleBordered target:self action:@selector(editWishList:)];
    
    if (![self.userId isEqualToString:JTGlobalCache.currentUser.userId]) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    _headerView.hidden = YES;
    _actionView.hidden = YES;
    _noResultLabel.hidden = YES;
    SetFrameHeight(_wishCollectionView, contentHeight);
    
    if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:self.userId]) {
        _noResultLabel.hidden = YES;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self refreshData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
  
    if (_currentOperationItemIndex != -1) {
        _currentOperationItemIndex = -1;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editWishList:(id)sender {
    _isEdit = !_isEdit;
    if (_isEdit) {
        self.navigationItem.rightBarButtonItem = _doneItem;
        _actionView.hidden = NO;
        SetFrameY(_actionView, contentHeight);
        [UIView animateWithDuration:0.4f delay:0 usingSpringWithDamping:0.6f initialSpringVelocity:0 options:0 animations:^{
            SetFrameY(_actionView, contentHeight - _actionView.frame.size.height + 8);
        } completion:^(BOOL finished) {
            SetFrameHeight(_wishCollectionView, _wishCollectionView.frame.size.height - _actionView.frame.size.height + 8);
        }];
    }else {
        self.navigationItem.rightBarButtonItem = _editItem;
        SetFrameHeight(_wishCollectionView, contentHeight);
        [UIView animateWithDuration:0.4f delay:0 usingSpringWithDamping:0.6f initialSpringVelocity:0 options:0 animations:^{
            SetFrameY(_actionView, contentHeight);
        } completion:^(BOOL finished) {
            _actionView.hidden = YES;
        }];
        
        [_selectedSet removeAllIndexes];
    }
    
    [_wishCollectionView reloadData];
}

- (IBAction)delete:(id)sender {
    if (_selectedSet.count == 0) {
        return;
    }
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    [_selectedSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
        TasteItem *item = _tasteItems[idx];
        [[YouPinAPI sharedAPI] addWishListWithTaste:item.productId add:NO callback:^(int wishCount, NSArray *currentWishListUsers, NSError *error) {
            
        }];
    }];
    
    [_tasteItems removeObjectsAtIndexes:_selectedSet];
    [_wishCollectionView deleteItemsAtIndexPaths:indexPaths];
    ((UILabel *)[_headerView viewWithTag:1000]).text = [NSString stringWithFormat:@"共%ld个宝贝", _tasteItems.count];
    [_selectedSet removeAllIndexes];
    
    if (_tasteItems.count == 0) {
        [self editWishList:nil];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        _noResultLabel.hidden = NO;
        _headerView.hidden = YES;
    }
}

- (void)tasteItemDidRemove:(NSNotification *)notification {
    if (_currentOperationItemIndex != -1) {
        [_tasteItems removeObjectAtIndex:_currentOperationItemIndex];
        [_wishCollectionView reloadData];
        _currentOperationItemIndex = -1;
    }
}

- (NSTimeInterval)startTimestamp {
    NSTimeInterval startTimestamp = 0;
    if (_tasteItems && _tasteItems.count > 0) {
        startTimestamp = [_tasteItems[0] updateTime];
    }
    
    return startTimestamp;
}

- (NSTimeInterval)endTimestamp {
    NSTimeInterval endTimestamp = 0;
    if (_tasteItems.count > 0) {
        endTimestamp = [[_tasteItems lastObject] updateTime];
    }
    return endTimestamp;
}

- (void)refreshData {
    [[YouPinAPI sharedAPI] fetchWishListWithStartTimestamp:[self startTimestamp] endTimestamp:0 userId:self.userId Sender:self callback:^(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            [_tasteItems removeAllObjects];
            [_tasteItems addObjectsFromArray:items];
            [_wishCollectionView reloadData];
            ((UILabel *)[_headerView viewWithTag:1000]).text = [NSString stringWithFormat:@"共%ld个宝贝", totalCount];
            if (_tasteItems.count == 0) {
                self.navigationItem.rightBarButtonItem.enabled = NO;
                _headerView.hidden = YES;
                if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:self.userId]) {
                    _noResultLabel.hidden = NO;
                }else {
                    _noResultLabel.hidden = YES;
                }
            }else {
                _noResultLabel.hidden = YES;
                _headerView.hidden = NO;
            }
            
            UIImageView *wishListIconView = (UIImageView *)[_headerView viewWithTag:2000];
            wishListIconView.image = [UIImage imageNamed: totalCount > 0? @"icon_wishlist_normal": @"icon_heartlist_nomal"];
        }else {
            
        }
    }];
}

- (void)loadMoreData {
    [[YouPinAPI sharedAPI] fetchPersonalTimelineWithStartTimestamp:0 endTimestamp:[self endTimestamp] Sender:self callback:^(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            [_tasteItems addObjectsFromArray:items];
            [_wishCollectionView reloadData];
        }else {
            
        }
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _tasteItems.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (!_headerView) {
            _headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WishListHeaderView" forIndexPath:indexPath];
        }
        
        return _headerView;
    }else {
        return nil;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ProfileTasteItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileTasteItemCell" forIndexPath:indexPath];
    TasteItem *item = _tasteItems[indexPath.row];
    if (item.realShotPics.count > 0) {
        [cell.thumbnailView setImageWithURL:item.realShotPics[0] placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectNone];
    }

    cell.titleLabel.text = item.tasteDescription;
    cell.experienceIconView.hidden = !item.experience;
    
    if (_isEdit) {
        cell.selectionStateView.hidden = NO;
        
        BOOL isSelected = [_selectedSet containsIndex:indexPath.row];
        cell.selectionStateView.image = [UIImage imageNamed:isSelected ? @"btn_wishlist_push" : @"btn_wishlist_normal"];
    }else {
        cell.selectionStateView.hidden = YES;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (!_isEdit) {
        _currentOperationItemIndex = indexPath.row;
        TasteItem *item = _tasteItems[indexPath.row];
        DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
        detailController.tasteId = item.productId;
        [self.navigationController pushViewController:detailController animated:YES];
    }else {
        ProfileTasteItemCell *cell = (ProfileTasteItemCell *)[collectionView cellForItemAtIndexPath:indexPath];
        if ([_selectedSet containsIndex:indexPath.row]) {
            [_selectedSet removeIndex:indexPath.row];
            cell.selectionStateView.image = [UIImage imageNamed:@"btn_wishlist_normal"];
        }else {
            [_selectedSet addIndex:indexPath.row];
            cell.selectionStateView.image = [UIImage imageNamed:@"btn_wishlist_push"];
        }
    }
}

@end
