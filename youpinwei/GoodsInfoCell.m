//
//  GoodsInfoCell.m
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "GoodsInfoCell.h"

@implementation GoodsInfoCell
{
    IBOutlet UIImageView *_bgView;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _bgView.image = [[UIImage imageNamed:@"bg_purchasing info"] stretchableImageWithLeftCapWidth:0 topCapHeight:10];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
 

+ (CGFloat)height {
    return 115;
}

@end
