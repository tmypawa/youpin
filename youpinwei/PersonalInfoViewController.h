//
//  MyInfoViewController.h
//  youpinwei
//
//  Created by tmy on 14/11/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileSettingViewController.h"
#import "PersonalInfoHeaderView.h"
#import "DetailViewController.h"
#import "WishListViewController.h"
#import "UIScrollView+UzysAnimatedGifPullToRefresh.h"
#import "UITableViewCollection+LoadMore.h"

@interface PersonalInfoViewController : ShaibaCollectionViewController<ProfileSettingViewControllerDelegate,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray      *_tasteItems;
    PersonalInfoHeaderView *_headerView;
    UICollectionReusableView *_noMoreFooterView;
    UICollectionReusableView *_loadMoreFooterView;
    IBOutlet UIView *_placeHolderView;
    IBOutlet UIImageView *_wishListThumbnailView;
    IBOutlet UIView *_defaultBgView;
    NSInteger       _currentOperationItemIndex;
    User            *_user;
    NSDictionary    *_levelDict;
    BOOL            _hasNext;
    BOOL            _hasLoadRefreshView;
    NSInteger       _totalTasteItemCount;
    NSInteger       _totalTasteExpressCount;
    CGFloat         _originalHeaderViewHeight;
}

- (void)initViews;
- (void)handleForDoubleTapTab;
- (void)loadPullRereshAnimation;
- (void)addLoadMoreView;
- (void)refreshData;
- (void)loadMoreData;
- (NSTimeInterval)startTimestamp;
- (NSTimeInterval)endTimestamp;
- (void)showUserInfoHeader;
- (void)initForHeaderView;
- (void)showRegisterAlert;

@end
