//
//  GoodsPhotoSelectionController.h
//  youpinwei
//
//  Created by tmy on 14/12/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsPhotoSelectionController : UICollectionViewController

@property (nonatomic, strong) NSString *itemId;

@end
