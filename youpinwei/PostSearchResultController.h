//
//  PostSearchResultController.h
//  youpinwei
//
//  Created by tmy on 15/3/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"
#import "MainViewController.h"

@interface PostSearchResultController : MainViewController

@property (nonatomic, strong) NSString *keyword;

@end
