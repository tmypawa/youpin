//
//  RecommendTagViewCell.h
//  youpinwei
//
//  Created by tmy on 15/3/27.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendTagViewCell : UITableViewCell<UICollectionViewDataSource, UICollectionViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, copy) void(^itemSelectHandler)(NSInteger tagItemIndex);
@property (nonatomic, copy) void(^showMoreHandler)(void);

- (void)setItems:(NSArray *)items;

@end
