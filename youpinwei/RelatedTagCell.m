//
//  RelatedTagCell.m
//  youpinwei
//
//  Created by tmy on 15/3/24.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "RelatedTagCell.h"

@implementation RelatedTagCell

- (void)awakeFromNib {
    self.imageView.layer.cornerRadius = 2;
}

@end
