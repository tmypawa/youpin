//
//  DiscoveryViewController.h
//  youpinwei
//
//  Created by tmy on 15/3/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"

@protocol DiscoverySearchControllerDelegate;

@interface DiscoveryViewController : ShaibaBaseViewController<UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, DiscoverySearchControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end
