//
//  CategoryChooseViewController.m
//  youpinwei
//
//  Created by tmy on 15/3/21.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "CategoryChooseViewController.h"
#import "CategoryChooseCell.h"
#import "UITableViewCollection+LoadMore.h"
#import "ColorUtils.h"

#define MIN_SELECT_COUNT 5

@interface CategoryChooseViewController ()

@end

@implementation CategoryChooseViewController
{
    IBOutlet UICollectionView *_categoryCollectionView;
    IBOutlet UIView *_actionView;
    IBOutlet UIButton *_actionBtn;
    UICollectionReusableView       *_noMoreFooterView;
    UICollectionReusableView       *_loadMoreFooterView;
    NSMutableArray       *_categoryList;
    NSMutableArray       *_selectedList;
    BOOL                 _hasNext;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _categoryList = [[NSMutableArray alloc] init];
        _selectedList = [[NSMutableArray alloc] init];
        _hasNext = YES;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = YES;
    
    [_categoryCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter  withReuseIdentifier:@"LoadMoreFooterView"];
    
    [self updateActionButton];
    
    [self addLoadMoreView];
    
    [self loadCategoryList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addLoadMoreView {
    UIImageView *loadingMoreView = [[UIImageView alloc] initWithImage:[UIImage animatedImageNamed:@"R" duration:1.6]];
    loadingMoreView.frame = CGRectMake(0, 0, screenWidth, 56);
    loadingMoreView.center = CGPointMake(screenWidth/2, loadingMoreView.center.y);
    loadingMoreView.contentMode = UIViewContentModeCenter;
    loadingMoreView.backgroundColor = MAIN_CELL_BG_COLOR;
    [loadingMoreView stopAnimating];
    loadingMoreView.hidden = YES;
    
    [_categoryCollectionView setupLoadMoreWithCustomView:loadingMoreView];
    [_categoryCollectionView addTarget:self forLoadMoreAction:@selector(loadNext:)];
}

- (NSTimeInterval)startTime {
    if (!_categoryList || _categoryList.count == 0) {
        return 0;
    }else {
        Tag *tag = _categoryList[0];
        return tag.updateTime;
    }
}

- (NSTimeInterval)endTime {
    if (!_categoryList || _categoryList.count == 0) {
        return 0;
    }else {
        Tag *tag = [_categoryList lastObject];
        return tag.updateTime;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(!self.view.superview)
        return;
    
    if (!_categoryCollectionView.isLoadingMore && _hasNext) {
        float maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
        float offsetY = scrollView.contentOffset.y;
        if (offsetY >= maxOffsetY - 170) {
            [_categoryCollectionView startLoadMore];
        }
    }
}

- (void)loadCategoryList {
    [self showHUDViewWithTitle:@""];
    [[YouPinAPI sharedAPI] fetchCategoryChooseListWithStartTime:[self startTime] endTime:[self endTime] callback:^(NSArray *tags, NSError *error) {
        [self hideHUDView];
        if (!error) {
            [_categoryList addObjectsFromArray:tags];
            [_categoryCollectionView reloadData];
        }
    }];
}

- (void)loadNext:(UIImageView *)loadingView {
    if (!_hasNext) {
        return;
    }
    
    loadingView.hidden = NO;
    [loadingView startAnimating];
    
    [self loadNextData];
}

- (void)loadNextData {
    [[YouPinAPI sharedAPI] fetchCategoryChooseListWithStartTime:[self startTime] endTime:[self endTime] callback:^(NSArray *tags, NSError *error) {
        if (!error) {
            if (tags.count == 0) {
                _hasNext = NO;
            }
            
            [_categoryList addObjectsFromArray:tags];
            [_categoryCollectionView endLoadMoreWithHandler:^(UIView *customView) {
                [((UIImageView *)customView) stopAnimating];
                //                customView.hidden = YES;
            }];
            
            [_categoryCollectionView reloadData];
        }else {
            
        }
    }];
}

- (void)updateActionButton {
    if (_selectedList.count < MIN_SELECT_COUNT) {
        [_actionBtn setBackgroundImage:[[UIImage imageNamed:@"btn-grey"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
        [_actionBtn setTitle:[NSString stringWithFormat:@"请至少再选%d个", MIN_SELECT_COUNT - _selectedList.count] forState:UIControlStateNormal];
        [_actionBtn setTitleColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1] forState:UIControlStateNormal];
    }else {
        [_actionBtn setBackgroundImage:[[UIImage imageNamed:@"btn-red"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
        [_actionBtn setTitle:@"开始使用" forState:UIControlStateNormal];
        [_actionBtn setTitleColor:[UIColor colorWithRed:1 green:0.44 blue:0.38 alpha:1] forState:UIControlStateNormal];
    }
    
    _categoryCollectionView.contentInset = UIEdgeInsetsMake(0, 0, _selectedList.count > 0 ? _actionView.frame.size.height : 0, 0);
    _actionView.hidden = _selectedList.count == 0;
}

- (IBAction)next:(id)sender {
    if (_selectedList.count < MIN_SELECT_COUNT)  return;
    
    NSArray *selectedTagNames = [_selectedList valueForKeyPath:@"name"];
    [self showHUDViewWithTitle:@""];
    [[YouPinAPI sharedAPI] selectCategories:selectedTagNames callback:^(NSError *error) {
        [self hideHUDView];
        if (!error) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(categoryChooseViewControllerDidFinish:)]) {
                [self.delegate categoryChooseViewControllerDidFinish:self];
            }
            
            if (!self.forSingleUse) {
                [self finishLoginFlow];
            }else {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }];
}

- (void)finishLoginFlow {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationUserDidLogin object:nil userInfo:nil];
    }];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
   if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        if (!_hasNext) {
            if (!_noMoreFooterView) {
                _noMoreFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"NoMoreFooterView" forIndexPath:indexPath];
            }
            
            return _noMoreFooterView;
        }else {
            if (!_loadMoreFooterView) {
                _loadMoreFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"LoadMoreFooterView" forIndexPath:indexPath];
            }
            
            [_loadMoreFooterView addSubview:_categoryCollectionView.loadMoreView];
            return _loadMoreFooterView;
        }
    }
    else {
        return nil;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _categoryList ? _categoryList.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryChooseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryChooseCell" forIndexPath:indexPath];
    Tag *tag = _categoryList[indexPath.row];
    cell.thumbnailView.backgroundColor = [UIColor colorWithString:tag.color];
    [cell.thumbnailView setImageWithURL:tag.thumbnailsUrl effect:SDWebImageEffectFade];
    cell.titleLabel.text = tag.name;
    
    BOOL hasChoose = [_selectedList containsObject:tag];
    [cell.chooseButton setImage:[UIImage imageNamed: hasChoose ? @"icon-choose" : @"icon-no choose"] forState:UIControlStateNormal];
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryChooseCell *cell = (CategoryChooseCell *)[collectionView cellForItemAtIndexPath:indexPath];
    Tag *tag = _categoryList[indexPath.row];
    BOOL hasChoose = ![_selectedList containsObject:tag];
    
    [cell.chooseButton setImage:[UIImage imageNamed: hasChoose ? @"icon-choose" : @"icon-no choose"] forState:UIControlStateNormal];
    
    if (!hasChoose) {
        [_selectedList removeObject:tag];
    }else {
        [_selectedList addObject:tag];
    }
    
    [self updateActionButton];
}

@end
