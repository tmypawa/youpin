//
//  PublishCommentViewController.h
//  youpinwei
//
//  Created by tmy on 15/3/18.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"
#import "Comment.h"

@protocol PublishCommentViewControllerDelegate;

@interface PublishCommentViewController : ShaibaBaseViewController<UIAlertViewDelegate>

@property (nonatomic, weak) id<PublishCommentViewControllerDelegate> delegate;
@property (nonatomic, strong) TasteItem *tasteItem;
@property (nonatomic, strong) Comment *replyComment;

@end


@protocol PublishCommentViewControllerDelegate <NSObject>

- (void)publishCommentViewController:(PublishCommentViewController *)controller didPublishComment:(Comment *)comment;
- (void)publishCommentViewControllerFailedToPublish:(PublishCommentViewController *)controller;

@end