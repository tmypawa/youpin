//
//  MenuViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "MenuViewController.h"
#import "RegisterViewController.h"
#import "UIKit+RoundImage.h"

@interface MenuViewController ()

@end

@implementation MenuViewController
{
    int     _currentSelectedIndex;
    IBOutlet UIButton *_profileBtn;
    IBOutlet UIButton *_nicknameBtn;
    IBOutlet UIScrollView *_scollView;
}

@synthesize drawer;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _currentSelectedIndex = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_profileBtn makeRound];
    _scollView.scrollsToTop = NO;
    [self showUserProfile];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:YouPinNotificationUserDidLogin object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showUserProfile {
    if (JTGlobalCache.currentUser && JTGlobalCache.currentUser.avatar) {
        [_profileBtn setImageWithURL:JTGlobalCache.currentUser.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectFade];
        [_nicknameBtn setTitle:JTGlobalCache.currentUser.nickname forState:UIControlStateNormal];
    }
}

- (void)registerRemoteNotification {
    
}

- (void)userDidLogin:(NSNotification *)notification {
    [self showUserProfile];
    [self registerRemoteNotification];
}

- (void)showRegisterAlert {
    AlertWithActionsAndTag(1000, @"登录晒吧", @"继续操作之前先登录一下吧~~", @"看看再说", @"马上GO~~");
}

- (IBAction)setProfile:(id)sender {
    if (JTGlobalCache.currentUser) {
        ProfileSettingViewController *profileController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSettingViewController"];
        profileController.delegate = self;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:profileController];
        [self presentViewController:navController animated:YES completion:nil];
    }else {
        [self showRegisterAlert];
    }
}

- (IBAction)setNickname:(id)sender {
    if (JTGlobalCache.currentUser) {
        ProfileSettingViewController *profileController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSettingViewController"];
        profileController.delegate = self;
        profileController.isEditNickname = YES;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:profileController];
        [self presentViewController:navController animated:YES completion:nil];

    }else {
        [self showRegisterAlert];
    }
}


- (IBAction)showMessages:(id)sender {
    if (self.centerViewController.topViewController != self.notificationController) {
        [self.centerViewController pushViewController:self.notificationController animated:NO];
    }
    
    [self.drawer close];
}

- (IBAction)showSettings:(id)sender {
    if (self.centerViewController.topViewController != self.settingViewController) {
        [self.centerViewController pushViewController:self.settingViewController animated:NO];
    }
    
    [self.drawer close];
}

- (IBAction)about:(id)sender {
}

- (void)profileSettingViewController:(ProfileSettingViewController *)controller didSetProfileInfo:(NSDictionary *)profileInfo {
    [_profileBtn setImageWithURL:JTGlobalCache.currentUser.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectFade];
    [_nicknameBtn setTitle:JTGlobalCache.currentUser.nickname forState:UIControlStateNormal];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1000) {
        if (buttonIndex == 1) {
            RegisterViewController *registerController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerController];
            navController.navigationBar.translucent = NO;
            [self presentViewController:navController animated:YES completion:nil];
        }
    }
}

@end
