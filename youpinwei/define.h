//
//  define.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#ifndef youpinwei_define_h
#define youpinwei_define_h

#define isIPhone4 ([UIScreen mainScreen].bounds.size.height == 480)
#define screenFrame [UIScreen mainScreen].bounds
#define screenHeight [UIScreen mainScreen].bounds.size.height
#define screenWidth [UIScreen mainScreen].bounds.size.width
#define contentHeight ([UIScreen mainScreen].bounds.size.height-64)
#define iOSVersion [[UIDevice currentDevice].systemVersion floatValue]

//引导Key值
#define TUTORIAL_TIMELINE_TAG @"点击标签\n查看更多相似内容"
#define TUTORIAL_PUBLISH_TEXT_TAG @"还没有添加宝贝描述噢~"
#define TUTORIAL_PUBLISH_IMAGE_TAG @"点击可添加多张宝贝图片"
#define TUTORIAL_PUBLISH_SORT @"长按并拖动可进行排版"
#define TUTORIAL_PUBLISH_INSTRUCTION @"TUTORIAL_PUBLISH_INSTRUCTION"


typedef enum : NSUInteger {
    InitModeNormal = 0,
    InitModeCrash = 1,
    InitModeReview = 2
} InitMode;

typedef enum : NSUInteger {
    DiscoveryBannerTypeActivity = 1,
    DiscoveryBannerTypeTag,
    DiscoveryBannerTypeHTML,
} DiscoveryBannerType;
 

///Notifications

//External URL Open
#define YouPinNotificationAppDidOpenURL @"YouPinNotificationAppDidOpenURL"

//User Login/Logout
#define YouPinNotificationUserDidLogin @"YouPinNotificationUserDidLogin"
#define YouPinNotificationUserDidLogout @"YouPinNotificationUserDidLogout"
#define YouPinNotificationUserDidVisitAnonymously @"YouPinNotificationUserDidVisitAnonymously"
#define YouPinNotificationUserDidBindAccount @"YouPinNotificationUserDidBindAccount"

//Taste Item Publish
#define YouPinNotificationNewTasteItemDidSend @"YouPinNotificationNewTasteItemDidSend"
#define YouPinNotificationNewTasteItemDidPublish @"YouPinNotificationNewTasteItemDidPublish"
#define YouPinNotificationNewTasteItemFailedToPublish @"YouPinNotificationNewTasteItemFailedToPublish"
#define YouPinNotificationTasteItemDidFinishEditing @"YouPinNotificationTasteItemDidFinishEditing"
#define YouPinNotificationTasteItemDidEditedSuccess @"YouPinNotificationTasteItemDidEditedSuccess"
#define YouPinNotificationTasteItemFailedToEdit @"YouPinNotificationTasteItemFailedToEdit"

//Photo Edit
#define YouPinNotificationPhotoDidFinishEdit @"YouPinNotificationPhotoDidFinishEdit"

#define YouPinNotificationDidReceiveRemoteNotification @"YouPinNotificationDidReceiveRemoteNotification"
#define YouPinNotificationDidRemoveTasteItem @"YouPinNotificationDidRemoveTasteItem"


//Notification Keys
#define YouPinNotificationPublishTasteItemKey @"YouPinNotificationPublishTasteItemKey"
#define YouPinNotificationEditPhotoKey @"YouPinNotificationEditPhotoKey"


//Error Code
#define NO_SUBSCRIPTION_CATEGORY 2000024


#define Alert(title,text,cancel) [[[UIAlertView alloc] initWithTitle:title message:text delegate:nil cancelButtonTitle:cancel otherButtonTitles: nil] show];


#define AlertWithTag(tag,title,text,cancel)\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:text delegate:nil cancelButtonTitle:cancel otherButtonTitles: nil];\
[alert setTag:tag];\
[alert show];\


#define AlertWithActions(title,text,cancel,...) [[[UIAlertView alloc] initWithTitle:title message:text delegate:self cancelButtonTitle:cancel otherButtonTitles:__VA_ARGS__, nil] show];


#define AlertWithActionsAndTag(tag,title,text,cancel,...)\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:text delegate:self cancelButtonTitle:cancel otherButtonTitles:__VA_ARGS__, nil];\
[alert setTag:tag];\
[alert show];\

#define SetFrameOrigin(view, xValue, yValue)\
{\
CGRect originFrame = view.frame;\
originFrame.origin.x = xValue;\
originFrame.origin.y = yValue;\
view.frame = originFrame;\
}\

#define SetFrameX(view, xValue)\
{\
CGRect originFrame = view.frame;\
originFrame.origin.x = xValue;\
view.frame = originFrame;\
}\

#define SetFrameY(view, yValue)\
{\
CGRect originFrame = view.frame;\
originFrame.origin.y = yValue;\
view.frame = originFrame;\
}\

#define SetFrameSize(view, widthValue, heightValue)\
{\
CGRect originFrame = view.frame;\
originFrame.size.width = widthValue;\
originFrame.size.height = heightValue;\
view.frame = originFrame;\
}\

#define SetFrameWidth(view, widthValue)\
{\
CGRect originFrame = view.frame;\
originFrame.size.width = widthValue;\
view.frame =  originFrame;\
}\

#define SetFrameHeight(view, heightValue)\
{\
CGRect originFrame = view.frame;\
originFrame.size.height = heightValue;\
view.frame = originFrame;\
}\

#define CalculateLabelHeight(labelView)\
[labelView.text boundingRectWithSize:CGSizeMake(labelView.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : labelView.font} context:nil].size.height

#define AdjustLabelHeight(labelView)\
{\
CGFloat labelHeight = [labelView.text boundingRectWithSize:CGSizeMake(labelView.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : labelView.font} context:nil].size.height;\
CGRect labelFrame = labelView.frame;\
labelFrame.size.height = labelHeight;\
labelView.frame = labelFrame;\
}\

#define SafeValue(value) ((value)!= nil && (NSNull *)(value)!= [NSNull null]?(value):@"")


#define IsSafeValue(value) ((value)!= nil && (NSNull *)(value)!= [NSNull null])

#define NotEmptyValue(value) ((value) != nil && (NSNull *)(value) != [NSNull null] && [value isKindOfClass:[NSString class]] && ![value isEqualToString:@""])

#define IsEmptyValue(value) ((value)== nil || (NSNull *)(value) == [NSNull null] || ![value isKindOfClass:[NSString class]] || [value isEqualToString:@""])

#endif
