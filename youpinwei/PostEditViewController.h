//
//  PostEditViewController.h
//  youpinwei
//
//  Created by tmy on 15/1/20.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishViewController.h"
@class TasteItem;

@interface PostEditViewController : PublishViewController

@property (nonatomic, strong) TasteItem *editedTasteItem;

@end
