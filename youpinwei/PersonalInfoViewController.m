//
//  MyInfoViewController.m
//  youpinwei
//
//  Created by tmy on 14/11/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PersonalInfoViewController.h"
#import "ShaibaViewController.h"
#import "UIKit+RoundImage.h"
#import "LoginViewController.h"
#import "NotificationViewController.h"
#import "PersonalInfoHeaderView.h"
#import "ProfileTasteItemCell.h"
#import "SettingsViewController.h"
#import "FriendListController.h"

@interface PersonalInfoViewController ()

@end

@implementation PersonalInfoViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _tasteItems = [[NSMutableArray alloc] init];
        _currentOperationItemIndex = -1;
        _hasNext = YES;
        if (JTGlobalCache.isLogin) {
            _user = JTGlobalCache.currentUser;
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tasteItemDidPublish:) name:YouPinNotificationNewTasteItemDidPublish object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tasteItemDidRemove:) name:YouPinNotificationDidRemoveTasteItem object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:YouPinNotificationUserDidLogin object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogout:) name:YouPinNotificationUserDidLogout object:nil];
    }
    
    return self;
}

- (void)dealloc {
    [self.collectionView removePullToRefreshActionHandler];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.backgroundView = nil;
    self.collectionView.alwaysBounceVertical = YES;
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter  withReuseIdentifier:@"LoadMoreFooterView"];
    
    [self initViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [((ShaibaViewController *)self.rdv_tabBarController) setNavigationTitle:@"我的"];
    
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
    
    [self updateFriendBadge];
    
    if (_currentOperationItemIndex != -1) {
        _currentOperationItemIndex = -1;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)updateFriendBadge {
    if (_headerView) {
        if (JTGlobalCache.friendNotificationBadge > 0) {
            _headerView.friendBadgeView.text = [NSString stringWithFormat:@"%ld", JTGlobalCache.friendNotificationBadge];
            SetFrameOrigin(_headerView.friendBadgeView, 117, 12);
        }else {
            _headerView.friendBadgeView.text = @"0";
        }
    }
}

- (void)initViews {
    if (JTGlobalCache.isLogin) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self refreshData];
    }else {
        [self showLoginHeader];
    }
}

- (void)handleForDoubleTapTab {
    CGFloat baseOffset = -self.collectionView.contentInset.top;
    if (self.collectionView.contentOffset.y > baseOffset) {
        [self.collectionView setContentOffset:CGPointMake(0, baseOffset) animated:YES];
    }else if(self.collectionView.pullToRefreshView.state != UZYSGIFPullToRefreshStateLoading){
        [self.collectionView triggerPullToRefresh];
    }
}

- (void)addLoadMoreView {
    UIImageView *loadingMoreView = [[UIImageView alloc] initWithImage:[UIImage animatedImageNamed:@"R" duration:1.6]];
    loadingMoreView.frame = CGRectMake(0, 0, screenWidth, 56);
    loadingMoreView.center = CGPointMake(screenWidth/2, loadingMoreView.center.y);
    loadingMoreView.contentMode = UIViewContentModeCenter;
    loadingMoreView.backgroundColor = MAIN_CELL_BG_COLOR;
    [loadingMoreView stopAnimating];
    loadingMoreView.hidden = YES;
    
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView.backgroundColor = LIST_BG_COLOR;
    self.collectionView.backgroundView = nil;
    [self.collectionView setupLoadMoreWithCustomView:loadingMoreView];
    [self.collectionView addTarget:self forLoadMoreAction:@selector(loadNext:)];
}

- (void)loadPullRereshAnimation {
    NSMutableArray *pullAnimations = [[NSMutableArray alloc] init];
    NSMutableArray *loadingAnimations = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 23; ++i) {
        [pullAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
    for (int i = 0; i < 45; ++i) {
        [loadingAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
    __weak PersonalInfoViewController *_self = self;
    [self.collectionView addPullToRefreshActionHandler:^{
        if (JTGlobalCache.isLogin) {
            [_self refreshData];
        }
    } ProgressImages:pullAnimations LoadingImages:loadingAnimations ProgressScrollThreshold:20 LoadingImagesFrameRate:12];
    self.collectionView.showAlphaTransition = NO;
    self.collectionView.showVariableSize = NO;
    self.collectionView.showPullToRefresh = YES;
    self.collectionView.showTitle = NO;
 
    [self.collectionView addTopInsetInPortrait:0 TopInsetInLandscape:0];
}

- (void)tasteItemDidPublish:(NSNotification *)notification {
    [self.collectionView triggerPullToRefresh];
}

- (void)tasteItemDidRemove:(NSNotification *)notification {
    if (_currentOperationItemIndex != -1) {
        TasteItem *tasteItem = _tasteItems[_currentOperationItemIndex];
        _totalTasteItemCount--;
        
        if (tasteItem.experience) {
            _totalTasteExpressCount--;
        }
        
        [_tasteItems removeObjectAtIndex:_currentOperationItemIndex];
        
        [self updateTotalCountView];
        [self.collectionView reloadData];
        _currentOperationItemIndex = -1;
    }
}

- (void)showUserProfile {
    _headerView.hidden = NO;
    if (_user) {
        _headerView.loginBtn.hidden = YES;
        _headerView.infoContainerView.hidden = NO;
        [self showUserInfoHeader];
    }else {
        [self showLoginHeader];
    }
}

- (void)showUserInfoHeader {
    _headerView.hidden = NO;
    [_headerView.avatarView setImageWithURL:_user.avatar placeholderImage:[UIImage imageNamed:@"icon_mine_default_Head"] effect:SDWebImageEffectNone];
    [_headerView.nicknameBtn setTitle:_user.nickname forState:UIControlStateNormal];
    [_headerView.nicknameBtn setTitleColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1] forState:UIControlStateNormal];
    [_headerView.nicknameBtn setBackgroundImage:nil forState:UIControlStateNormal];
    NSMutableAttributedString *levelTitleStr = nil;
    if(_user.degree > 0) {
        _headerView.levelView.hidden = NO;
        levelTitleStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"体验家%ld级, %ld 米阳光", _user.degree, _user.points]];
        NSRange levelRange = [levelTitleStr.string rangeOfString:[NSString stringWithFormat:@"%ld", _user.degree]];
        [levelTitleStr addAttributes:@{NSForegroundColorAttributeName: LevelColor(_user.degree)} range:levelRange];
    }else {
        if (_user.points > 0) {
            _headerView.levelView.hidden = NO;
            levelTitleStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld 米阳光", _user.points]];
        }else {
            _headerView.levelView.hidden = YES;
            levelTitleStr = [[NSMutableAttributedString alloc] initWithString:@"你还没有阳光"];
        }
    }
        _headerView.pointsLabel.attributedText = levelTitleStr;
    if (_user.gender != 0) {
        _headerView.sexIconView.image = [UIImage imageNamed:_user.gender == 1?@"icon_boy":@"icon_girl"];
    }
    
    [_headerView updateLayout];
    
    if (_levelDict) {
        NSInteger currentLevel = _user.degree;
        NSInteger nextLevel = currentLevel + 1;
        NSInteger currentPointStart = currentLevel > 0 ? [_levelDict[[@(currentLevel - 1) stringValue]] integerValue] : 0;
        NSInteger nextPointStart = [_levelDict[[@(nextLevel - 1) stringValue]] integerValue];
        
        CGFloat currentProgress = (CGFloat)(_user.points - currentPointStart)/(nextPointStart - currentPointStart);
        [_headerView showLevelProgress:currentProgress currentLevel:_user.degree nextLevel:nextLevel];
    }

}

- (void)showLoginHeader {
    _headerView.totalCountView.hidden = YES;
    _headerView.loginBtn.hidden = NO;
    _headerView.infoContainerView.hidden = YES;
    [_headerView.avatarView setImage:[UIImage imageNamed:@"icon_mine_default_Head"] forState:UIControlStateNormal];
    [_headerView.nicknameBtn setTitle:@"立即加入晒吧" forState:UIControlStateNormal];
    [_headerView.nicknameBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_headerView.nicknameBtn setBackgroundImage:[[UIImage imageNamed:@"btn_profile_ok_normal"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    _headerView.totalCountLabel.text = @"";

}

- (void)userDidLogin:(NSNotification *)notification {
    _user = JTGlobalCache.currentUser;
    if (self.view) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self refreshData];
    }
}

- (void)userDidLogout:(NSNotification *)notification {
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    
    [_tasteItems removeAllObjects];
    [self.collectionView reloadData];
    [self initViews];
}


- (void)showRegisterAlert {
    AlertWithActionsAndTag(1000, @"登录晒吧", @"继续操作之前先登录一下吧~~", @"看看再说", @"马上GO~~");
}

 
- (void)profileSettingViewController:(ProfileSettingViewController *)controller didSetProfileInfo:(User *)profileInfo {
    [self showUserProfile];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1000) {
        if (buttonIndex == 1) {
            [self showRegisterView];
        }
    }
}

- (IBAction)showRegisterView {
    [self presentLoginController];
}

- (IBAction)showProfile:(id)sender {
    if (JTGlobalCache.isLogin) {
        ProfileSettingViewController *profileController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSettingViewController"];
        profileController.delegate = self;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:profileController];
        [self presentViewController:navController animated:YES completion:nil];
    }else {
        [self showRegisterAlert];
    }
}

- (IBAction)setNickname:(id)sender {
    if (JTGlobalCache.currentUser) {
        ProfileSettingViewController *profileController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileSettingViewController"];
        profileController.delegate = self;
        profileController.isEditNickname = YES;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:profileController];
        [self presentViewController:navController animated:YES completion:nil];
    }else {
        [self showRegisterView];
    }
}

- (IBAction)about:(id)sender {
}

- (IBAction)showFriendList:(id)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else {
        FriendListController *friendListController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendListController"];
        [self.navigationController pushViewController:friendListController animated:YES];
    }
}


- (IBAction)showWishList:(id)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else {
        WishListViewController *wishListController = [self.storyboard instantiateViewControllerWithIdentifier:@"WishListViewController"];
        wishListController.userId = JTGlobalCache.currentUser.userId;
        [self.navigationController pushViewController:wishListController animated:YES];
    }
}

- (IBAction)showNotificationView:(id)sender {
    if (!JTGlobalCache.isLogin) {
        [self showRegisterAlert];
    }else {
        NotificationViewController *notificationController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
        [self.navigationController pushViewController:notificationController animated:YES];
    }
}

- (IBAction)showSettingsView:(id)sender {
    SettingsViewController *settingController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self.navigationController pushViewController:settingController animated:YES];
}

- (NSTimeInterval)startTimestamp {
    NSTimeInterval startTimestamp = 0;
    if (_tasteItems && _tasteItems.count > 0) {
        startTimestamp = [_tasteItems[0] updateTime];
    }
    
    return startTimestamp;
}

- (NSTimeInterval)endTimestamp {
    NSTimeInterval endTimestamp = 0;
    if (_tasteItems.count > 0) {
        endTimestamp = [[_tasteItems lastObject] updateTime];
    }
    return endTimestamp;
}

- (void)updateTotalCountView {
     _headerView.totalCountLabel.text = [NSString stringWithFormat:@"共%ld个宝贝, %ld个精品", _totalTasteItemCount, _totalTasteExpressCount];
    _headerView.totalCountView.hidden = _totalTasteItemCount == 0;
    if (_totalTasteItemCount > 0) {
        self.collectionView.backgroundColor = [UIColor whiteColor];
        [_defaultBgView removeFromSuperview];
    }else {
        self.collectionView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
        SetFrameY(_defaultBgView, 0);
        [self.collectionView insertSubview:_defaultBgView atIndex:0];
    }
}

- (void)refreshData {
    _hasNext = YES;
    __weak PersonalInfoViewController *__self = self;
    [[YouPinAPI sharedAPI] fetchPersonalTimelineWithStartTimestamp:[self startTimestamp] endTimestamp:0 Sender:self callback:^(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error) {
        [__self reloadItems:items hasNext:hasNext totalCount:totalCount expressCount:expressCount userInfo:userInfo levelDict:levelDict error:error];
    }];
}

- (void)reloadItems:(NSArray *)items hasNext:(BOOL) hasNext totalCount:(NSInteger)totalCount expressCount:(NSInteger) expressCount userInfo: (User *) userInfo levelDict:  (NSDictionary *) levelDict error:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    [self.collectionView stopRefreshAnimation];
    if (!error) {
        _levelDict = levelDict;
        [_tasteItems removeAllObjects];
        [_tasteItems addObjectsFromArray:items];
        
        _totalTasteItemCount = totalCount;
        _totalTasteExpressCount = expressCount;
        
        if (userInfo) {
            JTGlobalCache.currentUser = userInfo;
            _user = JTGlobalCache.currentUser;
            [JTGlobalCache synchronize];
            
            if (!_hasLoadRefreshView) {
                [self loadPullRereshAnimation];
                [self addLoadMoreView];
                _hasLoadRefreshView = YES;
            }
            
            [self showUserProfile];
        }
        
        [self.collectionView reloadData];
        [self updateTotalCountView];
        [self updateFriendBadge];
    }else {
        
    }
}

- (void)loadNext:(UIImageView *)loadingView {
    if (!_hasNext) {
        return;
    }
    
    loadingView.hidden = NO;
    [loadingView startAnimating];
    
    [self loadMoreData];
}

- (void)loadMoreData {
    [[YouPinAPI sharedAPI] fetchPersonalTimelineWithStartTimestamp:0 endTimestamp:[self endTimestamp] Sender:self callback:^(NSArray *items, BOOL hasNext, NSInteger totalCount, NSInteger expressCount, User * userInfo, NSDictionary * levelDict,NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            if (items.count == 0) {
                _hasNext = NO;
            }
            
            [_tasteItems addObjectsFromArray:items];
            [self.collectionView endLoadMoreWithHandler:^(UIView *customView) {
                [((UIImageView *)customView) stopAnimating];
//                customView.hidden = YES;
            }];
            
            [self.collectionView reloadData];
        }else {
            
        }
    }];
}

- (void)initForHeaderView {
    _headerView.userInteractionEnabled = YES;
    _headerView.hidden = YES;
    _headerView.totalCountView.hidden = YES;
    _originalHeaderViewHeight = _headerView.frame.size.height;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(!self.view.superview)
        return;
    
    if (!self.collectionView.isLoadingMore && _hasNext) {
        float maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
        float offsetY = scrollView.contentOffset.y;
        if (offsetY >= maxOffsetY - 170) {
            [self.collectionView startLoadMore];
        }
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(139, 185);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 14;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 14;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGFloat height =  _tasteItems.count > 0 ? 53 : 0;
    return CGSizeMake(collectionView.frame.size.width, height);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _tasteItems.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (!_headerView) {
            _headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"PersonalInfoHeaderView" forIndexPath:indexPath];
            [self initForHeaderView];
            [self updateFriendBadge];
            [self showUserProfile];
        }
        
        return _headerView;
    }else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        if (!_hasNext) {
            if (!_noMoreFooterView) {
                _noMoreFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"NoMoreFooterView" forIndexPath:indexPath];
            }
            
            return _noMoreFooterView;
        }else {
            if (!_loadMoreFooterView) {
                _loadMoreFooterView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"LoadMoreFooterView" forIndexPath:indexPath];
            }
            
            [_loadMoreFooterView addSubview:self.collectionView.loadMoreView];
            return _loadMoreFooterView;
        }
    }
    else {
        return nil;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ProfileTasteItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileTasteItemCell" forIndexPath:indexPath];
    TasteItem *item = _tasteItems[indexPath.row];
    
    cell.thumbnailView.image = nil;
    
    if (item.realShotPics.count > 0) {
        [cell.thumbnailView setImageWithURL:item.realShotPics[0] placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectFade];
    }
    
    cell.titleLabel.text = item.tasteDescription;
    cell.experienceIconView.hidden = !item.experience;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    _currentOperationItemIndex = indexPath.row;
    TasteItem *item = _tasteItems[indexPath.row];
    DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
    detailController.tasteId = item.productId;
    [self.navigationController pushViewController:detailController animated:YES];
}


@end
