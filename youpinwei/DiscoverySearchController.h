//
//  DiscoverySearchController.h
//  youpinwei
//
//  Created by tmy on 15/3/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"

@protocol DiscoverySearchControllerDelegate;

@interface DiscoverySearchController : ShaibaBaseViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) id<DiscoverySearchControllerDelegate> delegate;

@end


@protocol DiscoverySearchControllerDelegate <NSObject>

- (void)discoverySearchControllerDidCancel:(DiscoverySearchController *)controller;

@end