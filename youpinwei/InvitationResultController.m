//
//  InvitationResultController.m
//  youpinwei
//
//  Created by tmy on 15/3/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "InvitationResultController.h"
#import "FriendInfoViewController.h"
#import "UIKit+RoundImage.h"

#define MAX_DISPLAY_NAME_LENGHT 5

@interface InvitationResultController ()

@end

@implementation InvitationResultController
{
    IBOutlet UITableView *_usersTableView;
    
    IBOutlet UIImageView *_headerImageView;
    IBOutlet UILabel *_headerTitleLabel;
    IBOutlet UIView *_headerView;
    IBOutlet UIButton *_addBtn;
    IBOutlet UIView *_centerResultView;
    IBOutlet UILabel *_centerLabel;
    IBOutlet UIImageView *_centerImage;
    IBOutlet UIButton *_centerViewBtn;
    IBOutlet UIButton *_retryBtn;
    IBOutlet UIView *_actionView;
    
    User    *_inviter;
    NSArray *_userList;
    BOOL _success;
    NSMutableSet    *_selectedUserSet;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _selectedUserSet = [[NSMutableSet alloc] init];
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    _actionView.hidden = YES;
    
    [_addBtn setBackgroundImage:[[UIImage imageNamed:@"btn_red_normal"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    [_addBtn setBackgroundImage:[[UIImage imageNamed:@"btn_red_highlight"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateHighlighted];
    
    _usersTableView.dataSource = self;
    _usersTableView.delegate = self;
    
    [_centerResultView removeFromSuperview];
    [_usersTableView removeFromSuperview];
    
    if (NotEmptyValue(self.invitationCode)) {
        [self acceptInvitation];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)retry:(id)sender {
    [self acceptInvitation];
}

- (IBAction)viewFriend:(id)sender {
    FriendInfoViewController *friendController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
    friendController.userId = _inviter.userId;
    [self.navigationController pushViewController:friendController animated:YES];
}

- (IBAction)addFriends:(id)sender {
    if (!_userList || _userList.count == 0) {
        [self acceptInvitation];
    }else {
        NSString *multipleUserID =[[_selectedUserSet allObjects] componentsJoinedByString:@","];
        NSString *introduce = [NSString stringWithFormat:@"我是%@", JTGlobalCache.currentUser.nickname];
        
        [self showHUDViewWithTitle:@"正在添加..."];
        [[YouPinAPI sharedAPI] sendFriendInvitationToUser:multipleUserID introduce:introduce callback:^(NSError *error) {
            [self hideHUDView];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}

- (void)acceptInvitation {
    [self showHUDViewWithTitle:@""];
    [[YouPinAPI sharedAPI] acceptFriendInvitationWithCode:self.invitationCode callback:^(BOOL success, User *inviter, NSArray *knownUserList, NSError *error) {
        [self hideHUDView];
        if (!error) {
            _success = success;
            _userList = knownUserList;
            [_selectedUserSet addObjectsFromArray:[_userList valueForKeyPath:@"userId"]];
            
            _retryBtn.hidden = _success;
            NSString *personName = @"";
            NSString *title = @"";
            if (inviter) {
                _inviter = inviter;
                NSString *name = inviter.nickname;
                if(name.length > MAX_DISPLAY_NAME_LENGHT) {
                    name = [NSString stringWithFormat:@"%@...", [name substringToIndex:MAX_DISPLAY_NAME_LENGHT - 1]];
                }
                
                personName = name;
                
                if (_success) {
                    title = [NSString stringWithFormat:@"你和 %@ 已经成为好友", personName];
                }else {
                    title = [NSString stringWithFormat:@"添加 %@ 失败", personName];
                }
            }else {
                title = @"邀请者不存在";
            }
            
            
            
            if (knownUserList && knownUserList.count > 0) {
                [self.view addSubview:_usersTableView];
                [_centerResultView removeFromSuperview];
                
                _headerTitleLabel.text = title;
                _headerImageView.image = [UIImage imageNamed:_success ? @"icon_adddone_L" : @"icon_addfail_L"];
                [_addBtn setTitle:@"一键添加" forState:UIControlStateNormal];
                
                [_usersTableView reloadData];
            }else {
                [self.view addSubview:_centerResultView];
                [_usersTableView removeFromSuperview];
                
                _centerLabel.text = title;
                _centerImage.image = [UIImage imageNamed:_success ? @"icon_adddone_L" : @"icon_addfail_L"];
                [_addBtn setTitle:@"重试" forState:UIControlStateNormal];
            }
            
            _actionView.hidden = (_success && _userList.count == 0);
            [self.view bringSubviewToFront:_actionView];
        }else {
            
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _userList ? _userList.count : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 52;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
    if (cell.tag == 0) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_selected"]];
        UIImageView *avatarView = (UIImageView *)[cell.contentView viewWithTag:2000];
        [avatarView makeRound];
    }
    
    User *user = _userList[indexPath.row];
    
    cell.tag = indexPath.row + 1;
    
    UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:1000];
    UIImageView *avatarView = (UIImageView *)[cell.contentView viewWithTag:2000];
    [avatarView setImageWithURL:user.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    nameLabel.text = user.nickname;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    User *user = _userList[indexPath.row];
    
    if ([_selectedUserSet containsObject:user.userId]) {
        [_selectedUserSet removeObject:user.userId];
        cell.accessoryView.hidden = YES;
    }else {
        [_selectedUserSet addObject:user.userId];
        cell.accessoryView.hidden = NO;
    }
}

@end
