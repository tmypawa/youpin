//
//  RegisterViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "RegisterViewController.h"
#import "VerifyViewController.h"
#import "UMSocialSnsPlatformManager.h"

@implementation RegisterViewController
{
    __weak IBOutlet UITextField *_phoneField;
    __weak IBOutlet UIButton *_registerBtn;
    __weak IBOutlet UIView *_floatView;
    __weak IBOutlet UIButton *_maskBtn;
    
    IBOutlet UIButton *_weixinLoginBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"输入手机号";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    self.navigationController.view.tintColor = GlobalTintColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: GlobalTintColor};
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageFromColor:[UIColor clearColor]]];
    
    [_phoneField setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    _phoneField.tintColor = [UIColor whiteColor];
    
    _maskBtn.hidden = YES;
    
    [self adjustForiPhone4];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameDidChange:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)adjustForiPhone4 {
    if (isIPhone4) {
        SetFrameY(_floatView, _floatView.frame.origin.y - 30);
    }
}

- (IBAction)cancelInput:(id)sender {
    [_phoneField resignFirstResponder];
    _maskBtn.hidden = YES;
}

- (void)keyboardFrameDidChange:(NSNotification *)notification {
    float duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    CGRect floatFrame = _floatView.frame;
    floatFrame.origin.y -= 140;
    _floatView.frame = floatFrame;
    [UIView commitAnimations];
}

- (void)keyboardWillDisappear:(NSNotification *)notification {
    float duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    CGRect floatFrame = _floatView.frame;
    floatFrame.origin.y += 140;
    _floatView.frame = floatFrame;
    [UIView commitAnimations];
}

- (IBAction)notRegister:(id)sender {
    JTGlobalCache.isReturnUser = YES;
    [JTGlobalCache synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doRegister:(id)sender {
    NSString *phone = [_phoneField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if(phone.length == 0) {
        Alert(NSLocalizedString(@"请输入您的手机号码", nil), @"", NSLocalizedString(@"确定", nil));
        return;
    }
    
    NSString *phoneRegex = @"^[0-9]{11}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    if([predicate evaluateWithObject:phone]) {
        [_phoneField resignFirstResponder];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[YouPinAPI sharedAPI] registerWithPhone:phone loginType:JTGlobalCache.currentLoginType sender:self callback:^(NSError *error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if(!error){
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                VerifyViewController *verifyController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyViewController"];
                verifyController.phone = phone;
                [self.navigationController pushViewController:verifyController animated:YES];
            }else {
                Alert(NSLocalizedString(@"注册失败", nil), error.localizedDescription, NSLocalizedString(@"确定", nil));
            }
        }];
    }else {
        Alert(NSLocalizedString(@"请输入有效的手机号码", nil), @"", NSLocalizedString(@"确定", nil));
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _maskBtn.hidden = NO;
}

@end
