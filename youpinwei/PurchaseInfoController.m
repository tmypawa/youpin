//
//  PurchaseInfoController.m
//  youpinwei
//
//  Created by tmy on 15/3/25.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PurchaseInfoController.h"
#import "HYActivityView.h"
#import "ZFModalTransitionAnimator.h"
#import "UIKit+RoundImage.h"
#import "GoodsChooseController.h"


@implementation PurchaseInfoController
{
    IBOutlet UIImageView *_infoBgView;
    IBOutlet UITextField *_nameTextField;
    
    IBOutlet UITextField *_priceTextField;
    IBOutlet UIView *_defaultSourceView;
    IBOutlet UIView *_selectedSourceView;
    IBOutlet UILabel *_sourceTitleLabel;
    IBOutlet UIImageView *_sourceIconView;
    IBOutlet UICollectionView *_usersCollectionView;
    IBOutlet UIView *_usersView;
    IBOutlet UIButton *_cancelBtn;
    IBOutlet UIButton *_maskBtn;
    
    HYActivityView      *_sourceActivityView;
    GoodsSource     _selectedSource;
    GoodsSource     _tempSource;
    NSString        *_goodsSourceInfoValue;
    
    ZFModalTransitionAnimator *_animator;
    
    NSArray *sourceIcons;
    NSArray *_smallIcons;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    _infoBgView.image = [[UIImage imageNamed:@"bg_purchasing info"] stretchableImageWithLeftCapWidth:0 topCapHeight:10];
    _nameTextField.delegate = self;
    _priceTextField.delegate = self;
    _selectedSourceView.hidden = YES;
    [_maskBtn removeFromSuperview];
    
    sourceIcons = @[@"icon_taobao_b", @"icon_jingdong_b", @"icon_dangdang_b", @"icon_amazon_b", @"icon_1haodian_b", @"icon_others_b"];
    _smallIcons = @[@"icon_taobao_s", @"icon_jingdong_s", @"icon_dangdang_s", @"icon_amazon_s", @"icon_1haodian_s", @"icon_others_s"];
    
    _selectedSource = GoodsSourceNone;
    _tempSource = GoodsSourceNone;
    
    if (self.goods) {
        _nameTextField.text = self.goods.title;
        _priceTextField.text = self.goods.price;
        _selectedSource = self.goods.source;
        _goodsSourceInfoValue = self.goods.buylink;
        [self showSelectedSourceView];
    }
    
    if (!self.userList || self.userList.count == 0) {
        _usersView.hidden = YES;
    }
    
    if ([self.navigationController.viewControllers firstObject] != self) {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

- (IBAction)cancelInput:(id)sender {
    [self.view endEditing:YES];
    [_maskBtn removeFromSuperview];
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirm:(id)sender {
    NSString *nameValue = [_nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    NSString *priceValue = [_priceTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (IsEmptyValue(nameValue)) {
        Alert(@"请填写宝贝的品牌&名称", @"",@"确定");
    }else if(IsEmptyValue(priceValue)) {
        Alert(@"请填写价格", @"", @"确定");
    }else if (_selectedSource == GoodsSourceNone) {
        Alert(@"请选择宝贝的购买方式", @"", @"确定");
    }else {
        Goods *goods = [[Goods alloc] init];
        goods.title = nameValue;
        goods.price = priceValue;
        goods.source = _selectedSource;
        goods.buylink = _goodsSourceInfoValue;
        if (self.delegate && [self.delegate respondsToSelector:@selector(purchaseInfoController:didFillGoods:)]) {
            [self.delegate purchaseInfoController:self didFillGoods:goods];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)showSourceMenu:(id)sender {
    [[self sourceActivityView] show];
}

- (IBAction)cancelSource:(id)sender {
    _sourceTitleLabel.text = @"";
    _selectedSourceView.hidden = YES;
    _defaultSourceView.hidden = NO;
    
    SetFrameHeight(_selectedSourceView, 21);
    SetFrameHeight(_infoBgView, CGRectGetMaxY(_selectedSourceView.frame) + 20);
    SetFrameY(_usersView, CGRectGetMaxY(_infoBgView.frame) + 5);
}

- (HYActivityView *)sourceActivityView {
    if (!_sourceActivityView) {
        _sourceActivityView = [[HYActivityView alloc] initWithTitle:@"选择购买方式" referView:self.navigationController.view];
        _sourceActivityView.titleLabel.textColor = [UIColor colorWithRed:0.73 green:0.73 blue:0.73 alpha:1];
        _sourceActivityView.titleLabel.font = [UIFont systemFontOfSize:14];
        
        _sourceActivityView.cancelButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_sourceActivityView.cancelButton setTitleColor:[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1] forState:UIControlStateNormal];
        [_sourceActivityView.cancelButton setBackgroundImage:[[UIImage imageNamed:@"bg_inputView"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
        
        __weak PurchaseInfoController *__self = self;
        for (int i = 1; i<=5; i++) {
            ButtonView *btnView = [[ButtonView alloc] initWithText:@"" image:[UIImage imageNamed:sourceIcons[i-1]] handler:^(ButtonView *buttonView) {
                [__self selectGoodsSource:buttonView.tag];
            }];
            btnView.tag = i;
            [_sourceActivityView addButtonView:btnView];
        }
        
        ButtonView *btnView = [[ButtonView alloc] initWithText:@"" image:[UIImage imageNamed:[sourceIcons lastObject]] handler:^(ButtonView *buttonView) {
            [__self selectGoodsSource:buttonView.tag];
        }];
        btnView.tag = 0;
        [_sourceActivityView addButtonView:btnView];
    }
    
    return _sourceActivityView;
}

- (void)selectGoodsSource:(GoodsSource)goodsSource {
    _tempSource = goodsSource;
    
    if (_tempSource == GoodsSourceTaobao) {
        GoodsChooseController *goodsChooseController = [self.storyboard instantiateViewControllerWithIdentifier:@"GoodsChooseController"];
        goodsChooseController.delegate = self;
        [self.navigationController pushViewController:goodsChooseController animated:YES];
    }else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self presentSourceInfoView];
        });
    }
}

- (void)goodsChooseController:(GoodsChooseController *)controller didSelectGoods:(Goods *)goods {
    _nameTextField.text = goods.title;
    _priceTextField.text = goods.price;
    _sourceTitleLabel.text = goods.buylink;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)presentSourceInfoView {
    PurchaseSourceInfoController *sourceInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PurchaseSourceInfoController"];
    sourceInfoController.modalPresentationStyle = UIModalPresentationCustom;
    sourceInfoController.delegate = self;
    
    if (_tempSource != GoodsSourceOthers) {
        sourceInfoController.iconImage = [UIImage imageNamed:sourceIcons[_tempSource - 1]];
    }else {
        sourceInfoController.iconImage = [UIImage imageNamed:[sourceIcons lastObject]];
    }
    
    _animator = [[ZFModalTransitionAnimator alloc] initWithModalViewController:sourceInfoController];
    _animator.dragable = YES;
    _animator.bounces = NO;
    _animator.behindViewAlpha = 0.5f;
    _animator.behindViewScale = 0.9f;
    _animator.transitionDuration = 0.7f;
    _animator.direction = ZFModalTransitonDirectionBottom;
    sourceInfoController.transitioningDelegate = _animator;
    
    [self presentViewController:sourceInfoController animated:YES completion:nil];
}

- (void)showSelectedSourceView {
    _defaultSourceView.hidden = YES;
    _selectedSourceView.hidden = NO;
    
    if (_selectedSource != GoodsSourceOthers) {
        _sourceIconView.image = [UIImage imageNamed:sourceIcons[_selectedSource - 1]];
    }else {
        _sourceIconView.image = [UIImage imageNamed:[sourceIcons lastObject]];
    }
    
    if ([_goodsSourceInfoValue hasPrefix:@"http"]) {
        _sourceTitleLabel.text = @"宝贝购买链接";
        _sourceTitleLabel.textColor = [UIColor colorWithRed:0.31 green:0.7 blue:0.96 alpha:1];
        SetFrameHeight(_selectedSourceView, 21);
    }else {
        if (_selectedSource != GoodsSourceOthers) {
            NSString *sourceTitle = @"";
            switch (_selectedSource) {
                case GoodsSourceTaobao:
                    sourceTitle = @"淘宝";
                    break;
                case GoodsSourceJD:
                    sourceTitle = @"京东";
                    break;
                case GoodsSourceDangdang:
                    sourceTitle = @"当当";
                    break;
                case GoodsSourceAmazonCN:
                    sourceTitle = @"亚马逊";
                    break;
                case GoodsSourceYiHaoDian:
                    sourceTitle = @"1号店";
                    break;
                default:
                    break;
            }
            _sourceTitleLabel.text = [NSString stringWithFormat:@"%@搜索 “%@”",sourceTitle, _goodsSourceInfoValue];
        }else {
            _sourceTitleLabel.text = _goodsSourceInfoValue;
        }
        
        _sourceTitleLabel.textColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1];
        AdjustLabelHeight(_sourceTitleLabel);
        SetFrameHeight(_selectedSourceView, _sourceTitleLabel.frame.size.height);
    }
    
    CGPoint iconPoint = _sourceIconView.center;
    iconPoint.y = _sourceTitleLabel.center.y;
    _sourceIconView.center = iconPoint;
    
    CGPoint btnPoint = _cancelBtn.center;
    btnPoint.y = _sourceTitleLabel.center.y;
    _cancelBtn.center = btnPoint;
    
    SetFrameHeight(_infoBgView, CGRectGetMaxY(_selectedSourceView.frame) + 20);
    SetFrameY(_usersView, CGRectGetMaxY(_infoBgView.frame) + 5);
}

- (void)PurchaseSourceInfoController:(PurchaseSourceInfoController *)controller didFillInfo:(NSString *)sourceInfoValue {
    _selectedSource = _tempSource;
    _tempSource = GoodsSourceNone;
    _goodsSourceInfoValue = sourceInfoValue;
    [self showSelectedSourceView];
}

- (void)PurchaseSourceInfoControllerDidCancel:(PurchaseSourceInfoController *)controller {
    _tempSource = GoodsSourceNone;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.view addSubview:_maskBtn];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == _priceTextField) {
        NSString *value = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if ([value containsString:@"."]) {
            NSString *fractionValue = [value componentsSeparatedByString:@"."][1];
            return fractionValue.length <= 2;
        }else {
            return YES;
        }
    }else {
        return YES;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.userList ? self.userList.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserCell" forIndexPath:indexPath];
    UIImageView *avatarView = (UIImageView *)[cell.contentView viewWithTag:1000];
    if (cell.tag == 0) {
        [avatarView makeRound];
    }
    
    cell.tag = indexPath.row + 1;
    
    User *user = self.userList[indexPath.row];
    [avatarView setImageWithURL:user.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    return cell;
}

@end
