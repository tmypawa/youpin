//
//  SettingsViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-29.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "SettingsViewController.h"
#import "LoginViewController.h"
#import "UMFeedback.h"
#import "PBWebViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

@synthesize drawer;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = SETTING_BG_COLOR;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)logout {
    [[YouPin sharedInstance] logout];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return JTGlobalCache.isLogin ? 2 : 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
            {
                [self.navigationController presentViewController:[UMFeedback feedbackModalViewController] animated:YES completion:nil];
            }
                break;
            case 1:
            {
                NSURL *appURL = [NSURL URLWithString:@"https://itunes.apple.com/zh/app/shai-ba/id923822302?l=zh&ls=1&mt=8"];
                [[UIApplication sharedApplication] openURL:appURL];
            }
                break;
            case 2:
            {
                PBWebViewController *webController = [[PBWebViewController alloc] init];
                webController.URL = [NSURL URLWithString:@"http://pinv.me/txt/privacy.html"];
                [self.navigationController pushViewController:webController animated:YES];
            }
                break;
            case 3:
                
                break;
            default:
                break;
        }
    }else if(indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
            {
                if (JTGlobalCache.mode != InitModeCrash) {
                    AlertWithActionsAndTag(1000, @"确定要注销当前账号吗?", @"", @"取消", @"确定");
                }else {
                    [[YouPin sharedInstance] performSelector:@selector(logut) withObject:nil];
                }
            }
                break;
            default:
                break;
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1000) {
        if (buttonIndex == 1) {
            [self showHUDViewWithTitle:@"正在注销..."];
            [[YouPinAPI sharedAPI] logout:^(NSError *error) {
                [self hideHUDView];
                if (!error) {
                    [[YouPin sharedInstance] logout];
                    [self presentLoginController];
                    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationUserDidLogout object:nil];
                }else {
                    [self showToastWithTitle:@"注销失败，请重试"];
                }
            }];
        }
    }
}

@end
