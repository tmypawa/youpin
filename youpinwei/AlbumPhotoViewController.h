//
//  AlbumPhotoViewController.h
//  youpinwei
//
//  Created by tmy on 14/11/18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumPhotoViewController : UIImagePickerController<UINavigationControllerDelegate>

@end
