//
//  PhotoEditViewController.h
//  youpinwei
//
//  Created by tmy on 14-9-16.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PhotoFilterViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UIImage *photo;

@end


