//
//  TagSelectionViewController.h
//  youpinwei
//
//  Created by tmy on 14-10-14.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"

@protocol TagSelectionViewControllerDelegate;

@interface TagSelectionViewController : ShaibaBaseViewController<DWTagListDelegate, DWTagViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<TagSelectionViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray *existTags;

@end


@protocol TagSelectionViewControllerDelegate <NSObject>

- (void)tagSelectionViewController:(TagSelectionViewController *)controller didSelectTags:(NSArray *)tags;

@end