//
//  BannerViewController.m
//  youpinwei
//
//  Created by tmy on 14/12/12.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "BannerViewController.h"
#import "GoodsChooseController.h"
#import "PublishViewController.h"

@interface BannerViewController ()

@end

@implementation BannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
//    NSString *schema = request.URL.scheme;
//    NSString *queryStr = request.URL.query;
//    if ([schema isEqualToString:@"shaibapublish"]) {
//        NSArray *params = [queryStr componentsSeparatedByString:@"="];
//        NSString *tagValue = [params[1] URLDecodedString];
//        [PublishParamSet sharedInstance].tags = @[tagValue];
//        
//        CameraViewController *cameraController = [[CameraViewController alloc] init];
//        cameraController.cameraDelegate = self;
//        [self.navigationController presentViewController:cameraController animated:YES completion:nil];
//        return NO;
//    }else if ([schema isEqualToString:@"shaibataobaolist"]) {
//        NSArray *params = [queryStr componentsSeparatedByString:@"="];
//        NSString *tagValue = [params[1] URLDecodedString];
//        [PublishParamSet sharedInstance].tags = @[tagValue];
//        
//        GoodsChooseController *goodsChooseController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GoodsChooseController"];
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:goodsChooseController];
//        [self.navigationController presentViewController:navController animated:YES completion:nil];
//        return NO;
//    }else {
//        return YES;
//    }
    return YES;
}

#pragma mark CameraViewControllerDelegate
- (void)cameraViewController:(CameraViewController *)controller didPickImage:(UIImage *)image {
    [PublishParamSet sharedInstance].photos = @[image];
    
    PublishViewController *publishController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PublishViewController"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:publishController];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)cameraViewControllerDidCancel:(CameraViewController *)controller {
    
}

@end
