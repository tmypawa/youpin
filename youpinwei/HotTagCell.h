//
//  HotTagCell.h
//  youpinwei
//
//  Created by tmy on 15/2/9.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotTagCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
