//
//  Comment.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "Comment.h"
#import "JTAddressBookManager.h"

@implementation Comment

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.headImage  forKey:@"headImage"];
    [encoder encodeObject:self.nick forKey:@"nick"];
    [encoder encodeObject:self.comment forKey:@"comment"];
    [encoder encodeObject:self.commentId forKey:@"commentId"];
    [encoder encodeDouble:self.createTime forKey:@"createTime"];
    [encoder encodeObject:self.extraId forKey:@"extraId"];
    [encoder encodeInt:self.id forKey:@"id"];
    [encoder encodeObject:self.productId forKey:@"productId"];
    [encoder encodeInt:self.state forKey:@"state"];
    [encoder encodeDouble:self.updateTime forKey:@"updateTime"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.receiverId forKey:@"receiverId"];
    [encoder encodeObject:self.receiverPhone forKey:@"receiverPhone"];
    [encoder encodeObject:self.receiverNick forKey:@"receiverNick"];
    [encoder encodeObject:self.requestId forKey:@"requestId"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.headImage = [decoder decodeObjectForKey:@"headImage"];
        self.nick = [decoder decodeObjectForKey:@"nick"];
        self.comment = [decoder decodeObjectForKey:@"comment"];
        self.commentId = [decoder decodeObjectForKey:@"commentId"];
        self.createTime = [decoder decodeDoubleForKey:@"createTime"];
        self.updateTime = [decoder decodeDoubleForKey:@"updateTime"];
        self.extraId = [decoder decodeObjectForKey:@"extraId"];
        self.id = [decoder decodeIntForKey:@"id"];
        self.productId = [decoder decodeObjectForKey:@"productId"];
        self.state = [decoder decodeIntForKey:@"state"];
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.receiverId = [decoder decodeObjectForKey:@"receiverId"];
        self.receiverPhone = [decoder decodeObjectForKey:@"receiverPhone"];
        self.receiverNick = [decoder decodeObjectForKey:@"receiverNick"];
        self.requestId = [decoder decodeObjectForKey:@"requestId"];
    }
    return self;
}

- (NSString *)displayNameWithPosterId:(NSString *)posterId {
    NSString *displayName = @"";
    if (JTGlobalCache.currentUser) {
        if ([self.userId isEqualToString:posterId] && ![posterId isEqualToString:JTGlobalCache.currentUser.userId]) {
            displayName = @"楼主";
        }else {
            JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:self.extraId];
            if (person) {
                displayName = person.fullName;
            }else {
                displayName = self.nick;
            }
        }
    }else {
        JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:self.extraId];
        if (person) {
            displayName = person.fullName;
        }else {
            displayName = self.nick;
        }
    }
    
    return displayName;
}

- (NSString *)responseDisplayNameWithPosterId:(NSString *)posterId {
    NSString *displayName = @"";
    if (JTGlobalCache.currentUser) {
        if ([self.receiverId isEqualToString:posterId] && ![posterId isEqualToString:JTGlobalCache.currentUser.userId]) {
            displayName = @"楼主";
        }else {
            JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:self.receiverPhone];
            if (person) {
                displayName = person.fullName;
            }else {
                displayName = self.receiverNick;
            }
        }
    }else {
        JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:self.receiverPhone];
        if (person) {
            displayName = person.fullName;
        }else {
            displayName = self.receiverNick;
        }
    }
    
    return displayName;
}

@end
