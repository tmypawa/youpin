//
//  ProfileSettingViewController.m
//  youpinwei
//
//  Created by tmy on 14-9-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "ProfileSettingViewController.h"
#import "NYXImagesKit.h"
#import "MBProgressHUD.h"
#import "UIKit+RoundImage.h"

#define AVATAR_SIZE 200

@interface ProfileSettingViewController ()

@end

@implementation ProfileSettingViewController
{
    IBOutlet UIImageView *_tipBgView;
    __weak IBOutlet UIButton *_avatarBtn;
    __weak IBOutlet UITextField *_nicknameTextField;
    __weak IBOutlet UITextField *_genderTextField;
    IBOutlet UIButton *_maskBtn;
    __weak IBOutlet UIView *_floatView;
    IBOutlet UIButton *_confirmBtn;
    UIImage     *_avatar;
    int         _gender; //1为男, 2为女
    User        *_currentUser;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    if (JTGlobalCache.currentUser) {
        _currentUser = JTGlobalCache.currentUser;
    }else if(self.existUser) {
        _currentUser = self.existUser;
    }
    
    if (_currentUser) {
        if (_currentUser.avatar.length > 0) {
            [_avatarBtn setImageWithURL:_currentUser.avatar placeholderImage:[UIImage imageNamed:@"icon_mine_default_Head"] effect:SDWebImageEffectNone];
        }else {
            [_avatarBtn setImage:[UIImage imageNamed:@"btn_take_photo"] forState:UIControlStateNormal];
        }
        
        _nicknameTextField.text = _currentUser.nickname;
        if (_currentUser.gender != 0) {
            _genderTextField.text = _currentUser.gender == 1? @"帅哥":@"美女";
            _gender = _currentUser.gender;
        }
    }else {
        
    }
    
    _nicknameTextField.tintColor = NavigationBarTintColor;
    _tipBgView.image = [[UIImage imageNamed:@"profile_tip_bg"] stretchableImageWithLeftCapWidth:35 topCapHeight:0];
    [_confirmBtn setBackgroundImage:[[UIImage imageNamed:@"btn_profile_ok_normal"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    
    [_maskBtn removeFromSuperview];
    [_avatarBtn makeRound];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    if (self.isEditNickname) {
        [_nicknameTextField becomeFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)selectAvatar:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"请选择头像" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册中选择", nil];
    actionSheet.tag = 100;
    [actionSheet showInView:self.navigationController.view];
}

- (IBAction)cancel:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(profileSettingViewControllerDidCancel:)]) {
        [self.delegate profileSettingViewControllerDidCancel:self];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirm:(id)sender {
    NSString *nickname = [_nicknameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
   
    if (!nickname || nickname.length == 0) {
        Alert(@"请设置您的昵称", nil, @"确定");
        return;
    }
 
    if (_gender == 0) {
        Alert(@"请选择您的性别", nil, @"确定");
        return;
    }
    
    if ((!_avatar && !_currentUser) || (!_avatar && _currentUser && _currentUser.avatar.length == 0)) {
        Alert(@"请选择您的头像", nil, @"确定");
        return;
    }
    
    if (_nicknameTextField.isFirstResponder) {
        [self hiddenInput];
    }
    
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    [[YouPinAPI sharedAPI] setProfileWithNickname:nickname gender:_gender avatar:_avatar phone:_currentUser.phone headUrl:nil location:nil userOpenId:nil sender:self callback:^(User *userInfo, NSError *error) {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (!error) {
            if (!JTGlobalCache.isLogin) {
                [[YouPin sharedInstance] loginWithUser:userInfo];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(profileSettingViewController:didSetProfileInfo:)]) {
                    [self.delegate profileSettingViewController:self didSetProfileInfo:JTGlobalCache.currentUser];
                }
                
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:YouPinNotificationUserDidLogin object:nil userInfo:nil];
                }];
            }else {
                JTGlobalCache.currentUser.nickname = userInfo.nickname;
                JTGlobalCache.currentUser.avatar = userInfo.avatar;
                JTGlobalCache.currentUser.gender = userInfo.gender;
                [JTGlobalCache synchronize];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(profileSettingViewController:didSetProfileInfo:)]) {
                    [self.delegate profileSettingViewController:self didSetProfileInfo:JTGlobalCache.currentUser];
                }
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
        }else {
            
        }

    }];
}

- (IBAction)cancelInput:(id)sender {
    [self hiddenInput];
}


- (void)hiddenInput {
    [_nicknameTextField resignFirstResponder];
    [_maskBtn removeFromSuperview];
    [UIView animateWithDuration:0.3f animations:^{
        float offset = isIPhone4 ? 133 : 50;
        SetFrameY(_floatView, _floatView.frame.origin.y + offset);
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == _genderTextField) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"请选择性别" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"帅哥", @"美女", nil];
        actionSheet.tag = 200;
        [actionSheet showInView:self.navigationController.view];
        return NO;
    }else {
        [self.view addSubview:_maskBtn];
        [UIView animateWithDuration:0.3f animations:^{
            float offset = isIPhone4 ? 133 : 50;
            SetFrameY(_floatView, _floatView.frame.origin.y - offset);
        }];
        return YES;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 100 && buttonIndex != 2) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = buttonIndex == 0 ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        imagePickerController.delegate = self;
        [self.navigationController presentViewController:imagePickerController animated:YES completion:nil];
    }else if(actionSheet.tag == 200 && buttonIndex !=2) {
        _gender = (int)buttonIndex + 1;
        _genderTextField.text = _gender == 1 ? @"帅哥" : @"美女";
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    _avatar = [image scaleToCoverSize:CGSizeMake(AVATAR_SIZE, AVATAR_SIZE)];
    [_avatarBtn setImage:_avatar forState:UIControlStateNormal];
    [_avatarBtn setImage:_avatar forState:UIControlStateHighlighted];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
