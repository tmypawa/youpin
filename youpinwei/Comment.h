//
//  Comment.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject<NSCoding>
@property (strong, nonatomic) NSString *headImage;
@property (strong, nonatomic) NSString *nick;
@property (strong, nonatomic) NSString *comment;
@property (strong, nonatomic) NSString *commentId;
@property (nonatomic) NSTimeInterval  createTime;
@property (strong, nonatomic) NSString *extraId;
@property (nonatomic) int id;
@property (strong, nonatomic) NSString *productId;
@property (nonatomic) int state;
@property (nonatomic) NSTimeInterval updateTime;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *receiverId;
@property (strong, nonatomic) NSString *receiverPhone;
@property (strong, nonatomic) NSString *receiverNick;
@property (strong, nonatomic) NSString *requestId;

- (NSString *)displayNameWithPosterId:(NSString *)posterId;
- (NSString *)responseDisplayNameWithPosterId:(NSString *)posterId;

@end
