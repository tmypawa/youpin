//
//  EditorCommentCell.h
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostContentCell.h"

@interface EditorCommentCell : PostContentCell
@property (strong, nonatomic) IBOutlet UIImageView *avatarView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;

+ (CGFloat)heightForText:(NSString *)text;

@end
