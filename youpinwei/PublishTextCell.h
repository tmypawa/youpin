//
//  PublishTextCell.h
//  youpinwei
//
//  Created by tmy on 15/1/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMMoveTableViewCell.h"
#import "JTFixedTextView.h"

@interface PublishTextCell : FMMoveTableViewCell
@property (strong, nonatomic) IBOutlet JTFixedTextView *inputView;
@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *maskButton;

@end
