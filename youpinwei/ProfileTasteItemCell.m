//
//  ProfileTasteItemCell.m
//  youpinwei
//
//  Created by tmy on 14/12/8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "ProfileTasteItemCell.h"

@implementation ProfileTasteItemCell

- (void)awakeFromNib {
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1].CGColor;
}

@end
