//
//  CommentCell.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *avatarView;

@property (weak, nonatomic) IBOutlet UILabel  *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

+ (CGFloat)heightForComment:(NSString *)comment;
- (void)updateLayout;

@end
