//
//  PublishPhotoBrowerController.m
//  youpinwei
//
//  Created by tmy on 14/12/22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PublishPhotoBrowerController.h"

@interface PublishPhotoBrowerController ()

@end

@implementation PublishPhotoBrowerController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIButton *trashBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    trashBtn.frame = CGRectMake(screenWidth - 60, screenHeight - 60, 40, 40);
    [trashBtn setImage:[UIImage imageNamed:@"btn_trash_normal"] forState:UIControlStateNormal];
    [trashBtn setImage:[UIImage imageNamed:@"btn_trash_highlight"] forState:UIControlStateHighlighted];
    [trashBtn addTarget:self action:@selector(deletePhoto:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:trashBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)deletePhoto:(UIButton *)sender {
    [_imageURLs removeObjectAtIndex:self.currentImageIndex];
    if (_imageURLs.count > 0) {
       NSIndexPath *indexPath = [_imageCollectionView indexPathsForVisibleItems][0];
        [_imageCollectionView deleteItemsAtIndexPaths:@[indexPath]];
        if (self.publishPhotoDelegate && [self.publishPhotoDelegate respondsToSelector:@selector(publishPhotoBrowerController:didDeletePhotoItemAtIndex:)]) {
            [self.publishPhotoDelegate publishPhotoBrowerController:self didDeletePhotoItemAtIndex:indexPath.row];
        }
    }else {
        if (self.publishPhotoDelegate && [self.publishPhotoDelegate respondsToSelector:@selector(publishPhotoBrowerController:didDeletePhotoItemAtIndex:)]) {
            [self.publishPhotoDelegate publishPhotoBrowerController:self didDeletePhotoItemAtIndex:0];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageBrowerCell" forIndexPath:indexPath];
    UIImageView  *tilingView = nil;
    if (![cell.contentView viewWithTag:1000]) {
        tilingView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        tilingView.contentMode = UIViewContentModeScaleAspectFit;
        tilingView.backgroundColor = [UIColor clearColor];
        tilingView.tag = 1000;
        [cell.contentView addSubview:tilingView];
    }else {
        tilingView = (UIImageView *)[cell.contentView viewWithTag:1000];
    }
    
    NSURL *url = _imageURLs[indexPath.row];
    if (url.isFileURL) {
        tilingView.image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]];
    }else {
        [tilingView setImageWithURL:url.absoluteString];
    }
    
    return cell;
}

@end
