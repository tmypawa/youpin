//
//  NewFriendViewController.m
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "NewFriendViewController.h"
#import "FriendInfoCell.h"
#import "FriendInfoViewController.h"

@implementation NewFriendViewController
{
    NSArray  *_friendNotificationList;
    NSMutableSet *_acceptNotificationSet;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _acceptNotificationSet = [[NSMutableSet alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [[YouPinAPI sharedAPI] fetchFriendNotificationListWithUpdateTime:JTGlobalCache.lastFriendNotificationTime sender:self callback:^(NSArray *notificationList, NSInteger notificationCount, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        if (!error) {
            JTGlobalCache.friendNotificationBadge = 0;
            if (notificationList.count > 0) {
                JTGlobalCache.lastFriendNotificationTime = [notificationList[0] updateTime];
            }else {
                [self showNoItemsViewWithTitle:@"没有新的好友"];
            }

            [JTGlobalCache synchronize];
            _friendNotificationList = notificationList;
            [self.tableView reloadData];
        }
    }];
}

- (void)viewFriendInfo:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    Notification *notification = _friendNotificationList[index];
    FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
    friendInfoController.userId = notification.userId;
    [self.navigationController pushViewController:friendInfoController animated:YES];
}

- (void)acceptInvitation:(UIButton *)sender {
    NSInteger index = sender.tag - 1;
    Notification *notification = _friendNotificationList[index];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[YouPinAPI sharedAPI] acceptFriend:notification.userId notifyId:notification.notifyId callback:^(NSError *error) {
        if (!error) {
            [self showSuccessHUDWithTitle:@"接受邀请"];
            [_acceptNotificationSet addObject:notification.notifyId];
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }else {
            [self showFailHUDWithTitle:@"操作失败"];
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _friendNotificationList ? _friendNotificationList.count : 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Notification *notification = _friendNotificationList[indexPath.row];
    return [FriendInfoCell heightWithSubtitleContent:notification.content];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendInfoCell *cell = (FriendInfoCell *)[tableView dequeueReusableCellWithIdentifier:@"FriendInfoCell" forIndexPath:indexPath];
    Notification *notification = _friendNotificationList[indexPath.row];
    
    if (cell.avatarBtn.tag == 0) {
        [cell.avatarBtn addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
        [cell.addBtn addTarget:self action:@selector(acceptInvitation:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.avatarBtn.tag = indexPath.row + 1;
    cell.addBtn.tag = indexPath.row + 1;
    
    [cell.avatarBtn setImageWithURL:notification.headImage placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
    cell.nameTitleLabel.text = [notification displayName];
    cell.subtitleLabel.text = notification.content;
    
    if([notification.type integerValue] == NotificationTypeWaitAccept){
        cell.statusLabel.text = @"等待验证";
        cell.statusLabel.hidden = NO;
        cell.addBtn.hidden = YES;
    }else if([notification.type integerValue] == NotificationTypeFriendAccept || [_acceptNotificationSet containsObject:notification.notifyId]) {
        cell.addBtn.hidden = YES;
        cell.statusLabel.hidden = NO;
        cell.statusLabel.text = @"已添加";
    }else if([notification.type integerValue] == NotificationTypeFriendInvitation) {
        [cell.addBtn setTitle:@"接受" forState:UIControlStateNormal];
        [cell.addBtn setTitleColor:NavigationBarTintColor forState:UIControlStateNormal];
        cell.addBtn.hidden = NO;
        cell.statusLabel.hidden = YES;
    }
    
    [cell updateLayout];
    
    return cell;
}

@end
