//
//  FriendAddingMethodController.m
//  youpinwei
//
//  Created by tmy on 15/1/17.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "FriendAddingMethodController.h"
#import "FriendInfoViewController.h"
#import "FriendInfoCell.h"
#import "JTAddressBookManager.h"
#import "ContactFriendListController.h"

@implementation FriendAddingMethodController
{
    NSArray  *_searchUserList;
    NSMutableSet    *_invitationSet;
    NSInteger       _currentActionIndex;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _invitationSet = [[NSMutableSet alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchDisplayController.searchBar.backgroundImage = [UIImage imageFromColor:[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:255/255.0]];
    self.searchDisplayController.searchBar.tintColor = NavigationBarTintColor;
    self.searchDisplayController.searchBar.translucent = NO;
    
    if (!JTGlobalCache.hasAllowedSyncAddressBook) {
        [[JTAddressBookManager defaultManager] requestAuthorization:^(bool granted) {
            if (granted) {
                JTGlobalCache.hasAllowedSyncAddressBook = YES;
                [JTGlobalCache synchronize];
                
                
                [[JTAddressBookManager defaultManager] fetchAllContacts:^(NSArray *allContacts, NSArray *updatedContacts) {
                    [self showHUDViewWithTitle:@"正在查找好友..."];
    
                    [[YouPinAPI sharedAPI] uploadAddressBook:allContacts callback:^(NSError *error) {
                        if (!error) {
                            [self hideHUDView];
                            
                            NSLog(@"uploaded address book %ld", JTGlobalCache.lastAddressBookSyncTime == 0? allContacts.count : updatedContacts.count);
                            JTGlobalCache.lastAddressBookSyncTime = [[NSDate date] timeIntervalSince1970];
                            
                            ContactFriendListController *contactFriendController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactFriendListController"];
                            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactFriendController];
                            [self.navigationController presentViewController:navController animated:YES completion:nil];
                        }
                    }];
                }];
            }
        }];
    }
}

- (void)addFriend:(UIButton *)sender {
    User *friend = nil;
    NSInteger index = sender.tag - 1;
    _currentActionIndex = index;
    friend = _searchUserList[index];
    
    FriendInvitationController *invitationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInvitationController"];
    invitationController.userId = friend.userId;
    invitationController.delegate = self;
    [self.navigationController pushViewController:invitationController animated:YES];
}

- (void)viewFriendInfo:(UIButton *)sender {
    NSInteger index = 0;
    if (sender) {
        index = sender.tag - 1;
    }else if(_currentActionIndex != -1){
        index = _currentActionIndex;
    }
    
    User *friend = _searchUserList[index];
    
    if (JTGlobalCache.isLogin && [JTGlobalCache.currentUser.userId isEqualToString:friend.userId]) {
        PersonalInfoViewController *personInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalInfoViewController"];
        [self.navigationController pushViewController:personInfoController animated:YES];
    }else {
        FriendInfoViewController *friendInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendInfoViewController"];
        friendInfoController.userId = friend.userId;
        [self.navigationController pushViewController:friendInfoController animated:YES];
    }
}

- (void)friendInvitationControllerDidSendInvitation:(FriendInvitationController *)controller {
    if (_currentActionIndex != -1) {
        User *friend = _searchUserList[_currentActionIndex];
        
        [_invitationSet addObject:friend.userId];
        
        UITableView *toRefreshTableView = self.searchDisplayController.isActive ? self.searchDisplayController.searchResultsTableView : self.tableView;
        [toRefreshTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_currentActionIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        _currentActionIndex = -1;
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *keyword = [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (keyword.length > 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[YouPinAPI sharedAPI] searchUserByNickname:keyword sender:self callback:^(NSArray *userList, NSError *error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            if (!error) {
                _searchUserList = userList;
                [self.searchDisplayController.searchResultsTableView reloadData];
            }
        }];
    }
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    return NO;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView {
    [tableView registerNib:[UINib nibWithNibName:@"FriendSearchCell" bundle:nil] forCellReuseIdentifier:@"FriendInfoCell"];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    UISearchBar *searchBar = controller.searchBar;
    UIView *superView = searchBar.superview;
    if (![superView isKindOfClass:[UITableView class]]) {
        [searchBar removeFromSuperview];
        [self.tableView addSubview:searchBar];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return _searchUserList ? _searchUserList.count : 0;
    }else {
        return 3;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView == self.tableView? 44 : 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        FriendInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FriendInfoCell" forIndexPath:indexPath];
      
        User *friend = _searchUserList[indexPath.row];
        
        if (cell.addBtn.tag == 0) {
            [cell.addBtn addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
            [cell.avatarBtn addTarget:self action:@selector(viewFriendInfo:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.addBtn.tag = indexPath.row + 1;
        cell.avatarBtn.tag = indexPath.row + 1;
        
        [cell.avatarBtn setImageWithURL:friend.avatar placeholderImage:DEFAULT_AVATAR effect:SDWebImageEffectNone];
        cell.nameTitleLabel.text = friend.nickname;
        
        if(friend.relation == UserRelationTypeWaitAccept || [_invitationSet containsObject:friend.userId]){
            cell.statusLabel.text = @"等待验证";
            cell.statusLabel.hidden = NO;
            cell.addBtn.hidden = YES;
        }else if(friend.relation == UserRelationTypeFriend) {
            cell.addBtn.hidden = YES;
            cell.statusLabel.hidden = NO;
            cell.statusLabel.text = @"已添加";
        }else if(friend.relation == UserRelationTypeNotFriend) {
            [cell.addBtn setTitle:@"添加" forState:UIControlStateNormal];
            [cell.addBtn setTitleColor:NavigationBarTintColor forState:UIControlStateNormal];
            cell.addBtn.hidden = NO;
            cell.statusLabel.hidden = YES;
        }
        
        [cell updateLayout];
        
        
        return cell;
    }else {
        FriendInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"method%ld", indexPath.row + 1] forIndexPath:indexPath];
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *friend = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        _currentActionIndex = indexPath.row;
        [self viewFriendInfo:nil];
        friend = _searchUserList[indexPath.row];
    }else {
        if (indexPath.row == 2) {
            [self inviteFriendViaWeixin];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    }
}

- (void)inviteFriendViaWeixin {
    static NSString *defaultTitle = @"【晒吧】分享真实的购物体验";
    static NSString *defaultDescription = @"【晒吧】发现最好的购物经验，告别坑爹的网购退货史。这里的朋友们喜欢分享、乐于助人，Always等待着和你一起玩耍~~";
    static NSString *defaultImageUrl = @"http://shai.ba/pic/system/icon.png";
    static NSString *defaultUrl = @"http://shai.ba";
    
    NSString *imageUrl = JTGlobalCache.wxShare ? JTGlobalCache.wxShare[@"productUrl"] : defaultImageUrl;
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url: imageUrl];
    
    NSString *description = JTGlobalCache.wxShare ? JTGlobalCache.wxShare[@"description"] : defaultDescription;
    
    NSString *title = JTGlobalCache.wxShare ? JTGlobalCache.wxShare[@"title"] : defaultTitle;
    [UMSocialData defaultData].extConfig.wechatSessionData.title = title;
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;

    NSString *url = JTGlobalCache.wxShare ? JTGlobalCache.wxShare[@"wapUrl"] : defaultUrl;
    url = [url stringByAppendingFormat:@"&requestId=%@",[NSUUID UUID].UUIDString];
    [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = url;
    
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UMENG_ID
                                      shareText:description
                                     shareImage:nil
                                shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline]
                                       delegate:self];
}

-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    if(response.responseCode == UMSResponseCodeSuccess)
    {
   
    }
}

@end
