//
//  VerifyViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileSettingViewController.h"

@interface VerifyViewController : LoginBaseViewController<ProfileSettingViewControllerDelegate>

@property (nonatomic, strong) NSString *phone;

@end
