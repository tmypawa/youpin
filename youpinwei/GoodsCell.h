//
//  GoodsCell.h
//  youpinwei
//
//  Created by tmy on 14-8-27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
