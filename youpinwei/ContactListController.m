//
//  ContactListController.m
//  youpinwei
//
//  Created by tmy on 15/3/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ContactListController.h"
#import "JTAddressBookManager.h"
#import "FriendInfoCell.h"

@implementation ContactListController
{
    NSMutableArray     *_contactPersons;
    IBOutlet UIView    *_defaultView;
    IBOutlet UIButton  *_addButton;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _contactPersons = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setTableHeaderView:nil];
    
    [_addButton setBackgroundImage:[[UIImage imageNamed:@"btn_red_normal"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateNormal];
    [_addButton setBackgroundImage:[[UIImage imageNamed:@"btn_red_highlight"] stretchableImageWithLeftCapWidth:5 topCapHeight:5] forState:UIControlStateHighlighted];
    
    [self refreshData];
}

- (void)refreshData {
    [[JTAddressBookManager defaultManager] requestAuthorization:^(bool granted) {
        if (granted) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [self loadContactList];
        }else {
            [self.tableView addSubview:_defaultView];
            self.tableView.scrollEnabled = NO;
        }
    }];
}

- (void)loadContactList {
    [[YouPinAPI sharedAPI] fetchContactInvitationList:self callback:^(NSArray *friendList, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        if (!error) {
             [[JTAddressBookManager defaultManager] fetchAllContacts:^(NSArray *allContacts, NSArray *updatedContacts) {
                 if (allContacts) {
                     [_contactPersons addObjectsFromArray:allContacts];
                     for (User *user in friendList) {
                         JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:user.phone];
                         if (person) {
                             [_contactPersons removeObject:person];
                         }
                     }
                 }
                 
                 [self.tableView reloadData];
             }];
        }
    }];
}

- (IBAction)allowContactAccess:(id)sender {
    Alert(@"允许访问通讯录", @"请到iPhone的设置 -> 隐私 -> 通讯录中允许晒吧访问通讯录", @"确定");
}

- (void)invitateFriend:(UIButton *)sender {
    NSInteger rowIndex = sender.tag - 1;
    JTAddressBookPerson *person = _contactPersons[rowIndex];
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    if (messageController) {
        if (person.phoneNumbers.count > 0) {
            NSString *friendPhone = person.phoneNumbers[0];
            messageController.view.tintColor = [UIColor whiteColor];
            messageController.messageComposeDelegate = self;
            [messageController setRecipients:@[friendPhone]];
            
            NSString *bodyText = @"";
            if (JTGlobalCache.smsShare) {
                bodyText = [JTGlobalCache.smsShare stringByAppendingString:friendPhone];
            }else {
                bodyText = [NSString stringWithFormat:@"我在【晒吧】里分享了一些败来的宝贝，安装后搜索我的昵称“%@”添加我好友。下载地址：http://shai.ba?s=1&u=%@&p=%@", JTGlobalCache.currentUser.nickname, JTGlobalCache.currentUser.userId, friendPhone];
            }
            
            [messageController setBody: bodyText];
            [self.navigationController presentViewController:messageController animated:YES completion:nil];
        }else {
            Alert(@"该通讯录好友电话号码为空", @"", @"确定");
        }
        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contactPersons ? _contactPersons.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactInfoCell" forIndexPath:indexPath];
    if (cell.addBtn.tag == 0) {
        [cell.addBtn addTarget:self action:@selector(invitateFriend:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.addBtn.tag = indexPath.row + 1;

    
    JTAddressBookPerson *person = _contactPersons[indexPath.row];
    
    cell.showAvatar = NO;
    cell.nameTitleLabel.text = person.fullName;
    [cell updateLayout];
    
    return cell;
}

@end
