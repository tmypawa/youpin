//
//  PublishParamSet.h
//  youpinwei
//
//  Created by tmy on 14/12/12.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Goods.h"
#import "TasteItem.h"

@interface PublishParamSet : NSObject

//external publish params
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) Goods *goods;
@property (nonatomic, strong) NSArray *goodsPhotoURLs;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) NSArray *texts;

//generated post
@property (nonatomic, strong) TasteItem *post;

+ (instancetype)sharedInstance;

- (void)reset;

@end
