//
//  Goods.m
//  youpinwei
//
//  Created by tmy on 14-8-23.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "Goods.h"

@implementation Goods

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.billTime forKey:@"billTime"];
    [encoder encodeObject:self.buyerNick forKey:@"buyerNick"];
    [encoder encodeObject:self.buylink forKey:@"buylink"];
    [encoder encodeObject:self.categoryId forKey:@"categoryId"];
    [encoder encodeDouble:self.createTime forKey:@"createTime"];
    [encoder encodeObject:self.goodsDescription forKey:@"goodsDescription"];
    [encoder encodeObject:self.extraId forKey:@"extraId"];
    [encoder encodeObject:@(self.id) forKey:@"id"];
    [encoder encodeObject:self.itemId forKey:@"itemId"];
    [encoder encodeObject:self.orderId forKey:@"orderId"];
    [encoder encodeObject:self.payPrice forKey:@"payPrice"];
    [encoder encodeObject:self.pic forKey:@"pic"];
    [encoder encodeObject:self.price forKey:@"price"];
    [encoder encodeObject:self.purchaseId forKey:@"purchaseId"];
    [encoder encodeObject:self.quantity forKey:@"quantity"];
    [encoder encodeInt:self.seazon forKey:@"seazon"];
    [encoder encodeObject:self.sellerId forKey:@"sellerId"];
    [encoder encodeObject:self.sellerNick forKey:@"sellerNick"];
    [encoder encodeObject:self.shopName forKey:@"shopName"];
    [encoder encodeInt:self.source forKey:@"source"];
    [encoder encodeInt:self.state forKey:@"state"];
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeDouble:self.updateTime forKey:@"updateTime"];
    [encoder encodeObject:self.userId forKey:@"userId"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.billTime = [decoder decodeObjectForKey:@"billTime"];
        self.buyerNick = [decoder decodeObjectForKey:@"buyerNick"];
        self.buylink = [decoder decodeObjectForKey:@"buylink"];
        self.categoryId = [decoder decodeObjectForKey:@"categoryId"];
        self.createTime = [decoder decodeDoubleForKey:@"createTime"];
        self.goodsDescription = [decoder decodeObjectForKey:@"goodsDescription"];
        self.extraId = [decoder decodeObjectForKey:@"extraId"];
        self.id = [[decoder decodeObjectForKey:@"id"] longLongValue];
        self.itemId = [decoder decodeObjectForKey:@"itemId"];
        self.orderId = [decoder decodeObjectForKey:@"orderId"];
        self.payPrice = [decoder decodeObjectForKey:@"payPrice"];
        self.pic = [decoder decodeObjectForKey:@"pic"];
        self.price = [decoder decodeObjectForKey:@"price"];
        self.purchaseId = [decoder decodeObjectForKey:@"purchaseId"];
        self.quantity = [decoder decodeObjectForKey:@"quantity"];
        self.seazon = [decoder decodeIntForKey:@"seazon"];
        self.sellerId = [decoder decodeObjectForKey:@"sellerId"];
        self.sellerNick = [decoder decodeObjectForKey:@"sellerNick"];
        self.shopName = [decoder decodeObjectForKey:@"shopName"];
        self.source = [decoder decodeIntForKey:@"source"];
        self.state = [decoder decodeIntForKey:@"state"];
        self.title = [decoder decodeObjectForKey:@"title"];
        self.updateTime = [decoder decodeDoubleForKey:@"updateTime"];
        self.userId = [decoder decodeObjectForKey:@"userId"];
    }
    return self;
}

@end
