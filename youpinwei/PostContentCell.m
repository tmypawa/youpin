//
//  PostContentCell.m
//  youpinwei
//
//  Created by tmy on 15/2/5.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PostContentCell.h"

@implementation PostContentCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)updateLayout {

}

+ (CGFloat)height {
    return 44;
}

@end
