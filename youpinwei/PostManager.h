//
//  PostManager.h
//  youpinwei
//
//  Created by tmy on 15/2/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TasteItem.h"

@interface PostManager : NSObject

+ (instancetype)defaultManager;
- (void)publishPost:(TasteItem *)post;
- (void)updatePost:(TasteItem *)post;
- (void)updatePost:(TasteItem *)post callback:(void(^)(TasteItem *newTaste, NSError *error))callback;
- (void)cancelPublishForPost:(TasteItem *)post;
- (void)clearDrafts;
- (NSString *)postImageFileWithImageName:(NSString *)imageName;

@end
