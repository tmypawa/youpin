//
//  Tag.m
//  youpinwei
//
//  Created by tmy on 15/2/10.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "Tag.h"

@implementation Tag

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.bannerHeight forKey:@"bannerHeight"];
    [encoder encodeObject:self.bannerUrl forKey:@"bannerUrl"];
    [encoder encodeObject:self.bannerWidth forKey:@"bannerWidth"];
    [encoder encodeDouble:self.createTime forKey:@"createTime"];
    [encoder encodeObject:self.tagDescription forKey:@"tagDescription"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.picHeight forKey:@"picHeight"];
    [encoder encodeObject:self.picWidth forKey:@"picWidth"];
    [encoder encodeObject:self.picUrl forKey:@"picUrl"];
    [encoder encodeObject:self.thumbnailsHeight forKey:@"thumbnailsHeight"];
    [encoder encodeObject:self.thumbnailsUrl forKey:@"thumbnailsUrl"];
    [encoder encodeObject:self.thumbnailsWidth forKey:@"thumbnailsWidth"];
    [encoder encodeObject:self.interestUrl forKey:@"interestUrl"];
    [encoder encodeDouble:self.updateTime forKey:@"updateTime"];
    [encoder encodeObject:self.color forKey:@"color"];
    [encoder encodeBool:self.showPhoto forKey:@"showPhoto"];
    [encoder encodeObject:self.QRurl forKey:@"QRurl"];
    [encoder encodeObject:self.QRThumbnail forKey:@"QRThumbnail"];
    [encoder encodeBool:self.subscribe forKey:@"subscribe"];
    [encoder encodeInteger:self.subscribeCount forKey:@"subscribeCount"];
    [encoder encodeInteger:self.count forKey:@"count"];
    [encoder encodeObject:self.relevantTags forKey:@"relevantTags"];
    [encoder encodeInteger:self.listType forKey:@"listType"];
    [encoder encodeInteger:self.tagType forKey:@"tagType"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.bannerHeight = [decoder decodeObjectForKey:@"bannerHeight"];
        self.bannerUrl = [decoder decodeObjectForKey:@"bannerUrl"];
        self.bannerWidth = [decoder decodeObjectForKey:@"bannerWidth"];
        self.createTime = [decoder decodeDoubleForKey:@"createTime"];
        self.tagDescription = [decoder decodeObjectForKey:@"tagDescription"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.picHeight = [decoder decodeObjectForKey:@"picHeight"];
        self.picWidth = [decoder decodeObjectForKey:@"picWidth"];
        self.picUrl = [decoder decodeObjectForKey:@"picUrl"];
        self.thumbnailsHeight = [decoder decodeObjectForKey:@"thumbnailsHeight"];
        self.thumbnailsUrl = [decoder decodeObjectForKey:@"thumbnailsUrl"];
        self.thumbnailsWidth = [decoder decodeObjectForKey:@"thumbnailsWidth"];
        self.interestUrl = [decoder decodeObjectForKey:@"interestUrl"];
        self.updateTime = [decoder decodeDoubleForKey:@"updateTime"];
        self.showPhoto = [decoder decodeBoolForKey:@"showPhoto"];
        self.color = [decoder decodeObjectForKey:@"color"];
        self.QRurl = [decoder decodeObjectForKey:@"QRurl"];
        self.QRThumbnail = [decoder decodeObjectForKey:@"QRThumbnail"];
        self.subscribe = [decoder decodeBoolForKey:@"subscribe"];
        self.subscribeCount = [decoder decodeIntegerForKey:@"subscribeCount"];
        self.count = [decoder decodeIntegerForKey:@"count"];
        self.relevantTags = [decoder decodeObjectForKey:@"relevantTags"];
        self.listType = [decoder decodeIntegerForKey:@"listType"];
        self.tagType = [decoder decodeIntegerForKey:@"tagType"];
    }
    return self;
}

@end
