//
//  MenuViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICSDrawerController.h"
#import "ProfileSettingViewController.h"

@interface MenuViewController : UIViewController <ICSDrawerControllerChild, ICSDrawerControllerPresenting, ProfileSettingViewControllerDelegate>

@property (nonatomic, strong) UIViewController *mainViewController;
@property (nonatomic, strong) UIViewController *settingViewController;
@property (nonatomic, strong) UIViewController *notificationController;
@property (nonatomic, strong) UINavigationController *centerViewController;

@end
