//
//  PostContentCell.h
//  youpinwei
//
//  Created by tmy on 15/2/5.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostContentCell : UITableViewCell

- (void)updateLayout;

+ (CGFloat)height;

@end
