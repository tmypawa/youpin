//
//  Notification.h
//  youpinwei
//
//  Created by tmy on 14/10/22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSInteger {
    NotificationTypeNone = 0,
    NotificationTypePost = 1,
    NotificationTypeComment = 2,
    NotificationTypePraise = 3,
    NotificationTypeFriendInvitation = 4,
    NotificationTypeFriendAccept = 5,
    NotificationTypeWaitAccept = 6,
    NotificationTypeWishList = 7,
    NotificationTypeNewUserJoin = 8
} NotificationType;

@interface Notification : NSObject<NSCoding>

@property (nonatomic, strong) NSString *content;
@property (nonatomic) NSTimeInterval createTime;
@property (nonatomic) NSTimeInterval updateTime;
@property (nonatomic, strong) NSString *headImage;
@property (nonatomic, strong) NSArray *headImageList;
@property (nonatomic, strong) NSString *nick;
@property (nonatomic, strong) NSString *notifyId;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *toNick;
@property (nonatomic, strong) NSString *toPhone;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *productImage;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *productDesc;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *tips;

- (NSString *)displayName;

@end
