//
//  TagViewController.m
//  youpinwei
//
//  Created by tmy on 14-10-16.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "TagViewController.h"
#import "PublishViewController.h"
#import "RelatedTagsViewController.h"
#import "TagQRViewController.h"
#import "RelatedTagCell.h"
#import "ColorUtils.h"
#import "UIKit+RoundImage.h"

@implementation TagViewController
{
    IBOutlet UIView *_headerView;
    IBOutlet UIImageView *_headerImageView;
    IBOutlet UILabel *_headerDescriptionLabel;
    IBOutlet UIView *_actionView;
    IBOutlet UILabel *_headerCountLabel;
    IBOutlet UIButton *_publishBtn;
    IBOutlet UIView *_headerInfoBgView;
    IBOutlet UIView *_headerBottomLine;
    IBOutlet UIButton *_subscribeBtn;
    IBOutlet UILabel *_postsCountLabel;
    IBOutlet UILabel *_usersCountLabel;
    IBOutlet UILabel *_descriptionLabel;
    IBOutlet UICollectionView *_tagsCollectionView;
    IBOutlet UIView *_tabView;
    IBOutlet UIButton *_favoriteBtn;
    IBOutlet UIButton *_latestBtn;
    IBOutlet UIView *_bottomLine;
    IBOutlet UIView *_qrvIEW;
    IBOutlet UIImageView *_QRImageView;
    Tag     *_tag;
    BOOL    _hasMoreRelatedTags;
    NSInteger   _currentTagListType;
}

- (void)dealloc {
    [self.tableView removePullToRefreshActionHandler];
}
 

- (void)internalInit {
    _tasteItems = [[NSMutableArray alloc] init];
    _currentOperationItemIndex = -1;
    _hasNext = YES;
    _scaledRowIndexSet = [[NSMutableIndexSet alloc] init];
    _currentTagListType = 2;
}

- (void)awakeFromNib {
    [self internalInit];
    [self registerNotifications];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.hidden = YES;
    _actionView.hidden = YES;
    _headerDescriptionLabel.hidden = YES;
    [_headerImageView makeRound];
    _tagsCollectionView.scrollsToTop = NO;
}
 
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.navigationController.topViewController != self) {
        [[YouPinAPI sharedAPI] cancelRequestForSender:self];
    }
}
 

- (void)viewDidLayoutSubviews {
    self.tableView.frame = CGRectMake(0, 0, screenWidth, screenHeight - 64);
    CGFloat bottomInset = _tag && _tag.showPhoto ? _actionView.frame.size.height : 0;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottomInset, 0);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTasteItemDidPublish:) name:YouPinNotificationNewTasteItemDidPublish object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tasteItemDidRemove:) name:YouPinNotificationDidRemoveTasteItem object:nil];
}

- (void)initView {
    NSString *trimmedTitle = [self.tagName stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"-sys"]];
    self.title = [NSString stringWithFormat:@"#%@", trimmedTitle];
    [self showHUDViewWithTitle:@""];
    [self refreshData];
}

- (void)loadPullRereshAnimation {
    [super loadPullRereshAnimation];
    self.tableView.pullToRefreshView.originalTopInset = 0;
    
}

- (void)handleForNavigationAppear:(UIScrollView *)scrollView {
    
}

- (void)updateHeaderView {
    CGFloat headerHeight = 92;
    [_headerImageView setImageWithURL:_tag.thumbnailsUrl placeholderImage:DEFAULT_TIMELINE_PHOTO effect:SDWebImageEffectNone];
    
    _postsCountLabel.text = [NSString stringWithFormat:@"%ld篇帖子", _tag.count];
    _usersCountLabel.text = [NSString stringWithFormat:@"%ld人关注", _tag.subscribeCount];
    
    [self updateSubscriptionButton];
    
    if (NotEmptyValue(_tag.QRurl)) {
        _qrvIEW.hidden = NO;
        SetFrameWidth(_headerDescriptionLabel, 210);
        [_QRImageView setImageWithURL:_tag.QRThumbnail  effect:SDWebImageEffectNone];
    }else {
        _qrvIEW.hidden = YES;
        SetFrameWidth(_headerDescriptionLabel, 290);
    }
    
    if (NotEmptyValue(_tag.tagDescription)) {
        SetFrameY(_headerDescriptionLabel, headerHeight );
        _headerDescriptionLabel.hidden = NO;
        _headerDescriptionLabel.text = _tag.tagDescription;
        CGFloat descriptionHeight = CalculateLabelHeight(_headerDescriptionLabel);
        AdjustLabelHeight(_headerDescriptionLabel);
        headerHeight += (descriptionHeight + 14);
    }
    
    
    if (_tag.relevantTags && _tag.relevantTags.count > 0) {
        SetFrameY(_tagsCollectionView, headerHeight)
        [_tagsCollectionView reloadData];
        headerHeight += (_tagsCollectionView.frame.size.height + 14);
    }
    
    if (_tag.tagType == 1) {
        SetFrameY(_tabView, headerHeight);
        _bottomLine.hidden = YES;
        _tabView.hidden = NO;
        [self selectTagButtonWithIndex:0];
        headerHeight += _tabView.frame.size.height;
    }else {
        SetFrameY(_bottomLine, headerHeight - 0.5f);
        _bottomLine.hidden = NO;
        _tabView.hidden = YES;
    }
    
    SetFrameHeight(_headerInfoBgView, headerHeight);
    SetFrameHeight(_headerView, headerHeight);
    [self.tableView setTableHeaderView:nil];
    [self.tableView setTableHeaderView:_headerView];
    
    if (_tag.showPhoto) {
        NSMutableAttributedString *publishTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"我也来晒  #%@", _tag.name] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        NSInteger titleIndex = [publishTitle.string rangeOfString:_tag.name].location;
        [publishTitle addAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:13]} range:NSMakeRange(titleIndex, _tag.name.length)];
        [_publishBtn setAttributedTitle:publishTitle forState:UIControlStateNormal];
        
        UIEdgeInsets tableInsets = self.tableView.contentInset;
        tableInsets.bottom = _actionView.frame.size.height;
        self.tableView.contentInset = tableInsets;
    }else {
        [self.tableView setTableFooterView:nil];
        [_actionView removeFromSuperview];
    }
}

- (void)selectTagButtonWithIndex:(NSInteger)buttonIndex {
    UIColor *disableColor = [UIColor colorWithRed:0.59 green:0.59 blue:0.59 alpha:1];
    [_favoriteBtn setTitleColor:(buttonIndex == 0 ? NavigationBarTintColor : disableColor) forState:UIControlStateNormal];
    [_latestBtn setTitleColor:(buttonIndex == 1 ? NavigationBarTintColor : disableColor) forState:UIControlStateNormal];
}

- (void)updateSubscriptionButton {
    if (!_tag.subscribe) {
        _subscribeBtn.tintColor = NavigationBarTintColor;
        [_subscribeBtn setBackgroundImage:[[[UIImage imageNamed:@"btn-add friends-normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] stretchableImageWithLeftCapWidth:10 topCapHeight:5] forState:UIControlStateNormal];
        [_subscribeBtn setTitle:@"关注" forState:UIControlStateNormal];
        [_subscribeBtn setTitleColor:NavigationBarTintColor forState:UIControlStateNormal];
        [_subscribeBtn setImage:[[UIImage imageNamed:@"icon－add attention"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }else {
        _subscribeBtn.tintColor = [UIColor colorWithRed:0.69 green:0.69 blue:0.69 alpha:1];
        [_subscribeBtn setBackgroundImage:[[[UIImage imageNamed:@"btn-add friends-normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] stretchableImageWithLeftCapWidth:10 topCapHeight:5] forState:UIControlStateNormal];
        [_subscribeBtn setTitle:@"已关注" forState:UIControlStateNormal];
        [_subscribeBtn setTitleColor:[UIColor colorWithRed:0.69 green:0.69 blue:0.69 alpha:1] forState:UIControlStateNormal];
        [_subscribeBtn setImage:[[UIImage imageNamed:@"icon－ attention"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
}

- (IBAction)subscribe:(id)sender {
    _tag.subscribe = !_tag.subscribe;
    [self updateSubscriptionButton];
    [[YouPinAPI sharedAPI] subscribeTag:_tag.name value:_tag.subscribe callback:^(NSError *error) {
        if (error) {
            _tag.subscribe = NO;
            [self updateSubscriptionButton];
            [self showFailHUDWithTitle:@"订阅失败"];
        }
    }];
}

- (IBAction)showQRCodeView:(id)sender {
    TagQRViewController * QRController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagQRViewController"];
    QRController.QRCodeUrl = _tag.QRurl;
    [self.navigationController pushViewController:QRController animated:YES];
}

- (IBAction)selectFavorite:(id)sender {
    if(_currentTagListType == 1) return;
    
    [self selectTagButtonWithIndex:0];
    _currentTagListType = 1;
    [self showHUDViewWithTitle:@""];
    [self refreshData];
}

- (IBAction)selectLatest:(id)sender {
    if(_currentTagListType == 0) return;
    
    [self selectTagButtonWithIndex:1];
    _currentTagListType = 0;
    [self showHUDViewWithTitle:@""];
    [self refreshData];
}

- (void)refreshData {
    [[YouPinAPI sharedAPI] fetchTagTimelineWithTagName:self.tagName tagType:_currentTagListType startTimestamp:[self startTimestamp] endTimestamp:0 Sender:self callback:^(NSArray *items, Tag *tag, BOOL hasNext, NSError *error) {
        [self hideHUDView];
        if (!error) {
            _tag = tag;
            
            if (_tag) {
                if (_tag.relevantTags && _tag.relevantTags.count > 9) {
                    _hasMoreRelatedTags = YES;
                }
                
                _currentTagListType = _tag.tagType;
                
                [self updateHeaderView];
                _actionView.hidden = NO;
            }else {
                SetFrameHeight(_headerView, 0);
                [self.tableView setTableHeaderView:nil];
                [self.tableView setTableHeaderView:_headerView];
            }
            
            [self reloadItems:items hasNext:hasNext error:error];
            self.tableView.hidden = NO;
        }
    }];
}


- (void)loadNextData {
    [[YouPinAPI sharedAPI] fetchTagTimelineWithTagName:self.tagName tagType:_currentTagListType startTimestamp:0 endTimestamp:[self endTimestamp] Sender:self callback:^(NSArray *items, Tag *tag, BOOL hasNext, NSError *error) {
        [self loadNextItems:items hasNext:hasNext error:error];
    }];
}

- (void)loadNextItems:(NSArray *)items hasNext:(BOOL)hasNext error:(NSError *)error {
    if (!error) {
        if (items.count > 0) {
            [_tasteItems addObjectsFromArray:items];
            [self.tableView reloadData];
        }else {
            _hasNext = NO;
            [self.tableView dismissLoadMore];
        }
        
        [self.tableView endLoadMoreWithHandler:^(UIView *customView) {
            [((UIImageView *)customView) stopAnimating];
            customView.hidden = YES;
        }];
        
    }else {
        
    }
}

- (IBAction)publishPost:(id)sender {
    [PublishParamSet sharedInstance].tags = @[_tag.name];
    ShaibaViewController *tabController  = self.navigationController.viewControllers[0];
    [tabController presentCameraViewControllerForPublish];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(_tag.relevantTags) {
        return _hasMoreRelatedTags ? 10 : _tag.relevantTags.count;
    }else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RelatedTagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RelatedTagCell" forIndexPath:indexPath];
    Tag *relatedTag = _tag.relevantTags[indexPath.row];
    if (indexPath.row < 9) {
        cell.titleLabel.text = relatedTag.name;
    }else {
        cell.titleLabel.text = @"...";
    }
    
    cell.imageView.backgroundColor = [UIColor colorWithString:relatedTag.color];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 9) {
        RelatedTagsViewController *relatedTagController = [self.storyboard instantiateViewControllerWithIdentifier:@"RelatedTagsViewController"];
        relatedTagController.tagName = _tag.name;
        [self.navigationController pushViewController:relatedTagController animated:YES];
    }else {
        Tag *relatedTag = _tag.relevantTags[indexPath.row];
        TagViewController *tagController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
        tagController.tagName = relatedTag.name;
        [self.navigationController pushViewController:tagController animated:YES];
    }
}

@end
