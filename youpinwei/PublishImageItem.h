//
//  PublishImageItem.h
//  youpinwei
//
//  Created by tmy on 15/1/31.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishItem.h"

typedef enum : NSInteger {
    PublishImageSourceLocal,
    PublishImageSourceOnline
} PublishImageSource;

@interface PublishImageItem : PublishItem

@property (nonatomic) PublishImageSource source;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, readonly) NSString *imageName;

@end
