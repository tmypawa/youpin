//
//  PublishParamSet.m
//  youpinwei
//
//  Created by tmy on 14/12/12.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PublishParamSet.h"

@implementation PublishParamSet

+ (instancetype)sharedInstance {
    static PublishParamSet *SharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SharedInstance = [[self alloc] init];
    });
    
    return SharedInstance;
}

- (void)reset {
    self.tags = nil;
    self.goods = nil;
    self.goodsPhotoURLs = nil;
    self.photos = nil;
    self.texts = nil;
}

@end
