//
//  TagViewController.h
//  youpinwei
//
//  Created by tmy on 14-10-16.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "MainViewController.h"

@interface TagViewController : MainViewController<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) NSString *tagName;

@end
