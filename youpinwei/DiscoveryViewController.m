//
//  DiscoveryViewController.m
//  youpinwei
//
//  Created by tmy on 15/3/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaViewController.h"
#import "DiscoveryViewController.h"
#import "DiscoverySearchController.h"
#import "UITableViewCollection+LoadMore.h"
#import "UIScrollView+UzysAnimatedGifPullToRefresh.h"
#import "RecommendTag.h"
#import "DiscoveryBanner.h"
#import "TagViewController.h"
#import "RecommendTagViewCell.h"
#import "BannerViewController.h"

@implementation DiscoveryViewController
{
    UINavigationController       *_discoverySearchNavController;
    IBOutlet UIView *_headerView;
    IBOutlet UICollectionView *_topBannersCollectionView;
    IBOutlet UITableView *_tableView;
    IBOutlet UIButton *_searchBtn;
    NSMutableArray          *_topBanners;
    NSMutableArray          *_recommendTags;
    BOOL                    _hasNext;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _recommendTags = JTGlobalCache.discoveryRecommendTags;
        _topBanners = JTGlobalCache.discoveryBanners;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    [_searchBtn setBackgroundImage:[[UIImage imageNamed:@"bg_search"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    
    [self addLoadMoreView];
    [self loadPullRereshAnimation];
    
    
    if (_recommendTags.count == 0) {
        [self showHUDViewWithTitle:@""];
        [self refreshData];
    }else {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.tableView triggerPullToRefresh];
//        });
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [((ShaibaViewController *)self.rdv_tabBarController) setNavigationTitle:@"发现"];
}

- (void)addLoadMoreView {
    UIImageView *loadingMoreView = [[UIImageView alloc] initWithImage:[UIImage animatedImageNamed:@"R" duration:1.6]];
    loadingMoreView.frame = CGRectMake(0, 0, screenWidth, 56);
    loadingMoreView.center = CGPointMake(screenWidth/2, loadingMoreView.center.y);
    loadingMoreView.contentMode = UIViewContentModeCenter;
    loadingMoreView.backgroundColor = MAIN_CELL_BG_COLOR;
    [loadingMoreView stopAnimating];
    loadingMoreView.hidden = YES;
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.backgroundColor = LIST_BG_COLOR;
    self.tableView.backgroundView = nil;
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.tableView setupLoadMoreWithCustomView:loadingMoreView];
    [self.tableView addTarget:self forLoadMoreAction:@selector(loadNext:)];
}

- (void)loadPullRereshAnimation {
    NSMutableArray *pullAnimations = [[NSMutableArray alloc] init];
    NSMutableArray *loadingAnimations = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 23; ++i) {
        [pullAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
    for (int i = 0; i < 45; ++i) {
        [loadingAnimations addObject:[UIImage imageNamed:[NSString stringWithFormat:@"R%d", i+1]]];
    }
    
    
    __weak  DiscoveryViewController *_self = self;
    __weak  UITableView *__tableView = _tableView;
    [_tableView addPullToRefreshActionHandler:^{
        if (__tableView.superview) {
            [_self refreshData];
        }
    } ProgressImages:pullAnimations LoadingImages:loadingAnimations ProgressScrollThreshold:20 LoadingImagesFrameRate:32];
    _tableView.showAlphaTransition = NO;
    _tableView.showVariableSize = NO;
    _tableView.showPullToRefresh = YES;
    _tableView.showTitle = NO;
    
    [_tableView addTopInsetInPortrait:0 TopInsetInLandscape:0];
}


- (void)handleForDoubleTapTab {
    CGFloat baseOffset = -self.tableView.contentInset.top;
    if (self.tableView.contentOffset.y > baseOffset) {
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    }else if(!self.tableView.pullToRefreshView.state != UZYSGIFPullToRefreshStateLoading){
        [self.tableView triggerPullToRefresh];
    }
}

- (NSTimeInterval)endTimestamp {
    NSTimeInterval endTimestamp = 0;
    if (_recommendTags.count > 0) {
        endTimestamp = [[_recommendTags lastObject] updateTime];
    }
    return endTimestamp;
}

- (void)refreshData {
    [[YouPinAPI sharedAPI] fetchDiscoveryListWithEndTimestamp:0 Sender:self callback:^(NSArray *banners, NSArray *recommendTag, BOOL hasNext, NSError *error) {
        [self hideHUDView];
        
        if (_tableView.pullToRefreshView.state == UZYSGIFPullToRefreshStateLoading) {
            [_tableView stopRefreshAnimation];
        }
        
        if (!error) {
            if (recommendTag) {
                [_recommendTags removeAllObjects];
                [_recommendTags addObjectsFromArray:recommendTag];
            }
            
            if (banners) {
                [_topBanners removeAllObjects];
                [_topBanners addObjectsFromArray:banners];
                [_topBannersCollectionView reloadData];
                SetFrameHeight(_headerView, 207);
            }else if(!_topBanners || _topBanners.count == 0){
                SetFrameHeight(_headerView, 44);
            }
            
            [_tableView setTableHeaderView:nil];
            [_tableView setTableHeaderView:_headerView];
            
            
            [JTGlobalCache synchronize];
            
            [self.tableView reloadData];
            
            if (!_hasNext && _recommendTags.count > 0) {
                _hasNext = YES;
            }
        }else {
            
        }
//        
//        if (!self.tableView.pullToRefreshView) {
//            [self loadPullRereshAnimation];
//        }else {
//            [self.tableView stopRefreshAnimation];
//        }
//        
//        if (!self.tableView.loadMoreView) {
//            [self addLoadMoreView];
//        }
        
        if (_hasNext) {
            [self.tableView showLoadMoreView];
        }
    }];
}

- (void)loadNext:(UIImageView *)loadingView {
    if (!_hasNext) {
        return;
    }
    
    loadingView.hidden = NO;
    [loadingView startAnimating];
    [self.tableView showLoadMoreView];
    
    [self loadNextData];
}

- (void)loadNextData {
    [[YouPinAPI sharedAPI] fetchDiscoveryListWithEndTimestamp:[self endTimestamp] Sender:self callback:^(NSArray *banners, NSArray *recommendTag, BOOL hasNext, NSError *error) {
        if (recommendTag.count > 0) {
            [_recommendTags addObjectsFromArray:recommendTag];
            [JTGlobalCache synchronize];
            [self.tableView reloadData];
        }else {
            _hasNext = NO;
            [self.tableView dismissLoadMore];
        }
        
        [self.tableView endLoadMoreWithHandler:^(UIView *customView) {
            [((UIImageView *)customView) stopAnimating];
            customView.hidden = YES;
        }];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(!self.view.superview)
        return;
    
//    [self handleForNavigationAppear:scrollView];
    
    if (_recommendTags.count > 0 && !self.tableView.isLoadingMore && _hasNext) {
        float maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
        float offsetY = scrollView.contentOffset.y;
        if (offsetY >= maxOffsetY - 170) {
            [self.tableView startLoadMore];
        }
    }
}

- (IBAction)showSearchView:(id)sender {
    DiscoverySearchController *discoverySearchController = [self.storyboard instantiateViewControllerWithIdentifier:@"DiscoverySearchController"];
    discoverySearchController.delegate = self;
    
    _discoverySearchNavController = [[UINavigationController alloc] initWithRootViewController:discoverySearchController];
    
    UIViewController *rootController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootController addChildViewController:_discoverySearchNavController];
    [rootController.view addSubview:_discoverySearchNavController.view];
    [_discoverySearchNavController didMoveToParentViewController:rootController];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)dismissSearchView {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [_discoverySearchNavController willMoveToParentViewController:nil];
    [_discoverySearchNavController.view removeFromSuperview];
    [_discoverySearchNavController removeFromParentViewController];
    _discoverySearchNavController = nil;
}

- (void)discoverySearchControllerDidCancel:(DiscoverySearchController *)controller {
    [self dismissSearchView];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _topBanners ? _topBanners.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TopBanner" forIndexPath:indexPath];
    DiscoveryBanner *banner = _topBanners[indexPath.row];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:2000];
    UIImageView *bgView = (UIImageView *)[cell.contentView viewWithTag:1000];
    titleLabel.text = banner.name;
    [bgView setImageWithURL:banner.backgroundUrl effect:SDWebImageEffectFade];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DiscoveryBanner *banner = _topBanners[indexPath.row];
    switch (banner.type) {
        case DiscoveryBannerTypeActivity:
        case DiscoveryBannerTypeTag:
        {
            TagViewController *tagController = [self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
            tagController.tagName = banner.name;
            [self.navigationController pushViewController:tagController animated:YES];
        }
            break;
        case DiscoveryBannerTypeHTML:
        {
            BannerViewController *bannerController = [[BannerViewController alloc] init];
            bannerController.URL = [NSURL URLWithString:banner.htmlUrl];
            [self.navigationController pushViewController:bannerController animated:YES];
        }
            break;
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _recommendTags.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecommendTag *recommendTag = _recommendTags[indexPath.row];
    RecommendTagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecommendTagViewCell" forIndexPath:indexPath];
    __weak DiscoveryViewController *__self = self;
    cell.tag = indexPath.row + 1;
    cell.showMoreHandler = ^(){
        TagViewController *tagController = [__self.storyboard instantiateViewControllerWithIdentifier:@"TagViewController"];
        tagController.tagName = recommendTag.tagName;
        [__self.navigationController pushViewController:tagController animated:YES];
    };
    
    cell.itemSelectHandler = ^(NSInteger itemIndex) {
        DetailViewController *detailController = [__self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
        detailController.tasteId = ((RecommendTagItem *)recommendTag.productList[itemIndex]).productId;
        [__self.navigationController pushViewController:detailController animated:YES];
    };
    
    cell.titleLabel.text = recommendTag.tagName;
    [cell setItems:recommendTag.productList];
    return cell;
}

@end
