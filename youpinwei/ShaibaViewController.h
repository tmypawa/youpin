//
//  ShaibaViewController.h
//  youpinwei
//
//  Created by tmy on 14/11/27.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "RDVTabBarController.h"
#import "CameraViewController.h"
#import "PhotoSelectionController.h"

@interface ShaibaViewController : RDVTabBarController<CameraViewControllerDelegate, PhotoSelectionControllerDelegate, RDVTabBarControllerDelegate, UIAlertViewDelegate>

@property (nonatomic, readonly) UIView *navigationTitleView;

- (void)setNavigationTitle:(NSString *)title;
- (void)presentCameraViewControllerForPublish;

@end
