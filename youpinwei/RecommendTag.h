//
//  RecommendTag.h
//  youpinwei
//
//  Created by tmy on 15/3/27.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecommendTagItem.h"

@interface RecommendTag : NSObject

@property (nonatomic, strong) NSString *tagName;
@property (nonatomic, assign) NSTimeInterval updateTime;
@property (nonatomic, strong) NSArray *productList;

@end
