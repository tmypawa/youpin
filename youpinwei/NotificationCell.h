//
//  NotificationCell.h
//  youpinwei
//
//  Created by tmy on 14/10/22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *avatarView;
@property (strong, nonatomic) IBOutlet UILabel *actionLabel;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UIImageView *titleBgView;
@property (strong, nonatomic) IBOutlet UILabel *goodsTitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *unreadIconView;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailView;


@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *timeIconView;

+ (CGFloat)heightWithTitle:(NSString *)title;
- (void)updateLayout;
- (void)setAvatarViewsWithURLs:(NSArray *)URLs;

@end
