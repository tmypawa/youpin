//
//  PostEditViewController.m
//  youpinwei
//
//  Created by tmy on 15/1/20.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PostEditViewController.h"
#import "PostContentItem.h"

@interface PostEditViewController ()

@end

@implementation PostEditViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleForTasteDidEditSuccess:) name:YouPinNotificationTasteItemDidEditedSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleForTasteFailedToEdit:) name:YouPinNotificationTasteItemFailedToEdit object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateViewFromExternalParams {
    if (_snapshotTaste) {
        [self restoreViewFromSnapshot];
    }else if(self.editedTasteItem){
        if (self.editedTasteItem.contentItems && self.editedTasteItem.contentItems.count > 0) {
            for (PostContentItem *item in self.editedTasteItem.contentItems) {
                if (item.type == PostContentTypeText) {
                    PublishTextItem *textItem = [[PublishTextItem alloc] init];
                    textItem.text = item.text;
                    [_publishContentItems addObject:textItem];
                    [_publishTextItems addObject:textItem];
                }else if(item.type == PostContentTypeImage) {
                    PublishImageItem *imageItem = [[PublishImageItem alloc] init];
                    imageItem.source = PublishImageSourceOnline;
                    imageItem.imagePath = item.image;
                    [_publishContentItems addObject:imageItem];
                    [_publishImageItems addObject:imageItem];
                }
            }
        }
        
        _titleTextField.text = self.editedTasteItem.title;
        [PublishParamSet sharedInstance].tags = self.editedTasteItem.tags;
        [PublishParamSet sharedInstance].goods = self.editedTasteItem.goods;
    
    }
    
    [_tableView reloadData];
}

- (void)saveTasteSnapshot {
    [super saveTasteSnapshot];
    if (self.editedTasteItem) {
        _snapshotTaste.productId = self.editedTasteItem.productId;
    }

    [JTGlobalCache synchronize];
}

- (TasteItem *)generateTasteItem {
    TasteItem *taste = [super generateTasteItem];
    taste.productId = _snapshotTaste.productId;
    
    return taste;
}

- (void)dismiss {
    MBProgressHUD *hudView = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hudView.labelText = @"更新中...";
}

- (void)doPublishWithTaste:(TasteItem *)taste {
    [[PostManager defaultManager] updatePost:taste];
}

- (void)handleForTasteDidEditSuccess:(NSNotification *)notification {
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleForTasteFailedToEdit:(NSNotification *)notification  {
    [self.navigationController showFailHUDWithTitle:@"更新失败"];
}

@end
