//
//  Notification.m
//  youpinwei
//
//  Created by tmy on 14/10/22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "Notification.h"
#import "JTAddressBookManager.h"

@implementation Notification

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.content forKey:@"content"];
    [encoder encodeDouble:self.createTime forKey:@"createTime"];
    [encoder encodeDouble:self.updateTime forKey:@"updateTime"];
    [encoder encodeObject:self.headImage forKey:@"headImage"];
    [encoder encodeObject:self.headImageList forKey:@"headImageList"];
    [encoder encodeObject:self.nick forKey:@"nick"];
    [encoder encodeObject:self.notifyId forKey:@"notifyId"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeObject:self.productId forKey:@"productId"];
    [encoder encodeObject:self.toNick forKey:@"toNick"];
    [encoder encodeObject:self.toPhone forKey:@"toPhone"];
    [encoder encodeObject:self.type forKey:@"type"];
    [encoder encodeObject:self.productImage forKey:@"productImage"];
    [encoder encodeObject:self.productName forKey:@"productName"];
    [encoder encodeObject:self.productDesc forKey:@"productDesc"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.tips forKey:@"tips"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.content = [decoder decodeObjectForKey:@"content"];
        self.createTime = [decoder decodeDoubleForKey:@"createTime"];
        self.updateTime = [decoder decodeDoubleForKey:@"updateTime"];
        self.headImage = [decoder decodeObjectForKey:@"headImage"];
        self.headImageList = [decoder decodeObjectForKey:@"headImageList"];
        self.nick = [decoder decodeObjectForKey:@"nick"];
        self.notifyId = [decoder decodeObjectForKey:@"notifyId"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
        self.productId = [decoder decodeObjectForKey:@"productId"];
        self.toNick = [decoder decodeObjectForKey:@"toNick"];
        self.toPhone = [decoder decodeObjectForKey:@"toPhone"];
        self.type = [decoder decodeObjectForKey:@"type"];
        self.productImage = [decoder decodeObjectForKey:@"productImage"];
        self.productName = [decoder decodeObjectForKey:@"productName"];
        self.productDesc = [decoder decodeObjectForKey:@"productDesc"];
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.tips = [decoder decodeObjectForKey:@"tips"];
    }
    return self;
}

- (NSString *)displayName{
    NSString *displayName = @"";
    if (JTGlobalCache.currentUser && [JTGlobalCache.currentUser.userId isEqualToString:self.userId]) {
        displayName = self.nick;
    }else {
        JTAddressBookPerson *person = [[JTAddressBookManager defaultManager] personForPhoneNumber:self.phone];
        if (person) {
            displayName = person.fullName;
        }else {
            displayName = self.nick;
        }
    }
    
    return displayName;
}

@end
