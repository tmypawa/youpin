//
//  PublishImageCell.m
//  youpinwei
//
//  Created by tmy on 15/1/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishImageCell.h"

@implementation PublishImageCell
{
    IBOutlet UIView *_bgView;
    
}
@synthesize bgView = _bgView;

- (void)awakeFromNib {
    _bgView.layer.borderColor = PUBLISH_BORDER_COLOR.CGColor;
    _bgView.layer.borderWidth = 0.5f;
    _bgView.backgroundColor = [UIColor whiteColor];
    
    self.thumbnailView.clipsToBounds = YES;
    self.thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:255/255.0];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)prepareForMove {
    [super prepareForMove];
    _bgView.hidden = YES;
}

- (void)prepareForMoveSnapshot {
    [self prepareForShow];
}

- (void)prepareForShow {
    _bgView.hidden = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
