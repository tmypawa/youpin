//
//  AppDelegate.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "AppDelegate.h"
#import "TaobaoURLProtocol.h"
#import "BPush.h"
#import "YouPin.h"
#import "iRate.h"

@implementation AppDelegate

+ (void)initialize {
    [iRate sharedInstance].daysUntilPrompt = 0;
    [iRate sharedInstance].usesUntilPrompt = 2;
    [iRate sharedInstance].appStoreID = 923822302;
    [iRate sharedInstance].message = @"";
    [iRate sharedInstance].messageTitle = @"程序小哥累觉不爱，给个好评点亮希望~~";
    [iRate sharedInstance].cancelButtonLabel = @"残忍的拒绝";
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [BPush setupChannel:launchOptions];
    [BPush setDelegate:[YouPin sharedInstance]];
    
    [[YouPin sharedInstance] doInitialCheck];
    
    NSDictionary *remoteNotification = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    [YouPin sharedInstance].receivedRemoteNotification = remoteNotification;
    [application setApplicationIconBadgeNumber:0];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {  
    return [[YouPin sharedInstance] handleForOpenURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    NSLog(@"register user notification success!!!!");
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"register device token: %@", [deviceToken description]);
    [BPush registerDeviceToken: deviceToken];
    [BPush bindChannel];
 
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[YouPin sharedInstance] handleForReceivedRemoteNotification:userInfo];
}
 


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [application setApplicationIconBadgeNumber:0];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[YouPin sharedInstance] handleForForeground];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [UMSocialSnsService  applicationDidBecomeActive];
    [[YouPin sharedInstance] handleForAppActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
