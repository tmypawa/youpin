//
//  PublishStatusController.m
//  youpinwei
//
//  Created by tmy on 15/2/6.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishStatusController.h"
#import "PublishStatusCell.h"
#import "PublishStatusItem.h"

@implementation PublishStatusController
{
    IBOutlet UITableView *_tableView;
    NSMutableArray      *_statusItems;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _statusItems = [[NSMutableArray alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTasteItemDidSend:) name:YouPinNotificationNewTasteItemDidSend object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTasteItemDidPublish:) name:YouPinNotificationNewTasteItemDidPublish object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTasteItemFailedToPublish:) name:YouPinNotificationNewTasteItemFailedToPublish object:nil];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:255/255.0];
    _tableView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:255/255.0];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 65;
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)adjustViewHeight {
    CGFloat height = _statusItems.count * _tableView.rowHeight;
    SetFrameHeight(self.view, height + 5);
    SetFrameHeight(_tableView, height);
    SetFrameOrigin(_tableView, 0, 0);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(publishStatusControllerDidChangeContentHeight:)]) {
        [self.delegate publishStatusControllerDidChangeContentHeight:self];
    }
}

- (void)newTasteItemDidSend:(NSNotification *)notification {
    TasteItem *newPost = notification.userInfo[YouPinNotificationPublishTasteItemKey];
    
    NSUInteger existIndex = [_statusItems indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [[obj post].draftIdentifier isEqualToString:newPost.draftIdentifier];
    }];
    
    if (existIndex == NSNotFound) {
        PublishStatusItem *item = [[PublishStatusItem alloc] init];
        item.post = newPost;
        item.status = PublishStatusCellTypeSending;
        [_statusItems insertObject:item atIndex:0];
        
        [_tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [self adjustViewHeight];
    }else {
        PublishStatusItem *item = _statusItems[existIndex];
        item.status = PublishStatusCellTypeSending;
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:existIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)newTasteItemDidPublish:(NSNotification *)notification {
    TasteItem *newTasteItem = notification.userInfo[YouPinNotificationPublishTasteItemKey];
    NSUInteger existIndex = [_statusItems indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [[obj post].draftIdentifier isEqualToString:newTasteItem.draftIdentifier];
    }];
    
    if (existIndex != NSNotFound) {
        [_statusItems removeObjectAtIndex:existIndex];
        [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:existIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [self adjustViewHeight];
    }
}

- (void)newTasteItemFailedToPublish:(NSNotification *)notification {
    TasteItem *newTasteItem = notification.userInfo[YouPinNotificationPublishTasteItemKey];
    NSUInteger existIndex = [_statusItems indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [[obj post].draftIdentifier isEqualToString:newTasteItem.draftIdentifier];
    }];
    
    if (existIndex != NSNotFound) {
        PublishStatusItem *statusItem = _statusItems[existIndex];
        statusItem.status = PublishStatusCellTypeFailed;
        
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:existIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (PublishStatusCell *)statusCellForCellElement:(UIView *)view {
    PublishStatusCell *cell = nil;
    if (iOSVersion < 8.0f) {
        cell = (PublishStatusCell *)view.superview.superview.superview.superview;
    }else {
        cell = (PublishStatusCell *)view.superview.superview.superview;
    }
    
    return cell;
}

- (void)resendPost:(UIButton *)sender {
    PublishStatusCell *cell = [self statusCellForCellElement:sender];
    if (cell) {
        NSInteger rowIndex = [_tableView indexPathForCell:cell].row;
        PublishStatusItem *item = _statusItems[rowIndex];
        item.status = PublishStatusCellTypeSending;
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
        [[PostManager defaultManager] publishPost:item.post];
    }
}

- (void)cancelPost:(UIButton *)sender {
    PublishStatusCell *cell = [self statusCellForCellElement:sender];
    if (cell) {
        NSInteger rowIndex = [_tableView indexPathForCell:cell].row;
        TasteItem *post = [_statusItems[rowIndex] post];
        [[PostManager defaultManager] cancelPublishForPost:post];
        
        [_statusItems removeObjectAtIndex:rowIndex];
        [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:rowIndex inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [self adjustViewHeight];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _statusItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublishStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PublishStatusCell" forIndexPath:indexPath];
    if (cell.refteshButton.tag == 0) {
        [cell.refteshButton addTarget:self action:@selector(resendPost:) forControlEvents:UIControlEventTouchUpInside];
        [cell.deleteButton addTarget:self action:@selector(cancelPost:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.refteshButton.tag = indexPath.row + 1;
    cell.deleteButton.tag = indexPath.row + 1;
    
    PublishStatusItem *item = _statusItems[indexPath.row];
    TasteItem *post = item.post;
    cell.titleLabel.text = [post displayTitle];
    if (NotEmptyValue(post.littleThumbnailImage)) {
        if ([post.littleThumbnailImage hasPrefix:@"http"]) {
            [cell.thumbnailView setImageWithURL:post.littleThumbnailImage];
        }else {
            NSString *thumnailPath = [[PostManager defaultManager] postImageFileWithImageName:post.littleThumbnailImage];
            cell.thumbnailView.image = [UIImage imageWithContentsOfFile:thumnailPath];
        }
    }
    
    cell.status = item.status;
    
    [cell updateViewForStatus];
    
    return cell;
}

@end
