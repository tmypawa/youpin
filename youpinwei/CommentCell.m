//
//  CommentCell.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "CommentCell.h"
#import "UIKit+RoundImage.h"

@implementation CommentCell
{
    IBOutlet UIView *_sepratorLineView;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    self.avatarView.clipsToBounds = YES;
    [self.avatarView makeRound];
    self.contentLabel.textColor = CommentCellTitleColor;
    self.contentLabel.tintColor = NavigationBarTintColor;
    self.timeLabel.textColor = CommentCellTitleColor;
    self.infoLabel.textColor = MAIN_CELL_COMMENT_NAME_COLOR;
    self.avatarView.backgroundColor = DEFAULT_IMAGE_BG_COLOR;
    self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UIEdgeInsets)layoutMargins {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)heightForComment:(NSString *)comment {
    CGFloat titleHeight = [comment boundingRectWithSize:CGSizeMake(238, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size.height;
    return titleHeight + 38;
}

- (void)updateLayout {
    CGFloat height = [self.contentLabel.attributedText boundingRectWithSize:CGSizeMake(self.contentLabel.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    height = ceil(height);
    SetFrameHeight(self.contentLabel, height);
    
    CGSize fitTimeSize = [self.timeLabel sizeThatFits:CGSizeMake(self.timeLabel.frame.size.width, CGFLOAT_MAX)];
    fitTimeSize.height =  self.timeLabel.frame.size.height;
    SetFrameWidth(self.timeLabel, fitTimeSize.width);
    SetFrameX(self.timeLabel, screenWidth - self.timeLabel.frame.size.width - 10);
    self.timeLabel.center = CGPointMake(self.timeLabel.center.x, self.infoLabel.center.y);
}

@end
