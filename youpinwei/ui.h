//
//  ui.h
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#ifndef youpinwei_ui_h
#define youpinwei_ui_h

#define GlobalTintColor [UIColor colorWithRed:1 green:1 blue:1 alpha:1]
#define NavigationBarTintColor [UIColor colorWithRed:0.99 green:0.44 blue:0.39 alpha:1]
#define LoginNavigationBarTintColor [UIColor colorWithRed:0.99 green:0.44 blue:0.39 alpha:1]
#define LIST_BG_COLOR [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]

#define DEFAULT_AVATAR [UIImage imageNamed:@"avatar_default"]
#define DEFAULT_TIMELINE_PHOTO [UIImage imageNamed:@"bg_default"]
#define PHOTO_DEFAULT_TITLE_COLOR [UIColor colorWithRed:0.45 green:0.45 blue:0.45 alpha:1]

#define MAIN_CELL_BG_COLOR [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]
#define MainCellPersonTitleColor [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1]
#define MainCellTitleColor [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1]
#define MAIN_CELL_FIRST_SENTENCE_COLOR NavigationBarTintColor
#define MainCellPriceColor [UIColor colorWithRed:0.34 green:0.34 blue:0.34 alpha:1]
#define MainCellInfoColor [UIColor colorWithRed:0.73 green:0.73 blue:0.73 alpha:1]
#define MAIN_CELL_COMMENT_NAME_COLOR [UIColor colorWithRed:0.24 green:0.57 blue:0.58 alpha:1]
#define MAIN_CELL_ACTION_BUTTON_TITLE_COLOR [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1]
#define TAG_TEXT_COLOR [UIColor colorWithRed:0.29 green:0.56 blue:0.89 alpha:1]

#define CommentCellPosterTitleColor [UIColor colorWithRed:0.27 green:0.68 blue:0.65 alpha:1]
#define CommentCellTitleColor [UIColor colorWithRed:0.24 green:0.24 blue:0.24 alpha:1]
#define CommentCellInfoColor [UIColor colorWithRed:0.69 green:0.69 blue:0.69 alpha:1]

#define GoodsCellTitleColor [UIColor colorWithRed:0.49 green:0.49 blue:0.49 alpha:1]

#define PUBLISH_NAV_BG_COLOR [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:255/255.0]
#define PUBLISH_TITLE_COLOR [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:255/255.0]
#define PUBLISH_PLACEHOLDER_COLOR [UIColor colorWithRed:0.54 green:0.54 blue:0.54 alpha:1]
#define PUBLISH_PRICE_COLOR [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1]
#define PUBLISH_BORDER_COLOR [UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:255/255.0]

#define LOGIN_TEXT_FIELD_COLOR [UIColor colorWithRed:1 green:1 blue:1 alpha:1]

#define SETTING_BG_COLOR [UIColor colorWithRed:0.96 green:0.94 blue:0.94 alpha:1]

#define DEFAULT_IMAGE_BG_COLOR [UIColor colorWithWhite:1 alpha:1]

//LEVEL COLORS
static UIColor * LevelColor(NSInteger level) {
    UIColor *color = nil;
    switch (level) {
        case 1:
            color = [UIColor colorWithRed:1 green:0.69 blue:0.69 alpha:1];
            break;
        case 2:
            color = [UIColor colorWithRed:1 green:0.61 blue:0.27 alpha:1];
            break;
        case 3:
            color = [UIColor colorWithRed:1 green:0.77 blue:0.24 alpha:1];
            break;
        case 4:
            color = [UIColor colorWithRed:0.67 green:0.84 blue:0.34 alpha:1];
            break;
        case 5:
            color = [UIColor colorWithRed:0.44 green:0.86 blue:0.74 alpha:1];
            break;
        case 6:
            color = [UIColor colorWithRed:0.4 green:0.63 blue:0.87 alpha:1];
            break;
        case 7:
            color = [UIColor colorWithRed:0.49 green:0.49 blue:0.69 alpha:1];
            break;
        default:
            break;
    }
    
    return color;
}



#endif
