//
//  CategoryChooseCell.m
//  youpinwei
//
//  Created by tmy on 15/3/21.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "CategoryChooseCell.h"

@implementation CategoryChooseCell

- (void)awakeFromNib {
    self.thumbnailView.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
    self.thumbnailView.layer.cornerRadius = 4;
    self.thumbnailView.layer.masksToBounds = YES;
}

@end
