//
//  PublishCommentViewController.m
//  youpinwei
//
//  Created by tmy on 15/3/18.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishCommentViewController.h"
#import "JTFixedTextView.h"

@implementation PublishCommentViewController
{
    IBOutlet JTFixedTextView *_textInputView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _textInputView.tintColor = NavigationBarTintColor;
    if (!self.replyComment) {
        _textInputView.placeholderText = @"填写评论...";
    }else {
        _textInputView.placeholderText = [NSString stringWithFormat:@"回复%@:", [self.replyComment displayNameWithPosterId: self.tasteItem.userId]];
    }
    
    [_textInputView becomeFirstResponder];
}

- (IBAction)cancel:(id)sender {
    if (NotEmptyValue(_textInputView.text)) {
        AlertWithActions(@"确定要取消评论吗?", @"", @"取消", @"确定");
    }else {
        [_textInputView resignFirstResponder];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)publish:(id)sender {
    NSString *comment = [_textInputView.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    if (NotEmptyValue(comment)) {
        [_textInputView resignFirstResponder];
        [self publishComment:comment];
    }else {
        Alert(@"评论内容不能为空", @"", @"确定");
    }
}

- (void)publishComment:(NSString *)comment {
    NSString *toSendComment = @"";
    
    Comment *newComment = [[Comment alloc] init];
    newComment.headImage = JTGlobalCache.currentUser.avatar;
    newComment.nick = JTGlobalCache.currentUser.nickname;
    newComment.extraId = JTGlobalCache.currentUser.phone;
    newComment.productId = self.tasteItem.productId;
    newComment.createTime = [[NSDate date] timeIntervalSince1970]*1000;
    
    if (!self.replyComment) {
        toSendComment = comment;
        newComment.receiverNick = @"";
        newComment.receiverPhone = @"";
    }else {
        toSendComment = comment;
        newComment.receiverNick = self.replyComment.nick;
        newComment.receiverPhone = self.replyComment.extraId;
        newComment.receiverId = self.replyComment.userId;
    }
    
    newComment.comment = toSendComment;
    newComment.requestId = [[NSUUID UUID] UUIDString];
  
    [self showGlobalHUDViewWithTitle:@"正在发表..."];
    
    [[YouPinAPI sharedAPI] publishCommentWithTasteId:self.tasteItem.productId comment:newComment replyTargetPhone:self.replyComment ? self.replyComment.extraId : nil sender:self callback:^(Comment *newComment, NSError *error) {
        [self hideGlobalHUDView];
        if (!error) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(publishCommentViewController:didPublishComment:)]) {
                [self.delegate publishCommentViewController:self didPublishComment:newComment];
            }
            
            [self showSuccessHUDWithTitle:@"评论成功!"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            });
        }else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(publishCommentViewControllerFailedToPublish:)]) {
                [self.delegate publishCommentViewControllerFailedToPublish:self];
            }
            
            [self showFailHUDWithTitle:@"评论失败"];
        }
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [_textInputView resignFirstResponder];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

 

@end
