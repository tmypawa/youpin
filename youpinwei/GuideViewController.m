//
//  GuideViewController.m
//  youpinwei
//
//  Created by tmy on 14/11/20.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "GuideViewController.h"

#define DURATION 30

@implementation GuideViewController
{
    IBOutlet UIView *_floatViewContainer;

    IBOutlet UIView *_bottomView;
    
    UIImageView *_floatImageViewLeft;
    UIImageView *_floatImageViewRight;
    CGFloat   _movingSpeed;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    
    if (isIPhone4) {
        [self adjustForiPhone4];
    }
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageFromColor:[UIColor clearColor]]];
    
    [self loadImages];
    
    _movingSpeed = _floatImageViewLeft.frame.size.width / DURATION;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self moveAnimationForLeftImageView];
    [self moveAnimationForRightImageView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adjustForiPhone4 {
    if (isIPhone4) {
      
    }
}

- (void)loadImages {
    _floatImageViewLeft = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1000, 450)];
    _floatImageViewLeft.contentMode = UIViewContentModeScaleAspectFit;
    _floatImageViewLeft.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"guide_1" ofType:@"jpg"]];
    [_floatViewContainer insertSubview:_floatImageViewLeft atIndex:0];
    
    _floatImageViewRight = [[UIImageView alloc] initWithFrame:CGRectMake(999, 0, 764, 450)];
    _floatImageViewRight.contentMode = UIViewContentModeScaleAspectFit;
    _floatImageViewRight.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"guide_2" ofType:@"jpg"]];
    [_floatViewContainer insertSubview:_floatImageViewRight atIndex:0];
}

- (void)moveAnimationForLeftImageView {
    float distance = _floatImageViewLeft.frame.origin.x + _floatImageViewLeft.frame.size.width;
    float actualDuration = distance / _movingSpeed;
    
    [UIView animateWithDuration:actualDuration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        SetFrameX(_floatImageViewLeft, - _floatImageViewLeft.frame.size.width);
    } completion:^(BOOL finished) {
        if (finished) {
            SetFrameX(_floatImageViewLeft, _floatImageViewRight.frame.size.width - 2);
            [self moveAnimationForLeftImageView];
        }else {
            [_floatImageViewLeft.layer removeAllAnimations];
        }
    }];
}

- (void)moveAnimationForRightImageView {
    float distance = _floatImageViewRight.frame.origin.x + _floatImageViewRight.frame.size.width;
    float actualDuration = distance / _movingSpeed;
    [UIView animateWithDuration:actualDuration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        SetFrameX(_floatImageViewRight, - _floatImageViewRight.frame.size.width);
    } completion:^(BOOL finished) {
        if (finished) {
            SetFrameX(_floatImageViewRight, _floatImageViewLeft.frame.size.width - 2);
            [self moveAnimationForRightImageView];
        }else {
            [_floatImageViewRight.layer removeAllAnimations];
        }
    }];
}

- (void)stopMovingAnimation {
    [_floatImageViewLeft.layer removeAllAnimations];
    [_floatImageViewRight.layer removeAllAnimations];
}

- (void)releaseImages {
    _floatImageViewLeft.image = nil;
    _floatImageViewRight.image = nil;
}
 
- (IBAction)next:(id)sender {
    [UIView animateWithDuration:0.4f animations:^{
        SetFrameY(_floatViewContainer, - _floatViewContainer.frame.size.height);
        _bottomView.alpha = 0;
    } completion:^(BOOL finished) {
        [self stopMovingAnimation];
        [self releaseImages];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(guideViewControllerDidFinishGuiding:)]) {
            [self.delegate guideViewControllerDidFinishGuiding:self];
        }
    }];
}
 
- (IBAction)swipeGestureDidRecognizer:(id)sender {
    [self next:nil];
}

@end
