//
//  TagListCell.m
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "TagListCell.h"

@implementation TagListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _tagView.font = [UIFont systemFontOfSize:14];
    _tagView.scrollEnabled = NO;
    _tagView.backgroundColor = [UIColor clearColor];
    [_tagView setTagBackgroundColor:[UIColor clearColor]];
    _tagView.borderWidth = 0;
    _tagView.labelMargin = 0;
    _tagView.horizontalPadding = 3;
    _tagView.verticalPadding = 0;
    _tagView.bottomMargin = 0;
    [_tagView setCornerRadius:4];
    [_tagView setTextColor:TAG_TEXT_COLOR];
    _tagView.highlightedBackgroundColor = [UIColor colorWithRed:0.87 green:0.87 blue:0.87 alpha:1];
    _tagView.textShadowOffset = CGSizeMake(0, 0);
    _tagView.automaticResize = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateLayout {
    SetFrameY(self.tagView, 15);
}

+ (CGFloat)heightForTags:(NSArray *)tags {
    NSString *tagStr = [tags componentsJoinedByString:@" #"];
    tagStr = [NSString stringWithFormat:@"#%@", tagStr];
    CGFloat titleHeight = [tagStr boundingRectWithSize:CGSizeMake(300, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
    return titleHeight + 35;
}

@end
