//
//  GoodsChooseCell.h
//  youpinwei
//
//  Created by tmy on 14-8-22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsChooseCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headerView;
@property (strong, nonatomic) IBOutlet UIImageView *circleView;
@property (strong, nonatomic) IBOutlet UILabel *_timeLabel;
@property (strong, nonatomic) IBOutlet UIView *timelineCircleView;

 

+ (CGFloat)heightWithTitle:(NSString *)title;
- (void)updateTimelineViewWithTimeShowing:(BOOL)isTimeShow;

@end
