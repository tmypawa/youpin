//
//  TaobaoURLProtocol.m
//  youpinwei
//
//  Created by tmy on 14-8-13.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "TaobaoURLProtocol.h"

#define REQUEST_HANDLING_KEY @"REQUEST_HANDLING_KEY"

static id<TaobaoURLProtocolDelegate> Delegate = nil;

@implementation TaobaoURLProtocol
{
    NSURLConnection     *_con;
    NSMutableData       *_data;
}

+ (void)setDelegate:(id<TaobaoURLProtocolDelegate>)delegate {
    Delegate = delegate;
}

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
    NSString *url = request.URL.description;
    if ([url rangeOfString:@"http://api.m.taobao.com/rest/h5ApiUpdate.do"].location != NSNotFound && [url rangeOfString:@"api=mtop.order.queryOrderList"].location != NSNotFound) {
        if ([[NSURLProtocol propertyForKey:REQUEST_HANDLING_KEY inRequest:request] boolValue]) {
            return NO;
        }

        return YES;
    }else {
        return NO;
    }
}


+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {
    return request;
}


+ (BOOL)requestIsCacheEquivalent:(NSURLRequest *)a toRequest:(NSURLRequest *)b {
    return [super requestIsCacheEquivalent:a toRequest:b];
}


- (void)startLoading {
    _data = [[NSMutableData alloc] init];
    NSMutableURLRequest *request = [self.request mutableCopy];
    request.cachePolicy = NSURLCacheStorageNotAllowed;
    [NSURLProtocol setProperty:@(YES) forKey:REQUEST_HANDLING_KEY inRequest:request];
    _con = [NSURLConnection connectionWithRequest:request delegate:self];
}

- (void)stopLoading {
//    if (_con) {
//        _data = nil;
//        [_con cancel];
//        _con = nil;
//    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    [self.client URLProtocol:self wasRedirectedToRequest:request redirectResponse:response];
    return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    [self.client  URLProtocol:self didReceiveAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    [self.client URLProtocol:self didCancelAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_data appendData:data];
    [self.client URLProtocol:self didLoadData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.client URLProtocolDidFinishLoading:self];
    NSString *responseContent =  [[NSString alloc] initWithData:_data encoding:NSUTF8StringEncoding];
    BOOL success = [responseContent rangeOfString:@"调用成功"].location != NSNotFound;
    if (success && Delegate && [Delegate respondsToSelector:@selector(TaobaoURLProtocol:didFetchOrderList:)    ]) {
        [Delegate TaobaoURLProtocol:self didFetchOrderList:responseContent];
    }
    _con = nil;
    _data = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.client URLProtocol:self didFailWithError:error];
    _con = nil;
    _data = nil;
}


@end
