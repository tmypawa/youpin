//
//  CenterNavigationController.h
//  youpinwei
//
//  Created by tmy on 14-8-22.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICSDrawerController.h"

@interface CenterNavigationController : UINavigationController<ICSDrawerControllerChild, ICSDrawerControllerPresenting>

@end
