//
//  PersonalInfoHeaderView.m
//  youpinwei
//
//  Created by tmy on 14/12/9.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "PersonalInfoHeaderView.h"
#import "UIKit+RoundImage.h"



@implementation PersonalInfoHeaderView
{
    IBOutlet UIImageView *_currentLevelIconView;
    
    IBOutlet UIImageView *_nextLevelIconView;
}

- (void)awakeFromNib {
    self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    self.pointsLabel.textColor = [UIColor colorWithRed:0.61 green:0.61 blue:0.67 alpha:1];
    [self.avatarView makeRound];
    self.avatarView.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    SetFrameOrigin(self.friendBadgeView, 117, 11);
    self.friendBadgeView.hidesWhenZero = YES;
    self.friendBadgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentNone;
    self.friendBadgeView.verticalAlignment = M13BadgeViewVerticalAlignmentNone;
    self.friendBadgeView.badgeBackgroundColor = NavigationBarTintColor;
    self.friendBadgeView.textColor = [UIColor whiteColor];
    self.friendBadgeView.font = [UIFont systemFontOfSize:12];
    self.friendBadgeView.text = @"0";
    
    SetFrameOrigin(self.notificationBadgeView, 90, 12);
    self.notificationBadgeView.hidesWhenZero = YES;
    self.notificationBadgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentNone;
    self.notificationBadgeView.verticalAlignment = M13BadgeViewVerticalAlignmentNone;
    self.notificationBadgeView.badgeBackgroundColor = NavigationBarTintColor;
    self.notificationBadgeView.textColor = [UIColor whiteColor];
    self.notificationBadgeView.font = [UIFont systemFontOfSize:12];
    self.notificationBadgeView.text = @"0";
    
    [self.addFriendButton setTitleColor:NavigationBarTintColor forState:UIControlStateNormal];
    [self.addFriendButton setBackgroundImage:[[UIImage imageNamed:@"btn-add friends-normal"] stretchableImageWithLeftCapWidth:10 topCapHeight:15] forState:UIControlStateNormal];
    
    self.addFriendButton.hidden = YES;
    self.friendStatusView.hidden = YES;
}

- (void)updateLayout {
    [self.nicknameBtn sizeToFit];
    self.sexIconView.center = CGPointMake(0, self.nicknameBtn.center.y);
    SetFrameX(self.sexIconView, CGRectGetMaxX(self.nicknameBtn.frame) + 6);
}

- (void)showLevelProgress:(CGFloat)progress currentLevel:(NSInteger)currentLevel nextLevel:(NSInteger)nextLevel {
    if (progress > 0) {
        CGFloat totalLength = _nextLevelIconView.frame.origin.x - _nextLevelIconView.frame.size.width;
        CGFloat currentLength = totalLength * progress;
        SetFrameWidth(self.leverProgressView, currentLength);
        _currentLevelIconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_lv_%ld", currentLevel]];
        _nextLevelIconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_level%ld", nextLevel]];
     }
}

@end
