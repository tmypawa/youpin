//
//  DialogViewController.h
//  youpinwei
//
//  Created by tmy on 14/11/28.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DialogViewControllerDelegate;

@interface DialogViewController : UIViewController

@property (nonatomic, assign) id<DialogViewControllerDelegate> delegate;
@property (nonatomic) NSInteger tag;

- (void)setTitle:(NSString *)title subtitle:(NSString *)subtitle icon:(UIImage *)iconImage descriptionText:(NSString *)descriptionText okButtonTitle:(NSString *)okTitle cancelButtonTitle:(NSString *)cancelTitle;

@end


@protocol DialogViewControllerDelegate <NSObject>

- (void)dialogViewController:(DialogViewController *)controller didFinishWithResult:(BOOL)result;

@end