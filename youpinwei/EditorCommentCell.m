//
//  EditorCommentCell.m
//  youpinwei
//
//  Created by tmy on 15/2/4.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "EditorCommentCell.h"
#import "UIKit+RoundImage.h"

@implementation EditorCommentCell
{
    IBOutlet UIView *_bottomLine;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.avatarView makeRound];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateLayout {
    AdjustLabelHeight(self.contentLabel);
    CGFloat titleHeight = CalculateLabelHeight(self.contentLabel);
    SetFrameY(_bottomLine, titleHeight + 40 - 0.5f);
}

+ (CGFloat)heightForText:(NSString *)text {
    CGFloat titleHeight = [text boundingRectWithSize:CGSizeMake(246, CGFLOAT_MAX) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12]} context:nil].size.height;
    return titleHeight + 40;
}

@end
