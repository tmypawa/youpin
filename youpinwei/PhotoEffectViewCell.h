//
//  PhotoEffectViewCell.h
//  youpinwei
//
//  Created by tmy on 14-9-16.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoEffectViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)setSelectStatus:(BOOL)isSelect;

@end
