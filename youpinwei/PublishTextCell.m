//
//  PublishTextCell.m
//  youpinwei
//
//  Created by tmy on 15/1/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "PublishTextCell.h"
#import "JTInputView.h"

@implementation PublishTextCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:255/255.0];
    
    self.inputView.placeholderText = @"添加宝贝描述";
    self.inputView.placeholderColor = [UIColor colorWithRed:199/255.0 green:199/255.0 blue:199/255.0 alpha:255/255.0];
    self.inputView.textColor = [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:255/255.0];
    self.inputView.tintColor = NavigationBarTintColor;
    self.inputView.layer.borderColor = PUBLISH_BORDER_COLOR.CGColor;
    self.inputView.layer.borderWidth = 0.5f;
    self.inputView.backgroundColor = [UIColor whiteColor];
}

- (void)prepareForMove {
    [super prepareForMove];
    self.inputView.hidden = YES;
    self.closeButton.hidden = YES;
}

- (void)prepareForMoveSnapshot {
    [self prepareForShow];
}

- (void)prepareForShow {
    self.inputView.hidden = NO;
    self.closeButton.hidden = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
