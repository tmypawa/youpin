//
//  YouPin.h
//  youpinwei
//
//  Created by tmy on 14-9-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface YouPin : NSObject

@property (nonatomic, strong) NSDictionary *receivedRemoteNotification;

+ (instancetype)sharedInstance;
- (void)doInitialCheck;
- (void)syncAddressBook;
- (void)bindPushInfo;
- (void)loginWithUser:(User *)user;
- (void)logout;

- (void)handleForForeground;
- (void)handleForAppActive;
- (void)handleForReceivedRemoteNotification:(NSDictionary *)userInfo;
- (BOOL)handleForOpenURL:(NSURL*)url sourceApplication:(NSString *)application annotation:(id)annotation;

@end
