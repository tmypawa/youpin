//
//  RegisterViewController.m
//  youpinwei
//
//  Created by tmy on 14-8-8.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "AccountBindingViewController.h"
#import "GoodsChooseController.h"
#import "Goods.h"

#define TAOBAO_ORDER_LIST_URL @"http://h5.m.taobao.com/awp/mtb/mtb.htm?sprefer=p23590#!/awp/mtb/olist.htm?sta=4"
#define TAOBAO_LOGIN_URL @"http://login.m.taobao.com/login.htm?spm=0.0.0.0&tpl_redirect_url=http%3A%2F%2Fm.taobao.com%2F%3Fsprefer%3Dsypc00"
//#define ALIPAY_LOGIN_URL @"https://openauth.alipay.com/oauth2/authorize.htm?client_id=%@&view=wap"

@interface AccountBindingViewController ()

@end

@implementation AccountBindingViewController
{
    __weak IBOutlet UIWebView *_webView;
    __weak IBOutlet UITextView *_textView;
    __weak IBOutlet UIBarButtonItem *_doneItem;
    NSString    *_registerUrl;
    int         _pageIndex;
    int         _totalPageCount;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]){

    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
 


@end
