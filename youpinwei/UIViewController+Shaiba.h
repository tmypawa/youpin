//
//  UIViewController+Shaiba.h
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Shaiba)

- (void)presentLoginController;
- (void)presentPublishViewController;
- (void)showToastWithTitle:(NSString *)title;
- (void)showHUDViewWithTitle:(NSString *)title;
- (void)hideHUDView;
- (void)showGlobalHUDViewWithTitle:(NSString *)title;
- (void)hideGlobalHUDView;
- (void)showSuccessHUDWithTitle:(NSString *)title;
- (void)showFailHUDWithTitle:(NSString *)title;

- (void)showNoItemsViewWithTitle:(NSString *)title;
- (void)dimissNoItemsView;

- (void)showNavigationToastWithTitle:(NSString *)title image:(UIImage *)image actionView:(UIView *)actionView;
- (void)showStatusBarToastWithTitle:(NSString *)title;

- (void)showShareMenuWithTasteItem:(TasteItem *)tasteItem;

@end
