//
//  PublishViewController.h
//  youpinwei
//
//  Created by tmy on 14-8-25.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoSelectionController.h"
#import "TagSelectionViewController.h"
#import "CameraViewController.h"
#import "PostSettingViewController.h"
#import "PublishPhotoBrowerController.h"
#import "JTFixedTextView.h"
#import "PublishTextItem.h"
#import "PublishImageItem.h"
#import "FMMoveTableView.h"

#define TAOBAO_TAG @"淘宝晒单-sys"
#define ZOOM_SIZE 450
#define MAX_PHOTO_NUMBER 8
#define SNAPSHOT_INTERVAL 5

@class Goods;

@interface PublishViewController : ShaibaBaseViewController<FMMoveTableViewDataSource, FMMoveTableViewDelegate, UITextViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, CameraViewControllerDelegate, UIActionSheetDelegate, PostSettingViewControllerDelegate,UIViewControllerTransitioningDelegate, PublishPhotoBrowerControllerDelegate, UIAlertViewDelegate, PhotoSelectionControllerDelegate>
{
    IBOutlet FMMoveTableView *_tableView;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_priceLabel;
    __weak IBOutlet UIButton *_publishBtn;
    __weak IBOutlet UIButton *_tagBtn;
    IBOutlet UITextField *_titleTextField;
    IBOutlet UIButton *_maskBtn;
    IBOutlet UIView *_headerView;
    IBOutlet UIView *_titleBgView;
    IBOutlet UIView *_toolBgView;
    IBOutlet UIView *_footerView;
    IBOutlet UIButton *_addTextBtn;
    IBOutlet UIButton *_addImageBtn;
    IBOutlet UIView *_instructionView;
    IBOutlet UIButton *_instructionBtn;
    NSMutableArray     *_publishContentItems;
    NSMutableArray     *_publishTextItems;
    NSMutableArray     *_publishImageItems;
    NSArray            *_tags;
    BOOL        _isUpdatingForMaxPhotos;
    
    NSString    *_cacheDir;
    UIImageView  *_thumbnailShotView;
    PublishPhotoBrowerController    *_imageBrowerController;
    NSTimer    *_snapshotTimer;
    TasteItem *_snapshotTaste;
    NSInteger       _actionRowIndex;
    BOOL            _isEditingText;
    NSInteger       _editingCellIndex;
    BOOL            _hasShowTextTutorial;
    BOOL            _hasShowImageTutorial;
}

@property (nonatomic) BOOL autoSaveSnapshot;
@property (nonatomic, strong) TasteItem *draftTasteItem;
@property (nonatomic, strong) Goods *goods;

- (void)saveTasteSnapshot;
- (NSString *)localFilePathWithFileName:(NSString *)fileName;
- (NSArray *)filePathesFromFilenames:(NSArray *)filenames;
- (NSArray *)filenamesFromFilePathes:(NSArray *)filePathes;
- (NSArray *)photoItemPathesFromCurrentPhotos;
- (UIImage*)generateThumbnailFromPath:(NSString *)imagePath atSize:(CGFloat)maxSize;
- (void)restoreViewFromSnapshot;
- (TasteItem *)generateTasteItem;
- (void)startPublishFlowWithTaste:(TasteItem *)taste;
- (void)prepareUploadFilesForTaste:(TasteItem *)taste;
- (void)doPublishWithTaste:(TasteItem *)taste;
- (void)clearAllPhotos;
- (void)dismiss;

@end
