//
//  PhotoSelectionController.m
//  youpinwei
//
//  Created by tmy on 14-10-13.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "PhotoSelectionController.h"
#import "PhotoEditViewController.h"
#import "PublishViewController.h"
#import "NYXImagesKit.h"
#import "UIImage+Utility.h"

@implementation PhotoSelectionController
{
    UIImageView     *_headerView;
    ALAssetsLibrary *_assetsLibrary;
    NSMutableArray  *_photoAssets;
    NSArray         *_goodsURLs;
    CameraViewController       *_cameraController;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _photoAssets = [[NSMutableArray alloc] init];
        _assetsLibrary = [[ALAssetsLibrary alloc] init];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: GlobalTintColor}];
    self.navigationController.navigationBar.tintColor = GlobalTintColor;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageFromColor:[UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageFromColor:[UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1]]];
    
    [self fetchPhotoAlbum];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(photoEditFinishNotification:) name:YouPinNotificationPhotoDidFinishEdit object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[YouPinAPI sharedAPI] cancelRequestForSender:self];
}

- (void)fetchPhotoAlbum {
    ALAssetsLibraryAccessFailureBlock failureblock = ^(NSError *error){
        if (error.code == ALAssetsLibraryAccessGloballyDeniedError || error.code == ALAssetsLibraryAccessUserDeniedError) {
            Alert(@"无法访问相册.请在'设置->定位服务'设置为打开状态.", @"", @"确定");
        }else{
            Alert(@"相册访问失败", @"", @"确定");
        }
    };
    
    ALAssetsGroupEnumerationResultsBlock groupBlock = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if ([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
            [_photoAssets addObject:result];
        }
    };
    
    ALAssetsLibraryGroupsEnumerationResultsBlock groupsBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [group enumerateAssetsUsingBlock:groupBlock];
        }else {
            NSArray *reverseAssets =  [[_photoAssets reverseObjectEnumerator] allObjects];
            [_photoAssets removeAllObjects];
            [_photoAssets addObjectsFromArray:reverseAssets];
            [self.collectionView reloadData];
        }
    };
    
    [_assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:groupsBlock failureBlock:^(NSError *error) {
        NSLog(@"photo failed");
    }];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)photoEditFinishNotification:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    UIImage *photo = notification.userInfo[YouPinNotificationEditPhotoKey];
    photo = [photo scaleToSize:CGSizeMake(CROP_IMAGE_SIZE, CROP_IMAGE_SIZE) usingMode:NYXResizeModeAspectFit];
    
    if (_cameraController) {
        [_cameraController dismissViewControllerAnimated:YES completion:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                if (self.photoDelegate && [self.photoDelegate respondsToSelector:@selector(photoSelectionController:didSelectAlbumPhoto:)]) {
                    [self.photoDelegate photoSelectionController:self didSelectAlbumPhoto:photo];
                }
            }];
        }];
    }else {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            if (self.photoDelegate && [self.photoDelegate respondsToSelector:@selector(photoSelectionController:didSelectAlbumPhoto:)]) {
                [self.photoDelegate photoSelectionController:self didSelectAlbumPhoto:photo];
            }
        }];
    }
}

- (void)showCamera {
   _cameraController = [[CameraViewController alloc] init];
    _cameraController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _cameraController.cameraDelegate = self;
    [self.navigationController presentViewController:_cameraController animated:YES completion:nil];
}

- (void)imageCropViewController:(RSKImageCropViewController *)controller didCropImage:(UIImage *)croppedImage {
    PhotoEditViewController *editController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PhotoEditViewController"];
    editController.photo = croppedImage;
    [self.navigationController pushViewController:editController animated:YES];
}

- (UIImage *)cropImage:(UIImage *)originImage {
    float fixedSize = CROP_IMAGE_SIZE;
    float zoomSize = originImage.size.height/originImage.size.width * fixedSize;
    UIImage *cropImage = [originImage zoomImageWithSize:CGSizeMake(zoomSize, zoomSize)];
    cropImage = [cropImage clipImageWithRect:CGRectMake(0, 40 * fixedSize / screenWidth, fixedSize, fixedSize)];
    return cropImage;
}

- (void)cameraViewControllerDidCancel:(CameraViewController *)controller {
    _cameraController = nil;
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photoAssets.count + 1;
}


//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
//        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
//        
//        UILabel *titleLabel = (UILabel *)[headerView viewWithTag:100];
//        if (indexPath.section == 0) {
//            titleLabel.text = @"从相册中选择";
//        }
//        
//        return headerView;
//    }else {
//        return nil;
//    }
//}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = nil;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CameraCell" forIndexPath:indexPath];
        }else {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
            UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:100];
            ALAsset *asset = _photoAssets[indexPath.row - 1];
            UIImage *thumbnailsImage = [[UIImage alloc] initWithCGImage:asset.thumbnail];
            imageView.image = thumbnailsImage;
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UIImage *selectPhoto = nil;
    if(indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self showCamera];
        }else {
            ALAsset *asset = _photoAssets[indexPath.row - 1];
            selectPhoto = [[UIImage alloc] initWithCGImage:asset.defaultRepresentation.fullScreenImage];
            
            if (!selectPhoto) {
                return;
            }
            
            selectPhoto = [selectPhoto scaleToSize:CGSizeMake(CROP_IMAGE_SIZE, CROP_IMAGE_SIZE) usingMode:NYXResizeModeAspectFit];
            
            PhotoCropViewController *cropViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoCropViewController"];
            cropViewController.originalImage = selectPhoto;
            cropViewController.delegate = self;
            [self.navigationController pushViewController:cropViewController animated:YES];
        }
    }
}

@end
