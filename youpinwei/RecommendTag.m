//
//  RecommendTag.m
//  youpinwei
//
//  Created by tmy on 15/3/27.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "RecommendTag.h"

@implementation RecommendTag

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.tagName forKey:@"tagName"];
    [encoder encodeDouble:self.updateTime forKey:@"updateTime"];
    [encoder encodeObject:self.productList forKey:@"productList"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.tagName = [decoder decodeObjectForKey:@"tagName"];
        self.updateTime = [decoder decodeDoubleForKey:@"updateTime"];
        self.productList = [decoder decodeObjectForKey:@"productList"];
    }
    return self;
}

@end
