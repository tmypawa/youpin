//
//  InvitationResultController.h
//  youpinwei
//
//  Created by tmy on 15/3/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "ShaibaBaseViewController.h"

@interface InvitationResultController : ShaibaBaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSString *invitationCode;

@end
