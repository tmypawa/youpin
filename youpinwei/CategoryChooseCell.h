//
//  CategoryChooseCell.h
//  youpinwei
//
//  Created by tmy on 15/3/21.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryChooseCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *chooseButton;

@end
