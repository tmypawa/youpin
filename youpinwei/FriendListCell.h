//
//  FriendListCell.h
//  youpinwei
//
//  Created by tmy on 15/1/14.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *avatarBtn;
@property (strong, nonatomic) IBOutlet UILabel *nicknameLabel;

@end
