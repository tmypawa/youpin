//
//  JTTextView.m
//  youpinwei
//
//  Created by tmy on 15/1/5.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "JTTextView.h"

@implementation JTTextView
{
    UIMenuController    *_menuController;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self internalInit];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self internalInit];
    }
    
    return self;
}

- (void)internalInit {
    self.textContainerInset = UIEdgeInsetsMake(0, -5, 0, -5);
}


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    _menuController = sender;
    NSArray *validMenuActions = @[@"selectAll:", @"copy:"];
    if ([validMenuActions containsObject:NSStringFromSelector(action)]) {
        return YES;
    }else {
        return NO;
    }
}

- (void)selectAll:(id)sender {
    self.selectedRange = NSMakeRange(0, self.text.length);
    [_menuController setTargetRect:self.bounds inView:self];
    [_menuController setMenuVisible:YES animated:YES];
}

- (void)copy:(id)sender {
    [UIPasteboard generalPasteboard].string = [self.text substringWithRange:self.selectedRange];
    [_menuController setMenuVisible:NO animated:YES];
    self.selectedRange = NSMakeRange(0, 0);
    [self resignFirstResponder];
}

@end
