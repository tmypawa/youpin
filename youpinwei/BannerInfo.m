//
//  BannerInfo.m
//  youpinwei
//
//  Created by tmy on 14/12/12.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "BannerInfo.h"

@implementation BannerInfo

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.picUrl forKey:@"picUrl"];
    [encoder encodeObject:self.gotoUrl forKey:@"gotoUrl"];
    [encoder encodeObject:self.tag forKey:@"tag"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self) {
        self.picUrl = [decoder decodeObjectForKey:@"picUrl"];
        self.gotoUrl = [decoder decodeObjectForKey:@"gotoUrl"];
        self.tag = [decoder decodeObjectForKey:@"tag"];
    }
    return self;
}

@end
