//
//  PublishImageCell.h
//  youpinwei
//
//  Created by tmy on 15/1/30.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMMoveTableViewCell.h"

@interface PublishImageCell : FMMoveTableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (nonatomic, readonly) UIView *bgView;

@end
