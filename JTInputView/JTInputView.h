//
//  JTInputView.h
//  JapanDrama
//
//  Created by tmy on 14-8-9.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTFixedTextView.h"

@protocol JTInputViewDelegate;

@interface JTInputView : UIView

- (id)initWithScrollView:(UIScrollView *)scrollView;

@property (nonatomic, weak) id<JTInputViewDelegate> delegate;
@property (nonatomic, readonly) NSString *inputText;
@property (nonatomic, readonly) JTFixedTextView *textView;
@property (nonatomic, readonly) UIButton    *sendButton;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIScrollView *attachedScrollView;
@property (nonatomic) CGFloat horizontalTextViewMargin;
@property (nonatomic) CGFloat verticalTextViewMargin;

@property (nonatomic) BOOL showSendButton;
@property (nonatomic) NSUInteger maxLineNumber;
@property (nonatomic) BOOL autoFollowKeyboard;

- (void)clearInput;

@end

@protocol JTInputViewDelegate <NSObject>
@optional
- (void)JTInputViewDidChangeHeight:(JTInputView *)inputView;
- (void)JTInputViewDidPressSendButton:(JTInputView *)inputView;
- (void)JTInputViewDidEndEdit:(JTInputView *)inputView;

@end