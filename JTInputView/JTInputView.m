//
//  JTInputView.m
//  JapanDrama
//
//  Created by tmy on 14-8-9.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "JTInputView.h"

#define MAX_LINE_NUMBER 5

@implementation JTInputView
{
    float                   _singleLineInputHeight;
    float                   _singleLineHeight;
    float                   _singleSendButtonOriginY;
    float                   _originalHeight;
    float                   _totalInputViewHeight;
    float                   _totalTranslationY;
    float                   _currentKeyboardHeight;
    int                     _lineNumber;
    UIScrollView            *_attachedScrollView;
}
@synthesize attachedScrollView = _attachedScrollView;

- (void)setShowSendButton:(BOOL)showSendButton {
    _showSendButton = showSendButton;
    
    if (showSendButton) {
        if (!_sendButton) {
            [self addSendButton];
        }
    }else {
        if (_sendButton) {
            [_sendButton removeFromSuperview];
            _sendButton = nil;
        }
    }
    
    [self updateInputLayout];
}

- (void)setHorizontalTextViewMargin:(CGFloat)horizontalTextViewMargin {
    _horizontalTextViewMargin = horizontalTextViewMargin;
    [self updateInputLayout];
}

- (void)setVerticalTextViewMargin:(CGFloat)verticalTextViewMargin {
    _verticalTextViewMargin = verticalTextViewMargin;
    [self updateInputLayout];
}

- (void)updateInputLayout {
    CGFloat textViewWidth = self.frame.size.width - self.horizontalTextViewMargin * 2;
    if (self.showSendButton) {
        textViewWidth -= (_sendButton.frame.size.width + 10);
    }
    
    CGRect textViewFrame = _textView.frame;
    textViewFrame.size.width = textViewWidth;
    _textView.frame = textViewFrame;
}

- (void)setBackgroundView:(UIView *)backgroundView {
    if (backgroundView != _backgroundView) {
        [_backgroundView removeFromSuperview];
        _backgroundView = backgroundView;
        [self insertSubview:_backgroundView atIndex:0];
    }
}

- (NSString *)inputText {
    return _textView.text;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self internalInit];
    }
    
    return self;
}

- (void)awakeFromNib {
    [self initViews];
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame: frame]) {
        [self internalInit];
        [self initViews];
    }
    
    return self;
}

- (void)internalInit {
    _showSendButton = true;
    _horizontalTextViewMargin = 10;
    _verticalTextViewMargin = 7;
    self.maxLineNumber = MAX_LINE_NUMBER;
    self.autoFollowKeyboard = YES;
}

- (void)initViews {
    _totalInputViewHeight = self.frame.size.height;
    _lineNumber = 0;
    _originalHeight = 0;
    
    CGFloat textViewWidth = self.frame.size.width - self.horizontalTextViewMargin * 2;
    
    if (self.showSendButton) {
        [self addSendButton];
        textViewWidth -= (_sendButton.frame.size.width + 10);
    }
    
    _textView = [[JTFixedTextView alloc] initWithFrame:CGRectMake(self.horizontalTextViewMargin, self.verticalTextViewMargin,  textViewWidth, self.frame.size.height - self.verticalTextViewMargin * 2)];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.font = [UIFont systemFontOfSize:15];
    _textView.showsHorizontalScrollIndicator = NO;
    _textView.showsVerticalScrollIndicator = NO;
    _textView.scrollsToTop = NO;
    _textView.contentInset = UIEdgeInsetsZero;
    _textView.layer.cornerRadius = 2;
    //        _textView.textContainerInset = UIEdgeInsetsMake(0, 0, -2, 0);
    
    [self addSubview:_textView];
    
    
    
    self.autoresizingMask = UIViewAutoresizingNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidChangeText:) name:UITextViewTextDidChangeNotification object:_textView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidEndEditing:) name:UITextViewTextDidEndEditingNotification object:_textView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDisappear:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)addSendButton {
    _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _sendButton.titleLabel.font = [UIFont systemFontOfSize:15];
    CGFloat defaultButtonWidth = 52;
    CGFloat defaultButtonHeight = 30;
    _sendButton.frame = CGRectMake(self.frame.size.width - self.horizontalTextViewMargin - defaultButtonWidth, self.frame.size.height - self.verticalTextViewMargin - defaultButtonHeight, defaultButtonWidth, defaultButtonHeight);
    _sendButton.enabled = NO;
    [_sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_sendButton addTarget:self action:@selector(sendButtonDidPress:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_sendButton];
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    if (self.superview) {
        _singleLineHeight = _textView.frame.size.height;
        _singleLineInputHeight = self.frame.size.height;
        _singleSendButtonOriginY = self.sendButton.frame.origin.y;
        [_textView setContentOffset:CGPointMake(0, 2) animated:NO];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)sendButtonDidPress:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(JTInputViewDidPressSendButton:)]) {
        [self.delegate JTInputViewDidPressSendButton:self];
    }
}

- (void)clearInput {
    _textView.text = @"";
    
    _originalHeight = 0;
    _lineNumber = 0;
    CGRect textViewFrame = _textView.frame;
    textViewFrame.size.height = _singleLineHeight;
    _textView.frame = textViewFrame;
    CGRect frame = self.frame;
    frame.size.height = _singleLineInputHeight;
    self.frame = frame;
    
    if (self.showSendButton) {
        _sendButton.enabled = NO;
        CGRect sendBtnFrame = _sendButton.frame;
        sendBtnFrame.origin.y = _singleSendButtonOriginY;
        _sendButton.frame = sendBtnFrame;
    }
    
    _totalInputViewHeight = _singleLineInputHeight;
    _totalTranslationY = 0;
    
    if (_backgroundView) {
        CGRect backgroundFrame = _backgroundView.frame;
        backgroundFrame.size.height = _singleLineInputHeight;
        _backgroundView.frame = backgroundFrame;
    }
    
    [_textView resignFirstResponder];
}

- (void)setScrollViewBottomInset:(float)bottomInset {
    UIEdgeInsets contentInsets = _attachedScrollView.contentInset;
    contentInsets.bottom = bottomInset;
    _attachedScrollView.contentInset = contentInsets;
    
    UIEdgeInsets indicatorInsets = _attachedScrollView.scrollIndicatorInsets;
    indicatorInsets.bottom = bottomInset;
    _attachedScrollView.scrollIndicatorInsets = indicatorInsets;
}
 

- (void)keyboardWillAppear:(NSNotification *)notification {
    if (!self.autoFollowKeyboard) return;
        
    CGRect frame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _currentKeyboardHeight = frame.size.height;
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    if (self.superview) {
        CGRect parentFrame = self.superview.frame;
        CGRect inputFrame = self.frame;
        inputFrame.origin.y = parentFrame.size.height - frame.size.height - self.frame.size.height;
        self.frame = inputFrame;
    }
    [UIView commitAnimations];
    
    if (_attachedScrollView) {
        [self setScrollViewBottomInset:frame.size.height];
    }
}

- (void)keyboardWillDisappear:(NSNotification *)notification {
    if (!self.autoFollowKeyboard) return;
    
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    if (self.superview) {
        CGRect parentFrame = self.superview.frame;
        CGRect inputFrame = self.frame;
        inputFrame.origin.y = parentFrame.size.height - self.frame.size.height;
        self.frame = inputFrame;
    }
    [UIView commitAnimations];
    
    if (_attachedScrollView) {
        [self setScrollViewBottomInset:0];
    }
}

- (void)keyboardDidDisappear:(NSNotification *)notification {
    if (!self.autoFollowKeyboard) return;
}


- (void)textViewDidEndEditing:(NSNotification *)notification {
    if (self.delegate && [self.delegate respondsToSelector:@selector(JTInputViewDidEndEdit:)]) {
        [self.delegate JTInputViewDidEndEdit:self];
    }
}

- (void)textViewDidChangeText:(NSNotification *)notification {
    if (self.showSendButton) {
        if([_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]].length == 0) {
            _sendButton.enabled = NO;
        }else {
            _sendButton.enabled =  YES;
        }
    }
    
    CGRect frame = [_textView.text boundingRectWithSize:CGSizeMake(_textView.frame.size.width - 10, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: _textView.font} context:nil];
    
    float height = frame.size.height;
    
    if(_originalHeight == 0) {
        _originalHeight = height;
        _lineNumber= 1;
        [_textView setContentOffset:CGPointMake(0, _textView.textContainerInset.top/2) animated:NO];
    }else if(height != _originalHeight) {
        if(height > _originalHeight) {
            ++_lineNumber;
        }else {
            --_lineNumber;
        }
        
        _originalHeight = height;
        
        if (_lineNumber > self.maxLineNumber) {
            return;
        }
        
        float oldHeight = _textView.frame.size.height;
        float diff = (height - oldHeight) + 8;
        CGRect textFrame = _textView.frame;
        textFrame.size.height += diff;
  
        CGRect sendBtnFrame;
        if (self.showSendButton) {
            sendBtnFrame = _sendButton.frame;
            sendBtnFrame.origin.y += diff;
        }
        
        
        CGRect frame = self.frame;
        frame.size.height += diff;
        frame.origin.y -= diff;
        _totalTranslationY -= diff;
        
        if (_lineNumber == 1) {
            textFrame.size.height = _singleLineHeight;
            frame.size.height = _singleLineInputHeight;
            frame.origin.y = self.superview.frame.size.height - _currentKeyboardHeight - frame.size.height;
            sendBtnFrame.origin.y = _singleSendButtonOriginY;
            _totalTranslationY = 0;
        }
        
        if (self.showSendButton) {
            _sendButton.frame = sendBtnFrame;
        }
        
        _textView.frame = textFrame;
        self.frame = frame;
        _totalInputViewHeight = frame.size.height;
        
        
        if(self.backgroundView) {
            CGRect backgroundFrame = self.backgroundView.frame;
            backgroundFrame.size.height = frame.size.height;
            self.backgroundView.frame = backgroundFrame;
        }
        
        float maxOffsetY = _textView.contentSize.height - _textView.frame.size.height;

        if(_lineNumber <= self.maxLineNumber) {
            [_textView setContentOffset:CGPointMake(0, maxOffsetY/2) animated:NO];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(JTInputViewDidChangeHeight:)]) {
            [self.delegate JTInputViewDidChangeHeight:self];
        }
    } 
    
    if(_textView.text.length == 0) {
        [_textView setContentOffset:CGPointMake(0, 2) animated:NO];
    }
}

@end
