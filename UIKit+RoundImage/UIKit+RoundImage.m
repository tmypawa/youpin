//
//  UIImageView+Round.m
//  SpringFlowDemo
//
//  Created by tmy on 13-12-28.
//  Copyright (c) 2013年 nobuta. All rights reserved.
//

#import "UIKit+RoundImage.h"
#import <objc/runtime.h>

 NSString * const BorderLayerKey = @"BorderLayerKey";

CAShapeLayer * GetCircleShape(UIColor *borderColor, CGSize size)
{
    CAShapeLayer *circleShape = [CAShapeLayer layer];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(size.width/2, size.height/2) radius:size.width/2 startAngle:0 endAngle:2*M_PI clockwise:YES];
    circleShape.path = circlePath.CGPath;
    return circleShape;
}

CAShapeLayer * GetBorderShape(UIColor *borderColor, CGSize size, CGFloat borderWidth)
{
    CAShapeLayer *borderShape = [CAShapeLayer layer];
    UIBezierPath *borderPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(size.width/2, size.height/2) radius:size.width/2 startAngle:0 endAngle:2*M_PI clockwise:YES];
    
    borderShape.path = borderPath.CGPath;
    borderShape.lineWidth = borderWidth;
    borderShape.lineCap = kCALineCapRound;
    borderShape.lineJoin = kCALineJoinRound;
    borderShape.fillColor = [UIColor clearColor].CGColor;
    borderShape.strokeColor = borderColor.CGColor;
    borderShape.opaque = NO;
    return borderShape;
}


@implementation UIView (RoundImage)

- (void)setBorderLayer:(CAShapeLayer *)layer {
    objc_setAssociatedObject(self, (__bridge const void *)(BorderLayerKey), layer, OBJC_ASSOCIATION_ASSIGN);
}

- (CAShapeLayer *)borderLayer {
   return objc_getAssociatedObject(self, (__bridge const void *)(BorderLayerKey));
}

- (void)makeRound
{
    [self makeRoundWithBorderColor:nil];
}

- (void)makeRoundWithBorderColor:(UIColor *)borderColor
{
    if(borderColor)
    {
        CAShapeLayer *preBorderLayer = [self borderLayer];
        if (preBorderLayer) {
            [preBorderLayer removeFromSuperlayer];
        }
        
        CAShapeLayer *borderLayer = GetBorderShape(borderColor, self.frame.size, 1);
        [self setBorderLayer:borderLayer];
        [self.layer addSublayer:borderLayer];
    }
    
    CAShapeLayer *maskLayer = GetCircleShape(borderColor, self.frame.size);
    self.layer.mask = maskLayer;
}

- (void)makeRoundWithBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth {
    if(borderColor)
    {
        CAShapeLayer *preBorderLayer = [self borderLayer];
        if (preBorderLayer) {
            [preBorderLayer removeFromSuperlayer];
        }
        
        CAShapeLayer *borderLayer = GetBorderShape(borderColor, self.frame.size, borderWidth >= 0? borderWidth : 0);
        [self setBorderLayer:borderLayer];
        [self.layer addSublayer:borderLayer];
    }
    
    CAShapeLayer *maskLayer = GetCircleShape(borderColor, self.frame.size);
    self.layer.mask = maskLayer;
}

@end
