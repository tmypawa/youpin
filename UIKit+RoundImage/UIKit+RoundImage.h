//
//  UIImageView+Round.h
//  SpringFlowDemo
//
//  Created by tmy on 13-12-28.
//  Copyright (c) 2013年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RoundImage)
- (void)makeRound;
- (void)makeRoundWithBorderColor:(UIColor *)borderColor;
- (void)makeRoundWithBorderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth;
@end
