//
//  NSString+NSString_MD5.h
//  XYSDKDemo
//
//  Created by tmy on 13-6-26.
//  Copyright (c) 2013年 kingnet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *) md5Value;

@end
