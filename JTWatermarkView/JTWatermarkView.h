//
//  JTWatermarkView.h
//  youpinwei
//
//  Created by tmy on 14-9-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneFingerRotationGestureRecognizer.h"

@interface JTWatermarkView : UIView<OneFingerRotationGestureRecognizerDelegate,UIGestureRecognizerDelegate>

@property (nonatomic) UIImage *backgroundImage;
@property (nonatomic, readonly) UIButton   *closeButton;
@property (nonatomic, readonly) UIImageView   *zoomButton;
@property (nonatomic, readonly) CGPoint transformedLocation;

- (id)initWithFrame:(CGRect)frame watermarkImage:(UIImage *)watermarkImage containerView:(UIView *)containerView;

- (UIImage *)exportTransformImage;

@end
