//
//  JTWatermarkView.m
//  youpinwei
//
//  Created by tmy on 14-9-18.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "JTWatermarkView.h"
#import "OneFingerRotationGestureRecognizer.h"
#import "NYXImagesKit.h"

#define MIN_ZOOM_OUT 0.4f
#define MAX_ZOOM_IN 2

@implementation JTWatermarkView
{
    UIView      *_containerView;
    UIImageView *_backgroundView;
    UIImageView *_watermarkView;
    UIImage     *_originalWatermarkImage;
    UIButton    *_closeButton;
    UIImageView    *_zoomButton;
    UIPanGestureRecognizer  *_watermarkGestureRecognizer;
    UIPanGestureRecognizer  *_zoomGestureRecognizer;
    OneFingerRotationGestureRecognizer      *_rotationGestureRecognizer;
    CGPoint     _lastTranslation;
    CGPoint     _totalTranslation;
    float       _totalRotationAngle;
    CGPoint     _totalZoomTranslation;
    CGPoint     _lastZoomTranslation;
    float       _totalZoomLevel;
    CGPoint     _originalLocation;
}

- (CGPoint)transformedLocation {
    return CGPointMake(_originalLocation.x + _totalTranslation.x, _originalLocation.y + _totalTranslation.y);
}

- (void)dealloc {
    int a = 1;
}

- (id)initWithFrame:(CGRect)frame watermarkImage:(UIImage *)watermarkImage containerView:(UIView *)containerView {
    CGRect finalFrame = CGRectMake(frame.origin.x - 20, frame.origin.y - 20, frame.size.width + 40,  frame.size.height + 40);
    _originalLocation = frame.origin;
    if (self = [super initWithFrame:finalFrame]) {
        self.userInteractionEnabled = YES;
        
        _totalZoomLevel = 1;
        _containerView = containerView;
        _originalWatermarkImage = watermarkImage;
        _backgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, frame.size.width, frame.size.height)];
        _backgroundView.backgroundColor = [UIColor clearColor];
        _backgroundView.userInteractionEnabled = NO;
        [self addSubview:_backgroundView];
        
        _watermarkView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, frame.size.width, frame.size.height)];
        _watermarkView.backgroundColor = [UIColor clearColor];
        _watermarkView.image = _originalWatermarkImage;
        _watermarkView.userInteractionEnabled = NO;
        [self addSubview:_watermarkView];
        
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeButton.frame = CGRectMake(0, 0, 40, 40);
        [_closeButton setImage:[UIImage imageNamed:@"close_watermark_normal"] forState:UIControlStateNormal];
        [_closeButton setImage:[UIImage imageNamed:@"close_watermark_press"] forState:UIControlStateHighlighted];
        [_closeButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_closeButton];
        
        _zoomButton = [[UIImageView alloc] initWithFrame:CGRectMake(finalFrame.size.width - 40, finalFrame.size.height - 40, 40, 40)];
        _zoomButton.userInteractionEnabled = YES;
        _zoomButton.image = [UIImage imageNamed:@"zoom_watermark_normal"];
        _zoomButton.autoresizingMask = UIViewAutoresizingNone;
        [self addSubview:_zoomButton];
    }
    
    return self;
}


- (void)didMoveToSuperview {
    if (self.superview) {
        float radius = sqrtf(powf(self.frame.size.width/2, 2) + powf(self.frame.size.height/2, 2));
        CGPoint midPoint = CGPointMake(self.center.x + _totalTranslation.x, self.center.y + _totalTranslation.y);
        _rotationGestureRecognizer = [[OneFingerRotationGestureRecognizer alloc] initWithMidPoint: midPoint innerRadius:radius -30  outerRadius:radius + 30 target:self];
        _rotationGestureRecognizer.delegate = self;
        [_containerView addGestureRecognizer:_rotationGestureRecognizer];
        
        _zoomGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(zoomGestureRecognizerDidPan:)];
        _zoomGestureRecognizer.maximumNumberOfTouches = 1;
        _zoomGestureRecognizer.delegate = self;
        [_containerView addGestureRecognizer:_zoomGestureRecognizer];
    
        
        _watermarkGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(watermarkGestureRecognizerDidPan:)];
        _watermarkGestureRecognizer.maximumNumberOfTouches = 1;
        _watermarkGestureRecognizer.delegate = self;
        [_containerView addGestureRecognizer:_watermarkGestureRecognizer];
    }else {
        [_containerView removeGestureRecognizer:_watermarkGestureRecognizer];
        _watermarkGestureRecognizer = nil;
        
        [_containerView removeGestureRecognizer:_rotationGestureRecognizer];
        _rotationGestureRecognizer = nil;
        
        [_containerView removeGestureRecognizer:_zoomGestureRecognizer];
        _zoomGestureRecognizer = nil;
    }
}

- (void)close:(UIButton *)sender {
    [self removeFromSuperview];
}



- (CGAffineTransform)transformFromCurrentState {
    CGAffineTransform translationTransform = CGAffineTransformMakeTranslation(_totalTranslation.x, _totalTranslation.y);
    CGAffineTransform translationRotationTransform = CGAffineTransformRotate(translationTransform, _totalRotationAngle * M_PI / 180);
 
    return translationRotationTransform;
}

- (void)updateRotationGestureState {
    float radius = sqrtf(powf(self.bounds.size.width/2, 2) + powf(self.bounds.size.height/2, 2));
    float innerRadius = radius - 30;
    float outerRadius = radius + 30;
    
    if (_totalZoomLevel < 1.0f) {
        innerRadius *= _totalZoomLevel;
    }

    if (_totalZoomLevel > 1.0f) {
        outerRadius *= _totalZoomLevel;
    }
    
    CGPoint midPoint = CGPointMake(self.center.x + _totalTranslation.x, self.center.y + _totalTranslation.y);
    [_rotationGestureRecognizer updateMidPoint:midPoint innerRadius:innerRadius outerRadius:outerRadius];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer == _watermarkGestureRecognizer || otherGestureRecognizer == _watermarkGestureRecognizer) {
        return NO;
    }
    
    if ((gestureRecognizer == _zoomGestureRecognizer || gestureRecognizer == _rotationGestureRecognizer) && (otherGestureRecognizer == _zoomGestureRecognizer || otherGestureRecognizer == _rotationGestureRecognizer)) {
        return YES;
    }else {
        return NO;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer == _watermarkGestureRecognizer) {
        if (CGRectContainsPoint(_zoomButton.frame, [gestureRecognizer locationInView:self])) {
            return NO;
        }else {
            _lastTranslation = CGPointZero;
        }
    }else if (gestureRecognizer == _zoomGestureRecognizer) {
        if (!CGRectContainsPoint(_zoomButton.frame, [gestureRecognizer locationInView:self])) {
            return NO;
        }else {
            _lastZoomTranslation = CGPointZero;
        }
    }else if (gestureRecognizer == _rotationGestureRecognizer) {
        if (!CGRectContainsPoint(_zoomButton.frame, [gestureRecognizer locationInView:self])) {
            return NO;
        }
    }

    return YES;
}


- (void)watermarkGestureRecognizerDidPan:(UIPanGestureRecognizer *)gestureRecognizer {
    CGPoint translation = [gestureRecognizer translationInView:gestureRecognizer.view];
    if (CGPointEqualToPoint(_lastTranslation, CGPointZero)) {
        _lastTranslation = translation;
        _totalTranslation.x += _lastTranslation.x;
        _totalTranslation.y += _lastTranslation.y;
    }else {
        _totalTranslation.x += (translation.x - _lastTranslation.x);
        _totalTranslation.y += (translation.y - _lastTranslation.y);
        _lastTranslation = translation;
    }
    
    [self updateRotationGestureState];
    
    self.transform = [self transformFromCurrentState];
}

- (void)zoomGestureRecognizerDidPan:(UIPanGestureRecognizer *)gestureRecognizer {
    CGPoint translationPoint = [gestureRecognizer translationInView:gestureRecognizer.view];
    if (CGPointEqualToPoint(translationPoint, CGPointZero)) {
        return;
    }
    
    if((translationPoint.x >= 0 && translationPoint.y <= 0) || (translationPoint.x <= 0 && translationPoint.y >= 0)){
        return;
    }
    
    
    if (CGPointEqualToPoint(_lastZoomTranslation, CGPointZero)) {
        _lastZoomTranslation = translationPoint;
 
    }else {
        float xOffset = (translationPoint.x - _lastZoomTranslation.x);
        float yOffset = (translationPoint.y - _lastZoomTranslation.y);
//        if ((_totalZoomTranslation.x + xOffset >=0 && _totalZoomTranslation.y + yOffset <= 0 ) || (_totalZoomTranslation.x + xOffset <= 0 && _totalZoomTranslation.y + yOffset >= 0 )) {
//            return;
//        }else {
//
//        }
        _totalZoomTranslation.x += xOffset;
        _totalZoomTranslation.y += yOffset;
        _lastZoomTranslation = translationPoint;
        
        NSLog(@"222222 %@", NSStringFromCGPoint(_totalZoomTranslation));
        float zoomDistance = sqrtf(powf(_totalZoomTranslation.x, 2) + powf(_totalZoomTranslation.y, 2));
        float zoomLevel = zoomDistance / sqrtf(powf(self.bounds.size.width, 2) + powf(self.bounds.size.height, 2));
        if(zoomLevel == 0 || _totalZoomTranslation.x == 0 || _totalZoomTranslation.y == 0) return;
        
        if (translationPoint.x < 0  ) {
            //zoom out
            _totalZoomLevel = 1 - zoomLevel;
            if (_totalZoomLevel < MIN_ZOOM_OUT) {
                _totalZoomLevel = MIN_ZOOM_OUT;
            }
            
            
        }else if(translationPoint.x > 0 ){
            //zoom in
            _totalZoomLevel = 1 + zoomLevel;
            if (_totalZoomLevel > MAX_ZOOM_IN) {
                _totalZoomLevel = MAX_ZOOM_IN;
            }
            
        }
        
        _zoomButton.frame = CGRectMake((self.bounds.size.width - 40) * (1 + (_totalZoomLevel - 1)/2), (self.bounds.size.height - 40) * (1 + (_totalZoomLevel - 1)/2), 40, 40);
        _closeButton.frame = CGRectMake(-_zoomButton.frame.origin.x + (self.bounds.size.width - 40), -_zoomButton.frame.origin.y + (self.bounds.size.height - 40), 40, 40);
        
        NSLog(@"zoom %f, distance %f, btn frame %@", _totalZoomLevel, zoomDistance, NSStringFromCGRect(self.bounds));
        _watermarkView.transform = CGAffineTransformMakeScale(_totalZoomLevel, _totalZoomLevel);
        [self updateRotationGestureState];

    }
}

- (void) rotation: (CGFloat) angle {
    _totalRotationAngle += angle;
    self.transform = [self transformFromCurrentState];
}


- (UIImage *)exportTransformImage {
    UIImage *zoomImage = [_originalWatermarkImage scaleByFactor:_totalZoomLevel];
    UIImage *finalImage = [zoomImage rotateInDegrees:-_totalRotationAngle];
    return finalImage;
}

@end
