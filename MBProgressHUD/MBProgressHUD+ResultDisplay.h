//
//  MBProgressHUD+ResultDisplay.h
//  youpinwei
//
//  Created by tmy on 15/1/13.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

@class MBProgressHUD;

@interface MBProgressHUD (ResultDisplay)

- (void)showResultWithText:(NSString *)text imageName:(NSString *)imageName;

@end
