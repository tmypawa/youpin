//
//  MBProgressHUD+ResultDisplay.m
//  youpinwei
//
//  Created by tmy on 15/1/13.
//  Copyright (c) 2015年 nobuta. All rights reserved.
//

#import "MBProgressHUD+ResultDisplay.h"
#import "MBProgressHUD.h"

@implementation MBProgressHUD (ResultDisplay)

- (void)showResultWithText:(NSString *)text imageName:(NSString *)imageName {
    self.mode = MBProgressHUDModeCustomView;
    self.labelText = text;
    UIImageView *hudImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];
    hudImageView.contentMode = UIViewContentModeScaleAspectFit;
    hudImageView.image = [UIImage imageNamed:imageName];
    self.customView = hudImageView;
    [self hide:YES afterDelay:1];
}

@end
