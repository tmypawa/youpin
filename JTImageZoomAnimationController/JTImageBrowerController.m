//
//  JTImageBrowerController.m
//  youpinwei
//
//  Created by tmy on 14-10-10.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import "JTImageBrowerController.h"
#import "UIImageView+SDWebImage_Effects.h"

@implementation JTImageBrowerController


- (UIImageView *)currentImageView {
    UICollectionViewCell *cell = [_imageCollectionView visibleCells][0];
    return (UIImageView *)[cell.contentView viewWithTag:1000];
}

- (NSInteger)currentImageIndex {
    if ([self numberOfImages] > 0) {
        return _imageCollectionView.contentOffset.x/_imageCollectionView.frame.size.width;
    }else {
        return NSNotFound;
    }
}

- (NSInteger)numberOfImages {
    return [_imageURLs count];
}

- (id)initWithImageURLs:(NSArray *)imageURLs {
    if (self = [super init]) {
        _imageURLs = [NSMutableArray arrayWithArray:imageURLs];
        self.imageMargin = 10;
    }
    
    return self;
}

- (void)loadView {
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view.backgroundColor = [UIColor blackColor];
    
    UICollectionViewFlowLayout *collectionLayout = [[UICollectionViewFlowLayout alloc] init];
    collectionLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    collectionLayout.itemSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    collectionLayout.minimumInteritemSpacing = 0;
    collectionLayout.minimumLineSpacing = self.imageMargin;
    collectionLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, self.imageMargin);
    
    _imageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width + self.imageMargin, [UIScreen mainScreen].bounds.size.height) collectionViewLayout:collectionLayout];
    _imageCollectionView.backgroundColor = [UIColor clearColor];
    _imageCollectionView.pagingEnabled = YES;
    _imageCollectionView.showsHorizontalScrollIndicator = NO;
    _imageCollectionView.showsVerticalScrollIndicator = NO;
    _imageCollectionView.delegate = self;
    _imageCollectionView.dataSource = self;
    
    [_imageCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"ImageBrowerCell"];
    [self.view addSubview:_imageCollectionView];
}

- (void)viewDidLoad {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDidRecognize:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];

    [_imageCollectionView reloadData];
    [_imageCollectionView setContentOffset:CGPointMake(self.showPageIndex * (self.view.frame.size.width + self.imageMargin), 0) animated:NO];
}

- (void)tapDidRecognize:(UITapGestureRecognizer *)recognizer {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _imageURLs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageBrowerCell" forIndexPath:indexPath];
    UIImageView  *tilingView = nil;
    if (![cell.contentView viewWithTag:1000]) {
        tilingView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        tilingView.contentMode = UIViewContentModeScaleAspectFit;
        tilingView.backgroundColor = [UIColor clearColor];
        tilingView.tag = 1000;
        [cell.contentView addSubview:tilingView];
    }else {
        tilingView = (UIImageView *)[cell.contentView viewWithTag:1000];
    }
    
    [tilingView setImageWithURL:[_imageURLs[indexPath.row] absoluteString] effect:SDWebImageEffectFade];
    
    return cell;
}


@end
