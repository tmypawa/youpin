//
//  JTImageBrowerController.h
//  youpinwei
//
//  Created by tmy on 14-10-10.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JTImageBrowerController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
{
    UICollectionView     *_imageCollectionView;
    NSMutableArray             *_imageURLs;
}

@property (nonatomic) NSInteger numberOfImages;
@property (nonatomic, readonly) NSInteger currentImageIndex;
@property (nonatomic, readonly) UIImageView *currentImageView;
@property (nonatomic) int showPageIndex;
@property (nonatomic) CGFloat  imageMargin;

- (id)initWithImageURLs:(NSArray *)imageURLs;


@end
