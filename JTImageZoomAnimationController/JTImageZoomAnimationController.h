//
//  JTImageZoomAnimationController.h
//  youpinwei
//
//  Created by tmy on 14-10-10.
//  Copyright (c) 2014年 nobuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface JTImageZoomAnimationController : NSObject<UIViewControllerAnimatedTransitioning>

- (id)initWithReferenceView:(UIImageView *)referenceImageView;

@end
