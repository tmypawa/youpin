//
//  StretchableTableHeaderView.m
//  StretchableTableHeaderView
//

#import "JTStretchableTableHeaderView.h"

@interface JTStretchableTableHeaderView()
{
    CGRect initialFrame;
    CGFloat defaultViewHeight;
    CGFloat defaultViewWidth;
}
@end


@implementation JTStretchableTableHeaderView

@synthesize tableView = _tableView;
@synthesize view = _view;

- (void)stretchHeaderForTableView:(UITableView*)tableView withView:(UIView*)view
{
    _tableView = tableView;
    _view = view;
    
    initialFrame       = _view.frame;
    defaultViewHeight  = initialFrame.size.height;
    defaultViewWidth = initialFrame.size.width;
}

- (void)scrollViewDidScroll:(UIScrollView*)scrollView
{
    CGRect f = _view.frame;
    f.size.width = _tableView.frame.size.width;
    _view.frame = f;
    
    if(scrollView.contentOffset.y < 0) {
        CGFloat offsetY = (scrollView.contentOffset.y + scrollView.contentInset.top) * -1;
        initialFrame.origin.y = offsetY * -1;
        initialFrame.size.height = defaultViewHeight + offsetY;
        initialFrame.size.width = defaultViewWidth + offsetY;
        if(initialFrame.size.width < scrollView.frame.size.width) {
            initialFrame.size.width = scrollView.frame.size.width;
        }
        
        initialFrame.origin.x = - (initialFrame.size.width - scrollView.frame.size.width)/2;
        _view.frame = initialFrame;
    }
}

- (void)resizeView
{
    _view.frame = initialFrame;
}


@end
